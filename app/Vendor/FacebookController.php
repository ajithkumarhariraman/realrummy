<?php
session_start();
define('FACEBOOK_SDK_V4_SRC_DIR', '../Vendor/Facebook/');
require_once("../Vendor/Facebook/autoload.php");
require_once("../Vendor/Facebook/FacebookSession.php");
require_once("../Vendor/Facebook/FacebookRequest.php");
require_once("../Vendor/Facebook/Helpers/FacebookRedirectLoginHelper.php");
require_once("../Vendor/Facebook/FacebookResponse.php");
require_once("../Vendor/Facebook/Exceptions/FacebookSDKException.php");
require_once("../Vendor/Facebook/Exceptions/FacebookAuthorizationException.php");
require_once("../Vendor/Facebook/GraphNodes/GraphObject.php");
require_once("../Vendor/Facebook/GraphNodes/GraphUser.php");
require_once("../Vendor/Facebook/GraphNodes/GraphSessionInfo.php"); 
use Facebook\FacebookSession;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookRequestException;
use Facebook\Exceptions\FacebookAuthorizationException;
use Facebook\GraphNodes\GraphObject;
use Facebook\GraphNodes\GraphUser;
use Facebook\GraphNodes\GraphSessionInfo;
//
//
//
//define('FACEBOOK_SDK_V4_SRC_DIR', '../Vendor/Facebook');
//require_once("../Vendor/Facebook/autoload.php");
//require_once("../Vendor/Facebook/FacebookSession.php");
//require_once("../Vendor/Facebook/FacebookApp.php");
//require_once("../Vendor/Facebook/FacebookRequest.php");
//require_once("../Vendor/Facebook/Helpers/FacebookRedirectLoginHelper.php");
//require_once("../Vendor/Facebook/FacebookResponse.php");
//require_once("../Vendor/Facebook/Exceptions/FacebookSDKException.php");
//require_once("../Vendor/Facebook/Exceptions/FacebookAuthorizationException.php");
//require_once("../Vendor/Facebook/GraphNodes/GraphObject.php");
//require_once("../Vendor/Facebook/GraphNodes/GraphUser.php");
//require_once("../Vendor/Facebook/GraphNodes/GraphSessionInfo.php");
//
//use Facebook\FacebookSession;
//use Facebook\FacebookRequest;
//use Facebook\FacebookResponse;
//use Facebook\Exceptions\FacebookSDKException;
//use Facebook\ExceptionsFacebookRequestException;
//use Facebook\Exceptions\FacebookAuthorizationException;
//use Facebook\Helpers\FacebookCanvasHelper;
//use Facebook\Helpers\FacebookJavaScriptHelper;
//use Facebook\Helpers\FacebookPageTabHelper;
//use Facebook\Helpers\FacebookRedirectLoginHelper;
//use Facebook\GraphNodes\GraphObject;
//use Facebook\GraphNodes\GraphUser;
//use Facebook\GraphNodes\GraphSessionInfo;

App::uses('AppController', 'Controller');
App::import('Vendor', 'Facebook', array('file' => 'Facebook/Facebook.php'));
App::import('Vendor', 'google', array('file' => 'google/config.php'));
App::import('Vendor', 'google', array('file' => 'google/Google_Client.php'));
App::import('Vendor', 'google', array('file' => 'google/contrib/Google_PlusService.php'));
App::import('Vendor', 'google', array('file' => 'google/contrib/Google_Oauth2Service.php'));

/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class FacebookController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('User');
    public $layout = 'front';

    public function googlelogin() {
        $this->autoRender = false;
        require_once '../Config/google_login.php';
        $client = new Google_Client();
        //print_r($client);exit;
        $client->setScopes(array('https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me'));
        $client->setApprovalPrompt('auto');
        $url = $client->createAuthUrl();
        $this->redirect($url);
    }

    /**
     * This function will handle Oauth Api response
     */
    public function google_login() {
        $this->autoRender = false;
        require_once '../Config/google_login.php';
        $client = new Google_Client();
        $client->setScopes(array('https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me'));
        $client->setApprovalPrompt('auto');

        $plus = new Google_PlusService($client);
        $oauth2 = new Google_Oauth2Service($client);
        if (isset($_GET['code'])) {
            $client->authenticate(); // Authenticate
            $_SESSION['access_token'] = $client->getAccessToken(); // get the access token here
        }

        if (isset($_SESSION['access_token'])) {
            $client->setAccessToken($_SESSION['access_token']);
        }

        if ($client->getAccessToken()) {
            $_SESSION['access_token'] = $client->getAccessToken();
            $user = $oauth2->userinfo->get();
            try {
                if (!empty($user)) {
                    $check = $this->User->find('first', array('conditions' => array('email' => $user['email'], 'status !=' => 'Trash')));
                    if (empty($check)) {
                        $data = array();
                        $data['email'] = $user['email'];
                        $data['name'] = $user['given_name'];
                        $data['user_name'] = $this->random_username($user['given_name']);
                        $this->User->save($data);

                        $id = $this->User->getLastInsertID();
                        $currentuser = $this->User->find('first', array('conditions' => array('user_id' => $id, 'status !=' => 'Trash')));

                        $this->Session->write('User', $currentuser['User']);
                        $this->Session->write('Userlogin', TRUE);

                        $this->Session->setFlash('Logged in Successfully !', '', array(''), 'front_success');
                        return $this->redirect(array('action' => 'profile', 'controller' => 'users'));
                    } else {
                        $this->Session->write('User', $check['User']);
                        $this->Session->write('Userlogin', TRUE);
                        $this->Session->setFlash('Logged in Successfully !', '', array(''), 'front_success');
                        return $this->redirect(array('action' => 'profile', 'controller' => 'users'));
                    }
                }
            } catch (Exception $e) {
                $this->Session->setFlash(GOOGLE_LOGIN_FAILURE, 'default', array('class' => 'message error'), 'error');
                $this->redirect(BASE_PATH . 'login');
            }
        }
    }

    public function fblogin() {
        $this->autoRender = false;
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        FacebookSession::setDefaultApplication(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET);
        $helper = new FacebookRedirectLoginHelper(FACEBOOK_REDIRECT_URI);
        $url = $helper->getLoginUrl(array('email'));
        $this->redirect($url);
    }

    public function fb_login() {
        $this->autoRender = false;
        FacebookSession::setDefaultApplication(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET);
        $helper = new FacebookRedirectLoginHelper(FACEBOOK_REDIRECT_URI);
        $session = $helper->getSessionFromRedirect();

        if (isset($_SESSION['access_token'])) {
            $session = new FacebookSession($_SESSION['access_token']);
            try {
                $session->validate(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET);
            } catch (FacebookAuthorizationException $e) {
                echo $e->getMessage();
            }
        }

        $data = array();
        $fb_data = array();

        if (isset($session)) {
            $_SESSION['access_token'] = $session->getToken();
            $request = new FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();
            $graph = $response->getGraphObject(GraphUser::className());

            $fb_data = $graph->asArray();
            $id = $graph->getId();
            $image = "https://graph.facebook.com/" . $id . "/picture?width=100";

            if (!empty($fb_data)) {
                $result = $this->User->findByEmail($fb_data['email']);
                if (!empty($result)) {
                    if ($this->Auth->login($result['User'])) {
                        $this->Session->setFlash(FACEBOOK_LOGIN_SUCCESS, 'default', array('class' => 'message success'), 'success');
                        $this->redirect(BASE_PATH);
                    } else {
                        $this->Session->setFlash(FACEBOOK_LOGIN_FAILURE, 'default', array('class' => 'message error'), 'error');
                        $this->redirect(BASE_PATH . 'login');
                    }
                } else {
                    $data['email'] = $fb_data['email'];
                    $data['first_name'] = $fb_data['first_name'];
                    $data['social_id'] = $fb_data['id'];
                    $data['picture'] = $image;
                    $data['uuid'] = String::uuid();
                    $this->User->save($data);
                    if ($this->User->save($data)) {
                        $data['id'] = $this->User->getLastInsertID();
                        if ($this->Auth->login($data)) {
                            $this->Session->setFlash(FACEBOOK_LOGIN_SUCCESS, 'default', array('class' => 'message success'), 'success');
                            $this->redirect(BASE_PATH);
                        } else {
                            $this->Session->setFlash(FACEBOOK_LOGIN_FAILURE, 'default', array('class' => 'message error'), 'error');
                            $this->redirect(BASE_PATH . 'index');
                        }
                    } else {
                        $this->Session->setFlash(FACEBOOK_LOGIN_FAILURE, 'default', array('class' => 'message error'), 'error');
                        $this->redirect(BASE_PATH . 'index');
                    }
                }
            } else {
                $this->Session->setFlash(FACEBOOK_LOGIN_FAILURE, 'default', array('class' => 'message error'), 'error');
                $this->redirect(BASE_PATH . 'index');
            }
        }
    }

}
