<?php

class YahooContacts {

    protected static $oauthConsumerKey = "dj0yJmk9MnI1YmpXU0ZCZlVpJmQ9WVdrOVNucHZjM2RqTXpnbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWRi";
    protected static $OauthConsumerSecret = "3edb293ebd854d8175cc51c537b6dad8e0e88206";
    protected static $oauthDomain = "https://project.rayaztech.com/rummy-club/users/yho_callback";

    public function __construct() {
        //Check Session is Start Or not 
        if (session_status() == PHP_SESSION_NONE) {
            //session_start();
        }
    }

    /**
     * Authentication user And Access Refresh and access token
     *
     * @author <Pawan Kumar>
     * @return type boolean
     * */
    public function getAuthorization($code) {
        $url = "https://api.login.yahoo.com/oauth2/get_token";

        $data = "grant_type=authorization_code&redirect_uri=" . self::$oauthDomain . "&code=" . $code;
        $auth = base64_encode(self::$oauthConsumerKey . ":" . self::$OauthConsumerSecret);

        $headers = array(
            'Authorization: Basic ' . $auth,
            'Content-Type: application/x-www-form-urlencoded'
        );

        try {
            $resultSet = self::makeRequest($url, $data, $headers);
            
            if ($resultSet->access_token) {
                $this->setAccessToken($resultSet->access_token);
                $this->setRefreshToken($resultSet->refresh_token);
                $this->setGuidToken($resultSet->xoauth_yahoo_guid);
                return true;
            }
        } catch (Exception $ex) {
            throw($ex);
        }
    }

    /**
     * Get All Contacts list From Yahoo API using Auth Access Token And oAuth Guid Token
     *
     * @author <Pawan Kumar>
     * @return type Object
     * */
    public function getUserContactsDetails() {
        /** Refresh Access Token is Expired * */
        $this->generateAccessToken();

        $guid = $this->getGuidToken();
        $token = $this->getAccessToken();

        $contactUrl = "https://social.yahooapis.com/v1/user/$guid/contacts?format=json";

        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => "Authorization: Bearer $token"
            )
        );

        $context = stream_context_create($opts);
        $file = file_get_contents($contactUrl, false, $context);
        
        $output = json_decode($file);
        return $output;
    }

    /**
     * Get New Access Token using Refresh Token
     *
     * @author <Pawan Kumar>
     * @return type boolean
     * */
    protected function generateAccessToken() {

        $url = "https://api.login.yahoo.com/oauth2/get_token";

        $refreshToken = $this->getRefreshToken();
        $data = "grant_type=refresh_token&redirect_uri=" . self::$oauthDomain . "&refresh_token=" . $refreshToken;

        $auth = base64_encode(self::$oauthConsumerKey . ":" . self::$OauthConsumerSecret);
        $headers = array(
            'Authorization: Basic ' . $auth,
            'Content-Type: application/x-www-form-urlencoded'
        );

        try {

            $resultSet = self::makeRequest($url, $data, $headers);

            if ($resultSet->access_token) {
                $this->setAccessToken($resultSet->access_token);
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw($ex);
        }
    }

    /**
     * Build a login url using oAuth Consumber Key And Redirect Domain
     *
     * @author Pawan Kumar
     * @return type String
     * */
    public static function getLoginUrl() {
        $loginUrl = "https://api.login.yahoo.com/oauth2/request_auth";
        $buildUrl = $loginUrl . "?client_id=" . self::$oauthConsumerKey . "&redirect_uri=" . self::$oauthDomain . "&response_type=code&language=en-us";
        return $buildUrl;
    }

    /**
     * Make  a Remote Post Request using MakeRequest Function
     *
     * @param Url String
     * @param $postData String Send Post Data With Request
     * @param headers Array Contain Auth basic information
     * @author Pawan Kumar
     * @return type Object
     * */
    public static function makeRequest($url, $postData, $headers) {

        try {

            if (empty($url))
                throw new Exception("Url is Not Format.");
            if (empty($postData))
                throw new Exception("Post Parameters is Not Defined");

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch, CURLOPT_URL, $url);

            $result = curl_exec($ch);
            $output = json_decode($result);

            return $output;
        } catch (\Exception $ex) {
            throw($ex);
        }
    }

    /**
     * @param RefreshToken to set String Token Into Session
     */
    public function setRefreshToken($token) {
        $_SESSION['refresh_token'] = $token;
    }

    /**
     * @return String Refresh Token From Session
     */
    public function getRefreshToken() {
        return $_SESSION['refresh_token'];
    }

    /**
     * @param AccessToken to set String Token into Session
     */
    public function setAccessToken($token) {
        $_SESSION['access_token'] = $token;
    }

    /**
     * @return String Access Token From Session
     */
    public function getAccessToken() {
        return $_SESSION['access_token'];
    }

    /**
     * @param GuidToken to set String Token into Session
     */
    public function setGuidToken($token) {
        $_SESSION['xoauth_yahoo_guid'] = $token;
    }

    /**
     * @return String Guid Token from Session
     */
    public function getGuidToken() {
        return $_SESSION['xoauth_yahoo_guid'];
    }

}

// Initialize Session If Session is Not Start
session_start();

if (isset($_GET['code'])) {
    $code = $_GET['code'];
    if (!empty($code)) {
        // create a instance of yahoo contacts
        $obj = new YahooContacts();
        //Successfully Authorization Process
        $obj->getAuthorization($code);
        Header("Location:http://yahoo.fansunite.com.au");
        die;
    }
} else {
    if (isset($_SESSION['access_token'])) {

        // create a instance of yahoo contacts
        $obj = new YahooContacts();

        //After Authorization Get User Contacts Email
        $res = $obj->getUserContactsDetails();
        print "<pre>";
        print_r($res);
    } else {
        $url = YahooContacts::getLoginUrl();
        echo "<center><strong><a href='$url'>Login With Yahoo Mail !</a></strong></center>";
    }
}
?>