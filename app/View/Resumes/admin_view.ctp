<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Uploaded Resume </span>  - Detail
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/resumes" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Name <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <?php echo (!empty($this->request->data['Resume']['name'])) ? $this->request->data['Resume']['name'] : "" ?>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Phone Number <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <?php echo (!empty($this->request->data['Resume']['phone_number'])) ? $this->request->data['Resume']['phone_number'] : "" ?>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Email <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <?php echo (!empty($this->request->data['Resume']['email'])) ? $this->request->data['Resume']['email'] : "" ?>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Current Employer  <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <?php echo (!empty($this->request->data['Resume']['current_employer'])) ? $this->request->data['Resume']['current_employer'] : "" ?>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> CTC <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <?php echo (!empty($this->request->data['Resume']['CTC'])) ? $this->request->data['Resume']['CTC'] : "" ?>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Fixed CTC <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <?php echo (!empty($this->request->data['Resume']['fixed_CTC'])) ? $this->request->data['Resume']['fixed_CTC'] : "" ?>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Expected CTC <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <?php echo (!empty($this->request->data['Resume']['expected_CTC'])) ? $this->request->data['Resume']['expected_CTC'] : "" ?>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Notice Period <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <?php echo (!empty($this->request->data['Resume']['notice_period'])) ? $this->request->data['Resume']['notice_period'] : "" ?>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Note / Reason for Change <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <?php echo (!empty($this->request->data['Resume']['note'])) ? $this->request->data['Resume']['note'] : "" ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>