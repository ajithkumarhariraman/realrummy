<section class="resume_upload">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <h4 class="text-center">RESUME UPLOAD FORM</h4>
                <form action="" method="post" class="resume-upload-form validation_form">
                    <div class="form-group">
                        <label>Name *</label>
                        <input type="text" name="data[Resume][name]" class="form-control validate[required]">
                    </div>
                    <div class="form-group">
                        <label>Phone *</label>
                        <input type="text" name="data[Resume][phone_number]" class="form-control validate[required,custom[phone]]" />
                    </div>
                    <div class="form-group">
                        <label>Email *</label>
                        <input type="email" name="data[Resume][email]" class="form-control validate[required,custom[email]]" />
                    </div>
                    <div class="form-group">
                        <label>Current Employer </label>
                        <input type="text" name="data[Resume][current_employer]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>CTC </label>
                        <input type="text" name="data[Resume][CTC]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Fixed CTC</label>
                        <input type="text" name="data[Resume][fixed_CTC]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Expected CTC</label>
                        <input type="text" name="data[Resume][expected_CTC]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Notice Period</label>
                        <input type="text" name="data[Resume][notice_period]" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Note / Reason for Change</label>
                        <input type="text" name="data[Resume][note]" class="form-control" />
                    </div>
                    <p>Mandatory *</p>
                    <button class="text-center upload-btn" type="submit">UPLOAD RESUME</button>
                </form>
            </div>
            <div class="col-md-8 col-sm-8">
                <img src="<?php echo BASE_URL; ?>images/resume_upload_banner.png" alt="Resume Upload" />
                <div class="job-seekers">
                    <h3>Why Addwiser- GetSetGo? (Job Seekers)</h3>
                    <p>Career advancement need not be predestined. You might be ensconced in your job, not actively considering a career move. 
                        At the same time, curious about new opportunities. that might steer the course of things to come, add a new dimension to your 
                        goals, spike a need for growth and fulfilment. That's why, when Addwiser consultants approach you, you might want to pause 
                        and listen</p>
                </div>
                <button class="text-center chat-btn">Have a Career Chat</button>
            </div>    
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <ul class="list">
                    <li>Search job</li>
                    <li>Spruce up your Resume</li>
                    <li>Get a career Advice</li>
                    <li>Create Login</li>
                </ul>
            </div>
        </div>
    </div>
</section>