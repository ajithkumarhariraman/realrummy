<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Core Team</span>  - Add
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/coreteams" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Name <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Coreteam][name]" value="<?php echo (!empty($this->request->data['Coreteam']['name'])) ? $this->request->data['Coreteam']['name'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Designation <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Coreteam][designation]" value="<?php echo (!empty($this->request->data['Coreteam']['designation'])) ? $this->request->data['Coreteam']['designation'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Detail <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <textarea type="text"  class="form-control validate[required]" name="data[Coreteam][detail]" value="<?php echo (!empty($this->request->data['Coreteam']['detail'])) ? $this->request->data['Coreteam']['detail'] : "" ?>"></textarea>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Profile <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="file"  class="form-control validate[required,custom[image]]" name="data[Coreteam][profile]"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Linkedin URL <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required,custom[url]]" name="data[Coreteam][linkedin_url]" value="<?php echo (!empty($this->request->data['Coreteam']['linkedin_url'])) ? $this->request->data['Coreteam']['linkedin_url'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> &nbsp;</label>
                        <div class="col-lg-5">
                            <button class="btn bg-teal" type="submit"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>