<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Article</span>  - Add
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/articles" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Title <span class="required">*</span></label>
                        <div class="col-lg-12">
                            <input type="text"  class="form-control validate[required]" name="data[Article][title]" value="<?php echo (!empty($this->request->data['Article']['title'])) ? $this->request->data['Article']['title'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Description <span class="required">*</span></label>
                        <div class="col-lg-12">
                            <textarea type="text" id="editor"  class="form-control validate[required]" name="data[Article][description]"><?php echo (!empty($this->request->data['Article']['description'])) ? $this->request->data['Article']['description'] : "" ?></textarea>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Thumbnail <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="file"  class="form-control validate[required,custom[image]]" name="data[Article][image]"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-lg-12">
                            <button class="btn bg-teal" type="submit"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>