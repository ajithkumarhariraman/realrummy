<section class="banner_sec" style="background: url(images/testimonial_banner.png);">
    <h2 class="text-center">CORE TEAM</h2>
</section>
<section class="core_team">
    <div class="container">
        <div class= "owl-carousel owl-theme myowl">
            <?php foreach ($teams as $team) { ?>
                <div class="item">
                    <img class="profile-img" src="<?php echo BASE_URL; ?>files/teams/<?php echo $team['Team']['profile']; ?>" alt="CEO" />
                    <img class="linkedin-img" src="images/linked_in.png"/>
                    <div class="person_details">
                        <h4 class="text-center"><?php echo $team['Team']['profile']; ?> I <?php echo $team['Team']['designation']; ?></h4>
                        <p class="text-center"><?php echo $team['Team']['detail']; ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>