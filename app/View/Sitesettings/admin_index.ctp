<div class="content-wrapper">
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Site Settings</span> - Edit</h4>
            </div>
        </div>
    </div>
    <!-- /page header -->
    <!-- Content area -->
    <div class="content">
        <!-- Form horizontal -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <form method="post" action="" class="validation_form" enctype="multipart/form-data">
                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Site Title <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Site Title" class="form-control validate[required]" name="data[Sitesetting][site_title]" value="<?php echo (isset($result['Sitesetting']['site_title'])) ? $result['Sitesetting']['site_title'] : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Logo </label>
                        <div class="col-sm-7">
                            <input type="file" class="file-styled validate[custom[image]]" name="data[Sitesetting][logo]"/>
                            <p><small>Recommended Size: 100*100</small></p>
                        </div>
                        <div class="col-md-2">
                            <img src="<?php echo BASE_URL; ?>img/<?php echo $result['Sitesetting']['logo']; ?>" class="img-circle img-responsive" style=""/>
                        </div>
                    </div>

                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Fav </label>
                        <div class="col-sm-7">
                            <input type="file" class="file-styled validate[custom[image]]" name="data[Sitesetting][fav_icon]"/>
                            <p><small>Recommended Size: 16*16 </small></p>
                        </div>
                        <div class="col-md-2">
                            <img src="<?php echo BASE_URL; ?>img/<?php echo $result['Sitesetting']['fav_icon']; ?>" class="img-circle img-responsive" style="object-fit: cover; height: 50px; width: 50px;"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Fav </label>
                        <div class="col-sm-7">
                            <input type="file" class="file-styled validate[custom[image]]" name="data[Sitesetting][invite_banner]"/>
                        </div>
                        <div class="col-md-12">
                            <img src="<?php echo BASE_URL; ?>img/<?php echo $result['Sitesetting']['invite_banner']; ?>" class="img-circle img-responsive" style="object-fit: cover; height: 150px; width: 150px;"/>
                        </div>
                    </div>
                    <h4>Static Contents</h4>
                    <div class="form-froup">
                        <label class="col-sm-12 control-label no-padding-right"> Play Rummy Online - Safe, Secure and Rewarding <span class="required">*</span></label>
                        <textarea placeholder="Play Rummy Online - Safe, Secure and Rewarding" id='editor' class="form-control validate[required]" name="data[Sitesetting][home_content_1]"/><?php echo (isset($result['Sitesetting']['footer_about'])) ? $result['Sitesetting']['home_content_1'] : ""; ?></textarea>
                    </div>
                    <h4>India's Most Trusted Rummy Site</h4>
                    <div class="form-froup clearfix">
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="data[Sitesetting][three_sec_title_1]" value="<?php echo (isset($result['Sitesetting']['three_sec_title_1'])) ? $result['Sitesetting']['three_sec_title_1'] : ""; ?>"/>
                            <br/>
                            <textarea placeholder="" class="form-control validate[required]" name="data[Sitesetting][three_sec_desc_1]"/><?php echo (isset($result['Sitesetting']['three_sec_desc_1'])) ? $result['Sitesetting']['three_sec_desc_1'] : ""; ?></textarea>
                            <br/>
                            <input type="file" class="form-control" name="data[Sitesetting][three_sec_img_1]"/>
                            <?php if (!empty($result['Sitesetting']['three_sec_img_1'])) { ?>
                                <img src="<?php echo BASE_URL; ?>img/<?php echo $result['Sitesetting']['three_sec_img_1']; ?>" style="max-width: 30px;"/>
                            <?php } ?>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="data[Sitesetting][three_sec_title_2]" value="<?php echo (isset($result['Sitesetting']['three_sec_title_2'])) ? $result['Sitesetting']['three_sec_title_2'] : ""; ?>"/>
                            <br/>
                            <textarea placeholder=""  class="form-control validate[required]" name="data[Sitesetting][three_sec_desc_2]"/><?php echo (isset($result['Sitesetting']['three_sec_desc_2'])) ? $result['Sitesetting']['three_sec_desc_2'] : ""; ?></textarea>
                            <br/>
                            <input type="file" class="form-control" name="data[Sitesetting][three_sec_img_2]"/>
                            <?php if (!empty($result['Sitesetting']['three_sec_img_2'])) { ?>
                                <img src="<?php echo BASE_URL; ?>img/<?php echo $result['Sitesetting']['three_sec_img_2']; ?>" style="max-width: 30px;"/>
                            <?php } ?>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="data[Sitesetting][three_sec_title_3]" value="<?php echo (isset($result['Sitesetting']['three_sec_title_3'])) ? $result['Sitesetting']['three_sec_title_3'] : ""; ?>"/>
                            <br/>
                            <textarea placeholder="" class="form-control validate[required]" name="data[Sitesetting][three_sec_desc_3]"/><?php echo (isset($result['Sitesetting']['three_sec_desc_3'])) ? $result['Sitesetting']['three_sec_desc_3'] : ""; ?></textarea>
                            <br/>
                            <input type="file" class="form-control" name="data[Sitesetting][three_sec_img_3]"/>
                            <?php if (!empty($result['Sitesetting']['three_sec_img_3'])) { ?>
                                <img src="<?php echo BASE_URL; ?>img/<?php echo $result['Sitesetting']['three_sec_img_3']; ?>" style="max-width: 30px;"/>
                            <?php } ?>
                        </div>
                    </div>
                    <h4>Contact Detail</h4>
                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Phone <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Phone" class="form-control validate[required,custom[phone]]" name="data[Sitesetting][phone]" value="<?php echo (isset($result['Sitesetting']['phone'])) ? $result['Sitesetting']['phone'] : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Email <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Email" class="form-control validate[required,custom[email]]" name="data[Sitesetting][email]" value="<?php echo (isset($result['Sitesetting']['email'])) ? $result['Sitesetting']['email'] : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Facebook Url <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Facebook Url" class="form-control validate[required,custom[url]]" name="data[Sitesetting][facebook_url]" value="<?php echo (isset($result['Sitesetting']['facebook_url'])) ? $result['Sitesetting']['facebook_url'] : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Instagram Url <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Instagram Url" class="form-control validate[required,custom[url]]" name="data[Sitesetting][instagram_url]" value="<?php echo (isset($result['Sitesetting']['instagram_url'])) ? $result['Sitesetting']['instagram_url'] : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Twitter Url <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Twitter Url " class="form-control validate[required,custom[url]]" name="data[Sitesetting][twitter_url]" value="<?php echo (isset($result['Sitesetting']['twitter_url'])) ? $result['Sitesetting']['twitter_url'] : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Pinterest Url <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Pinterest Url" class="form-control validate[required,custom[url]]" name="data[Sitesetting][pinterest_url]" value="<?php echo (isset($result['Sitesetting']['pinterest_url'])) ? $result['Sitesetting']['pinterest_url'] : ""; ?>"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-sm-3 control-label no-padding-right"> Footer About <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <textarea placeholder="Footer About" class="form-control validate[required]" name="data[Sitesetting][footer_about]"/><?php echo (isset($result['Sitesetting']['footer_about'])) ? $result['Sitesetting']['footer_about'] : ""; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-sm-9 col-md-offset-3 text-right">
                            <button class="btn btn-info" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>