<?php $settings = ClassRegistry::init('Sitesetting')->find('first', array('conditions' => array('id' => '1'))); ?>
<script src="https://www.google.com/recaptcha/api.js"></script>
<div class="container">
    <div class="page-inner bx-shadow mb-5">
        <div class="page-title pb-5 mb-5 pt-3">
            <h2 class="double-shadow m-0">RUMMY CLUB 24X7 GAME SUPPORT</h2>
        </div>
        <div class="row mb-5">
            <div class="text-center">
                <h3 class="text-success-2 mb-5 mt-0">Our customer support Team is here to help you 24X7</h3>
                <p class="h5">You can call us at <span><?php echo $settings['Sitesetting']['phone']; ?></span></p>
                <p class="h5">Prefer to email? Please tell us more about the issue. We will get back to you earliest</p>
            </div>
            <div class="bx-shadow-2 contact-inner clearfix mt-5">
                <div class="col-md-8 contact-left match-height">
                    <p class="h4 mb-5">Send Us Message</p>
                    <form class="validation_form" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name <i>*</i></label>
                                    <input type="text" class="form-control validate[required]" name='data[Contact][name]'/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email ID <i>*</i></label>
                                    <input type="text" class="form-control validate[required,custom[email]]" name='data[Contact][email]'/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone <i>*</i></label>
                                    <input type="text" class="form-control validate[required,custom[phone]]" name='data[Contact][phone]'/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Subject <i>*</i></label>
                                    <input type="text" class="form-control validate[required]" name='data[Contact][subject]'/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-4">
                            <textarea type="text" class="form-control validate[required]" placeholder="Message" name='data[Contact][message]'></textarea>
                        </div>
                        <div class="form-group mt-4">
                            <div class="g-recaptcha" data-sitekey="6LctgqoUAAAAAI8lCdvUoWxU0KE0WBAsNGCdh79l"></div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger"><i class="fa fa-paper-plane"></i> SEND</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-4 contact-right match-height">
                    <div class="">
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object" src="<?php echo BASE_URL; ?>img/loc.png" alt="...">
                            </div>
                            <div class="media-body">
                                <div class="media-heading h5">ADDRESS</div>
                                <p><?php echo $settings['Sitesetting']['address']; ?></p>
                            </div>
                        </div>
                        <hr/>
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object" src="<?php echo BASE_URL; ?>img/mail.png" alt="...">
                            </div>
                            <div class="media-body">
                                <div class="media-heading h5">EMAIL</div>
                                <p><?php echo $settings['Sitesetting']['email']; ?></p>
                            </div>
                        </div>
                        <hr/>
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object" src="<?php echo BASE_URL; ?>img/call.png" alt="...">
                            </div>
                            <div class="media-body">
                                <div class="media-heading h5">CONTACT US</div>
                                <p><?php echo $settings['Sitesetting']['phone']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.match-height').matchHeight();
</script>