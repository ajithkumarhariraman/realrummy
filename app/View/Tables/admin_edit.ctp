<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Table</span>  - Add
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/tables" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Table Type <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <select class="form-control validate[required]" name="data[Table][type]">
                                <option value="Cash" <?php echo (!empty($this->request->data['table']['type']) && $this->request->data['table']['type'] == 'Cash') ? "selected" : "" ?>>Cash</option>
                                <option value="Practice" <?php echo (!empty($this->request->data['table']['type']) && $this->request->data['table']['type'] == 'Practice') ? "selected" : "" ?>>Practice</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Table Category <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <select class="form-control validate[required]" name="data[Table][category]">
                                <option value="Points" <?php echo (!empty($this->request->data['table']['category']) && $this->request->data['table']['category'] == 'Points') ? "selected" : "" ?>>Points</option>
                                <option value="Pool" <?php echo (!empty($this->request->data['table']['category']) && $this->request->data['table']['category'] == 'Pool') ? "selected" : "" ?>>Pool</option>
                                <option value="Deals" <?php echo (!empty($this->request->data['table']['category']) && $this->request->data['table']['category'] == 'Deals') ? "selected" : "" ?>>Deals</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Table Name <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Table][name]" value="<?php echo (!empty($this->request->data['Table']['name'])) ? $this->request->data['Table']['name'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Decks <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Table][decks]" value="<?php echo (!empty($this->request->data['Table']['decks'])) ? $this->request->data['Table']['decks'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Max Players <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Table][maxplayer]" value="<?php echo (!empty($this->request->data['Table']['maxplayer'])) ? $this->request->data['Table']['maxplayer'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Point Value <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Table][pointvalue]" value="<?php echo (!empty($this->request->data['Table']['pointvalue'])) ? $this->request->data['Table']['pointvalue'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Min Entry <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Table][minentry]" value="<?php echo (!empty($this->request->data['Table']['minentry'])) ? $this->request->data['Table']['minentry'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> &nbsp;</label>
                        <div class="col-lg-5">
                            <button class="btn bg-teal" type="submit"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('.ui_datepicker').datepicker({format: 'dd-mm-yyyy'})
</script>