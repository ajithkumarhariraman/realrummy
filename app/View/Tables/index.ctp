<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow mb-5">
        <div class="row">
            <div class="col-md-12">
                <div>
                    <?php
                    $types = array('Cash', 'Practice');
                    $categories = array('Points', 'Pool', 'Deals');
                    ?>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($types as $key => $type) { echo "<script>alert(".$type.");</script>";?>
                            <li role="presentation" class="<?php echo $key == 0 ? "active" : "" ?>"><a href="#<?php echo $type; ?>" aria-controls="<?php echo $type; ?>" role="tab" data-toggle="tab"><?php echo $type; ?></a></li>
                        <?php } ?>
                        <li role="presentation" class=""><a href="#tournaments" aria-controls="tournaments" role="tab" data-toggle="tab"><?php echo 'Tournaments'; ?></a></li>
                        <li><a href="javascript:;" data-toggle="modal" data-target="#my-joined-games">My Joined Games</a></li>
                    </ul>

                    <!-- Modal -->
                    <div class="modal fade" id="my-joined-games" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <p class="pull-left m-0">My Joined Games</p>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Tab panes -->
                    <div class="tab-content pt-3 pb-2">
                        <?php foreach ($types as $ikey => $type) { ?>
                            <div role="tabpanel" class="tab-pane <?php echo $ikey == 0 ? "active" : "" ?>" id="<?php echo $type; ?>">
                                <ul class="nav nav-tabs" role="tablist">
                                    <?php foreach ($categories as $key => $category) { ?>
                                        <li role="presentation" class="<?php echo $key == 0 ? "active" : "" ?>">
                                            <a href="#<?php echo $type; ?><?php echo $category; ?>" aria-controls="<?php echo $type; ?><?php echo $category; ?>" role="tab" data-toggle="tab"><?php echo $category; ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <?php foreach ($categories as $key => $category) { ?>
                                        <div role="tabpanel" class="tab-pane <?php echo $key == 0 ? "active" : "" ?>" id="<?php echo $type; ?><?php echo $category; ?>">
                                            <div class="article max-h">
                                                <table class="table table-bordered table-striped">
                                                    <thead class="bg-success">
                                                        <tr>
                                                            <td>Table Name</td>
                                                            <td>Decks</td>
                                                            <td>Max Player</td>
                                                            <?php if ($category == 'Points') { ?>
                                                                <td>Point Value</td>
                                                            <?php } else if ($category == 'Pool') { ?>
                                                                <td>Type</td>
                                                                <td>Price</td>
                                                            <?php } else { ?>
                                                                <td>Deals</td>
                                                                <td>Price</td>
                                                            <?php } ?>
                                                            <td><?php echo ($category == 'Points') ? "Min Entry" : "Entry Fee"; ?></td>
                                                            <td>Active Players</td>
                                                            <td>Registering</td>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $tables = ClassRegistry::init('Table')->find('all', array('conditions' => array('status' => 'Active', 'category' => $category, 'type' => $type)));
                                                        if (!empty($tables)) {
                                                            foreach ($tables as $table) {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $table['Table']['name']; ?></td>
                                                                    <td><?php echo $table['Table']['decks']; ?></td>
                                                                    <td><?php echo $table['Table']['maxplayer']; ?></td>
                                                                    <?php
                                                                    if ($category == 'Points') {
                                                                        $rummy = 'POINTS RUMMY';
                                                                        ?>
                                                                        <td><?php echo $table['Table']['pointvalue']; ?></td>
                                                                        <?php
                                                                    } else if ($category == 'Pool') {
                                                                        $rummy = 'POOL RUMMY';
                                                                        ?>
                                                                        <td><?php echo $table['Table']['pool_type']; ?></td>
                                                                        <td><?php echo $table['Table']['price']; ?></td>
                                                                        <?php
                                                                    } else {
                                                                        $rummy = 'DEAL RUMMY';
                                                                        ?>
                                                                        <td><?php echo $table['Table']['deals']; ?></td>
                                                                        <td><?php echo $table['Table']['price']; ?></td>
                                                                    <?php } ?>
                                                                    <td><?php echo $table['Table']['minentry']; ?></td>
                                                                    <td><span id="active_<?php echo $table['Table']['table_id']; ?>"><?php echo '0'; ?></span></td>
                                                                    <td><span id="registering_<?php echo $table['Table']['table_id']; ?>"><?php echo '0/' . $table['Table']['maxplayer']; ?></span></td>
                                                                    <?php
																	if($table['Table']['type'] == 'Cash')
																	{
																	$matchamounttype=2;
																	}
																	else if($table['Table']['type'] == 'Practice')
																	{
																	$matchamounttype=1;
																	}

                                                                    $url = GAME_URL . "jointableweb.php?pid=" . $sessionuser['User']['user_id'] . "&max=" . $table['Table']['maxplayer'] . "&amount=" . $table['Table']['minentry'] . "&game=" . $rummy . "&deal=" . $table['Table']['deals'] . "&pool=" . $table['Table']['pool_type'] . "&type=".$matchamounttype."&category=" . $category . "&point=" . $table['Table']['pointvalue'] . "&table_id=" . $table['Table']['table_id'];

                                                                    ?>
                                                                    <td>
                                                                        <a href="<?php echo $url ?>" class="btn bg-success play_game">Play Now</a>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <tr>
                                                                <td colspan="8">No</td>
                                                            </tr>
                                                        <?php }
                                                        ?>
                                                    </tbody> 
                                                </table>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div role="tabpanel" class="tab-pane" id="tournaments">
                            <div class="tab-content">
                                <?php
                                $categories = array('Cash', 'Club', 'Jackpot');
                                ?>
                                <ul class="nav nav-tabs" role="tablist">
                                    <?php foreach ($categories as $key => $category) { ?>
                                        <li role="presentation" class="<?php echo $key == 0 ? "active" : "" ?>">
                                            <a href="#tournaments<?php echo $category; ?>" aria-controls="tournaments<?php echo $category; ?>" role="tab" data-toggle="tab"><?php echo $category; ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <?php foreach ($categories as $key => $category) { ?>
                                    <div role="tabpanel" class="tab-pane <?php echo $key == 0 ? "active" : "" ?>" id="tournaments<?php echo $category; ?>">
                                        <div class="article max-h">
                                            <table class="table table-bordered table-striped">
                                                <thead class="bg-success">
                                                    <tr>
                                                        <td>Name</td>
                                                        <td>Entry Fee (Rs.)</td>
                                                        <td>Prize</td>
                                                        <td>Joined</td>
                                                        <td>Starts</td>
                                                        <td>Action</td>
                                                        <td>Details</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $tournaments = ClassRegistry::init('Tournament')->find('all', array('conditions' => array('type' => $category)));
                                                    if (!empty($tournaments)) {
                                                        foreach ($tournaments as $tournament) {
                                                            $joined = ClassRegistry::init('Tournamentplayer')->find('count', array('conditions' => array('tournament_id' => $tournament['Tournament']['tournament_id'])));
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $tournament['Tournament']['name']; ?></td>
                                                                <td><i class="fa fa-inr"></i><?php echo $tournament['Tournament']['entryfee']; ?></td>
                                                                <td><i class="fa fa-inr"></i><?php echo $tournament['Tournament']['prizeamount']; ?></td>
                                                                <td><?php echo $joined; ?></td>
                                                                <td><?php echo date('h:i A M-d', strtotime($tournament['Tournament']['starts'])); ?></td>
                                                                <td>
                                                                    <?php if (strtotime($tournament['Tournament']['reg_starts']) > time()) { ?>
                                                                        <button class="btn btn-default" type="button">Join</button>
                                                                        <?php
                                                                    } else if (strtotime($tournament['Tournament']['reg_ends']) > time()) {
                                                                        $check = ClassRegistry::init('Tournamentplayer')->find('first', array('conditions' => array('tournament_id' => $tournament['Torunament']['tournament_id'])));
                                                                        if (empty($check)) {
                                                                            ?>
                                                                            <a class="btn btn-default" href="javascript" data-id="<?php echo $tournament['Tournament']['tournament_id']; ?>" id="join">Join</a>
                                                                        <?php } else {
                                                                            ?>
                                                                            <a class="btn btn-default" href="javascript" data-id="<?php echo $tournament['Tournament']['tournament_id']; ?>" id="withdraw">Withdraw</a>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        echo $tournament['Tournament']['tournament_status'];
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" data-toggle="modal" data-target="#tournament-details" data-id="<?php echo $tournament['Tournament']['tournament_id']; ?>">Details</a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <tr>
                                                            <td colspan="8">No</td>
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                </tbody> 
                                            </table>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right badge badge-default p-2">
                        <i class="fa fa-clock-o" aria-hidden="true"></i> Time: <?php echo date('h:i A'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tournament-details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script>
    $('a.play_game').click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        window.open(href, "mywindow", "location=0,menubar=1,toolbar=yes,scrollbars=yes,resizable=no,top=500,left=500,width=4000,height=4000");
        return false;
    });
</script>

<script>
    $('#tournament-details').on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget) // Button that triggered the modal
        var id = button.attr('data-id');
        jQuery.ajax({
            url: "<?php echo BASE_URL; ?>tournaments/popupDetail/" + id,
            dataType: "html",
            success: function (data) {
                $('#tournament-details .modal-body').html(data);
                $('.match-height').matchHeight();
            }
        });

    });

    setTimeout(function doSomething() {
        jQuery.ajax({
            url: "<?php echo BASE_URL; ?>tables/updatePlayers",
            dataType: "json",
            success: function (data) {
                $.each(data.active, function (i, active) {
                    $('#active_' + i).text(active);
                });
                $.each(data.registering, function (i, registering) {
                    $('#registering_' + i).text(registering);
                });
            }
        });
        jQuery.ajax({
            url: "<?php echo BASE_URL; ?>tournaments/tourAjax",
            dataType: "html",
            success: function (data) {
                $('#tournaments .tab-content').html(data);
            }
        });
        setTimeout(doSomething, 2000);
    }, 2000);



    $('#my-joined-games').on('show.bs.modal', function (event) {
        var modal = $(this);
        jQuery.ajax({
            url: "<?php echo BASE_URL; ?>tables/getMyGames",
            dataType: "html",
            success: function (data) {
                modal.find('.modal-body').html(data);
            }
        });
    });

    $(document).on('click', '#join', function () {
        var id = $(this).attr('data-id');
        jQuery.ajax({
            url: "<?php echo BASE_URL; ?>tournaments/join/" + id,
            dataType: "json",
            success: function (data) {
                swal({
                    title: "",
                    text: data.message,
                }, function () {
                    window.location.href = window.location.href;
                });
            }
        });
    });
    $(document).on('click', '#withdraw', function () {
        var id = $(this).attr('data-id');
        jQuery.ajax({
            url: "<?php echo BASE_URL; ?>tournaments/withdraw/" + id,
            dataType: "json",
            success: function (data) {
                swal({
                    text: data.message,
                }, function () {
                    window.location.href = window.location.href;
                });
            }
        });
    });
</script>
