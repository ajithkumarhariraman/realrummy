<?php if (!empty($tables)) { ?>
    <table class="table table-bordered table-striped">
        <thead class="bg-success">
            <tr>
                <td>Table Name</td>
                <td>Decks</td>
                <td>Active Players</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($tables as $table) {
                $table = ClassRegistry::init('Table')->find('all', array('conditions' => array('table_id' => $table['tbl_detail']['table_id'])));
                ?>
                <tr>
                    <td><?php echo $table['Table']['name']; ?></td>
                    <td><?php echo $table['Table']['maxplayer']; ?></td>
                    <td><span id="active_<?php echo $table['Table']['table_id']; ?>"><?php echo '0'; ?></span></td>
                </tr>
                <?php
            }
            ?>
        </tbody> 
    </table>
    <?php
} else {
    ?>
    <p>You are not part of any ongoing games at this moment.</p>
<?php }
?>