<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Email Content</span> - Edit
            </h4>
        </div>
        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo BASE_URL; ?>admin/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="<?php echo BASE_URL; ?>admin/emailcontents">Email Content</a></li>
            <li class="active">Edit</li>
        </ul>
        <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    </div>
</div>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Email Content</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <form class="form-horizontal full_frms full_view_borders validation_form" id="myForm" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6 r_text">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Email Title <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control validate[required]" id="title" name="data[Emailcontent][title]" value="<?php echo!empty($this->request->data['Emailcontent']['title']) ? $this->request->data['Emailcontent']['title'] : ''; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">From Name  <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control validate[required]" id="fromname" name="data[Emailcontent][fromname]" value="<?php echo!empty($this->request->data['Emailcontent']['fromname']) ? $this->request->data['Emailcontent']['fromname'] : ''; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">From Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control validate[custom[email]]" id="fromemail" name="data[Emailcontent][fromemail]" value="<?php echo!empty($this->request->data['Emailcontent']['fromemail']) ? $this->request->data['Emailcontent']['fromemail'] : ''; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">To Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control validate[custom[email]]" id="toemail" name="data[Emailcontent][toemail]" value="<?php echo!empty($this->request->data['Emailcontent']['toemail']) ? $this->request->data['Emailcontent']['toemail'] : ''; ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 r_text">
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">CC Mail</label>
                            <div class="col-sm-9">
                                <textarea  class="form-control" id="ccemail" rows="5" name="data[Emailcontent][ccemail]"><?php echo!empty($this->request->data['Emailcontent']['ccmail']) ? $this->request->data['Emailcontent']['ccmail'] : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Subject <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control validate[required]" id="subject" name="data[Emailcontent][subject]" value="<?php echo!empty($this->request->data['Emailcontent']['subject']) ? $this->request->data['Emailcontent']['subject'] : ''; ?>"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label for="inputPassword3" class="col-sm-12 control-label">Content <span class="required">*</span></label>
                    <div class="col-sm-12">
                        <?php
                        $label = explode(",", $this->request->data['Emailcontent']['label']);
                        $tbltxt = '';
                        foreach ($label as $lab) {
                            $labtxt = explode(":", $lab);
                            $tbltxt.='<tr><td>' . $labtxt[0] . '</td><td>:</td><td>' . $labtxt[1] . '</td></tr>';
                        }
                        ?>

                        <table style="width:100%">
                            <tr>
                                <td style="width:100%">
                                    <textarea placeholder="Content" id="content"  class="form-control validate[required] ckeditor" rows="5" name="data[Emailcontent][emailcontent]"><?php echo!empty($this->request->data['Emailcontent']['emailcontent']) ? $this->request->data['Emailcontent']['emailcontent'] : ''; ?></textarea></td>
                                <td>

                                </td>
                            </tr>
                        </table>
                        <table cellpadding="6">
                            <tr>
                                <td colspan="3" style="color:#F00 ; "><h4 style="margin:5px 0px;">Dynamic Variables</h4>( Dont edit this variable )</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $tbltxt; ?>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <button class="btn btn-info" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

