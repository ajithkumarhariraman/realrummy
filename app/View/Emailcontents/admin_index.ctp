<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Email Content</span> - List
            </h4>
        </div>
        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo BASE_URL; ?>admin/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active"><a href="javascript:;">Email Content</a></li>
        </ul>
        <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    </div>
</div>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Email Content</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <table class="table datatable-basic table-bordered" id="data-table">
                <thead>
                    <tr>
                        <th data-toggle="true"> S. No </th>
                        <th data-sort-ignore="true" width="70%"> Title </th>
                        <th data-hide="phone"class="act"> Action </th>
                    </tr>
                </thead>
                <?php
                $i = $this->Paginator->counter('{:start}');
                foreach ($emailcontents as $emailcontent) {
                    ?>
                    <tr class="trs">
                        <td class="all_view"><center><?php echo $i; ?></center></td>
                    <td class="all_view"><?php echo $emailcontent['Emailcontent']['title']; ?></td>

                    <td class=" detail_vi"  width="10%">
                        <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit/' . $emailcontent['Emailcontent']['emailcontent_id']), array('escape' => false, 'class' => 'green', "data-toggle" => "tooltip", "title" => "Edit")); ?>
                    </td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

