<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Faq category Management</span>  - Add
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/faqcategories" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Category Title <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Faqcategory][title]" value="<?php echo (!empty($this->request->data['Faqcategory']['title'])) ? $this->request->data['Faqcategory']['title'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> &nbsp;</label>
                        <div class="col-lg-5">
                            <button class="btn bg-teal" type="submit"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>