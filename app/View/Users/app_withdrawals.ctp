<div class="user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="clearfix menu-content">
        <div class="col-xs-3 match-height">
            <?php echo $this->element('app_user_sidebar'); ?>
        </div>
        <div class="col-xs-9 menu-content-right  alt-menu-right match-height">
            <div class="mt-3 mb-5">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs mb-4" role="tablist">
                    <li role="presentation" class="active"><a class="bg-success" href="<?php echo BASE_URL; ?>users/app_withdrawals?user_id=<?php echo $_REQUEST['user_id']; ?>" aria-controls="settings"  >Make Withdrawal</a></li>
                    <li role="presentation"><a href="<?php echo BASE_URL; ?>users/app_withdrawalstatus?user_id=<?php echo $_REQUEST['user_id']; ?>" aria-controls="settings"  >Withdrawal Status</a></li>
                </ul>
                <div class="p-2">
                    <div class="panel panel-success mb-5">
                        <div class="panel-heading bg-success">
                            <h4>Withdrawable Balance <i class="fa fa-inr"></i> <?php echo number_format($sessionuser['User']['withdrawable_balance'],2); ?></h4>
                        </div>
                        <div class="panel-body">
                            <?php if ($sessionuser['User']['withdrawable_balance'] != 0) { ?>
                                <form method="post" action="#" class="validation_form">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Enter Amount to Withdraw <i>*</i></label>
                                                <input type="text" class="form-control validate[required]" name="data[Withdrawal][amount]" value="<?php echo (!empty($this->request->data['Withdrawal']['amount'])) ? $this->request->data['Withdrawal']['amount'] : "" ?>"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Enter bank Account No <i>*</i></label>
                                                <input type="text" class="form-control validate[required]" id="bank_account_number" name="data[Withdrawal][bank_account_number]" value="<?php echo (!empty($this->request->data['Withdrawal']['bank_account_number'])) ? $this->request->data['Withdrawal']['bank_account_number'] : "" ?>"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Re Enter bank Account No <i>*</i></label>
                                                <input type="text" class="form-control validate[required,equals[bank_account_number]]" name="data[Withdrawal][bank_account_number]" value="<?php echo (!empty($this->request->data['Withdrawal']['bank_account_number'])) ? $this->request->data['Withdrawal']['bank_account_number'] : "" ?>"/>
                                            </div>
                                            <div class="form-group">
                                                <label>IFSC <i>*</i></label>
                                                <input type="text" class="form-control validate[required]" name="data[Withdrawal][ifsc]" value="<?php echo (!empty($this->request->data['Withdrawal']['ifsc'])) ? $this->request->data['Withdrawal']['ifsc'] : "" ?>"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Bank Name <i>*</i></label>
                                                <input type="text" class="form-control validate[required]" name="data[Withdrawal][bank_name]" value="<?php echo (!empty($this->request->data['Withdrawal']['bank_name'])) ? $this->request->data['Withdrawal']['bank_name'] : "" ?>"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Account Holder Name <i>*</i></label>
                                                <input type="text" class="form-control validate[required]" name="data[Withdrawal][account_holder]" value="<?php echo (!empty($this->request->data['Withdrawal']['account_holder'])) ? $this->request->data['Withdrawal']['account_holder'] : "" ?>"/>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-block bg-success btn-md">SUBMIT</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            <?php } else { ?>
                                <p class="text-center">Min Balance to withdraw is <i class="fa a-inr"></i> 100.00</p>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>