<div class="container">
    <div class="page-inner bx-shadow mb-5">
        <div class="clearfix">
            <div class="col-md-6">
                <img src="<?php echo BASE_URL; ?>img/bg_123.png" class="img-responsive"/>
            </div>
            <div class="col-md-6">
                <div class="verification-inner">
                    <h3 class="text-success-gradiant">CONGRATULATIONS!</h3>
                    <h4 style="
                        font-weight: 400;
                        margin-top: 28px;
                        font-size: 26px;
                        ">You have successfully registered on Rummy Club</h4>
                    <div class="your-name bg-success">
                        <label>Your User Name is</label>
                        <p class="h4"><?php echo (!empty($this->Session->read('user_name'))) ? $this->Session->read('user_name') : "No Name"; ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="bg-danger h5 p-3">
                    Verification link has been sent to your email address. Please click on the link to activate your account!
                </div>
            </div>
        </div>

    </div>
</div>