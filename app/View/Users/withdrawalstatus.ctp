<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow-2 mt-0 mb-5">
        <div class="row menu-content">
            <?php echo $this->element('user_sidebar'); ?>
            <div class="col-md-10 menu-content-right match-height">
                <h3 class="text-success-2 mb-5 mt-5">Withdrawals</h3>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs mb-5" role="tablist">
                    <li role="presentation"><a  href="<?php echo BASE_URL; ?>users/withdrawals" aria-controls="settings" >Make Withdrawal</a></li>
                    <li role="presentation" class="active"><a class="bg-success" href="<?php echo BASE_URL; ?>users/withdrawalstatus" aria-controls="settings" >Withdrawal Status</a></li>
                </ul>
                <div class="panel panel-success mb-5">
                    <div class="panel-heading bg-success">
                        <h4>Withdraw Status</h4>
                    </div>
                    <div class="panel-body p-5">
                        <table class="table table-striped">
                            <thead class="bg-success-1">
                                <tr>
                                    <th>Request Date</th>
                                    <th>Amount</th>
                                    <th>Withdrawal ID</th>
                                    <th>Current Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($withdrawals)) {
                                    foreach ($withdrawals as $withdrawal) {
                                        ?>
                                        <tr>
                                            <td><?php echo date('d.m.Y', strtotime($withdrawal['Withdrawal']['date'])); ?></td>
                                            <td><i class="la la-inr"></i> <?php echo $withdrawal['Withdrawal']['amount']; ?></td>
                                            <td><?php echo $withdrawal['Withdrawal']['withdrawal_id']; ?></td>
                                            <td><?php echo $withdrawal['Withdrawal']['status']; ?></td>
                                            <td>
                                                <?php if ($withdrawal['Withdrawal']['status'] == 'Pending') { ?>
                                                    <a href="<?php echo BASE_URL; ?>users/withdrawals/<?php echo $withdrawal['Withdrawal']['withdrawal_id']; ?>"><i class="la la-edit"></i></a>
                                                <?php } ?>
                                                <a href="<?php echo BASE_URL; ?>users/deletewithdrawals/<?php echo $withdrawal['Withdrawal']['withdrawal_id']; ?>" class="delconfirm"><i class="la la-trash-o text-danger"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td class="text-center" colspan="5">No Data Found</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>