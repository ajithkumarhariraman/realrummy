<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Send Mail</span>
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/mails" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Type <span class="required">*</span></label>
                        <div class="col-lg-12">
                            <select class="form-control" name="data[Letter][type]">
                                <option value="Common">To All</option>
<!--                                <option value="User">Specific User</option>-->
                            </select>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Subject <span class="required">*</span></label>
                        <div class="col-lg-12">
                            <input type="text"  class="form-control validate[required]" name="data[Letter][subject]" value="<?php echo (!empty($this->request->data['Letter']['subject'])) ? $this->request->data['Letter']['subject'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Detail <span class="required">*</span></label>
                        <div class="col-lg-12">
                            <textarea type="text" id="editor"  class="form-control validate[required]" name="data[Letter][detail]"><?php echo (!empty($this->request->data['Letter']['detail'])) ? $this->request->data['Letter']['detail'] : "" ?></textarea>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-lg-12">
                            <button class="btn bg-teal" type="submit"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>