<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Withdrawal Requests</span>  - List
            </h4>
        </div>
    </div>
</div>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form method="get" class="form-inline form-group">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Withdrawal ID" name="s" value="<?php echo (!empty($_REQUEST['s'])) ? $_REQUEST['s'] : "" ?>"/>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control ui_datepicker" placeholder="DATE" name="date" value="<?php echo (!empty($_REQUEST['date'])) ? $_REQUEST['date'] : "" ?>"/>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control " placeholder="Status" name="status" value="<?php echo (!empty($_REQUEST['status'])) ? $_REQUEST['status'] : "" ?>"/>
                </div>
                <div class="form-group">
                    <button name="search" type="submit" class="btn btn-success btn-sm">Search</button>
                </div>
                <?php if (isset($_REQUEST['search'])) { ?>
                    <div class="form-group">
                        <a href="<?php echo BASE_URL; ?>admin/users/withdrawals" class="btn btn-danger btn-sm">Cancel</a>
                    </div>
                <?php } ?>
            </form>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Request Date</th>
                            <th>User Name</th>
                            <th>Amount</th>
                            <th>Withdrawal ID</th>
                            <th>Current Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = $this->Paginator->counter('{:start}');
                        if (!empty($withdrawals)) {
                            foreach ($withdrawals as $withdrawal) {
                                $user = ClassRegistry::init('User')->find('first', array('conditions' => array('user_id' => $withdrawal['Withdrawal']['user_id'])));
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo date('d.m.Y', strtotime($withdrawal['Withdrawal']['date'])); ?></td>
                                    <td><a target="_blank" href="<?php echo BASE_URL; ?>admin/users/view/<?php echo $user['User']['user_id'] ?>"><?php echo $user['User']['name'] ?></a></td>
                                    <td><i class="fa fa-inr"></i> <?php echo number_format($withdrawal['Withdrawal']['amount'],2); ?></td>
                                    <td><?php echo $withdrawal['Withdrawal']['withdrawal_id']; ?></td>
                                    <td><?php echo $withdrawal['Withdrawal']['status']; ?></td>
                                    <td>
                                        <?php if ($withdrawal['Withdrawal']['status'] == 'Pending') { ?>
                                            <a href="<?php echo BASE_URL; ?>admin/users/updatewithdraw/<?php echo $withdrawal['Withdrawal']['id']; ?>">Transfer</a>
                                        <?php } else { ?>
                                            <a href="<?php echo BASE_URL; ?>admin/users/viewwithdrawrequest/<?php echo $withdrawal['Withdrawal']['id']; ?>">View Details</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
                            ?>
                            <tr>
                                <td class="text-center" colspan="5">No Data Found</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                        ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                        <ul class="pagination">
                            <?php
                            echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                            $numbers = $this->Paginator->numbers();
                            if (empty($numbers)) {
                                echo '<li class="active page-link"><a>1</a></li>';
                            } else {
                                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                            }
                            echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                            ?>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.ui_datepicker').datepicker({format: 'dd-mm-yyyy'});
</script>