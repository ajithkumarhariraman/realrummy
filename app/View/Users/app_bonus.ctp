<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="mt-0 mb-5">
        <div class="clearfix menu-content">
            <div class="col-xs-3 match-height">
                <?php echo $this->element('app_user_sidebar'); ?>
            </div>
            <div class="col-xs-9 menu-content-right match-height">
                <div class="bx-shadow mt-3">
                    <table class="table table-borderless m-0">
                        <tr>
                            <td>Available Bonus</td>
                            <td>:</td>
                            <td>
                                <?php echo $sessionuser['User']['pending_bonus'] ?>
                                <a href="#" class="btn bg-danger ml-5"><i class="la la-angle-right"></i> Claim Now</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Released Bonus</td>
                            <td>:</td>
                            <td><?php echo $sessionuser['User']['released_bonus'] ?></td>
                        </tr>
                    </table>
                </div>
                <div class="panel panel-success mb-5 mt-5 match-height" style="height: 213.5px;">
                    <div class="panel-heading bg-success">
                        <h4>Cash Account</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped">
                            <thead class="bg-success-1">
                                <tr>
                                    <td>Sl No</td>
                                    <td>Issued Date</td>
                                    <td>Expiry Date</td>
                                    <td>Amount</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="4">No Record Found</td>
                                </tr>
                            </tbody> 
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>