<div class="user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="clearfix menu-content">
        <div class="col-xs-3 match-height">
            <?php echo $this->element('app_user_sidebar'); ?>
        </div>
        <div class="col-xs-9 menu-content-right  alt-menu-right match-height">
            <div class="mt-3 mb-5">
                <div class="p-2">
                    <div class="panel panel-success mb-5">
                        <div class="panel-heading bg-success">
                            <h4>Game History</h4>
                        </div>
                        <div class="panel-body">
                            <?php
                            $history = ClassRegistry::init('History')->find('all', array('conditions' => array('pid' => $sessionuser['User']['user_id'])));
                            if (!empty($history)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Game</th>
                                            <th>Table ID</th>
                                            <th>Date Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($history as $history) {
                                            ?>
                                            <tr>
                                                <td><?php echo $history['History']['status']; ?></td>
                                                <td><?php echo $history['History']['tid']; ?></td>
                                                <td><?php echo date('d M Y h:i A', strtotime($history['History']['datetime'])); ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else {
                                ?>
                                <div class="text-center">
                                    <img src="<?php echo BASE_URL; ?>img/no-data.png"/>
                                    <p>No data found!</p>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>