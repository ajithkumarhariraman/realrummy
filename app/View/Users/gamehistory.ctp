<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow-2 mt-0 mb-5">
        <div class="row menu-content">
            <?php echo $this->element('user_sidebar'); ?>
            <div class="col-md-10 menu-content-right match-height">
                <div class="panel panel-success mb-5 mt-5">
                    <div class="panel-heading bg-success">
                        <h4>Game History</h4>
                    </div>
                    <div class="panel-body">
                        <?php
                        $history = ClassRegistry::init('History')->find('all', array('conditions' => array('pid' => $sessionuser['User']['user_id'])));
                        if (!empty($history)) {
                            ?>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Game</th>
                                        <th>Table ID</th>
                                        <th>Date Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($history as $history) {
                                        ?>
                                        <tr>
                                            <td><?php echo $history['History']['status']; ?></td>
                                            <td><?php echo $history['History']['tid']; ?></td>
                                            <td><?php echo date('d M Y h:i A', strtotime($history['History']['datetime'])); ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } else {
                            ?>
                            <div class="text-center">
                                <img src="<?php echo BASE_URL; ?>img/no-data.png"/>
                                <p>No data found!</p>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
