<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow-2 mt-0 mb-5">
        <div class="row menu-content">
            <?php echo $this->element('user_sidebar'); ?>
            <div class="col-md-10 menu-content-right match-height">
                <h3 class="text-success-2 mb-5 mt-5">Bonus</h3>
                <div class="bx-shadow">
                    <table class="table table-borderless">
                        <tr>
                            <td>Available Bonus</td>
                            <td>:</td>
                            <td><?php echo $sessionuser['User']['pending_bonus'] ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>