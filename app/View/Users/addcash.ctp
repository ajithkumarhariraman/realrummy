<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow-2 mt-0 mb-5">
        <div class="row menu-content">
            <?php echo $this->element('user_sidebar'); ?>
            <div class="col-md-10 menu-content-right match-height">
                <h3 class="text-success-2 mb-5 mt-5">Add Cash</h3>
                <div class="panel panel-success mb-5">
                    <div class="panel-heading bg-success">
                        <h4>Cashier</h4>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="#" class="validation_form">
                            <div class="jumbotron text-center clearfix">
                                <div class="col-md-6 col-md-offset-3">
                                    <label class="bg-danger btn">Enter Amount</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" max="20000" id="basic-addon1"><i class="la la-inr la-2x"></i></span>
                                        <input type="text" class="form-control validate[required]" name="data[Addcash][amount]" id="amount" placeholder="Enter Amount" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row amount-btns mb-5">
                                        <?php
                                        $sitesetting = ClassRegistry::init('Sitesetting')->find('first');
                                        $amounts = explode(',', $sitesetting['Sitesetting']['addcash_amounts']);
                                        foreach ($amounts as $amount) {
                                            ?>
                                            <div class="col-md-3"><a href="javascript:;" data-value="<?php echo $amount; ?>" data-bonus-id="1"><i class="la la-inr"></i> <?php echo $amount; ?></a></div>
                                        <?php } ?>
                                    </div>
                                    <!--                                    <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="discount-cash" id="1" data-from="500" data-to="1000" data-value="10">
                                                                                    <h4>10% CASH</h4>
                                                                                    <p><i class="la la-inr"></i> 500 - <i class="la la-inr"></i> 1000</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="discount-cash" id="2" data-from="5000" data-to="14999" data-value="15">
                                                                                    <h4>15% CASH</h4>
                                                                                    <p><i class="la la-inr"></i> 5000 - <i class="la la-inr"></i> 14999</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="discount-cash" id="3" data-from="15000" data-to="19999" data-value="20">
                                                                                    <h4>20% CASH</h4>
                                                                                    <p><i class="la la-inr"></i> 15000 - <i class="la la-inr"></i> 19999</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="discount-cash" id="4" data-from="20000" data-to="20000" data-value="25">
                                                                                    <h4>25% CASH</h4>
                                                                                    <p><i class="la la-inr"></i> 20000</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>-->
                                    <button class="btn bg-success btn-block btn-lg" type="submit">Deposite</button>
                                </div>
                                <div class="col-md-4">
                                    <div class="promo-code-inner bg-danger p-4">
                                        <div class="have-promocode mb-4">
                                            <p>Have Promocode ?</p>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="" name="data[Addcash][promocode]" id="promo-code">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default bg-danger" type="button" id="apply">APPLY</button>
                                                </span>
                                            </div><!-- /input-group -->
                                        </div>
                                        <p>Deposit</p>
                                        <h3><i class="la la-inr"></i> <span id="deposite">200</span></h3>
                                        <hr/>
                                        <p>Pending Bonus</p>
                                        <h3><i class="la la-inr"></i> <span id="pending-bonus">0</span></h3>
                                        <hr/>
                                        <p>Cash Bonus</p>
                                        <h3><i class="la la-inr"></i> <span id="cash-bonus">0</span></h3>
                                        <input type="hidden" name="data[Addcash][deposit]" id="deposite-input"/>
                                        <input type="hidden" name="data[Addcash][pending_bonus]" id="pending-bonus-input"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
//    $('.amount-btns a').click(function () {
//        $('.amount-btns a').removeClass('active');
//        $(this).addClass('active');
//        
//        var bonus_id = $(this).attr('data-bonus-id');
//        var deposite = $(this).attr('data-value');
//        if (bonus_id) {
//            $('.discount-cash').removeClass('active');
//            $('#' + bonus_id).addClass('active');
//            var per = $('#' + bonus_id).attr('data-value');
//            var amount = (parseInt(per) / 100) * parseInt(deposite);
//            var pending_bonus = amount;
//        } else {
//            var pending_bonus = 0;
//            $('.discount-cash').removeClass('active');
//        }
//        var cash_bonus = parseInt(pending_bonus) + parseInt(deposite);
//        $('#cash-bonus').text(cash_bonus);
//        $('#pending-bonus').text(pending_bonus);
//        $('#deposite').text(deposite);
//        $('#amount').val(deposite);
//
//        $('#pending-bonus-input').val(pending_bonus);
//        $('#deposite-input').val(deposite);
//    });

    $('.amount-btns a').click(function () {
        $('.amount-btns a').removeClass('active');
        $('.discount-cash').removeClass('active');
        $(this).addClass('active');
        var deposite = $(this).attr('data-value');
        depositeFn(deposite);
    });


    $(document).on('change', '#amount', function () {
        $('.amount-btns a').removeClass('active');
        $('.discount-cash').removeClass('active');
        var deposite = $(this).val();
        $('.amount-btns a').each(function () {
            if ($(this).attr('data-value') == deposite) {
                $(this).addClass('active');
            }
        });


        depositeFn(deposite);
    });

    function depositeFn(deposite) {
        var pending_bonus = 0;

//        $('.discount-cash').each(function () {
//            var from = parseInt($(this).attr('data-from'));
//            var to = parseInt($(this).attr('data-to'));
//
//            if (deposite >= from && deposite <= to) {
//                var bonus_id = $(this).attr('id');
//                $('.discount-cash').removeClass('active');
//                $('#' + bonus_id).addClass('active');
//                var per = $('#' + bonus_id).attr('data-value');
//                var amount = (parseInt(per) / 100) * parseInt(deposite);
//                pending_bonus = amount;
//                return false;
//            }
//        });


        var cash_bonus = parseInt(pending_bonus) + parseInt(deposite);
        $('#cash-bonus').text(cash_bonus);
        $('#pending-bonus').text(pending_bonus);
        $('#deposite').text(deposite);
        $('#amount').val(deposite);

        $('#pending-bonus-input').val(pending_bonus);
        $('#deposite-input').val(deposite);

        $('#apply').text('Apply').prop('disabled', false);
        $('#promo-code').prop('readonly',false);
        $('#promo-code').val('');
    }

    $(document).on('click', '#apply', function () {
        if ($('#promo-code').val() != '') {
            jQuery.ajax({
                url: "<?php echo BASE_URL; ?>users/applypromocode",
                data: {promocode: $('#promo-code').val(), 'amount': $('#amount').val()},
                dataType: "json",
                success: function (data) {
                    if (data.code == '0') {
                        swal({
                            title: "Success",
                            text: "Promocode Applied Successfully!",
                            type: "success",
                            closeOnConfirm: true,
                        });
                        var pending_bonus = data.amount;
                        var deposite = $('#amount').val();
                        var cash_bonus = parseInt(pending_bonus) + parseInt(deposite);
                        $('#cash-bonus').text(cash_bonus);
                        $('#pending-bonus').text(pending_bonus);
                        $('#deposite').text(deposite);
                        $('#amount').val(deposite);
                        $('#pending-bonus-input').val(pending_bonus);
                        $('#deposite-input').val(deposite);

                        $('#apply').text('Discount Applied').prop('disabled', true);
                        $('#promo-code').attr('readonly');
                    } else {
                        swal({
                            title: "Error",
                            text: data.message,
                            type: "error",
                            closeOnConfirm: true,
                        });
                    }
                }
            });
        }
    });
</script>
