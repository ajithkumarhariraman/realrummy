<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow mb-5">
        <h2 class="text-success">Invite by Free SMS & Email</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="article max-h">
                    <table class="table table-bordered table-striped">
                        <thead class="bg-success">
                            <tr>
                                <td><input type="checkbox" id="checkall"/></td>
                                <td>Name</td>
                                <td>Email</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($emails as $emails) { ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="email[]" value="<?php echo $emails['email']; ?>" id="invitecheck"/>
                                        <input type="hidden" name="name[]" value="<?php echo $emails['name']; ?>" id="invitecheck"/>
                                    </td>
                                    <td><?php echo $emails['name']; ?></td>
                                    <td><?php echo $emails['email']; ?></td>
                                    <td><a href="javascript:;" class="bg-success btn invite" data-name="<?php echo $emails['name']; ?>" data-email="<?php echo $emails['email']; ?>">INVITE</a></td>
                                </tr>
                            <?php } ?>
                        </tbody> 
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).on('click', '.invite', function () {
        var thiss = $(this);
        var email = $(this).attr('data-email');
        var name = $(this).attr('data-name');
        jQuery.ajax({
            type: "POST",
            url: "<?php echo BASE_URL; ?>users/app_sendinvitation/?user_id=<?php echo $this->Session->read('app_user_id'); ?>",
            data: 'email[]=' + email + '&name[]=' + name + '&ajax=1',
            dataType: 'json',
            beforeSend: function () {
                $("#loading").show();
            },
            complete: function () {
                $("#loading").hide();
            },
            success: function (data) {
                if (data.code == '200') {
                    thiss.text('INVITED').addClass('disabled');
                    swal({
                        title: "Success!",
                        text: data.message,
                        type: "success"
                    });
                } else {
                    swal({
                        title: "Error!",
                        text: data.message,
                        type: "error"
                    });
                }
            }
        });

    });
</script>