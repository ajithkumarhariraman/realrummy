<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>  
    </div>
</div>
<div class="container">
    <div class="clearfix menu-content">
        <div class="col-xs-3">
            <?php echo $this->element('app_user_sidebar'); ?>
        </div>
        <div class="col-xs-9 menu-content-right alt-menu-right match-height">
            <div class="mt-0 mb-5 text-center p-5">
                <h2 class="text-center text-success">Bring a Friend</h2>
                <img src="<?php echo BASE_URL; ?>img/mail1.png" class=" mb-3"/>
                <p class="h3 text-warning">Send invites to your friends</p>
                <p class="h3 ">Earn Upto <span class="text-success-3"><i class="fa fa-inr"></i> 1000</span> in bonus for each friends that joins</p>
                <div class="clearfix mt-5">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="invite-box">
                            <select class="form-control" id="invite-from">
                                <option value="gmail">Gmail</option>
                                <!--                        <option value="yahoo">Yahoo</option>-->
                            </select>
                            <a class="bg-success mt-4 btn-lg btn-block" id="invite" href="<?php echo BASE_URL; ?>users/app_google?user_id=<?php echo $_REQUEST['user_id'] ?>">INVITE FRIENDS</a>
                            <br/>
                            <a class="text-success" href="<?php echo BASE_URL; ?>users/app_invitationstatus?user_id=<?php echo $_REQUEST['user_id'] ?>">Invitation Status</a>
                        </div>
                    </div>
                </div>
                <br/>
                <p>( OR )</p>
                <br/>
                <ul class="list-inline invite-social">
                    <?php
                    $url = BASE_URL . '?refid=' . $sessionuser['User']['emailencode'];
                    ?>
                    <li>
                        <a target="_blank" href="<?php echo $this->App->share_link($url, 'fb'); ?>"><img src="<?php echo BASE_URL; ?>img/facebook1.png"/></a>
                        <a target="_blank" href="<?php echo $this->App->share_link($url, 'tw'); ?>"><img src="<?php echo BASE_URL; ?>img/twitter1.png"/></a>
                        <a target="_blank" href="<?php echo $this->App->share_link($url, 'pr'); ?>"><img src="<?php echo BASE_URL; ?>img/pin1.png"/></a>
                    </li>
                </ul>
                <a href="javascript:;" data-toggle="modal" class="pull-right" data-target="#friend-refer-terms">* Terms and Conditions</a>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="friend-refer-terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
                </div>
                <div class="modal-body">
                    <div class="max-h">
                        <?php
                        $terms = ClassRegistry::init('Staticpage')->find('first', array('conditions' => array('slug' => 'friend-refer-terms')));
                        echo $terms['Staticpage']['page_content'];
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('change', '#invite-from', function () {
        if ($(this).val() == 'gmail') {
            $('#invite').attr('href', '<?php echo BASE_URL; ?>users/app_google');
        } else {
            $('#invite').attr('href', '<?php echo BASE_URL; ?>users/app_yahoo');
        }
    });
</script>