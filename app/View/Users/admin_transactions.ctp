<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Withdrawal Requests</span>  - List
            </h4>
        </div>
    </div>
</div>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form method="get" class="form-inline form-group">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Transaction ID" name="s" value="<?php echo (!empty($_REQUEST['s'])) ? $_REQUEST['s'] : "" ?>"/>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control ui_datepicker" placeholder="DATE" name="date" value="<?php echo (!empty($_REQUEST['date'])) ? $_REQUEST['date'] : "" ?>"/>
                </div>
                <div class="form-group">
                    <button name="search" type="submit" class="btn btn-success btn-sm">Search</button>
                </div>
                <?php if (isset($_REQUEST['search'])) { ?>
                    <div class="form-group">
                        <a href="<?php echo BASE_URL; ?>admin/users/withdrawals" class="btn btn-danger btn-sm">Cancel</a>
                    </div>
                <?php } ?>
            </form>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Transaction ID</th>
                            <th>Transaction Type</th>
                            <th>Amount</th>
                            <th>Balance</th>
                            <th>Date & Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = $this->Paginator->counter('{:start}');
                        if (!empty($transactions)) {
                            foreach ($transactions as $transaction) {
                                $user = ClassRegistry::init('User')->find('first', array('conditions' => array('user_id' => $withdrawal['Withdrawal']['user_id'])));
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo date('d.m.Y', strtotime($withdrawal['Withdrawal']['date'])); ?></td>
                                    <td><?php echo $transaction['Transaction']['transaction_id']; ?></td>
                                    <td><?php echo $transaction['Transaction']['transaction_type']; ?></td>
                                    <td><i class="la la-inr"></i> <?php echo $transaction['Transaction']['amount']; ?></td>
                                    <td><i class="la la-inr"></i> <?php echo $transaction['Transaction']['balance']; ?></td>
                                    <td><?php echo date('d.m.Y h.i A', strtotime($transaction['Transaction']['datetime'])); ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="5" class="text-center">No Result Found</td>
                            </tr>
                        <?php }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                        ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                        <ul class="pagination">
                            <?php
                            echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                            $numbers = $this->Paginator->numbers();
                            if (empty($numbers)) {
                                echo '<li class="active page-link"><a>1</a></li>';
                            } else {
                                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                            }
                            echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                            ?>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.ui_datepicker').datepicker({format: 'dd-mm-yyyy'});
</script>