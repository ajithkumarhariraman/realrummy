<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow-2 mt-0 mb-5">
        <div class="row menu-content">
            <?php echo $this->element('user_sidebar'); ?>
            <div class="col-md-10 menu-content-right match-height">
                <form method="post" action="#">
                    <div class="pull-right del-all mb-5">
                        <label><input id="select-all" type="checkbox" class="checkbox"/> Select All</label>
                        <button class="btn btn-trans"><i class="la la-trash-o text-danger"></i></button>
                    </div>
                    <h3 class="text-success-2 mb-5 mt-5">Inbox</h3>
                    <!-- Nav tabs -->
                    <div class="mail-content mb-5">
                        <?php
                        if (!empty($letters)) {
                            foreach ($letters as $letter) {
                                ?>
                                <div class="mail-li">
                                    <input type="checkbox" class="checkbox check-letter" name="data[Letter][letter_id][]" value="<?php echo $letter['Letter']['letter_id']; ?>"/> 
                                    <a href="javascript:;" class="view-mail" data-id="<?php echo $letter['Letter']['letter_id']; ?>"><p class="m-0"><?php echo $letter['Letter']['subject']; ?></p></a>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="no-data-found text-center">
                                <img src="<?php echo BASE_URL; ?>img/no-data.png"/>
                                <p>No Data Found</p>
                            </div>
                        <?php }
                        ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Mail</h4>
            </div>
            <div class="modal-body">
                <h4 id="subject"></h4>
                <div id="message"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '#select-all', function () {
        $('.check-letter').prop('checked', true);
    });
    $(document).on('click', '.view-mail', function () {
        var id = $(this).attr('data-id');
        jQuery.ajax({
            url: "<?php echo BASE_URL; ?>users/viewmail",
            data: {id: id},
            dataType: "json",
            success: function (data) {
                if (data.code == '0') {
                    $('#subject').text(data.subject);
                    $('#message').html(data.message);
                    $('#myModal').modal('show');
                } else {
                    swal({
                        title: "Error",
                        text: data.message,
                        type: "error",
                        closeOnConfirm: true,
                    });
                }
            }
        });
    });
</script>