<div class="container">
    <div class="bx-shadow-2 mb-5">
        <div class="row menu-content">
            <?php echo $this->element('user_sidebar'); ?>
            <div class="col-md-10 menu-content-right match-height">
                <h3 class="text-success-2 mb-5 mt-5">Profile</h3>
                <!-- Nav tabs -->
                <?php echo $this->element('user_tab'); ?>
                <div class="panel panel-success mb-5">
                    <div class="panel-heading bg-success">
                        <h4>Verify Email</h4>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="#" class="validation_form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" readonly="" value="<?php echo $sessionuser['User']['email']; ?>" class="form-control validate[required]" name="data[User][pin]"/>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn bg-danger btn-md">RESEND ACTIVATION LINK</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>