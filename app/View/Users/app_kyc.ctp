<div class="user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="mt-0 mb-5">
        <div class="clearfix menu-content">
            <div class="col-xs-3 match-height">
                <?php echo $this->element('app_user_sidebar'); ?>
            </div>
            <div class="col-xs-9 alt-menu-right menu-content-right match-height">
                <!-- Nav tabs -->
                <?php echo $this->element('app_user_tab'); ?>
                <div class="bx-shadow">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="cardbg">
                                <div class="media mb-5">
                                    <div class="media-body">
                                        <h5>XXXXXXXXXXXXXXXX</h5>
                                        <h6>XXXXXXXXX</h6>
                                    </div>
                                    <div class="media-right">
                                        <img class="media-object img-circle profile-img" src="<?php echo BASE_URL; ?>img/profile.png">
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-body">
                                        <h5><?php echo $user['User']['email']; ?></h5>
                                        <h6><?php echo $user['User']['phone_number']; ?></h6>
                                    </div>
                                    <div class="media-right">
                                        <img class="media-object" src="<?php echo BASE_URL; ?>files/users/<?php echo $user['User']['profile']; ?>" onerror="src='<?php echo BASE_URL; ?>img/icon.png'" class="img-sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 profile-info">
                            <!--                            <dl class="dl-horizontal mb-0">
                                                            <dt class="text-left text-success-2">Account Name : </dt>
                                                            <dd><?php echo $user['User']['email']; ?></dd>
                                                            <dt class="text-left text-success-2">Account ID : </dt>
                                                            <dd>Mohan1233a</dd>
                                                        </dl>-->
                            <div>
                                <div class="media">
                                    <div class="media-left text-success-2">
                                        <img src="<?php echo BASE_URL; ?>img/email.png" class="icon mr-3"/>
                                        Email :
                                    </div>
                                    <div class="media-body">
                                        <?php echo $user['User']['email']; ?>
                                    </div>
                                    <div class="media-right">
                                        <?php if ($user['User']['emailverified'] == 1) { ?>
                                            <a href="verifymobile"><label class="label bg-success"><i class="la la-check-circle"></i> Verified</label></a>
                                            <a href="<?php echo BASE_URL; ?>users/app_changeemail?user_id=<?php echo $_REQUEST['user_id'] ?>" class="edit-info"><i class="la la-pencil"></i> Edit</a>
                                        <?php } else { ?>
                                            <a href="<?php echo BASE_URL; ?>users/app_verifyemail?user_id=<?php echo $_REQUEST['user_id'] ?>"><label class="label bg-danger"><i class="la la-times-circle"></i> Not Verified</label></a>
                                            <a href="<?php echo BASE_URL; ?>users/app_changeemail?user_id=<?php echo $_REQUEST['user_id'] ?>" class="edit-info"><i class="la la-pencil"></i> Edit</a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left text-success-2">
                                        <img src="<?php echo BASE_URL; ?>img/mob.png" class="icon mr-3"/>
                                        Mobile :
                                    </div>
                                    <div class="media-body">
                                        <?php echo $user['User']['phone_number']; ?>
                                    </div>
                                    <div class="media-right">
                                        <?php if ($user['User']['phoneverified'] == 1) { ?>
                                            <label class="label bg-success"><i class="la la-check-circle"></i> Verified</label>
                                            <a href="<?php echo BASE_URL; ?>users/app_changemobile?user_id=<?php echo $_REQUEST['user_id'] ?>" class="edit-info"><i class="la la-pencil"></i> Edit</a>
                                        <?php } else { ?>
                                            <a href="<?php echo BASE_URL; ?>users/app_verifymobile?user_id=<?php echo $_REQUEST['user_id'] ?>"><label class="label bg-danger"><i class="la la-times-circle"></i> Not Verified</label></a>
                                            <a href="<?php echo BASE_URL; ?>users/app_changemobile?user_id=<?php echo $_REQUEST['user_id'] ?>" class="edit-info"><i class="la la-pencil"></i> Edit</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-12">
                        <div class="panel panel-success">
                            <div class="panel-heading bg-success">
                                <h4>Documents</h4>
                            </div>
                            <div class="panel-body">
                                <div id="err" class="alert alert-danger mb-4" style="display:none;"></div>
                                <form id="form" method="post" action="#" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="upload-kyc match-height1" for="pan">
                                                <span class="bg-danger">PAN Card</span>
                                                <?php if (empty($pan)) { ?>
                                                    <div class="h4">
                                                        <i class="la la-cloud-upload"></i>
                                                        <p>Click To Upload</p>
                                                        <p class="small">Allowed Type jpeg,png,jpg</p>
                                                    </div>
                                                <?php } else {
                                                    ?>
                                                    <div class="kyc-img">
                                                        <img src="<?php echo BASE_URL; ?>files/users/<?php echo $pan['Kycdocument']['document_path']; ?>" class="img-responsive"/>
                                                    </div>
                                                    <hr/>
                                                    <div class="text-left">
                                                        <p>Status: <span class="<?php echo ($pan['Kycdocument']['status'] == 'Approved') ? "text-success" : "text-danger"; ?>"><?php echo $pan['Kycdocument']['status']; ?></span></p>
                                                        <label for="pan">Edit</label>
                                                    </div>
                                                <?php }
                                                ?>
                                                <input type="file" name='data[User][pancard]' class="form-control hidden" id="pan"/>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="upload-kyc match-height1" for="addressproof_front">
                                                <span class="bg-danger">Address Proof Front</span>
                                                <?php if (empty($front)) { ?>
                                                    <div class="h4">
                                                        <i class="la la-cloud-upload"></i>
                                                        <p>Click To Upload</p>
                                                        <p class="small">Allowed Type jpeg,png,jpg</p>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="kyc-img">
                                                        <img src="<?php echo BASE_URL; ?>files/users/<?php echo $front['Kycdocument']['document_path']; ?>" class="img-responsive"/>
                                                    </div>
                                                    <hr/>
                                                    <div class="text-left">
                                                        <p>Status: <span class="<?php echo ($front['Kycdocument']['status'] == 'Approved') ? "text-success" : "text-danger"; ?>"><?php echo $front['Kycdocument']['status']; ?></span></p>
                                                        <label for="pan">Edit</label>
                                                    </div>
                                                <?php } ?>
                                                <input type="file" name='data[User][addressproof_front]' class="form-control hidden" id="addressproof_front"/>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="upload-kyc" for="addressproof_back">
                                                <span class="bg-danger">Address Proof Back</span>
                                                <?php if (empty($back)) { ?>
                                                    <div class="h4">
                                                        <i class="la la-cloud-upload"></i>
                                                        <p>Click To Upload</p>
                                                        <p class="small">Allowed Type jpeg,png,jpg</p>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="kyc-img">
                                                        <img src="<?php echo BASE_URL; ?>files/users/<?php echo $back['Kycdocument']['document_path']; ?>" class="img-responsive"/>
                                                    </div>
                                                    <hr/>
                                                    <div class="text-left">
                                                        <p>Status: <span class="<?php echo ($back['Kycdocument']['status'] == 'Approved') ? "text-success" : "text-danger"; ?>"><?php echo $back['Kycdocument']['status']; ?></span></p>
                                                        <label for="pan">Edit</label>
                                                    </div>
                                                <?php } ?>
                                                <input type="file" name='data[User][addressproof_back]' class="form-control hidden" id="addressproof_back"/>
                                            </label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('change', '#state', function () {
        var state = $(this).val();
        $.ajax({
            url: "<?php echo BASE_URL; ?>app/getCities",
            data: {state: state},
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#city').html(data);
            }
        });
    });

    $(document).on('change', '#pan', function (e) {
        e.preventDefault();
        $.ajax({
            url: "<?php echo BASE_URL; ?>users/app_kycUpload/pan_card?user_id=<?php echo $_REQUEST['user_id'] ?>",
                        type: "POST",
                        data: new FormData($('#form')[0]),
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function ()
                        {
                            $("#loading").show();
                            $("#err").fadeOut();
                        },
                        complete: function () {
                            $("#loading").hide();
                        },
                        success: function (data)
                        {
                            if (data == 'success')
                            {
                                swal({
                                    title: "Success",
                                    text: "Successfully Uploaded",
                                    type: "warning",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-danger",
                                    closeOnConfirm: false
                                },
                                        function () {
                                            window.location.href = window.location.href;
                                        });
                                // invalid file format.

                            } else
                            {
                                $('#err').text(data).fadeIn();
                                $('#form')[0].reset();
                            }
                        },
                        error: function (e)
                        {
                            $("#err").html(e).fadeIn();
                            $('#form')[0].reset();
                        }
                    });
                });

                $(document).on('change', '#addressproof_front', function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: "<?php echo BASE_URL; ?>users/app_kycUpload/addressproof_front?user_id=<?php echo $_REQUEST['user_id'] ?>",
                                    type: "POST",
                                    data: new FormData($('#form')[0]),
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    beforeSend: function ()
                                    {
                                        $("#loading").show();
                                        $("#err").fadeOut();
                                    },
                                    complete: function () {
                                        $("#loading").hide();
                                    },
                                    success: function (data)
                                    {
                                        if (data == 'success')
                                        {
                                            swal({
                                                title: "Success",
                                                text: "Successfully Uploaded",
                                                type: "warning",
                                                showCancelButton: false,
                                                confirmButtonClass: "btn-danger",
                                                closeOnConfirm: false
                                            },
                                                    function () {
                                                        window.location.href = window.location.href;
                                                    });
                                            // invalid file format.

                                        } else
                                        {
                                            $('#err').text(data).fadeIn();
                                            $('#form')[0].reset();
                                        }
                                    },
                                    error: function (e)
                                    {
                                        $("#err").html(e).fadeIn();
                                        $('#form')[0].reset();
                                    }
                                });
                            });

                            $(document).on('change', '#addressproof_back', function (e) {
                                e.preventDefault();
                                $.ajax({
                                    url: "<?php echo BASE_URL; ?>users/app_kycUpload/addressproof_back?user_id=<?php echo $_REQUEST['user_id'] ?>",
                                                type: "POST",
                                                data: new FormData($('#form')[0]),
                                                contentType: false,
                                                cache: false,
                                                processData: false,
                                                beforeSend: function ()
                                                {
                                                    $("#loading").show();
                                                    $("#err").fadeOut();
                                                },
                                                complete: function () {
                                                    $("#loading").hide();
                                                },
                                                success: function (data)
                                                {
                                                    if (data == 'success')
                                                    {
                                                        swal({
                                                            title: "Success",
                                                            text: "Successfully Uploaded",
                                                            type: "warning",
                                                            showCancelButton: false,
                                                            confirmButtonClass: "btn-danger",
                                                            closeOnConfirm: false
                                                        },
                                                                function () {
                                                                    window.location.href = window.location.href;
                                                                });
                                                        // invalid file format.

                                                    } else
                                                    {
                                                        $('#err').text(data).fadeIn();
                                                        $('#form')[0].reset();
                                                    }
                                                },
                                                error: function (e)
                                                {
                                                    $("#err").html(e).fadeIn();
                                                    $('#form')[0].reset();
                                                }
                                            });
                                        });
                                        $('.match-height1').matchHeight();
</script>