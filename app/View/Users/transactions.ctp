<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow-2 mt-0 mb-5">
        <div class="row menu-content">
            <?php echo $this->element('user_sidebar'); ?>
            <div class="col-md-10 menu-content-right match-height">
                <h3 class="text-success-2 mb-5 mt-5">Transactions</h3>
                <!-- Nav tabs -->
                <table class="table table-striped">
                    <thead class="bg-success-1">
                        <tr>
                            <th>Transaction ID</th>
                            <th>Transaction Type</th>
                            <th>Amount</th>
                            <th>Balance</th>
                            <th>Date & Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($transactions)) {
                            foreach ($transactions as $transaction) {
                                ?>
                                <tr>
                                    <td><?php echo $transaction['Transaction']['transaction_id']; ?></td>
                                    <td><?php echo $transaction['Transaction']['transaction_type']; ?></td>
                                    <td><i class="la la-inr"></i> <?php echo $transaction['Transaction']['amount']; ?></td>
                                    <td><i class="la la-inr"></i> <?php echo $transaction['Transaction']['balance']; ?></td>
                                    <td><?php echo date('d.m.Y h.i A', strtotime($transaction['Transaction']['datetime'])); ?></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="5" class="text-center">No Result Found</td>
                            </tr>
                        <?php }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>