<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="clearfix menu-content">
        <div class="col-xs-3 match-height">
            <?php echo $this->element('app_user_sidebar'); ?>
        </div>
        <div class="col-xs-9 menu-content-right alt-menu-right  match-height">
            <!-- Nav tabs -->
            <?php echo $this->element('app_user_tab'); ?>
            <div class="bx-shadow">
                <div class="row">
                    <div class="col-md-5">
                        <div class="cardbg">
                            <div class="media mb-5">
                                <div class="media-body">
                                    <h5>XXXXXXXXXXXXXXXX</h5>
                                    <h6>XXXXXXXXX</h6>
                                </div>
                                <div class="media-right">
                                    <img class="media-object img-circle profile-img" src="<?php echo BASE_URL; ?>files/users/<?php echo $user['User']['profile']; ?>" onerror="src='<?php echo BASE_URL; ?>img/profile.png'">
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-body">
                                    <h5><?php echo $user['User']['email']; ?></h5>
                                    <h6><?php echo $user['User']['phone_number']; ?></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 profile-info">
                        <!--                            <dl class="dl-horizontal mb-0">
                                                        <dt class="text-left text-success-2">Account Name : </dt>
                                                        <dd><?php echo $user['User']['email']; ?></dd>
                                                        <dt class="text-left text-success-2">Account ID : </dt>
                                                        <dd>Mohan1233a</dd>
                                                    </dl>-->
                        <div>
                            <div class="media">
                                <div class="media-left text-success-2">
                                    <img src="<?php echo BASE_URL; ?>img/email.png" class="icon mr-3"/>
                                    Email :
                                </div>
                                <div class="media-body">
                                    <?php echo $user['User']['email']; ?>
                                </div>
                                <div class="media-right">
                                    <?php if ($user['User']['emailverified'] == 1) { ?>
                                        <a href="verifymobile"><label class="label bg-success"><i class="la la-check-circle"></i> Verified</label></a>
                                        <a href="<?php echo BASE_URL; ?>users/app_changeemail?user_id=<?php echo $_REQUEST['user_id'] ?>" class="edit-info"><i class="la la-pencil"></i> Edit</a>
                                    <?php } else { ?>
                                        <a href="<?php echo BASE_URL; ?>users/app_verifyemail?user_id=<?php echo $_REQUEST['user_id'] ?>"><label class="label bg-danger"><i class="la la-times-circle"></i> Not Verified</label></a>
                                        <a href="<?php echo BASE_URL; ?>users/app_changeemail?user_id=<?php echo $_REQUEST['user_id'] ?>" class="edit-info"><i class="la la-pencil"></i> Edit</a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left text-success-2">
                                    <img src="<?php echo BASE_URL; ?>img/mob.png" class="icon mr-3"/>
                                    Mobile :
                                </div>
                                <div class="media-body">
                                    <?php echo $user['User']['phone_number']; ?>
                                </div>
                                <div class="media-right">
                                    <?php if ($user['User']['phoneverified'] == 1) { ?>
                                        <label class="label bg-success"><i class="la la-check-circle"></i> Verified</label>
                                        <a href="<?php echo BASE_URL; ?>users/app_changemobile?user_id=<?php echo $_REQUEST['user_id'] ?>" class="edit-info"><i class="la la-pencil"></i> Edit</a>
                                    <?php } else { ?>
                                        <a href="<?php echo BASE_URL; ?>users/app_verifymobile?user_id=<?php echo $_REQUEST['user_id'] ?>"><label class="label bg-danger"><i class="la la-times-circle"></i> Not Verified</label></a>
                                        <a href="<?php echo BASE_URL; ?>users/app_changemobile?user_id=<?php echo $_REQUEST['user_id'] ?>" class="edit-info"><i class="la la-pencil"></i> Edit</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading bg-success">
                            <a class="btn bg-danger pull-right edit" href="javascript:;"><i class="la la-edit"></i> EDIT</a>
                            <h4>Personal Information</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-borderless">
                                <tr>
                                    <td>Name</td>
                                    <td>:</td>
                                    <td><?php echo $user['User']['name']; ?></td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td>:</td>
                                    <td><?php echo (!empty($user['User']['dob'])) ? $user['User']['dob'] : "Not Specified"; ?></td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>:</td>
                                    <td><?php echo (!empty($user['User']['dob'])) ? $user['User']['gender'] : "Not Specified"; ?></td>
                                </tr>
                            </table>
                            <form method="post" action="#" class="validation_form" style="display:none;" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" value="<?php echo $user['User']['name']; ?>" class="form-control validate[required]" name="data[User][name]"/>
                                </div>
                                <div class="form-group">
                                    <label>DOB</label>
                                    <input type="text" value="<?php echo $user['User']['dob']; ?>" class="form-control validate[required]" name="data[User][dob]"/>
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <div class="radio">
                                        <label class="mr-3">
                                            <input <?php echo ($user['User']['gender'] == 'Male') ? "checked" : "" ?> value="Male" name="data[User][gender]" type="radio" name="radio-styled-color" class="control-success">
                                            Male
                                        </label>
                                        <label>
                                            <input <?php echo ($user['User']['gender'] == 'Female') ? "checked" : "" ?> value="Female" name="data[User][gender]" type="radio" name="radio-styled-color" class="control-success">
                                            Female
                                        </label>
                                    </div>                                        
                                </div>
                                <div class="form-group"> 
                                    <label>Profile Picture</label>
                                    <input type="file" class="form-control validate[custom[image]]" name="data[User][profile]"/>
                                </div>
                                <div class="form-group">
                                    <button class="btn bg-success" type="submit">Submit</button>
                                    <button class="btn bg-danger cancel-btn" type="button">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading bg-success">
                            <a class="btn bg-danger pull-right edit" href="javascript:;"><i class="la la-edit"></i> EDIT</a>
                            <h4>Address Information</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-borderless">
                                <tr>
                                    <td>Address</td>
                                    <td>:</td>
                                    <td><?php echo (!empty($user['User']['address'])) ? $user['User']['address'] : "Not Specified"; ?></td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td>:</td>
                                    <td><?php echo (!empty($user['User']['city'])) ? $user['User']['city'] : "Not Specified"; ?></td>
                                </tr>
                                <tr>
                                    <td>State</td>
                                    <td>:</td>
                                    <td><?php echo (!empty($user['User']['state'])) ? $user['User']['state'] : "Not Specified"; ?></td>
                                </tr>
                                <tr>
                                    <td>Country</td>
                                    <td>:</td>
                                    <td><?php echo (!empty($user['User']['country'])) ? $user['User']['country'] : "Not Specified"; ?></td>
                                </tr>
                                <tr>
                                    <td>Pincode</td>
                                    <td>:</td>
                                    <td><?php echo (!empty($user['User']['pincode'])) ? $user['User']['pincode'] : "Not Specified"; ?></td>
                                </tr>
                            </table>
                            <form method="post" action="#" class="validation_form" style="display:none;">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control validate[required]" name="data[User][address]"><?php echo $user['User']['address']; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Country</label>
                                    <select class="form-control validate[required]" id="country"  name="data[User][country]">
                                        <option value="">Country</option>
                                        <?php
                                        $countries = ClassRegistry::init('Country')->find('all');
                                        foreach ($countries as $country) {
                                            ?>
                                            <option <?php echo ($user['User']['country'] == $country['Country']['name']) ? "selected" : ""; ?> value="<?php echo $country['Country']['name']; ?>"><?php echo $country['Country']['name']; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>State</label>
                                            <select class="form-control validate[required]" id="state" name="data[User][state]">
                                                <option value="">Sate</option>
                                                <?php
                                                $states = ClassRegistry::init('State')->find('all');
                                                foreach ($states as $state) {
                                                    ?>
                                                    <option <?php echo ($user['User']['state'] == $state['State']['name']) ? "selected" : ""; ?> value="<?php echo $state['State']['name']; ?>"><?php echo $state['State']['name']; ?></option>
                                                <?php }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>City</label>
                                            <select id="city" class="form-control validate[required]" name="data[User][city]">
                                                <option value="">City</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Pincode</label>
                                    <input type="text" class="form-control" name="data[User][pincode]" value="<?php echo $user['User']['pincode']; ?>"/>
                                </div>

                                <div class="form-group">
                                    <button class="btn bg-success" type="submit">Submit</button>
                                    <button class="btn bg-danger cancel-btn" type="button">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".edit").on("click", function (e) {
        e.preventDefault();
        $(this).parents('.panel').find('table').hide();
        $(this).parents('.panel').find('form').show();
        $(".edit").hide();
        $('.match-height').matchHeight();
    });

    $(".cancel-btn").on("click", function (e) {
        e.preventDefault();
        $(this).parents('.panel').find('form').hide();
        $(this).parents('.panel').find('table').show();
        $(".edit").show();
        $('.match-height').matchHeight();
    });

    $(document).ready(function () {
        $('#state').trigger('change');
    });

    $(document).on('change', '#state', function () {
        var state = $(this).val();
        $.ajax({
            url: "<?php echo BASE_URL; ?>app/getCities",
            data: {state: state},
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#city').html(data);
            }
        });
    });
    $(document).on('change', '#country', function () {
        var country = $(this).val();
        $.ajax({
            url: "<?php echo BASE_URL; ?>app/getStates",
            data: {country: country},
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#state').html(data);
            }
        });
    });
</script>