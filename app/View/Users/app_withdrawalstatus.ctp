<div class="user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="clearfix menu-content">
        <div class="col-xs-3 match-height">
            <?php echo $this->element('app_user_sidebar'); ?>
        </div>
        <div class="col-xs-9 menu-content-right alt-menu-right match-height">
            <div class="mt-5 mb-5">
                <ul class="nav nav-tabs mb-4" role="tablist">
                    <li role="presentation"><a  href="<?php echo BASE_URL; ?>users/app_withdrawals?user_id=<?php echo $_REQUEST['user_id']; ?>" aria-controls="settings" >Make Withdrawal</a></li>
                    <li role="presentation" class="active"><a class="bg-success" href="<?php echo BASE_URL; ?>users/app_withdrawalstatus?user_id=<?php echo $_REQUEST['user_id']; ?>" aria-controls="settings" >Withdrawal Status</a></li>
                </ul>
                <div class="panel panel-success mb-5">
                    <div class="panel-heading bg-success">
                        <h4>Withdraw Status</h4>
                    </div>
                    <div class="panel-body p-2">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="bg-success-1">
                                    <tr>
                                        <th>Request Date</th>
                                        <th>Amount</th>
                                        <th>Withdrawal ID</th>
                                        <th>Current Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($withdrawals)) {
                                        foreach ($withdrawals as $withdrawal) {
                                            ?>
                                            <tr>
                                                <td><?php echo date('d.m.Y', strtotime($withdrawal['Withdrawal']['date'])); ?></td>
                                                <td><i class="la la-inr"></i> <?php echo number_format($withdrawal['Withdrawal']['amount'],2); ?></td>
                                                <td><?php echo $withdrawal['Withdrawal']['withdrawal_id']; ?></td>
                                                <td><?php echo $withdrawal['Withdrawal']['status']; ?></td>
                                                <td>
                                                    <?php if ($withdrawal['Withdrawal']['status'] == 'Pending') { ?>
                                                        <a href="<?php echo BASE_URL; ?>users/app_withdrawals/<?php echo $withdrawal['Withdrawal']['withdrawal_id']; ?>?user_id=<?php echo $_REQUEST['user_id'] ?>"><i class="la la-edit"></i></a>
                                                    <?php } ?>
                                                    <a href="<?php echo BASE_URL; ?>users/app_deletewithdrawals/<?php echo $withdrawal['Withdrawal']['withdrawal_id']; ?>?user_id=<?php echo $_REQUEST['user_id'] ?>" class="delconfirm"><i class="la la-trash-o text-danger"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td class="text-center" colspan="5">No Data Found</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>