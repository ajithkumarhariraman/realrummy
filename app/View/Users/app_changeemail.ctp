<div class="user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="mt-0 mb-5">
       <div class="clearfix menu-content">
            <div class="col-xs-3 match-height">
                <?php echo $this->element('app_user_sidebar'); ?>
            </div>
            <div class="col-xs-9 alt-menu-right menu-content-right match-height">
                <!-- Nav tabs -->
                <?php echo $this->element('app_user_tab'); ?>
                <div class="panel panel-success mb-5">
                    <div class="panel-heading bg-success">
                        <h4>Change Email</h4>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="#" class="validation_form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>New Email Address</label>
                                        <input type="text" class="form-control validate[required,custom[email]]" name="data[User][email]"/>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <button type="submit" class="btn btn-block bg-danger btn-md">SUBMIT</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>