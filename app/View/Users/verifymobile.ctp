<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow-2 mt-0 mb-5">
        <div class="row menu-content">
            <?php echo $this->element('user_sidebar'); ?>
            <div class="col-md-10 menu-content-right match-height">
                <h3 class="text-success-2 mb-5 mt-5">Profile</h3>
                <!-- Nav tabs -->
                <?php echo $this->element('user_tab'); ?>
                <div class="panel panel-success mb-5">
                    <div class="panel-heading bg-success">
                        <h4>Verify Mobile</h4>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="#" class="validation_form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Enter PIN</label>
                                        <input type="text" class="form-control validate[required]" name="data[User][pin]"/>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn bg-success btn-md" name="process">Process</button>
                                        <a class="btn bg-danger btn-md" href="<?php echo BASE_URL; ?>users/resendpin">RESEND PIN</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>