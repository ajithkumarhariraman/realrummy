<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow-2 mt-0 mb-5">
        <div class="row menu-content">
            <?php echo $this->element('user_sidebar'); ?>
            <div class="col-md-10 menu-content-right match-height">
                <h3 class="text-success-2 mb-5 mt-5">My Account</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-success mb-5 match-height">
                            <div class="panel-heading bg-success">
                                <h4>Cash Account</h4>
                            </div>
                            <div class="panel-body">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Account Balance</td>
                                        <td>:</td>
                                        <td><i class="la la-inr"></i> <?php echo $sessionuser['User']['deposit_balance'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Withdrawable balance</td>
                                        <td>:</td>
                                        <td>
                                            <i class="la la-inr"></i>  <?php echo $sessionuser['User']['withdrawable_balance']; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total cash balance</td>
                                        <td>:</td>
                                        <td>
                                            <i class="la la-inr"></i>  <?php echo $sessionuser['User']['deposit_balance'] + $sessionuser['User']['withdrawable_balance']; ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-success mb-5 match-height">
                            <div class="panel-heading bg-success">
                                <h4>Bonus</h4>
                            </div>
                            <div class="panel-body">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Pending Bonus</td>
                                        <td>:</td>
                                        <td><i class="la la-inr"></i> <?php echo (!empty($sessionuser['User']['pending_bonus'])) ? $sessionuser['User']['pending_bonus'] : "0"; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Released Bonus</td>
                                        <td>:</td>
                                        <td><i class="la la-inr"></i> <?php echo (!empty($sessionuser['User']['released_bonus'])) ? $sessionuser['User']['released_bonus'] : "0"; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-success mb-5">
                            <div class="panel-heading bg-success">
                                <h4>Practice Account</h4>
                            </div>
                            <div class="panel-body">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Practice Cash</td>
                                        <td>:</td>
                                        <td><i class="la la-inr"></i> <?php echo (!empty($sessionuser['User']['practice_cash'])) ? $sessionuser['User']['practice_cash'] : "0"; ?> <a href="<?php echo BASE_URL; ?>users/refresh_practice" class="text-success btn"><i class="la la-refresh la-2x"></i></a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>