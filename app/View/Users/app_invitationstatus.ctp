<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow mb-5">
        <h2 class="text-success">Invitation Status</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="article max-h">
                    <table class="table table-bordered table-striped">
                        <thead class="bg-success">
                            <tr>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Registered</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($invitations)) {
                                foreach ($invitations as $invitation) {
                                    ?>
                                    <tr>
                                        <td><?php echo $invitation['Invitation']['name']; ?></td>
                                        <td><?php echo $invitation['Invitation']['email']; ?></td>
                                        <td><?php echo ($invitation['Invitation']['registered'] == '1') ? '<i class="fa fa-check fa fa-2 text-success" aria-hidden="true"></i>' : '<i class="fa fa-times fa-2 text-danger" aria-hidden="true"></i>'; ?></td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td>No</td>
                                </tr>
                            <?php }
                            ?>
                        </tbody> 
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
