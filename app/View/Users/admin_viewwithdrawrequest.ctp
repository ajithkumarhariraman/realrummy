<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Withdraw Request</span> - View
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/users/withdrawals" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-suceess">
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col-md-6">
                        User
                    </div>
                    <div class="col-md-6">
                        <a target="_blank" href="<?php echo BASE_URL; ?>admin/users/view/<?php echo $user['User']['user_id'] ?>"><?php echo $user['User']['name'] ?></a>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        Date
                    </div>
                    <div class="col-md-6">
                        <?php echo date('d.m.Y', $withdrawal['Withdrawal']['date']) ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        Amount
                    </div>
                    <div class="col-md-6">
                        <?php echo $withdrawal['Withdrawal']['amount'] ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        Bank Account No
                    </div>
                    <div class="col-md-6">
                        <?php echo $withdrawal['Withdrawal']['bank_account_number'] ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        IFSC
                    </div>
                    <div class="col-md-6">
                        <?php echo $withdrawal['Withdrawal']['ifsc'] ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        Bank Name
                    </div>
                    <div class="col-md-6">
                        <?php echo $withdrawal['Withdrawal']['bank_name'] ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        Account Holder Name
                    </div>
                    <div class="col-md-6">
                        <?php echo $withdrawal['Withdrawal']['account_holder'] ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        Status
                    </div>
                    <div class="col-md-6">
                        <?php echo $withdrawal['Withdrawal']['status'] ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        Notes
                    </div>
                    <div class="col-md-6" style="word-break: break-word;">
                        <?php echo $withdrawal['Withdrawal']['notes'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>