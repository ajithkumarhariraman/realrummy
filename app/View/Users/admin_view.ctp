<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Users</span>  - View
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/users" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading bg-success">
                    Personal Information
                </div>
                <div class="panel-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Name</td>
                            <td><?php echo $user['User']['name'] ?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>
                                <?php echo $user['User']['email'] ?>
                                <?php if ($user['User']['emailverified'] == '1') { ?>
                                    <label class="label label-success">Verified</label>
                                <?php } else { ?>
                                    <label class="label label-danger">Not Verified</label>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Phone Number</td>
                            <td><?php echo $user['User']['phone_number'] ?>
                                <?php if ($user['User']['phoneverified'] == '1') { ?>
                                    <label class="label label-success">Verified</label>
                                <?php } else { ?>
                                    <label class="label label-danger">Not Verified</label>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td><?php echo $user['User']['gender'] ?></td>
                        </tr>
                        <tr>
                            <td>Profile</td>
                            <td>
                                <img style="max-width: 40px;" src="<?php echo BASE_URL; ?>files/users/<?php echo $user['User']['profile'] ?>" onerror="src='<?php echo BASE_URL; ?>img/user.png'"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading bg-success">
                    Address Information
                </div>
                <div class="panel-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Address</td>
                            <td>:</td>
                            <td><?php echo (!empty($user['User']['address'])) ? $user['User']['address'] : "Not Specified"; ?></td>
                        </tr>
                        <tr>
                            <td>City</td>
                            <td>:</td>
                            <td><?php echo (!empty($user['User']['city'])) ? $user['User']['city'] : "Not Specified"; ?></td>
                        </tr>
                        <tr>
                            <td>State</td>
                            <td>:</td>
                            <td><?php echo (!empty($user['User']['state'])) ? $user['User']['state'] : "Not Specified"; ?></td>
                        </tr>
                        <tr>
                            <td>Country</td>
                            <td>:</td>
                            <td><?php echo (!empty($user['User']['country'])) ? $user['User']['country'] : "Not Specified"; ?></td>
                        </tr>
                        <tr>
                            <td>Pincode</td>
                            <td>:</td>
                            <td><?php echo (!empty($user['User']['pincode'])) ? $user['User']['pincode'] : "Not Specified"; ?></td>
                        </tr>
                    </table>
                    <form method="post" action="#" class="validation_form" style="display:none;">
                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control validate[required]" name="data[User][address]"><?php echo $user['User']['address']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Country</label>
                            <select class="form-control validate[required]" id="country"  name="data[User][country]">
                                <option value="">Country</option>
                                <?php
                                $countries = ClassRegistry::init('Country')->find('all');
                                foreach ($countries as $country) {
                                    ?>
                                    <option <?php echo ($user['User']['country'] == $country['Country']['name']) ? "selected" : ""; ?> value="<?php echo $country['Country']['name']; ?>"><?php echo $country['Country']['name']; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>State</label>
                                    <select class="form-control validate[required]" id="state" name="data[User][state]">
                                        <option value="">Sate</option>
                                        <?php
                                        $states = ClassRegistry::init('State')->find('all');
                                        foreach ($states as $state) {
                                            ?>
                                            <option <?php echo ($user['User']['state'] == $state['State']['name']) ? "selected" : ""; ?> value="<?php echo $state['State']['name']; ?>"><?php echo $state['State']['name']; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>City</label>
                                    <select id="city" class="form-control validate[required]" name="data[User][city]">
                                        <option value="">City</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Pincode</label>
                            <input type="text" class="form-control" name="data[User][pincode]" value="<?php echo $user['User']['pincode']; ?>"/>
                        </div>

                        <div class="form-group">
                            <button class="btn bg-success" type="submit">Submit</button>
                            <button class="btn bg-danger cancel-btn" type="button">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading bg-success">
                    Cash Account
                </div>
                <div class="panel-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Account Balance</td>
                            <td><?php echo $user['User']['account_balance'] ?></td>
                        </tr>
                        <tr>
                            <td>Withdrawable balance</td>
                            <td>
                                <?php echo $user['User']['withdrawable_balance']; ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading bg-success">
                    Bonus
                </div>
                <div class="panel-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Instant Deposit Bonus</td>
                            <td>:</td>
                            <td><?php echo (!empty($user['User']['deposit_bonus'])) ? $user['User']['deposit_bonus'] : "0"; ?></td>
                        </tr>
                        <tr>
                            <td>Cash Bonus</td>
                            <td>:</td>
                            <td><?php echo (!empty($user['User']['cash_bonus'])) ? $user['User']['cash_bonus'] : "0"; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>