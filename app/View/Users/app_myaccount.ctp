<div class="user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="mt-0 mb-5">
        <div class="clearfix menu-content">
            <div class="col-xs-3">
                <?php echo $this->element('app_user_sidebar'); ?>
            </div>
            <div class="col-xs-9 menu-content-right alt-menu-right match-height">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel mt-3 panel-success mb-5 ">
                            <div class="panel-heading bg-success">
                                <h4>Cash Account</h4>
                            </div>
                            <div class="panel-body">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Account Balance</td>
                                        <td>:</td>
                                        <td><i class="la la-inr"></i> <?php echo number_format($sessionuser['User']['deposit_balance'],2) ?></td>
                                    </tr>
                                    <tr>
                                        <td>Withdrawable balance</td>
                                        <td>:</td>
                                        <td>
                                            <i class="la la-inr"></i>  <?php echo number_format($sessionuser['User']['withdrawable_balance'],2); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total cash balance</td>
                                        <td>:</td>
                                        <td>
                                            <i class="la la-inr"></i>  <?php echo number_format($sessionuser['User']['deposit_balance'] + $sessionuser['User']['withdrawable_balance'],2); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-success mb-5 ">
                            <div class="panel-heading bg-success">
                                <h4>Bonus</h4>
                            </div>
                            <div class="panel-body">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Pending Bonus</td>
                                        <td>:</td>
                                        <td><i class="la la-inr"></i> <?php echo (!empty($sessionuser['User']['pending_bonus'])) ? $sessionuser['User']['pending_bonus'] : "0.00"; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Released Bonus</td>
                                        <td>:</td>
                                        <td><i class="la la-inr"></i> <?php echo (!empty($sessionuser['User']['released_bonus'])) ? $sessionuser['User']['released_bonus'] : "0.00"; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-success mb-5">
                            <div class="panel-heading bg-success">
                                <h4>Practice Account</h4>
                            </div>
                            <div class="panel-body">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Practice Cash</td>
                                        <td>:</td>
                                        <td><i class="la la-inr"></i> <?php echo (!empty(round($sessionuser['User']['practice_cash'],2))) ? number_format($sessionuser['User']['practice_cash'],2) : "0.00"; ?> <a href="<?php echo BASE_URL; ?>users/app_refresh_practice?user_id=<?php echo $_REQUEST['user_id']; ?>" class="text-success btn"><i class="la la-refresh la-2x"></i></a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>