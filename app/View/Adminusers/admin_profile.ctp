<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Profile</span> - Edit
            </h4>
        </div>
        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo BASE_URL; ?>admin/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="#">Profile</a></li>
        </ul>
        <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Edit Profile</h5>
            </div>
            <div class="panel-body">
                <form class="form-horizontal validation_form" role="form" id="myForm" name="profile" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"> Username</label>
                        <div class="col-sm-9">
                            <label class="col-xs-10 col-sm-5" style="margin-top:5px"><?php echo $this->request->data['Adminuser']['username']; ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"> Admin Name <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" id="firstname"   class="form-control validate[required]" name="data[Adminuser][adminname]" value="<?php echo $this->request->data['Adminuser']['adminname']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"> Email <span class="required">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" id="email"   class="form-control validate[required,custom[email]]" name="data[Adminuser][email]" value="<?php echo $this->request->data['Adminuser']['email']; ?>"/>
                        </div>
                    </div>
                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit"> <i class="ace-icon fa fa-check bigger-110"></i> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

