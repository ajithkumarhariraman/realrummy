<!-- Main content -->
<div class="content-wrapper">
    <!-- Content area -->
    <div class="content">
        <?php $settings = ClassRegistry::init('Sitesetting')->find('first'); ?>
        <!-- Advanced login -->
        <form action="" method="post" class="validation_form">
            <div class="panel panel-body login-form">
                <div class="text-center">
                    <img src="<?php echo BASE_URL; ?>img/<?php echo $settings['Sitesetting']['logo']; ?>" style="max-height:55px;margin-left:auto;margin-right:auto;display:block;" class="img-responsive"/>
                    <h5 class="content-group">Login to your account <small class="display-block">Your credentials</small></h5>
                </div>
                <div class="form-group has-feedback has-feedback-left">
                    <input type="text" class="form-control validate[required]" placeholder="Username" name="data[Adminuser][username]">
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>
                </div>
                <div class="form-group has-feedback has-feedback-left">
                    <input type="password" class="form-control validate[required]" placeholder="Password" name="data[Adminuser][password]">
                    <div class="form-control-feedback">
                        <i class="icon-lock2 text-muted"></i>
                    </div>
                </div>
                <div class="form-group login-options">
                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo BASE_URL; ?>admin/adminusers/forgot">Forgot password?</a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </form>
        <!-- /advanced login -->
        <!-- Footer -->
        <!-- /footer -->
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->
<style>
    .form-control-feedback .text-muted {
        margin-top: 0;
    }
</style>