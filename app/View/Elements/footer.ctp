<?php
$sitesetting = Classregistry::init('Sitesetting')->find('first');
?>
<footer>
    <div class="container mb-3">
        <div class="row">
            <div class="col-md-3">
                <img src="<?php echo BASE_URL; ?>img/<?php echo $sitesetting['Sitesetting']['logo']; ?>" class="img-responsive"/>
                <div class="mt-4 article">
                    <?php echo $sitesetting['Sitesetting']['footer_about']; ?>
                </div>
            </div>
            <div class="col-md-3">
                <h3>Quick Links</h3>
                <ul class="list-unstyled">
                    <li><a href="<?php echo BASE_URL; ?>home/about">About Us</a></li>
                    <li><a href="<?php echo BASE_URL; ?>promotions">Promotions</a></li>
                    <li><a href="#">Winner</a></li>
                    <li><a href="<?php echo BASE_URL; ?>home/mobile">Mobile</a></li>
                    <li><a href="<?php echo BASE_URL; ?>contacts/">Support</a></li>
                    <li><a href="<?php echo BASE_URL; ?>testimonials/">Testimonial</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h3>Informations</h3>
                <ul class="list-unstyled">
                    <li><a href="<?php echo BASE_URL; ?>faqs/">FAQ</a></li>
                    <li><a href="#">Sitemap</a></li>
                    <li><a href="<?php echo BASE_URL; ?>home/certification/">Certification</a></li>
                    <li><a href="<?php echo BASE_URL; ?>home/cms/terms-conditions">Terms of Use</a></li>
                    <li><a href="<?php echo BASE_URL; ?>home/cms/privacy-policy/">Privacy policy</a></li>
                    <li><a href="<?php echo BASE_URL; ?>home/cms/disclaimer/">Disclaimer</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h3>Stay Connected</h3>
                <ul class="list-inline social-links">
                    <?php if (!empty($sitesetting['Sitesetting']['facebook_url'])) { ?>
                        <li><a href="<?php echo $sitesetting['Sitesetting']['facebook_url']; ?>" target='_blank'><i class="fa fa-facebook"></i></a></li>
                    <?php } ?> 
                    <?php if (!empty($sitesetting['Sitesetting']['twitter_url'])) { ?>
                        <li><a href="<?php echo $sitesetting['Sitesetting']['twitter_url']; ?>" target='_blank'><i class="fa fa-twitter"></i></a></li>
                    <?php } ?>
                    <?php if (!empty($sitesetting['Sitesetting']['pinterest_url'])) { ?>
                        <li><a href="<?php echo $sitesetting['Sitesetting']['pinterest_url']; ?>" target='_blank'><i class="fa fa-pinterest"></i></a></li>
                    <?php } ?>
                    <?php if (!empty($sitesetting['Sitesetting']['instagram_url'])) { ?>
                        <li><a href="<?php echo $sitesetting['Sitesetting']['instagram_url']; ?>" target='_blank'><i class="fa fa-instagram"></i></a></li>
                    <?php } ?>
                </ul>
                <h4 class="mt-5 mb-4">Payments</h4>
                <img src="<?php echo BASE_URL; ?>img/payments.png" class="img-responsive"/>
            </div>
        </div>

    </div>
    <div class="text-center copy-right">
        <p class="m-0">&copy; <?php echo date('Y'); ?> Rummy Club. All Rights Reserved</p>
    </div>
</footer>