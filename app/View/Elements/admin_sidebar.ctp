<?php $admin = ClassRegistry::init('Adminuser')->find('first', array('conditions' => array('admin_id' => $this->Session->read('Adminuser.admin_id')))); ?>
<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <!-- Main -->
                    <?php
                    if (($this->params['controller'] == 'dashboard')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="icon-home4"></i> <span>Dashboard</span>', array("controller" => "dashboard", "action" => "index"), array('escapeTitle' => false)) ?></li>
                    <?php
                    if (($this->params['controller'] == 'sitesettings') || ($this->params['controller'] == 'sliders') || ($this->params['controller'] == 'emailcontents') || ($this->params['controller'] == 'smscontents')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>">
                        <a href="#" class="has-ul"><i class="fa fa-cogs"></i> <span>Settings</span></a>
                        <ul class="hidden-ul <?php echo $class; ?>">
                            <?php
                            if (($this->params['controller'] == 'sitesettings')) {
                                $class = "active";
                            } else {
                                $class = "";
                            }
                            ?>
                            <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-cog"></i> <span>Site Settings</span>', array("controller" => "sitesettings", "action" => "index"), array('escapeTitle' => false)) ?></li>
                            <?php
                            if (($this->params['controller'] == 'sliders')) {
                                $class = "active";
                            } else {
                                $class = "";
                            }
                            ?>
                            <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-cog"></i> <span>Home Page Slider</span>', array("controller" => "sliders", "action" => "index"), array('escapeTitle' => false)) ?></li>
                        </ul>
                    </li>
                    <?php
                    if (($this->params['controller'] == 'faqs') || ($this->params['controller'] == 'faqcategories') || ($this->params['controller'] == 'faqtypes')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>">
                        <a href="#" class="has-ul"><i class="icon-database-menu"></i> <span>Faqs</span></a>
                        <ul class="hidden-ul <?php echo $class; ?>">
                            <?php
                            if (($this->params['controller'] == 'faqcategories')) {
                                $class = "active";
                            } else {
                                $class = "";
                            }
                            ?>
                            <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class=" icon-drawer3"></i> <span>Faq Categories</span>', array("controller" => "faqcategories", "action" => "index"), array('escapeTitle' => false)) ?></li>
                            <?php
                            if (($this->params['controller'] == 'faqs')) {
                                $class = "active";
                            } else {
                                $class = "";
                            }
                            ?>
                            <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="icon-database"></i> <span>Faqs</span>', array("controller" => "faqs", "action" => "index"), array('escapeTitle' => false)) ?></li>
                        </ul>
                    </li>
                    <?php
                    if (($this->params['controller'] == 'testimonials') || ($this->params['controller'] == 'testicategories') || ($this->params['controller'] == 'testitypes')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>">
                        <a href="#" class="has-ul"><i class="icon-quotes-right"></i> <span>Testimonials</span></a>
                        <ul class="hidden-ul <?php echo $class; ?>">
                            <?php
                            if (($this->params['controller'] == 'testicategories')) {
                                $class = "active";
                            } else {
                                $class = "";
                            }
                            ?>
                            <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class=" icon-drawer3"></i> <span>Testi Categories</span>', array("controller" => "testicategories", "action" => "index"), array('escapeTitle' => false)) ?></li>
                            <?php
                            if (($this->params['controller'] == 'testimonials')) {
                                $class = "active";
                            } else {
                                $class = "";
                            }
                            ?>
                            <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="icon-quotes-right"></i> <span>Testimonials</span>', array("controller" => "testimonials", "action" => "index"), array('escapeTitle' => false)) ?></li>
                        </ul>
                    </li>
                    <?php
                    if (($this->params['controller'] == 'promotions')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-file-text"></i> <span>Promotions</span>', array("controller" => "promotions", "action" => "index"), array('escapeTitle' => false)) ?></li>
                    
                    <?php
                    if (($this->params['controller'] == 'promocodes')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-file-text"></i> <span>Promo codes</span>', array("controller" => "promocodes", "action" => "index"), array('escapeTitle' => false)) ?></li>

                    <?php
                    if (($this->params['controller'] == 'subscribers')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="icon-users2"></i> <span>Subscribers</span>', array("controller" => "subscribers", "action" => "index"), array('escapeTitle' => false)) ?></li>

                    <?php
                    if (($this->params['controller'] == 'staticpages')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-file-text"></i> <span>Pages</span>', array("controller" => "staticpages", "action" => "index"), array('escapeTitle' => false)) ?></li>
                    <?php
                    if (($this->params['controller'] == 'users') && ($this->params['controller'] == 'admin_index')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-file-text"></i> <span>Users</span>', array("controller" => "users", "action" => "index"), array('escapeTitle' => false)) ?></li>
                    <?php
                    if (($this->params['controller'] == 'users') && ($this->params['action'] == 'admin_transactions')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-file-text"></i> <span>Transactions</span>', array("controller" => "users", "action" => "transactions"), array('escapeTitle' => false)) ?></li>
                    <?php
                    if (($this->params['controller'] == 'users') && ($this->params['action'] == 'admin_withdrawals' || $this->params['action'] == 'admin_updatewithdraw' || $this->params['action'] == 'admin_viewwithdrawrequest' )) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-file-text"></i> <span>Withdrawals</span>', array("controller" => "users", "action" => "withdrawals"), array('escapeTitle' => false)) ?></li>
                    <?php
                    if (($this->params['controller'] == 'tournaments')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-file-text"></i> <span>Tournaments</span>', array("controller" => "tournaments", "action" => "index"), array('escapeTitle' => false)) ?></li>
                    <?php
                    if (($this->params['controller'] == 'users') && ($this->params['action'] == 'admin_sendmail' || $this->params['action'] == 'admin_mail')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-file-text"></i> <span>Mail</span>', array("controller" => "users", "action" => "mail"), array('escapeTitle' => false)) ?></li>
                    <?php
                    if (($this->params['controller'] == 'contacts')) {
                        $class = "active";
                    } else {
                        $class = "";
                    }
                    ?>
                    <li class="<?php echo $class; ?>"><?php echo $this->Html->link('<i class="fa fa-file-text"></i> <span>Enquiries</span>', array("controller" => "contacts", "action" => "index"), array('escapeTitle' => false)) ?></li>
                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>