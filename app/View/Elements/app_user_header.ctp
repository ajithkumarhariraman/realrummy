<?php
if (!empty($_REQUEST['user_id'])) {
    $sessionuser = ClassRegistry::init('User')->find('first', array('conditions' => array('user_id' => $_REQUEST['user_id'])));
}
?>
<div class="table-responsive">
    <table class="table bg-success-1">
        <tr>
            <td>
                <div class="media" style="width: 141px;">
                    <div class="media-left">
                        <?php
                        if (!empty($sessionuser['User']['profile'])) {
                            $profile = BASE_URL . 'files/users/' . $sessionuser['User']['profile'];
                        } elseif (!empty($sessionuser['User']['fbid'])) {
                            $profile = 'https://graph.facebook.com/' . $sessionuser['User']['fbid'] . '/picture';
                        } else {
                            $profile = BASE_URL . 'img/profile.png';
                        }
                        ?>
                        <img class="media-object img-circle" src="<?php echo $profile; ?>" onerror="src='<?php echo BASE_URL; ?>img/profile.png'" alt="...">
                    </div>
                    <div class="media-body" style="width: auto;">
                        <p><?php echo $sessionuser['User']['name']; ?></p>
                        <p><img src='<?php echo BASE_URL; ?>img/coin.png' class="icon-img mr-1"/> <?php echo $sessionuser['User']['deposit_balance'] ?></p>
                    </div>
                </div>
            </td>
            <td width="80%">
                <h4 class="double-shadow-1 pl-3">My Account</h4>
            </td>
            <td>
                <div class="media">
                    <div class="media-left">
                        <img class="media-object" src='<?php echo BASE_URL; ?>img/cub.png' alt="...">
                    </div>
                    <div class="media-body">
                        <p>Bonus <i class="la la-inr"></i> <?php echo round($sessionuser['User']['pending_bonus'], 2) ?></p>
                    </div>
                </div>
            </td>
            <td>
                <a href="http://localhost/rum/web5/web5/web2/close.php" class="close-btn"><i class="fa fa-times"></i></a>
            </td>

        </tr>
    </table>
</div>
<style>
    .play_game i {
        padding-right: 12px;
    }
    .play_game {
        padding: 10px 24px !important;
        font-size: 17px !important;
    }
    .close-btn{
        background: #FE3104;
        background: -webkit-radial-gradient(top, #FE3104, #820606);
        background: -moz-radial-gradient(top, #FE3104, #820606);
        background: radial-gradient(to bottom, #FE3104, #820606);
        padding: 10px 13px;
        border-radius: 6px;
        color: #fff;
    }
</style>