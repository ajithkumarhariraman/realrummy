<div class="col-md-2  match-height">
    <ul class="side-menu list-unstyled">

        <li class="<?php echo ($this->params['action'] == 'myaccount') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>users/myaccount">My Account</a></li>
        <li class="<?php echo ($this->params['action'] == 'profile') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>users/profile">Profile</a></li>
        <li class="<?php echo ($this->params['action'] == 'addcash') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>users/addcash">Add Cash</a></li>
        <li class="<?php echo ($this->params['action'] == 'withdrawals') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>users/withdrawals">Withdrawal</a></li>
        <li class="<?php echo ($this->params['action'] == 'bonus') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>users/bonus">Bonus</a></li>
        <li class="<?php echo ($this->params['action'] == 'gamehistory') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>users/gamehistory">Game History</a></li>
        <li class="<?php echo ($this->params['action'] == 'transactions') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>users/transactions">Transactions</a></li>
        <li class="<?php echo ($this->params['action'] == 'inbox') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>users/inbox">Mail Box</a></li>
    </ul>
</div>