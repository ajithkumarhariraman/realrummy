<?php
$sitesetting = Classregistry::init('Sitesetting')->find('first');
?>
<header>
    <nav class="navbar navbar-default nav-1">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <img alt="Brand" src="<?php echo BASE_URL; ?>img/<?php echo $sitesetting['Sitesetting']['logo']; ?>">
                </a>
            </div>
            <form class="navbar-form navbar-right validation_form" method="post" action="<?php echo BASE_URL; ?>users/login" role="search">
                <?php if (empty($this->Session->read('User.user_id'))) { ?>
                    <div class="form-group">
                        <input type="text" class="form-control validate[required]" name="data[User][email]" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control validate[required]" name="data[User][password]" placeholder="Password">
                        <a class="forgot-password" data-toggle="modal" data-target="#forgot-password">Forgot Password?</a>
                    </div>
                    <button type="submit" class="btn btn-success">LOGIN</button>
                    <span>(OR)</span>
                    <a href="http://www.facebook.com/dialog/oauth?
                       client_id=394025044537395&
                       redirect_uri=https://project.rayaztech.com/rummy-club/facebook/fb_login&
                       scope=email" class="social-login" target="_blank" class="social-login"><img src="<?php echo BASE_URL; ?>img/facebook.png"/></a>
<!--                    <a target="_blank" href="https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=https://project.rayaztech.com/rummy-club/facebook/google_login&client_id=561543208812-r02ml84popfcoaur8lsi2og41b8nlgvb.apps.googleusercontent.com&scope=email+profile&access_type=online&approval_prompt=auto" class="social-login"><img src="<?php echo BASE_URL; ?>img/google.png"/></a>-->
                <?php } else { ?>
                    <ul class="list-inline">
                        <li>
                            <a class="btn btn-success" href="<?php echo BASE_URL; ?>users/myaccount"> <i class="la la-user"></i> <?php echo $this->Session->read('User.name'); ?></a>
                        </li>
                        <li>
                            <a class="btn btn-danger" href="<?php echo BASE_URL; ?>users/logout">LOGOUT</a>
                        </li>
                    </ul>
                <?php } ?>
            </form>

        </div>
    </nav>
    <div class="container">
        <nav class="navbar navbar-default nav-2">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo ($this->params['action'] == 'index' && $this->params['controller'] == 'home') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>">Home </a></li>
                    <li class="<?php echo ($this->params['action'] == 'about' && $this->params['controller'] == 'home') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>home/about">About Us</a></li>
                    <li class="<?php echo ($this->params['controller'] == 'promotions') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>promotions">Promotions</a></li>
                    <li class="<?php echo ($this->params['action'] == 'index' && $this->params['controller'] == 'faqs') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>faqs">FAQ</a></li>
                    <li class="<?php echo ($this->params['action'] == 'mobile' && $this->params['controller'] == 'home') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>home/mobile">Mobile</a></li>
                    <li class="<?php echo ($this->params['action'] == 'index' && $this->params['controller'] == 'contacts') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>contacts">Support</a></li>
                    <li class="<?php echo ($this->params['action'] == 'index' && $this->params['controller'] == 'testimonials') ? "active" : ""; ?>"><a href="<?php echo BASE_URL; ?>testimonials">Testimonial</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div><!-- /.container-fluid -->
</header>

<!-- Modal -->
<div class="modal fade" id="forgot-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
            </div>
            <form method="post" action="<?php echo BASE_URL; ?>users/forgotpassword">
                <div class="modal-body">

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control validate[required,custom[email]]" name="data[User][email]"/>
                    </div>
                    <button type="submit" class="btn btn-success btn-block">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>