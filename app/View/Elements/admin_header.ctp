<?php
$setting = ClassRegistry::init('Sitesetting')->find('first');
$admin = ClassRegistry::init('Adminuser')->find('first', array('conditions' => array('admin_id' => $this->Session->read('Adminuser.admin_id'))));
?>
<style>
    .navbar-inverse .navbar-brand {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        color: #fff;
        margin-left: 15px;
        padding: 0;
    }
</style>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo BASE_URL ?>admin">
            <?php
            if ($setting['Sitesetting']['logo'] != '') {
                echo $this->Html->image($setting['Sitesetting']['logo'], array('alt' => 'CakePHP'));
            }
            ?>
        </a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li>
                <a class="sidebar-control sidebar-main-toggle hidden-xs">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <?php if ($this->Session->read('Adminuser')) { ?>
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo BASE_URL; ?>img/<?php echo $setting['Sitesetting']['logo'] ?>" alt="">
                        <span><?php echo $admin['Adminuser']['adminname']; ?></span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="<?php echo BASE_URL; ?>admin/adminusers/profile"><i class="icon-cog5"></i> Account settings</a></li>
                        <li><a href="<?php echo BASE_URL; ?>admin/adminusers/changepassword"><i class="fa fa-lock"></i> Change Password</a></li>
                        <li><a href="<?php echo BASE_URL; ?>admin/adminusers/logout"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<!-- /main navbar -->
<script>
    jQuery(document).ready(function () {
        $("#flashMessage").click(function () {
            $("#flashMessage").fadeOut(1000);
        });
    });
    setTimeout(function () {
        $('#flashMessage').fadeOut(1000);
    }, 5000);
</script>