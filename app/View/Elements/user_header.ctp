<div class="table-responsive">
    <table class="table bg-success-1">
        <tr>
            <td>
                <div class="media">
                    <div class="media-left">
                        <img class="media-object img-circle" src="<?php echo BASE_URL; ?>files/users/<?php echo $sessionuser['User']['profile']; ?>" onerror="src='<?php echo BASE_URL; ?>img/profile.png'" alt="...">
                    </div>
                    <div class="media-body" style="width: auto;">
                        <p><?php echo $sessionuser['User']['name']; ?></p>
                        <p><img src="<?php echo BASE_URL; ?>img/coin.png" class="icon-img mr-1"/> <?php echo round($sessionuser['User']['deposit_balance'],2) ?></p>
                    </div>
                </div>
            </td>
            <td>
                <a class="btn bg-success add-cash"><img src="<?php echo BASE_URL; ?>img/coin.png" class="icon-img mr-3"/> Add Cash</a>
            </td>
            <td>
                <div class="media">
                    <div class="media-left">
                        <img class="media-object" src='<?php echo BASE_URL; ?>img/cub.png' alt="...">
                    </div>
                    <div class="media-body">
                        <p>Bonus</p>
                        <p> <i class="la la-inr"></i> <?php echo $sessionuser['User']['pending_bonus'] ?></p>
                    </div>
                </div>
            </td>
            <td>
                <a class="btn bg-success play_game" target="_top" href="<?php echo BASE_URL;?>Tables">Play Game</a>
            </td>
            <td class="logout">
                <a class="btn bg-success"><i class="la la-power-off la-4x"></i> </a>
            </td>
        </tr>
    </table>
</div>
<script>
    $('a.play_game11').click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        window.open(href, "mywindow", "menubar=1,toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000");
        return false;
    });
</script>
<style>
    .play_game i {
    padding-right: 12px;
}
.play_game {
    padding: 10px 24px !important;
    font-size: 17px !important;
}
</style>