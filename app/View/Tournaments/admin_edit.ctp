<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Table</span>  - Add
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/tournaments" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Tournament Name</label>
                        <input type="text" class="form-control validate[required]" name="data[Tournament][name]" value="<?php echo (!empty($this->request->data['Tournament']['name'])) ? $this->request->data['Tournament']['name'] : "" ?>"/>
                    </div>
                    <div class="form-group">
                        <label class="">Tournament Type</label>
                        <?php
                        $type = (!empty($this->request->data['Tournament']['type'])) ? $this->request->data['Tournament']['type'] : "Cash";
                        ?>
                        <div class="row">
                            <label class="col-md-4"><input type="radio" class="tournament_type" name="data[Tournament][tournament_type]" <?php echo ($type == 'Cash') ? "checked" : "" ?> value="Cash"/> Cash</label>
                            <label class="col-md-4"><input type="radio" class="tournament_type" name="data[Tournament][tournament_type]" <?php echo ($type == 'Club') ? "checked" : "" ?> value="Club"/> Club</label>
                            <label class="col-md-4"><input type="radio" class="tournament_type" name="data[Tournament][tournament_type]" <?php echo ($type == 'Jackpot') ? "checked" : "" ?> value="Jackpot"/> Jackpot</label>
                        </div>
                    </div>
                    <div class="form-group entry_fee" style="<?php echo ($type == 'Cash') ? "display:block" : "display:none"; ?>">
                        <label>Entry Fee (Rs.)</label>
                        <input type="text" class="form-control validate[required]" name="data[Tournament][entryfee]" value="<?php echo (!empty($this->request->data['Tournament']['entryfee'])) ? $this->request->data['Tournament']['entryfee'] : "" ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Registration Start Date Time</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control datepicker validate[required]" placeholder="Date" name="data[Tournament][reg_starts_date]" value="<?php echo (!empty($this->request->data['Tournament']['reg_starts_date'])) ? $this->request->data['Tournament']['reg_starts_date'] : "" ?>"/>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control timepicker validate[required]" placeholder="Time" name="data[Tournament][reg_starts_time]" value="<?php echo (!empty($this->request->data['Tournament']['reg_starts_time'])) ? $this->request->data['Tournament']['reg_starts_time'] : "" ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Registration Close Date Time</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control datepicker validate[required]" placeholder="Date" name="data[Tournament][reg_ends_date]" value="<?php echo (!empty($this->request->data['Tournament']['reg_ends_date'])) ? $this->request->data['Tournament']['reg_ends_date'] : "" ?>"/>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control timepicker validate[required]" placeholder="Time" name="data[Tournament][reg_ends_time]" value="<?php echo (!empty($this->request->data['Tournament']['reg_ends_time'])) ? $this->request->data['Tournament']['reg_ends_time'] : "" ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Tournament start Date Time</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control datepicker validate[required]" placeholder="Date" name="data[Tournament][starts_date]" value="<?php echo (!empty($this->request->data['Tournament']['starts_date'])) ? $this->request->data['Tournament']['starts_date'] : "" ?>"/>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control timepicker validate[required]" placeholder="Time" name="data[Tournament][starts_time]" value="<?php echo (!empty($this->request->data['Tournament']['starts_time'])) ? $this->request->data['Tournament']['starts_time'] : "" ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Max Players</label>
                        <input type="text" class="form-control validate[required]" name="data[Tournament][maxplayers]" value="<?php echo (!empty($this->request->data['Tournament']['maxplayers'])) ? $this->request->data['Tournament']['maxplayers'] : "" ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Max Players Per Table</label>
                        <input type="text" class="form-control validate[required]" name="data[Tournament][maxplayers_table]" value="<?php echo (!empty($this->request->data['Tournament']['maxplayers_table'])) ? $this->request->data['Tournament']['maxplayers_table'] : "" ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Min players to run game</label>
                        <input type="text" class="form-control validate[required]" name="data[Tournament][minplayers_game]" value="<?php echo (!empty($this->request->data['Tournament']['minplayers_game'])) ? $this->request->data['Tournament']['minplayers_game'] : "" ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Total prize count</label>
                        <input type="text" class="form-control validate[required]" name="data[Tournament][totalprize_count]" value="<?php echo (!empty($this->request->data['Tournament']['totalprize_count'])) ? $this->request->data['Tournament']['totalprize_count'] : "" ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Expected prize amount (Rs.)</label>
                        <input type="text" class="form-control validate[required]" name="data[Tournament][prizeamount]" value="<?php echo (!empty($this->request->data['Tournament']['prizeamount'])) ? $this->request->data['Tournament']['prizeamount'] : "" ?>"/>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> &nbsp;</label>
                        <div class="col-lg-5">
                            <button class="btn bg-teal" type="submit"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('.datepicker').datepicker({format: 'dd-mm-yyyy'});
    $(document).on('change', '.tournament_type', function () {
        if ($(this).val() == 'Club') {
            $('.entry_fee').hide();
            $('.entry_fee input').val('');
        } else {
            $('.entry_fee').show();
            $('.entry_fee input').val('');
        }
    });
    $('.timepicker').timepicker();
</script>