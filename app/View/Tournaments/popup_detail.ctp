<div class="row mb-5">
    <div class="col-md-4">
        <div class="match-height">
            <?php
            $joined = ClassRegistry::init('Tournamentplayer')->find('count', array('conditions' => array('tournament_id' => $data['tournament_id'])));
            ?>
            <h4><img src="https://project.rayaztech.com/rummy-club/img/cub.png"/> <?php echo $data['name']; ?></h4>
            <p class="clearfix"><label class="col-md-5">Tournament Id:</label> <span class="text-brown"><?php echo $data['tournamentid']; ?></span></p>
            <p class="clearfix"><label class="col-md-5">Starts:</label> <span class="text-brown"><?php echo date('M-d | h:i A', strtotime($data['starts'])); ?></span></p>
            <p class="clearfix"><label class="col-md-5">Entry Fee:</label> <span class="text-brown"><?php echo ($data['type'] == 'Club') ? "Free" : $data['entryfee']; ?></span></p>
            <p class="clearfix"><label class="col-md-5">Joined:</label><span id="joined" class="text-brown"><?php echo $joined ?></span> / <?php echo $data['maxplayers'] ?></p>
            <p class="clearfix"><label class="col-md-5">Max Players per Table:</label><span class="text-brown"><?php echo $data['maxplayers_table'] ?></span></p>
            <hr/>
            <div class="prize-details text-center">
                <div class="expected-prize">
                    <h5>Expected Prize</h5>
                    <h3><i class="fa fa-inr"></i> <?php echo $data['totalprize_amount']; ?></h3>
                </div>
            </div>
            <table class="table text-center table-bordered">
                <tr>
                    <td>
                        <p>Total Prizes</p>
                        <h4 class="text-brown"><?php echo $data['totalprize_count']; ?></h4> 
                    </td>
                    <td>
                        <p> Total Rounds </p>
                        <h4 class="text-brown"><?php echo count($data['tournament_structure']); ?></h4>
                    </td>
                    <td>
                        <p> Prizes From </p>
                        <h4 class="text-brown">Final</h4>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-md-8">
        <div class="tournament-left-tab match-height">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Tournament Structure</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Tournament Progress</a></li>
                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Joined Players</a></li>
                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Tournament Prizes</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Round</th>
                                <th>Duration</th>
                                <th>Players</th>
                                <th>Qualifier</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($data['tournament_structure'] as $structure) {
                                ?>
                                <tr>
                                    <td>Round <?php echo $i; ?></td>
                                    <td>60 Mins</td>
                                    <td>
                                        <p><?php echo $structure['players']; ?></p>
                                        <p>on <?php echo $structure['table']; ?> table</p>
                                    </td>
                                    <td>
                                        <p> <?php echo $structure['qualifier']; ?> player(s)</p>
                                        <p>per table</p>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">...</div>
                <div role="tabpanel" class="tab-pane" id="messages">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Position</th>
                                <th>Player Name</th>
                                <th>Player Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (strtotime($data['reg_starts']) > time()) {
                                if (!empty($data['players'])) {
                                    $i = 1;
                                    foreach ($data['players'] as $player) {
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $player['player_name']; ?></td>
                                            <td><?php echo $player['status']; ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="3">No players joined yet!</td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="3"> This information is shared only once tournament registration starts. </td>
                                </tr>
                            <?php }
                            ?>

                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="settings">

                </div>
            </div>

        </div>
    </div>
</div>
<p class="small">*Details about Special Prizes and tickets can be found in 'Tournament Prizes' tab</p>
<p class="small">**Prize amounts & number of prizes are indicative and depends on the number of entries to this tournament. </p>
<div class="tournament-bottom">
    <table class="table" style="margin-bottom: 0;">
        <tr>
            <td>
                <h4 class="text-brown">Tournament Schedule </h4>
            </td>
            <td>
                <div class="bg-success-flat">
                    <p> Registration Starts</p>
                    <b><?php echo date('M-d | h:i A', strtotime($data['reg_starts'])); ?></b>
                </div>
            </td>
            <td>
                <div class="bg-success-flat">
                    <p>  Registration Closes </p>
                    <b><?php echo date('M-d | h:i A', strtotime($data['reg_starts'])); ?></b>
                </div>
            </td>
            <td>
                <div class="bg-success-flat">
                    <p>  Tournament Starts </p>
                    <b><?php echo date('M-d | h:i A', strtotime($data['starts'])); ?></b>
                </div>
            </td>
            <td>
                <?php if (strtotime($data['reg_starts']) > time()) { ?>
                    <button class="btn btn-default" type="button">Join</button>
                    <?php
                } else if (strtotime($data['reg_ends']) > time()) {
                    $check = ClassRegistry::init('Tournamentplayer')->find('first', array('conditions' => array('tournament_id' => $tournament['Torunament']['tournament_id'])));
                    if (empty($check)) {
                        ?>
                        <a class="bg-brown btn btn-lg" href="javascript" data-id="<?php echo $data['tournament_id']; ?>" id="join">Join Now</a>
                    <?php } else {
                        ?>
                        <a class="btn btn-default" href="javascript" data-id="<?php echo $data['tournament_id']; ?>" id="withdraw">Withdraw</a>
                        <?php
                    }
                } else {
                    echo $data['tournament_status'];
                }
                ?>
            </td>
        </tr>
    </table>
</div>
<script>
    setTimeout(function doSomething() {
        jQuery.ajax({
            url: "<?php echo BASE_URL; ?>tournaments/updatePopup",
            dataType: "html",
            success: function (data) {
                $('#tournaments .tab-content').html(data);
            }
        });
        setTimeout(doSomething, 2000);
    }, 2000);
</script>
<style>
    .tournament-left-tab ul {
        background: #0E761D;
        border: none !important;
    }
    .tournament-left-tab .active{
        background: #3AB54A;
    }
    .tournament-left-tab{
        background: #3AB54A;
        padding-bottom: 10px;
    }
    .tournament-left-tab .active a {
        background:  #3AB54A !important;
        box-shadow: unset !important;
        border-radius: 0 !important;
    }
    .tournament-left-tab li a{
        box-shadow: unset !important;
        border-radius: 0 !important;
        margin: 0 !important;
        color:#fff;
    }
    .tournament-left-tab  .tab-pane {
        background: #fff !important;
        margin: 12px;
    }
    #tournament-details .modal-lg {
        width: 90%;
    }
    .tournament-left-tab thead th {
        background: #EEE;
        border: none !important;
        color:#000;
        font-weight: 600;
        font-family: Oswald;
        font-size: 17px !important;
    }
    /*    #tournament-details {
            font-family: Oswald;
        }*/
    .expected-prize h5, .expected-prize h3 {
        color: #fff;
        margin: 8px 0;
    }
    .expected-prize {
        background: #A1410E;
        display: table;
        border-radius: 28px;
        padding: 2px 30px;
        margin-bottom: 20px;
        margin-left: auto;
        margin-right: auto;
        line-height: 2;
    }
    .text-brown {
        color:  #ED145B;
    }
    .bg-brown{
        background: #ED145B;
        color:#fff;
    }
    h4.text-brown {
        font-family: Poppins;
        font-size: 24px;
        font-weight: bold;
    }
    table td,table th{
        vertical-align: middle !important;
    }
    .bg-success-flat {
        background:#3AB54A;
        color:#fff;
        text-align: center;
        padding: 10px;
    }
    .tournament-bottom{
        -webkit-box-shadow: inset -1px 7px 16px -9px rgba(58,181,74,1);
        -moz-box-shadow: inset -1px 7px 16px -9px rgba(58,181,74,1);
        box-shadow: inset -1px 7px 16px -9px rgba(58,181,74,1);
    }
</style>