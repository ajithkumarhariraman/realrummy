<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="bx-shadow mb-5">
        <div class="row">
            <div class="col-md-12">
                <div>
                    <?php
                    $types = array('Cash', 'Practice');
                    $categories = array('Points', 'Pool', 'Deals');
                    ?>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($types as $key => $type) { ?>
                            <li role="presentation" class="<?php echo $key == 0 ? "active" : "" ?>"><a href="#<?php echo $type; ?>" aria-controls="<?php echo $type; ?>" role="tab" data-toggle="tab"><?php echo $type; ?></a></li>
                        <?php } ?>
                        <li><a href="javascript:;" data-toggle="modal" data-target="#my-joined-games">My Joined Games</a></li>
                    </ul>

                    <!-- Modal -->
                    <div class="modal fade" id="my-joined-games" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <p class="pull-left m-0">My Joined Games</p>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Tab panes -->
                    <div class="tab-content pt-3 pb-2">
                        <?php foreach ($types as $ikey => $type) { ?>
                            <div role="tabpanel" class="tab-pane <?php echo $ikey == 0 ? "active" : "" ?>" id="<?php echo $type; ?>">
                                <ul class="nav nav-tabs" role="tablist">
                                    <?php foreach ($categories as $key => $category) { ?>
                                        <li role="presentation" class="<?php echo $key == 0 ? "active" : "" ?>">
                                            <a href="#<?php echo $type; ?><?php echo $category; ?>" aria-controls="<?php echo $type; ?><?php echo $category; ?>" role="tab" data-toggle="tab"><?php echo $category; ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <?php foreach ($categories as $key => $category) { ?>
                                        <div role="tabpanel" class="tab-pane <?php echo $key == 0 ? "active" : "" ?>" id="<?php echo $type; ?><?php echo $category; ?>">
                                            <div class="article max-h">
                                                <table class="table table-bordered table-striped">
                                                    <thead class="bg-success">
                                                        <tr>
                                                            <td>Table Name</td>
                                                            <td>Decks</td>
                                                            <td>Max Player</td>
                                                            <?php if ($category == 'Points') { ?>
                                                                <td>Point Value</td>
                                                            <?php } else if ($category == 'Pool') { ?>
                                                                <td>Type</td>
                                                            <?php } else { ?>
                                                                <td>Deals</td>
                                                            <?php } ?>
                                                            <td><?php echo ($category == 'Points') ? "Min Entry" : "Entry Fee"; ?></td>
                                                            <td>Active Players</td>
                                                            <td>Registering</td>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $tables = ClassRegistry::init('Table')->find('all', array('conditions' => array('status' => 'Active', 'category' => $category, 'type' => $type)));
                                                        if (!empty($tables)) {
                                                            foreach ($tables as $table) {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $table['Table']['name']; ?></td>
                                                                    <td><?php echo $table['Table']['decks']; ?></td>
                                                                    <td><?php echo $table['Table']['maxplayer']; ?></td>
                                                                    <?php
                                                                    if ($category == 'Points') {
                                                                        $rummy = 'POINTS RUMMY';
                                                                        ?>
                                                                        <td><?php echo $table['Table']['pointvalue']; ?></td>
                                                                        <?php
                                                                    } else if ($category == 'Pool') {
                                                                        $rummy = 'POOL RUMMY';
                                                                        ?>
                                                                        <td><?php echo $table['Table']['pool_type']; ?></td>
                                                                        <?php
                                                                    } else {
                                                                        $rummy = 'DEAL RUMMY';
                                                                        ?>
                                                                        <td><?php echo $table['Table']['deals']; ?></td>
                                                                    <?php } ?>
                                                                    <td><?php echo $table['Table']['minentry']; ?></td>
                                                                    <td><span id="active_<?php echo $table['Table']['table_id']; ?>"><?php echo '0'; ?></span></td>
                                                                    <td><span id="registering_<?php echo $table['Table']['table_id']; ?>"><?php echo '0/' . $table['Table']['maxplayer']; ?></span></td>
                                                                    <?php
                                                                    $url = GAME_URL . "jointableweb.php?pid=" . $sessionuser['User']['user_id'] . "&max=" . $table['Table']['maxplayer'] . "&amount=" . $table['Table']['minentry'] . "&game=" . $rummy . "&deal=" . $table['Table']['deals'] . "&pool=" . $table['Table']['pool_type'] . "&category=" . $category . "&point=" . $table['Table']['pointvalue'] . "&table_id=" . $table['Table']['table_id'];
                                                                    ?>
                                                                    <td>
                                                                        <a href="<?php echo $url ?>" target="_blank" class="btn bg-success">Play Now</a>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <tr>
                                                                <td colspan="8">No</td>
                                                            </tr>
                                                        <?php }
                                                        ?>
                                                    </tbody> 
                                                </table>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="pull-right badge badge-default p-2">
                        <i class="fa fa-clock-o" aria-hidden="true"></i> Time: <?php echo date('h:i A'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    setTimeout(function doSomething() {
        jQuery.ajax({
            url: "<?php echo BASE_URL; ?>tables/updatePlayers",
            dataType: "json",
            success: function (data) {
                $.each(data.active, function (i, active) {
                    $('#active_' + i).text(active);
                });
                $.each(data.registering, function (i, registering) {
                    $('#registering_' + i).text(registering);
                });
            }
        });
        setTimeout(doSomething, 2000);
    }, 2000);

    $('#my-joined-games').on('show.bs.modal', function (event) {
        var modal = $(this);
        jQuery.ajax({
            url: "<?php echo BASE_URL; ?>tables/getMyGames",
            dataType: "html",
            success: function (data) {
                modal.find('.modal-body').html(data);
            }
        });
    });
</script>
