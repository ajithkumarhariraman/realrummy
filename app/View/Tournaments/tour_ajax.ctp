<?php
$categories = array('Cash', 'Club', 'Jackpot');
?>
<ul class="nav nav-tabs" role="tablist">
    <?php foreach ($categories as $key => $category) { ?>
        <li role="presentation" class="<?php echo $key == 0 ? "active" : "" ?>">
            <a href="#tournaments<?php echo $category; ?>" aria-controls="tournaments<?php echo $category; ?>" role="tab" data-toggle="tab"><?php echo $category; ?></a>
        </li>
    <?php } ?>
</ul>
<?php foreach ($categories as $key => $category) { ?>
    <div role="tabpanel" class="tab-pane <?php echo $key == 0 ? "active" : "" ?>" id="tournaments<?php echo $category; ?>">
        <div class="article max-h">
            <table class="table table-bordered table-striped">
                <thead class="bg-success">
                    <tr>
                        <td>Name</td>
                        <td>Entry Fee (Rs.)</td>
                        <td>Prize</td>
                        <td>Joined</td>
                        <td>Starts</td>
                        <td>Action</td>
                        <td>Details</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $tournaments = ClassRegistry::init('Tournament')->find('all', array('conditions' => array('type' => $category)));
                    if (!empty($tournaments)) {
                        foreach ($tournaments as $tournament) {
                            $joined = ClassRegistry::init('Tournamentplayer')->find('count', array('conditions' => array('tournament_id' => $tournament['Tournament']['tournament_id'])));
                            ?>
                            <tr>
                                <td><?php echo $tournament['Tournament']['name']; ?></td>
                                <td><i class="fa fa-inr"></i><?php echo $tournament['Tournament']['entryfee']; ?></td>
                                <td><i class="fa fa-inr"></i><?php echo $tournament['Tournament']['prizeamount']; ?></td>
                                <td><?php echo $joined; ?></td>
                                <td><?php echo date('h:i A M-d', strtotime($tournament['Tournament']['starts'])); ?></td>
                                <td>
                                    <?php if (strtotime($tournament['Tournament']['reg_starts']) > time()) { ?>
                                        <button class="btn btn-default" type="button">Join</button>
                                        <?php
                                    } else if (strtotime($tournament['Tournament']['reg_ends']) > time()) {
                                        $check = ClassRegistry::init('Tournamentplayer')->find('first', array('conditions' => array('tournament_id' => $tournament['Torunament']['tournament_id'])));
                                        if (empty($check)) {
                                            ?>
                                            <a class="btn btn-default" href="javascript" data-id="<?php echo $tournament['Tournament']['tournament_id']; ?>" id="join">Join</a>
                                        <?php } else {
                                            ?>
                                            <a class="btn btn-default" href="javascript" data-id="<?php echo $tournament['Tournament']['tournament_id']; ?>" id="withdraw">Withdraw</a>
                                            <?php
                                        }
                                    } else {
                                        echo $tournament['Tournament']['tournament_status'];
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a href="javascript:;" data-toggle="modal" data-target="#tournament-details" data-id="<?php echo $tournament['Tournament']['tournament_id']; ?>">Details</a>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="8">No</td>
                        </tr>
                    <?php }
                    ?>
                </tbody> 
            </table>
        </div>
    </div>
<?php } ?>