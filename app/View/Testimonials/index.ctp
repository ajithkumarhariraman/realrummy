<div class="container">
    <div class="page-inner testimonial-inner bx-shadow mb-5">
        <div class="page-title pb-5 mb-5 pt-3">
            <h2 class="double-shadow m-0">CUSTOMER REVIEWS</h2>
        </div>
        <div class="row menu-content">
            <div class="col-md-2 match-height">
                <ul class="side-menu list-unstyled">
                    <?php foreach ($categories as $category) { ?>
                        <li class="<?php echo ($category['Testicategory']['category_id'] == $c_category['Testicategory']['category_id']) ? "active" : "" ?>"><a href="<?php echo BASE_URL; ?>testimonials/index/<?php echo $category['Testicategory']['category_id'] ?>"><?php echo $category['Testicategory']['title'] ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-md-10 menu-content-right match-height">
                <h2 class="text-success-2 mb-5 mt-0">Rummy Game FAQ's</h2>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php foreach ($testimonials as $testimonial) { ?>
                        <div class="panel panel-default testi-panel mb-4">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="text-dark" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $testimonial['Testimonial']['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $testimonial['Testimonial']['id']; ?>">
                                        <?php echo $testimonial['Testimonial']['title']; ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?php echo $testimonial['Testimonial']['id']; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body article">
                                    <div class="media">
                                        <div class="media-left media-left-res">
                                            <img class="media-object" src="<?php echo BASE_URL; ?>files/testimonials/<?php echo $testimonial['Testimonial']['profile'] ?>" onerror="src='<?php echo BASE_URL; ?>img/pro_3.png'">
                                        </div>
                                        <div class="media-body">
                                            <p>
                                                <?php echo $testimonial['Testimonial']['detail']; ?>
                                            </p>
                                            <h5 class="text-danger"><?php echo $testimonial['Testimonial']['name']; ?>, <?php echo $testimonial['Testimonial']['city']; ?></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>