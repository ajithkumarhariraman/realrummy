<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Testimonial</span>  - Edit
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/testimonials" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Title <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Testimonial][title]" value="<?php echo (!empty($this->request->data['Testimonial']['title'])) ? $this->request->data['Testimonial']['title'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Detail <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <textarea type="text"  class="form-control validate[required]" name="data[Testimonial][detail]"><?php echo (!empty($this->request->data['Testimonial']['detail'])) ? $this->request->data['Testimonial']['detail'] : "" ?></textarea>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Name <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Testimonial][name]" value="<?php echo (!empty($this->request->data['Testimonial']['name'])) ? $this->request->data['Testimonial']['name'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> City <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Testimonial][city]" value="<?php echo (!empty($this->request->data['Testimonial']['city'])) ? $this->request->data['Testimonial']['city'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Profile <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="file"  class="form-control validate[optional,custom[image]]" name="data[Testimonial][profile]"/>
                            <br/>
                            <img src="<?php echo BASE_URL; ?>files/testimonials/<?php echo $this->request->data['Testimonial']['profile']; ?>" style="max-width: 60px;"/>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Category <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <ul class="list-unstyled">
                                <?php
                                $testicats = explode(',', $this->request->data['Testimonial']['category_id']);
                                foreach ($categories as $category) {
                                    ?>
                                    <li><input <?php echo (in_array($category['Testicategory']['category_id'], $testicats)) ? "checked" : "" ?> type="checkbox" name="data[Testimonial][category_id][]" value="<?php echo $category['Testicategory']['category_id'] ?>"/> <?php echo $category['Testicategory']['title'] ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> &nbsp;</label>
                        <div class="col-lg-5">
                            <button class="btn bg-teal" type="submit"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>