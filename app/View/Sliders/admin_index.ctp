<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Slider Management</span> 
            </h4>
            <?php
            echo $this->Html->link('<i class="ace-icon fa fa-plus icon-on-right"></i> <span class="bigger-110">Add Slider</span>', array('action' => 'add', 'controller' => 'sliders'), array('escape' => false, 'class' => 'btn btn-success pull-right'));
            ?>
        </div>
        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
</div>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th> # </th>
                            <th>Slider Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (empty($sliders)) {
                            ?>
                            <tr><td colspan="10" align="center">No records found</td>
                                <?php
                            } else {
                                $i = $this->Paginator->counter('{:start}');
                                foreach ($sliders as $slider) {
                                    ?>
                                <tr>
                                    <td width="5%"><?php echo $i; ?></td>
                                    <td>
                                        <img src="<?php echo BASE_URL; ?>files/sliders/<?php echo $slider['Slider']['image'] ?>" style="max-width:50px;"/>
                                    </td>
                                    <td width="5%">
                                        <ul class="list-inline">
                                            <li><?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('controller' => 'sliders', 'action' => 'Delete', $slider['Slider']['slider_id']), array('escapeTitle' => false, "data-toggle" => "tooltip", 'class' => 'delete', "title" => "Delete")); ?></li>
                                        </ul>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                        ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                        <ul class="pagination">
                            <?php
                            echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                            $numbers = $this->Paginator->numbers();
                            if (empty($numbers)) {
                                echo '<li class="active page-link"><a>1</a></li>';
                            } else {
                                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                            }
                            echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                            ?>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>