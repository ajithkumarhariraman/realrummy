<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Testimonial category Management</span> 
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/testicategories/add" class="backcss btn btn-primary"><i class="fa fa-plus"></i> Add</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form method="get" class="form-inline form-group">
                <div class="form-group">
                    <input type="text" class="form-control" name="s" value="<?php echo (!empty($_REQUEST['s'])) ? $_REQUEST['s'] : "" ?>"/>
                </div>
                <div class="form-group">
                    <button name="search" type="submit" class="btn btn-success btn-sm">Search</button>
                </div>
                <?php if (isset($_REQUEST['search'])) { ?>
                    <div class="form-group">
                        <a href="<?php echo BASE_URL; ?>admin/testicategories" class="btn btn-danger btn-sm">Cancel</a>
                    </div>
                <?php } ?>
            </form>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th> # </th>
                            <th>Category Title</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (empty($testicategories)) {
                            ?>
                            <tr><td colspan="10" align="center">No records found</td>
                                <?php
                            } else {
                                $i = $this->Paginator->counter('{:start}');
                                foreach ($testicategories as $testicategories) {
                                    ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $testicategories['Testicategory']['title']; ?></td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <?php
                                                echo $this->Html->link(
                                                        '<i class="fa fa-pencil"></i>', array('controller' => 'testicategories', 'action' => 'edit', $testicategories['Testicategory']['category_id']), array('escapeTitle' => false, "data-toggle" => "tooltip", "title" => "Edit")
                                                );
                                                ?>
                                            </li>
                                            <li>
                                                <?php
                                                echo $this->Html->link(
                                                        '<i class="fa fa-trash"></i>', array('controller' => 'testicategories', 'action' => 'Delete', $testicategories['Testicategory']['category_id']), array('escapeTitle' => false, "data-toggle" => "tooltip", 'class' => 'delconfirm', "title" => "Delete")
                                                );
                                                ?>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                        ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                        <ul class="pagination">
                            <?php
                            echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                            $numbers = $this->Paginator->numbers();
                            if (empty($numbers)) {
                                echo '<li class="active page-link"><a>1</a></li>';
                            } else {
                                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                            }
                            echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                            ?>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>

    </div>
</div>