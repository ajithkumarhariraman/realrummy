<?php echo $this->Html->script(array('common'));?>
<script>
(function($){$.fn.extend({leanModal:function(options){var defaults={top:100,overlay:0.5,closeButton:null};var overlay=$("<div id='lean_overlay'></div>");$("body").append(overlay);options=$.extend(defaults,options);return this.each(function(){var o=options;$(this).click(function(e){var modal_id=$(this).attr("href");$("#lean_overlay").click(function(){close_modal(modal_id)});$(o.closeButton).click(function(){close_modal(modal_id)});var modal_height=$(modal_id).outerHeight();var modal_width=$(modal_id).outerWidth();
$("#lean_overlay").css({"display":"block",opacity:0});$("#lean_overlay").fadeTo(200,o.overlay);$(modal_id).css({"display":"block","position":"fixed","opacity":0,"z-index":11000,"left":50+"%","margin-left":-(modal_width/2)+"px","top":o.top+"px"});$(modal_id).fadeTo(200,1);e.preventDefault()})});function close_modal(modal_id){$("#lean_overlay").fadeOut(200);$(modal_id).css({"display":"none"})}}})})(jQuery);
</script>
<script type="text/javascript">
$(function() {
$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });		
});
</script>

<div class="top-background">
  <div class="container">
    <div class="invite-friends">
      <h1><span>Invite friends</span></h1>
    </div>
  </div>
</div>
<div class="container">
  <div class="invite-mails">
    <div class="invite-friends-inner">
      <ul>
        <li><?php echo $this->Html->link($this->Html->image('gmail.png',array('border'=>0)).'Gmail',array('controller'=>'invitefriends','action'=>'google'),array('escape'=>false))?></li>
      </ul>
    </div>
    <div class="invite-friends-inner">
      <ul>
        <li><?php echo $this->Html->link($this->Html->image('yahoo.png',array('border'=>0)).'Yahoo mail',array('controller'=>'invitefriends','action'=>'yahoo'),array('escape'=>false))?> </li>
      </ul>
    </div>
    <!--<div class="invite-friends-inner">
      <ul>
        <li><?php echo $this->Html->link($this->Html->image('outlook.png',array('border'=>0)).'Outlook',array('controller'=>'invitefriends','action'=>'hotmail'),array('escape'=>false))?> </li>
      </ul>
    </div>
    <div class="invite-friends-inner">
      <ul>
        <li><?php echo $this->Html->link($this->Html->image('windows-live.png',array('border'=>0)).'Windows Live Mail',array('controller'=>'invitefriends','action'=>'hotmail'),array('escape'=>false))?> </li>
      </ul>
    </div>
    <div class="invite-friends-inner">
      <ul>
      <li><?php echo $this->Html->link($this->Html->image('msn.png',array('border'=>0)).'MSN',array('controller'=>'invitefriends','action'=>'hotmail'),array('escape'=>false))?> </li>       
      </ul>
    </div>-->
    <div class="invite-friends-inner">
      <ul>
        <li> <a id="go" rel="leanModal" name="signup" href="#signup" ><?php echo $this->Html->image('avatar.png',array('border'=>0));?>Invite Manually</a><?php /*?><?php echo $this->Html->link($this->Html->image('avatar.png',array('border'=>0)).'Invite Manually',array('controller'=>'invitefriends','action'=>'invitefriend'),array('escape'=>false))?><?php */?> </li>
      </ul>
    </div>
    </ul>
  </div>
</div>
<div id="signup" style="display:none">
  <div id="signup-ct">
    <div id="signup-header">
     <div style=" width:100%;" ><?php echo $this->Html->image('invite-avatar.png',array('border'=>0));?> 
      <h2>Invite Manually</h2></div>
      
      <a class="modal_close" href="#"></a> </div>
  
     <?php echo $this->Form->create('Invitefriend', array('action' => 'invitemanually','Controller'=>'invitefriends','id'=>'invitefriendid'),array('escape'=>'false')); ?>
      <div class="txt-fld">
       <p>Invite your friends by typing their email address below:</p>
       
        <input id="to_1" class="good_input  validate[required]"   type="text" name="data[Invitefriend][to_1]" placeholder="Email Address" />
     
      </div>
      <div class="txt-fld">
        
        <input id="to_2" name="data[Invitefriend][to_2]"   type="text" placeholder="Email Address"  />
      </div>
      <div class="txt-fld">
     
        <input id="to_3" name="data[Invitefriend][to_3]" type="text"  placeholder="Email Address" />
      </div>
      
      <div class="txt-fld">
     
        <textarea cols="51" rows="5"  name="data[Invitefriend][messages]" placeholder="I've been using rummoney.com to hire talented professionals for a fraction of the cost,and to make money by completing work."></textarea>
      </div>
      <div class="btn-fld">
        <button class="popup-button"  name="invitesubmit "type="submit">Invite Friends &raquo;</button>
      </div>
    </form>
  </div>
</div>
<div id="windows" style="display:none;">
  <div id="signup-ct">
    <div id="windows-header">
     <div style=" width:100%;" ><?php echo $this->Html->image('invite-avatar.png',array('border'=>0));?> 
      <h2>Miscrosoft Contacts</h2></div>
      
      <a class="modal_close" href="#"></a> </div>
  
     
  </div>
</div>