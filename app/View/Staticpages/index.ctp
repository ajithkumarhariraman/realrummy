<div class="static-page mt-5 mb-5">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <?php echo $result['Staticpage']['pagename']; ?>
            </div>
            <div class="card-body">
                <?php echo $result['Staticpage']['page_content']; ?>
            </div>
        </div>
    </div>
</div>
