<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">CMS</span> - Edit
            </h4>
        </div>
        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb pull-right back">
            <li><a href="<?php echo BASE_URL ?>admin/staticpages/" class="pull-right btn btn-info"><i class="fa fa-angle-left fa-lg"></i> Back</a></li>
        </ul>
        <ul class="breadcrumb">
            <li><a href="<?php echo BASE_URL; ?>admin/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="<?php echo BASE_URL; ?>admin/staticpages">CMS</a></li>
            <li class="active">Edit</li>
        </ul>
        <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Edit CMS</h5>
            </div>
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <fieldset class="content-group">
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Page Name <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text"  class="form-control validate[required]" name="data[Staticpage][pagename]" value="" />
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Page Title  <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text"  class="form-control  validate[required]" name="data[Staticpage][meta_title]" value="" />
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Page content <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <textarea name="data[Staticpage][page_content]" class="form-control validate[required]" id="editor"></textarea>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-4 control-label"> Image <span class="required">*</span></label>
                            <div class="col-lg-5">
                                <input type="file"  class="form-control validate[optional,custom[image]]" name="data[Coreteam][image]"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn bg-teal" type="submit"> Submit </button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
