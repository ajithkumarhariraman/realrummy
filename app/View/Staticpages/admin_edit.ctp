<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">CMS</span> - Edit
            </h4>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <fieldset class="content-group">
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Page Name <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text"  class="form-control validate[required]" name="data[Staticpage][pagename]" value="<?php echo (!empty($this->request->data['Staticpage']['pagename'])) ? $this->request->data['Staticpage']['pagename'] : "" ?>" />
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Page content <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <textarea name="data[Staticpage][page_content]" class="form-control validate[required]" id="editor"><?php echo (!empty($this->request->data['Staticpage']['page_content'])) ? $this->request->data['Staticpage']['page_content'] : "" ?></textarea>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-4 control-label"> Image <span class="required">*</span></label>
                            <div class="col-lg-5">
                                <input type="file"  class="form-control validate[optional,custom[image]]" name="data[Staticpage][image]"/>
                                <?php if (!empty($this->request->data['Staticpage']['image'])) { ?>
                                    <br/>
                                    <img src="<?php echo BASE_URL; ?>files/pages/<?php echo $this->request->data['Staticpage']['image']; ?>" style="max-width: 100px; "/>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn bg-teal" type="submit"> Submit </button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
