<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">CMS</span> - List
            </h4>
        </div>
    </div>
</div>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="5%"> # </th>
                        <th>Page Title</th>
                        <th width="5%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (empty($staticpages)) {
                        ?>
                        <tr><td colspan="10" align="center">No records found</td>
                            <?php
                        } else {
                            $i = $this->Paginator->counter('{:start}');
                            foreach ($staticpages as $staticpage) {
                                ?>
                            <tr>
                                <td class="center"><?php echo $i; ?></td>
                                <td><?php echo $staticpage['Staticpage']['pagename']; ?></td>
                                <td>
                                    <ul class="list-inline">
                                        <li>
                                            <?php
                                            echo $this->Html->link(
                                                    '<i class="fa fa-pencil"></i>', array('controller' => 'staticpages', 'action' => 'edit', $staticpage['Staticpage']['page_id']), array('escapeTitle' => false, "data-toggle" => "tooltip", "title" => "Edit")
                                            );
                                            ?>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">
                    <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                        ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                        <ul class="pagination">
                            <?php
                            echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                            $numbers = $this->Paginator->numbers();
                            if (empty($numbers)) {
                                echo '<li class="active page-link"><a>1</a></li>';
                            } else {
                                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                            }
                            echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                            ?>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
