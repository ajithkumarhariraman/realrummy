<div class="container-fluid text-center">
    <h1 class="error-title">404</h1>
    <h6 class="text-semibold content-group">Oops, We cannot find that page!</h6>

    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
            <form action="#" class="main-search">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="<?php echo BASE_URL; ?>" class="btn bg-teal-400 btn-rounded btn-block content-group"><i class="icon-circle-left2 position-left"></i> Go to Home</a>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<!-- /error wrapper -->

