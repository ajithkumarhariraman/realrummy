<div class="container">
    <div class="page-inner bx-shadow mb-5">
        <div class="page-title pb-5 mb-5 pt-3">
            <h2 class="double-shadow m-0"><?php echo $page['Staticpage']['pagename']; ?></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="article">
                    <?php echo $page['Staticpage']['page_content']; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-shadow mb-5">
        <h3 class="text-center mb-5 mt-0">Rummy Variants Available</h3>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-sm-4  col-xs-4">
                        <a href="#">
                            <img src="<?php echo BASE_URL; ?>img/points.png" class="img-responsive full-width"/>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <a href="#">
                            <img src="<?php echo BASE_URL; ?>img/deal.png" class="img-responsive full-width"/>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <a href="#">
                            <img src="<?php echo BASE_URL; ?>img/pool.png" class="img-responsive full-width"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>