<div class="container">
    <div class="page-inner bx-shadow mb-5">
        <div class="page-title pb-5 mb-5 pt-3">
            <h2 class="double-shadow m-0">About Us</h2>
        </div>
        <div class="row">
            <div class="col-md-6">
                <img src="<?php echo BASE_URL; ?>files/pages/<?php echo $page['Staticpage']['image']; ?>" onerror="src='<?php echo BASE_URL; ?>img/img.png'" class="img-responsive about-img full-width"/>
            </div>
            <div class="col-md-6 pl-0 about-left">
                <div class="about-inner article">
                    <?php echo $page['Staticpage']['page_content']; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="bx-shadow mb-5">
        <h3 class="text-center mb-5 mt-0">Rummy Variants Available</h3>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-sm-4  col-xs-4">
                        <a href="#">
                            <img src="<?php echo BASE_URL; ?>img/points.png" class="img-responsive full-width"/>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <a href="#">
                            <img src="<?php echo BASE_URL; ?>img/deal.png" class="img-responsive full-width"/>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <a href="#">
                            <img src="<?php echo BASE_URL; ?>img/pool.png" class="img-responsive full-width"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .about-inner.article h2 {
        font-family: 'Poppins', sans-serif;
    }
</style>