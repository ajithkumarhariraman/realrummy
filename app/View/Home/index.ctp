<?php
$sitesetting = Classregistry::init('Sitesetting')->find('first');
?>
<div class="reg-section">
    <div class="owl-carousel owl-theme main-carousel">
        <?php foreach ($sliders as $slider) { ?>
            <div class="item">
                <img src="<?php echo BASE_URL; ?>files/sliders/<?php echo $slider['Slider']['image'] ?>" onerror="src='<?php echo BASE_URL; ?>img/Banner.png'"/>
            </div>
        <?php } ?>
    </div>
    <?php if (empty($this->Session->read('User.user_id'))) { ?>
        <div class="reg-form row">
            <div class="container">
                <div class="col-sm-4">
                    <div class="reg-form-inner">
                        <img src="<?php echo BASE_URL; ?>img/top.png" class="top-img"/>
                        <form method="post" action="<?php echo BASE_URL; ?>users/register" class="validation_form">
                            <div class="form-group">
                                <input type="text" class="form-control validate[required]" name="data[User][name]" placeholder="Name"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control validate[required,custom[email]]" name="data[User][email]" placeholder="Email Id"/>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control validate[required,minSize[6]]" name="data[User][password]" placeholder="Password"/>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <select class="form-control validate[required]" name="data[User][gender]">
                                        <option value="">Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <select class="form-control validate[required]" name="data[User][state]">
                                        <option value="">State</option>
                                        <?php
                                        $states = ClassRegistry::init('State')->find('all');
                                        foreach ($states as $state) {
                                            ?>
                                            <option value="<?php echo $state['State']['name']; ?>"><?php echo $state['State']['name']; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control validate[required]" name="data[User][phone_number]" placeholder="Mobile"/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success btn-block">REGISTER</button>
                            </div>
                            <div class="form-group text-center">
                                ( OR )
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <button class="btn btn-fb btn-block"><img src="<?php echo BASE_URL; ?>img/fb1.png"/> FACEBOOK</button>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-google btn-block"><img src="<?php echo BASE_URL; ?>img/g2.png"/> FACEBOOK</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<div class="features text-center pt-4 pb-3">
    <div class="container">
        <ul class="list-inline">
            <li>
                <img src="<?php echo BASE_URL; ?>img/1.png"/>
                <p class="h4">Safe & Secure</p>
            </li>
            <li>
                <img src="<?php echo BASE_URL; ?>img/2.png"/>
                <p class="h4">24X7 Service</p>
            </li>
            <li>
                <img src="<?php echo BASE_URL; ?>img/3.png"/>
                <p class="h4">Play on Mobile</p>
            </li>
            <li>
                <img src="<?php echo BASE_URL; ?>img/4.png"/>
                <p class="h4">Win Cash</p>
            </li>
            <li class="no-border">
                <img src="<?php echo BASE_URL; ?>img/4.png"/>
                <p class="h4">100% Legal</p>
            </li>
        </ul>
    </div>
</div>
<div class="download-app pt-5 pb-5">
    <div class="container">
        <h2 class="double-shadow text-center"><span class="text-success">DOWNLOAD</span> <span class="text-danger">RUMMY CLUB</span> <span class="text-success">MOBILE APP</span></h2>
        <div class='row mt-5'>
            <div class="col-md-7">
                <img src="<?php echo BASE_URL; ?>img/app.png" class="img-responsive"/>
            </div>
            <div class="col-md-5">
                <h2 class="double-shadow text-dark text-normal">PLAY Rummy on your <span class="text-success-3">Mobile</span></h2>
                <h1 class="double-shadow mt-5 mb-5"><span class="text-danger">ON THE GO</span></h1>
                <p class=" mt-5 mb-5 h3">Play Any Time, Any Where</p>
                <div class="row">
                    <div class="col-md-4">
                        <a><img src="<?php echo BASE_URL; ?>img/google_play.png" class="img-responsive mb-2"/></a>
                        <a><img src="<?php echo BASE_URL; ?>img/app_store.png" class="img-responsive"/></a>
                    </div>
                    <div class="col-md-8">
                        <form method="post" action="<?php echo BASE_URL; ?>home/sendappsms">
                            <h5 class="text-dark">Download App Via SMS</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" name="mobile" placeholder="Mobile Number" aria-describedby="basic-addon2">
                                <div class="input-group-btn">
                                    <button class="btn btn-danger" type="submit">
                                        SEND LINK
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="most-trusted pt-5 pb-5">
    <div class="container">
        <h1 class="text-shadow sec-title text-center">India's Most Trusted Rummy Site</h1>
        <div class="row mt-5">
            <div class="col-md-4 text-center">
                <img src="<?php echo BASE_URL; ?>img/<?php echo $sitesetting['Sitesetting']['three_sec_img_1']; ?>"/>
                <h4 class="mt-4 mb-4"><?php echo $sitesetting['Sitesetting']['three_sec_title_1']; ?></h4>
                <p class="article">
                    <?php echo $sitesetting['Sitesetting']['three_sec_desc_1']; ?>
                </p>
            </div>
            <div class="col-md-4 text-center">
                <img src="<?php echo BASE_URL; ?>img/<?php echo $sitesetting['Sitesetting']['three_sec_img_2']; ?>"/>
                <h4 class="mt-4 mb-4"><?php echo $sitesetting['Sitesetting']['three_sec_title_2']; ?></h4>
                <p class="article">
                    <?php echo $sitesetting['Sitesetting']['three_sec_desc_2']; ?>
                </p>
            </div>
            <div class="col-md-4 text-center">
                <img src="<?php echo BASE_URL; ?>img/<?php echo $sitesetting['Sitesetting']['three_sec_img_3']; ?>"/>
                <h4 class="mt-4 mb-4"><?php echo $sitesetting['Sitesetting']['three_sec_title_3']; ?></h4>
                <p class="article">
                    <?php echo $sitesetting['Sitesetting']['three_sec_desc_3']; ?>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="fav-online-rummy pt-5 pb-5">
    <div class="container">
        <h2 class="double-shadow text-center"><span class="text-danger">INDIA'S FAVOURITE</span> <span class="text-success">ONLINE RUMMY</span> <span class="text-danger">SITE</span></h2>
        <div class="row mt-5">
            <div class="col-md-6 pr-0">
                <img src="<?php echo BASE_URL; ?>img/mockup.png" class="img-responsive full-width"/>
            </div>
            <div class="col-md-6  pl-0 article">
                <h3 class="mb-4 text-dark">Play Rummy Online - Safe, Secure and Rewarding</h3>
                <?php echo $sitesetting['Sitesetting']['home_content_1']; ?>
            </div>
        </div>
    </div>
</div>
<div class="testimonials">
    <div class="container">
        <h2 class="text-shadow sec-title text-center">What Players Say About Rummy Club</h2>
        <div class="owl-carousel testimonial-carousel owl-theme mt-5">
            <?php foreach ($testimonials as $testimonial) { ?>
                <div class="item">
                    <div class="testi-inner">
                        <div class="media">
                            <div class="media-left">
                                <img src="<?php echo BASE_URL; ?>img/pro_3.png" class="img-responsive"/>
                            </div>
                            <div class="media-body">
                                <h5 class="media-heading text-danger"><?php echo $testimonial['Testimonial']['name'] . ', ' . $testimonial['Testimonial']['city']; ?></h5>
                            </div>
                        </div>
                        <p class="h5 text-dark"><?php echo $testimonial['Testimonial']['title']; ?></p>
                        <p class="testi-desc">
                            " <?php echo $testimonial['Testimonial']['detail']; ?> "
                        </p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="subscribe mb-5">
    <div class="container mb-5 mt-5">
        <div class="row">
            <div class="col-md-6">
                <h3 class="text-dark mb-5">Invite Friends and Earn Money</h3>
                <img class="img-responsive match-height" style="width: 100%;object-fit: cover;" src="<?php echo BASE_URL; ?>img/<?php echo $sitesetting['Sitesetting']['invite_banner']; ?>"/>
            </div>
            <div class="col-md-6">
                <h3 class="text-dark mb-5">Latest Updates and Offers</h3>
                <div class="subscribe-box match-height">
                    <h3 class="our-newsletter">Our Newsletter</h3>
                    <form method="post" action="">
                        <div class="form-group">
                            <input type="text" class="form-control" name="data[Subscriber][email]" placeholder="Enter E-mail Address"/>
                        </div>
                        <div class="form-group">
                            <button class="btn subscribe-btn">Subscribe</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.testimonial-carousel').owlCarousel({
        loop: true,
        margin: 20,
        responsiveClass: true,
        navText: ['<i class="la la-arrow-circle-o-left la-2x"></i>', '<i class="la la-arrow-circle-o-right la-2x"></i>'],
        dots: false,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 2,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                loop: false
            }
        }
    });
    $('.main-carousel').owlCarousel({
        loop: true,
        nav: true,
        responsiveClass: true,
        navText: ['<i class="la la-arrow-circle-o-left la-2x"></i>', '<i class="la la-arrow-circle-o-right la-2x"></i>'],
        items: 1,
        autoHeight: true,
        dots: false
    });
    $('.main-carousel .item img').matchHeight({
        target: $('.reg-form')
    });
</script>
