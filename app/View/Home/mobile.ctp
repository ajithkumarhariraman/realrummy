<div class="container mobile_page">
    <div class="page-inner bx-shadow mb-5">
        <div class="page-title pb-5 mb-5 pt-3">
            <h2 class="double-shadow m-0">MOBILE APP</h2>
        </div>
        <h3 class="text-center rummy_subtitle">Rummy Club Game Download For Mobile App Free</h3>
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <a href=""><img src="<?php echo WEBROOT; ?>img/Ios.png" class="img-responsive mb-2"></a>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <a href=""><img src="<?php echo WEBROOT; ?>img/android.png" class="img-responsive"></a>
                    </div>
                </div>
            </div>
        </div>
        <img src="<?php echo WEBROOT; ?>img/app.png" class="img-responsive mobile-img">
        <h2 class="double-shadow text-dark text-normal">PLAY Rummy on your <span class="text-success-3">Mobile</span></h2>
        <h1 class="double-shadow"><span class="text-danger">ON THE GO!</span></h1>
        <p class="text-center h4">Play Any Time, Any Where!</p>
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <a href=""><img src="<?php echo WEBROOT; ?>img/google_play.png" class="img-responsive mb-2"></a>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <a href=""><img src="<?php echo WEBROOT; ?>img/app_store.png" class="img-responsive"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="row  app_sms">
                    <div class="col-md-2">
                        <a href=""><img src="<?php echo WEBROOT; ?>img/sms.png" class="img-responsive"></a> 
                    </div>
                    <div class="col-md-4">
                        <h3 class="download_app">Download App via SMS</h3>
                    </div>
                    <div class="col-md-6">
                        <form method="post" action="<?php echo BASE_URL; ?>home/sendappsms">
                            <div class="input-group send-link ">
                                <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" />
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> SEND LINK</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <img src="<?php echo WEBROOT; ?>img/Mobile-flat.png" class="img-responsive mobile-img">
        <div class="mobile_requirements" style="background-image:url('<?php echo WEBROOT; ?>img/require_bg.png');">
            <h3 class="sec-title text-center">MOBILE REQUIREMENTS</h3>
            <div class="row android_ios">
                <div class="col-md-4">
                    <div class="operating_system">
                        <h4>Operating System</h4>
                        <div class="row flash">
                            <div class="col-md-6">
                                <img src="<?php echo WEBROOT; ?>img/Ios.png" class="img-responsive">  
                                <p>iOS 9.3 and above</p>
                            </div>
                            <div class="col-md-6">
                                <img src="<?php echo WEBROOT; ?>img/android.png" class="img-responsive">  
                                <p>Android 4.4.2 and above</p>
                            </div>
                        </div></div>
                </div>
                <div class="col-md-4">
                    <div class="operating_system">
                        <h4>Flash Player</h4>
                        <div class="row flash">
                            <div class="col-md-5">
                                <img src="<?php echo WEBROOT; ?>img/flash.png" class="img-responsive">                           
                            </div>
                            <div class="col-md-7">                             
                                <h5>All browsers with minimum flash player version 11.04</h5>
                            </div>
                        </div></div>
                </div>
                <div class="col-md-4">
                    <div class="operating_system">
                        <h4>Hardware Requirement</h4>
                        <div class="row flash">
                            <div class="col-md-5">
                                <img src="<?php echo WEBROOT; ?>img/android_icon.png" class="img-responsive">                           
                            </div>
                            <div class="col-md-7">                             
                                <h5>ARMv7/x86 processor with vector FPU, minimum 550MHz, Open GL ES 2.0, H.264 and AAC HW decoders</h5>
                            </div>
                        </div></div>
                </div>
            </div>
        </div>
        <h3 class="text-center rummy_subtitle">How to Install Rummy Club Mobile App</h3>
        <div class="row">
            <div class="col-md-offset-4 col-md-4 android_div">
                <ul class="nav nav-pills">
                    <li class="active"><a data-toggle="pill" href="#home"><p><i class="fa fa-android" aria-hidden="true"></i>Android</p></a></li>
                    <li><a data-toggle="pill" href="#menu1"><p><i class="fa fa-apple" aria-hidden="true"></i>iOS</p></a></li>   
                </ul></div></div>
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <img src="<?php echo WEBROOT; ?>img/playstore.png" class="img-responsive playstore"> 
                <a data-toggle="modal" data-target="#android" onclick="playVid()"> <img src="<?php echo WEBROOT; ?>img/play_button.png" class="img-responsive play_button">  </a>                
            </div>
            <div id="menu1" class="tab-pane fade">
                <img src="<?php echo WEBROOT; ?>img/playstore.png" class="img-responsive playstore">  
                <a data-toggle="modal" data-target="#ios" onclick="playVid1()"> <img src="<?php echo WEBROOT; ?>img/play_button.png" class="img-responsive play_button"> </a> 
            </div>    
        </div>
        <div class="background_video" style="background:url('<?php echo WEBROOT; ?>img/video_bg.png');">

        </div>
    </div></div>
<div class="modal fade" id="android" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">  
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="pauseVid()">&times;</button>        
            </div>
            <div class="modal-body">
                <video id="myVideo" style="width:100%;height:400px;" controls autoplay="true">
                    <source src="<?php echo WEBROOT; ?>video/android.mp4" type="video/mp4">
                </video>   
            </div>                      
        </div>

    </div>
</div>
<div class="modal fade" id="ios" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">  
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="pauseVid1()">&times;</button>        
            </div>
            <div class="modal-body">
                <video id="myVideo1" style="width:100%;height:400px;" controls autoplay="true">
                    <source src="<?php echo WEBROOT; ?>video/android.mp4" type="video/mp4">
                </video>   
            </div>                      
        </div>

    </div>
</div>
<script>
    var vid = document.getElementById("myVideo");

    function playVid() {
        vid.play();
    }

    function pauseVid() {
        vid.pause();
    }
</script> 
<script>
    var vid1 = document.getElementById("myVideo1");

    function playVid1() {
        vid1.play();
    }

    function pauseVid1() {
        vid1.pause();
    }
</script> 
<style>
    .mobile_page {
        margin-bottom: 20px;
    }
    .mobile_page .rummy_subtitle{
        color:  #1b922c;
        margin-bottom: 25px;
        font-size: 28px;        
    }
    .mobile_page .img-responsive.mobile-img {
        margin-left: auto;
        margin-right: auto;
        display: block;
        margin-top: 20px;
    }
    .mobile_page .text-center.h4 {
        margin-top: 25px;
        margin-bottom: 30px;
    }
    .mobile_page  .img-responsive.mb-2 {
        margin-left: auto;
    }
    .mobile_page .app_sms {
        border: 2px solid #e3e3e3;
        padding-top: 20px;
        padding-bottom: 25px;
        margin-top: 30px;
        margin-bottom: 10px;
    }
    .mobile_page .app_sms .img-responsive {
        margin-left: auto;
        margin-right: auto;
        display: block;
    }
    .mobile_page .send-link  .form-control {
        height: 45px;
        border-radius: 0;    
        font-size: 17px;
        padding-left: 20px;
        color: #aaa;
        font-weight: lighter;
        border:1px solid #d8d8d8;
    }
    ::placeholder{
        color: #aaa;
        font-weight: lighter;
    }
    .mobile_page .send-link  .form-control:focus {
        border: 1px solid #ccc !important;
        outline: 0;
        -webkit-box-shadow: unset;
        box-shadow: unset;
        border-color:unset !important;
    }
    .mobile_page .send-link .btn.btn-default {
        height: 45px;
        background-color: #cf3140;
        color: #fff;
        font-weight: 500;
        padding-left: 20px;
        padding-right: 20px;
        font-size: 16px;
    }
    .mobile_page .download_app {
        margin-top: 20px;
        text-align: center;
        line-height: 40px;
    }
    .mobile_page .input-group.send-link {
        margin-top: 18px;
        padding-right: 20px;
    }
    .mobile_page  .sec-title.text-center {
        color: #fff;
        margin-bottom: 20px;
    }
    .mobile_page .mobile_requirements {
        margin-left: -30px;
        margin-right: -30px;
        padding-bottom: 45px;
        background-size: cover;
    }
    .mobile_page .android_ios{
        margin-left: 30px;
        margin-right: 30px;
    }
    .mobile_page .operating_system {
        background-color: #fff;
        text-align: center;
    }
    .mobile_page .operating_system .img-responsive {
        margin-left: auto;
        margin-right: auto;
    }
    .mobile_page  .operating_system h4 {
        color: #007d3b;
        background-color: #eaf3ee;
        padding: 20px;
        font-size: 20px;
    }
    .mobile_page .operating_system p {
        text-align: center;
        color: #9b9b9b;
        font-size: 15px;
        padding: 20px;
    }
    .mobile_page  .operating_system h5 {
        font-weight: lighter;
        font-family: unset;
        text-align: left;
        line-height: 30px;
        margin-left: -15px;
    }
    .mobile_page  .col-md-4:nth-child(2) .flash {
        padding-left: 30px;
        padding-right: 30px;   
    }
    .mobile_page  .col-md-4:first-child .operating_system .img-responsive {  
        height: 100px;
        object-fit: cover;
    }
    .mobile_page .fa.fa-paper-plane-o{
        display:none;
    }
    .mobile_page .nav.nav-pills li a p{
        text-align: center;
        font-size: 18px;
        padding-top: 2px;
        padding-bottom: 4px;
        margin: unset;
    }
    .mobile_page .nav.nav-pills li a  p .fa{
        font-size: 25px;
        margin-right: 10px;
        position: relative;
        top: 2px;
    }
    .mobile_page .nav-pills > li.active a,  .mobile_page .nav-pills > li a:hover,  .mobile_page .nav-pills > li a:focus,  .mobile_page .nav-pills > li.active a:hover,  .mobile_page .nav-pills > li.active a:focus{  
        color: #fff;
        background-color: #3ab54a;
    }
    .mobile_page .nav-pills > li {   
        width: 50%;
    }
    .mobile_page .nav-pills > li img {
        display: block;  
        position: absolute;
    }
    .mobile_page .nav.nav-pills li:first-child a {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }
    .mobile_page .nav.nav-pills li:last-child a {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
    }
    .mobile_page .nav.nav-pills {
        border: 1px solid #3ab54a;
        border-radius: 6px;
        display: flex;
        -webkit-box-shadow: -4px 15px 18px -3px rgba(0,0,0,0.75);
        -moz-box-shadow: -4px 15px 18px -3px rgba(0,0,0,0.75);
        box-shadow: -4px 15px 18px -3px rgba(0, 0, 0, 0.31);
    }
    .mobile_page .playstore{
        margin-left: auto;
        margin-right: auto;
        display: block;
        margin-top: 30px;
        border-radius: 10px;
        border: 5px solid #fff;
        -webkit-box-shadow: -1px 0px 18px 1px rgba(0,0,0,0.75);
        -moz-box-shadow: -1px 0px 18px 1px rgba(0,0,0,0.75);
        box-shadow: -1px 0px 18px 1px rgba(0, 0, 0, 0.24);
        position: relative;
        z-index: 9;
    }
    .mobile_page .img-responsive.play_button {
        z-index: 99;
    }
    /*--media max 991px-*/   
    .mobile_page .img-responsive.mobile-img {   
        position: relative;
        z-index: 9;
    }
    @media (min-width:992px){
        .mobile_page .app_sms .img-responsive {   
            margin-left: 20px;
        }
        .mobile_page   .col-md-4:last-child .operating_system .img-responsive {
            margin-top: 50px;
        }
        .mobile_page  .col-md-4:nth-child(2) .flash {   
            padding-top: 40px;
        }
        .mobile_page   .col-md-4:last-child h5 {
            margin-left: -30px;
            padding-right: 21px;
            margin-top: 15px;
        }
        .mobile_page .android_div  {
            padding: 15px 40px;
        }
        .mobile_page .img-responsive.play_button {
            position: absolute;
            left: 44%;
            margin-top: -337px;
        }
        .mobile_page .background_video{
            height: 400px;
            margin-left: -30px;
            margin-right: -30px;
            position: relative;
            margin-top: -280px;
            margin-bottom: -30px;
        }
        .mobile_page .mobile_requirements { 
            position: relative;
            margin-top: -160px;
            padding-top: 120px;
            margin-bottom: 40px;
        }
    }
    @media (max-width:991px){
        .mobile_page .operating_system h5 {
            text-align: center;
            margin-left: unset !important;
            padding-left: 20px;
            padding-right: 20px;
        }          
        .mobile_page .mobile_requirements {
            position: unset;
            padding-top: 20px;
        }
        .mobile_page .text-center.rummy_subtitle {
            line-height: 35px;
        }
        .mobile_page .double-shadow.text-dark.text-normal {
            line-height: 50px;
        }    
        .mobile_page .background_video{
            display:none;
        }
    }
    @media (max-width:500px){
        .mobile_page .btn.btn-default {
            font-size: 0 !important;
            padding-left: 15px !important;
            padding-right: 15px !important;
        }
        .mobile_page .fa.fa-paper-plane-o {
            font-size: 17px;
            display:block;
        }
        .mobile_page .nav.nav-pills li a p {
            font-size: 15px;
            padding-top: 2px;
        }
    }
    @media (min-width:992px) and (max-width:1200px){
        .mobile_page .android_div {
            padding: unset !important;
        }
    }
    @media (max-width:800px){
        .mobile_page .img-responsive.play_button {
            position: absolute;
            margin-top: -24%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    }
    @media (min-width:801px)  and (max-width:991px){
        .mobile_page .img-responsive.play_button {
            position: absolute;
            margin-top: -227px;  
            left: 50%;
            transform: translate(-50%, -40px);
        }
    }
    @media (min-width:992px) and (max-width:1199px){
        .mobile_page .img-responsive.play_button {
            position: absolute;
            left: 44%;
            margin-top: -323px;
        }
        .mobile_page .send-link .form-control {
            font-size: 13px;
            padding-left: 10px;
        }
        .mobile_page .download_app {
            font-size: 20px;
        }
    }
    @media (min-width:1200px){
        .mobile_page .img-responsive.play_button {
            position: absolute;
            left: 45%;
            margin-top: -340px;
        }
    }
</style>
<script>
    jQuery(function () {
        jQuery('.operating_system').matchHeight();
    });
</script>