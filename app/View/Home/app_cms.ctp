<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="clearfix menu-content">
        <div class="col-xs-3 match-height">
            <?php echo $this->element('app_user_sidebar'); ?>
        </div>
        <div class="col-xs-9 menu-content-right alt-menu-right  match-height">
            <div class="app-page-inner bx-shadow mb-5">
                <div class="page-title pb-5 mb-5 pt-3">
                    <h2 class="double-shadow m-0"><?php echo $page['Staticpage']['pagename']; ?></h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="article">
                            <?php echo $page['Staticpage']['page_content']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>