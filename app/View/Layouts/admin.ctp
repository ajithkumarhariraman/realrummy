<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $setting = ClassRegistry::init('Sitesetting')->find('first'); ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $setting['Sitesetting']['site_title']; ?></title>
        <link rel="icon" href="<?php echo BASE_URL ?>/img/<?php echo $setting['Sitesetting']['fav_icon']; ?>" type="image/png" sizes="16x16"> 
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/colors.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/font.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/validationEngine.jquery.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?php echo WEBROOT; ?>css/chosen.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo WEBROOT; ?>back/css/sweetalert.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" rel="stylesheet" type="text/css">

        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/core/libraries/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/plugins/loaders/blockui.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/plugins/tables/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/ckeditor4/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/uploaders/fileinput.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back//js/plugins/forms/inputs/formatter.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/pages/form_inputs.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/plugins/media/fancybox.min.js"></script>
        <!-- Theme JS files -->
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/core/app.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/pages/gallery.js"></script>
        <script src="<?php echo WEBROOT; ?>js/chosen.jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo WEBROOT; ?>back/js/sweetalert.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js" type="text/javascript"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js" type="text/javascript"></script> 


        <!-- /theme JS files -->
        <style>
            .select2.select2-container {
                width: 100% !important;
            }
            .select2-selection--multiple .select2-selection__choice {
                background-color: #455A64;
                color: #333;
            }
        </style>
    </head>

    <body class="">
        <!-- Main navbar -->
        <?php echo $this->Element('admin_header'); ?>
        <!-- /main navbar -->
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                <?php echo $this->Element('admin_sidebar'); ?>
                <!-- /main sidebar -->
                <!-- Main content -->
                <?php
                $success = $this->Session->flash('success');
                if (!empty($success)) {
                    ?>
                    <script>
                        swal("Success!", "<?php echo $success; ?>", "success")
                    </script>
                <?php } ?>
                <?php
                $danger = $this->Session->flash('danger');
                if (!empty($danger)) {
                    ?>
                    <script>
                        swal("Error!", "<?php echo $danger; ?>", "error")
                    </script>
                <?php } ?>
                <?php echo $this->fetch('content'); ?>
                <!-- /main content -->
            </div>
            <div class="modal fade delete_modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            You sure want to delete this?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                            <a href="#" class="btn btn-primary dlt_href">Yes</a>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(".delconfirm").on('click', function (e) {
                    e.preventDefault();
                    var href = $(this).attr('href');
                    var title = "Delete Confirmation";
                    $('.dlt_href').attr('href', href);
                    $('.delete_modal').modal('show');
                });
                $('.validation_form').validationEngine({scroll: false});
                $('#data-table').dataTable();
                $('.datepicker').datepicker({format: 'dd-mm-yyyy'});
                $(document).ready(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                });

                $("#flashMessage").click(function () {
                    $("#flashMessage").fadeOut(1000);
                });
                setTimeout(function () {
                    $('#flashMessage').fadeOut(1000);
                }, 5000);

                CKEDITOR.replace('editor');
                CKEDITOR.instances.editor.on('change', function () {
                    $('#editor').val(CKEDITOR.instances.editor.getData())
                });

            </script>
            <script>
                // Primary
                $(".control-primary").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-primary-600 text-primary-800'
                });
                // Danger
                $(".control-danger").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-danger-600 text-danger-800'
                });
                $(".chosen").chosen({no_results_text: "Oops, nothing found!"});
            </script>
            <!-- /page content -->
        </div>
        <!-- /page container -->
    </body>
</html>
