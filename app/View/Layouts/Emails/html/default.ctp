<?php $settings = ClassRegistry::init('Sitesetting')->find('first'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo $settings['Sitesetting']['site_title']; ?></title>
        <meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
    </head>
    <body>
        <style>
            p{
                margin-top: 2px;
                margin-bottom: 2px;
            }
        </style>
        <div class="wrapper" style="width: 600px;margin: 0 auto;font-family: calibri;font-size: 14px;">
            <div style="background: #00A32E;padding: 10px 10px;color:#fff">
                <ul style="list-style-type:none;margin: 0;">
                    <li style="display:inline-block;"><a style="color:#fff" href="<?php echo BASE_URL; ?>">Home</a></li>
                    <li style="display:inline-block;">|</li>
                    <li style="display:inline-block;"><a style="color:#fff" href="<?php echo BASE_URL; ?>home/promotions">Promotions</a></li>
                    <li style="display:inline-block;">|</li>
                    <li style="display:inline-block;"><a style="color:#fff" href="<?php echo BASE_URL; ?>contacts">Support</a></li>
                </ul>
            </div>
            <img src="<?php echo BASE_URL; ?>img/emailbanner.png" alt="logo"  border="0" style="width: 100%;height: 250px;object-fit: cover"/>
            <div style="margin-top: 15px;margin-bottom: 15px">
                <?php echo $this->fetch('content'); ?>
            </div>
            <p>Regards,</p>
            <p>Rummy Club Team</p>
            <div style="background: #00A32E;padding: 10px 10px;color:#fff;">
                <table style="border:none;width:100%">
                    <tr>
                        <td>
                            <p style="color:#fff;"><img style="max-width: 17px;vertical-align: middle;margin-right: 10px;" src="<?php echo BASE_URL; ?>img/mail.png"/> <a style="color:#fff;">support@rummyclub.com</a></p>
                            <p style="color:#fff;"><img style="max-width: 17px;vertical-align: middle;margin-right: 10px;" src="<?php echo BASE_URL; ?>img/call.png"/> +91 9876543210</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul style="list-style-type:none;margin:0;text-align: center">
                                <li style="display:inline-block;"><a href="<?php echo BASE_URL; ?>"><img style="max-width: 30px;" src="<?php echo BASE_URL; ?>img/facebook1.png"/></a></li>
                                <li style="display:inline-block;"><a href="<?php echo BASE_URL; ?>"><img style="max-width: 30px;" src="<?php echo BASE_URL; ?>img/twitter1.png"/></a></li>
                                <li style="display:inline-block;"><a href="<?php echo BASE_URL; ?>"><img style="max-width: 30px;" src="<?php echo BASE_URL; ?>img/instag.png"/></a></li>
                                <li style="display:inline-block;"><a href="<?php echo BASE_URL; ?>"><img style="max-width: 30px;" src="<?php echo BASE_URL; ?>img/pin1.png"/></a></li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
