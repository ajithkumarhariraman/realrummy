<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="Mohana">
        <?php
        $sitesetting = Classregistry::init('Sitesetting')->find('first');
        ?>
        <title><?php echo $sitesetting['Sitesetting']['site_title']; ?></title>
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap.min.css"/>
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/owl.theme.default.min.css"/>

        <link rel="stylesheet" href="<?php echo BASE_URL; ?>back/css/sweetalert.css"/>
        <link rel="shortcut icon" type="image/png" href="<?php echo BASE_URL; ?>img/<?php echo $sitesetting['Sitesetting']['fav_icon']; ?>">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i" rel="stylesheet"> 
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/line-awesome.min.css">
        <link href="<?php echo BASE_URL; ?>back/css/validationEngine.jquery.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/style.css"/>
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/app.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="<?php echo BASE_URL; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo BASE_URL; ?>js/owl.carousel.min.js"></script>
        <script src="<?php echo BASE_URL; ?>js/jquery.matchHeight.js"></script>
        <script src="<?php echo BASE_URL; ?>back/js/sweetalert.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>back/js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>back/js/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>js/uniform.min.js"></script>


    </head>
    <body data-spy="scroll" data-target="#navigation" data-offset="71" id="applayout">
        <?php //echo $this->Element('header'); ?>
        <!-- Page container -->
        <?php
        $success = $this->Session->flash('front_success');
        if (!empty($success)) {
            ?>
            <script>
                swal("Success!", "<?php echo $success; ?>", "success")
            </script>
        <?php } ?>
        <?php
        $danger = $this->Session->flash('front_danger');
        if (!empty($danger)) {
            ?>
            <script>
                swal("Error!", "<?php echo $danger; ?>", "error")
            </script>
        <?php } ?>
        <?php echo $this->fetch('content'); ?>
        <?php //echo $this->Element('footer'); ?>    
        <div id="loading" style="display:none;">
            <img src="https://thumbs.gfycat.com/DigitalHomelyAuk-max-1mb.gif"/>
        </div>


    </body>
    <script>
        $('.match-height').matchHeight();
        $('.validation_form').validationEngine({scroll: false});
        setTimeout(function () {
            $('#flashMessage').fadeOut(1000);
        }, 5000);
        $(".control-success").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-success-600 text-success-800'
        });
    </script>
</html>
