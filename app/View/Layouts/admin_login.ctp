<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $setting = ClassRegistry::init('Sitesetting')->find('first'); ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $setting['Sitesetting']['site_title']; ?></title>
        <link rel="icon" href="<?php echo BASE_URL ?>/img/<?php echo $setting['Sitesetting']['fav_icon']; ?>" type="image/png" sizes="16x16">

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/colors.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/font.css" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBROOT; ?>back/css/validationEngine.jquery.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/plugins/loaders/blockui.min.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/jquery.validationEngine-en.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="<?php echo WEBROOT; ?>back/js/core/app.js"></script>
        <!-- /theme JS files -->

    </head>

    <body class="login-cover">

        <!-- Main navbar -->
        <?php //echo $this->Element('admin_header'); ?>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container login-container" style="min-height:391px">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <?php //echo $this->Element('admin_sidebar'); ?>
                <!-- /main sidebar -->


                <!-- Main content -->
                <?php
                $success = $this->Session->flash('admin_success');
                if (!empty($success)) {
                    ?><div class="alert alert-success alert-styled-right alert-arrow-right alert-bordered" id="flashMessage"><?php echo $success; ?></div>
                <?php } ?>
                <?php
                $danger = $this->Session->flash('admin_danger');
                if (!empty($danger)) {
                    ?><div class="alert alert-danger alert-styled-right alert-bordered" id="flashMessage"><?php echo $danger; ?></div>
                <?php } ?>
                <?php echo $this->fetch('content'); ?>
                <!-- /main content -->

            </div>
            <!-- /page content -->
            <script>
                $('.validation_form').validationEngine({scroll: false});
                $("#flashMessage").click(function () {
                    $("#flashMessage").fadeOut(1000);
                });
                setTimeout(function () {
                    $('#flashMessage').fadeOut(1000);
                }, 5000);
            </script>
            <style>
                div#flashMessage {
                    white-space: nowrap;
                        width: 320px;
                }
                .alert-danger {
             
                    margin-top: 20px;
                  
                }
            </style>
        </div>
        <!-- /page container -->

    </body>
</html>
