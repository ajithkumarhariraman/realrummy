<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Promotions</span> - List
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/promotions/add" class="backcss btn btn-primary"><i class="fa fa-plus"></i> Add</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="5%"> # </th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Play now url</th>
                        <th width="5%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (empty($promotions)) {
                        ?>
                        <tr><td colspan="10" align="center">No records found</td>
                            <?php
                        } else {
                            $i = $this->Paginator->counter('{:start}');
                            foreach ($promotions as $promotion) {
                                ?>
                            <tr>
                                <td class="center"><?php echo $i; ?></td>
                                <td><?php echo $promotion['Promotion']['title']; ?></td>
                                <td>
                                  <img class="main-logo" src="<?php echo WEBROOT; ?>img/<?php echo $promotion['Promotion']['image']; ?>" alt="" />  
                                </td>
                                  <td>
                                    <?php echo $promotion['Promotion']['play_now_url']; ?>
                                </td>
                                <td class="actions" style="white-space: nowrap;">                           
                            <a data-toggle="tooltip" title="Edit" href="<?php echo BASE_URL; ?>admin/promotions/edit/<?php echo $promotion['Promotion']['promotion_id']; ?>" class="btn btn-secondary"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('controller' => 'promotions', 'action' => 'Delete', $promotion['Promotion']['promotion_id']), array('escapeTitle' => false, "data-toggle" => "tooltip", 'class' => '', "title" => "Delete", "onclick" => "return confirm('Are you sure you want to Delete?');")  ); ?>
                        </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">
                    <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                        ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                        <ul class="pagination">
                            <?php
                            echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                            $numbers = $this->Paginator->numbers();
                            if (empty($numbers)) {
                                echo '<li class="active page-link"><a>1</a></li>';
                            } else {
                                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                            }
                            echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                            ?>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .main-logo {
    width: 200px;
    height: 100px;
    object-fit: cover;
}
    </style>