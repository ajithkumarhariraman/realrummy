<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Promotion</span> - Add
            </h4>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <fieldset class="content-group">
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Title <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text"  class="form-control validate[required]" name="data[Promotion][title]"  />
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Description <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <textarea name="data[Promotion][description]" class="form-control validate[required]"></textarea>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Image <span class="required">*</span></label>
                            <div class="col-lg-5">
                                <input type="file" class="form-control file-upload validate[optional,custom[image]]" name="data[Promotion][image]"/>                               
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Content <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <textarea name="data[Promotion][content]" class="form-control validate[required]" id="editor"></textarea>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Play Now url <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text"  class="form-control validate[required]" name="data[Promotion][play_now_url]"  />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn bg-teal" type="submit"> Submit </button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .file-upload {
        padding-bottom: 42px;
    }
</style>
