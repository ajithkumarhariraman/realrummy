<div class="bx-shadow-2 user-header">
    <div class="container">
        <?php echo $this->element('app_user_header'); ?>
    </div>
</div>
<div class="container">
    <div class="clearfix menu-content">
        <div class="col-xs-3 match-height">
            <?php echo $this->element('app_user_sidebar'); ?>
        </div>
        <div class="col-xs-9 menu-content-right alt-menu-right  match-height">
            <?php
            if (empty($promotions)) {
                ?>
                <h2 class="double-shadow m-0 no-promossions">NO PROMOTIONS</h2>
            <?php } else { ?>
                <div class="page-inner bx-shadow mb-5">
                    <div class="page-title pb-5 mb-5 pt-3">
                        <h2 class="double-shadow m-0">PROMOTIONS</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">
                            <div class="row promotions">
                                <?php foreach ($promotions as $promotion) { ?>
                                    <div class="col-md-6">
                                        <div class="promotion-list">
                                            <a href="<?php echo BASE_URL; ?>promotions/detail/<?php echo $promotion['Promotion']['promotion_id']; ?>"> <img class="main-logo" src="<?php echo WEBROOT; ?>img/<?php echo $promotion['Promotion']['image']; ?>" alt="" /> </a>
                                            <a href="<?php echo BASE_URL; ?>promotions/detail/<?php echo $promotion['Promotion']['promotion_id']; ?>"><h3><?php echo $promotion['Promotion']['title']; ?></h3></a>
                                            <a href="<?php echo BASE_URL; ?>promotions/detail/<?php echo $promotion['Promotion']['promotion_id']; ?>"><p> <?php
                                                    if (!empty($promotion['Promotion']['description'])) {
                                                        echo substr($promotion['Promotion']['description'], 0, 120);
                                                        if ($numlength = mb_strlen($promotion['Promotion']['description']) >= 120) {
                                                            echo '...';
                                                        }
                                                    } else {
                                                        echo "-";
                                                    }
                                                    ?></p></a>

                                            <a href="<?php echo BASE_URL; ?>promotions/app_detail/<?php echo $promotion['Promotion']['promotion_id']; ?>?user_id=<?php echo $_REQUEST['user_id']; ?>" class="btn know-more">Know More</a>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<style>
    .main-logo {
        width: 100%;
        height: 200px;
        object-fit: cover;
    }
    .promotion-list {
        border: 1px solid #e4e4e4;
        padding-bottom: 50px;
        margin-bottom: 25px;
    }
    .promotion-list h3 {
        color: #424242;
        margin-left: 20px;
        margin-top: 20px;
        margin-bottom: 15px;
    }
    .promotion-list p {
        color: #5e5e5e;
        margin-left: 20px;
        margin-right: 20px;
        line-height: 25px;
    }
    .btn.know-more {
        float: right;
        border: 1px solid #bb0106;
        border-radius: 0;
        background-color: #bb0106;
        color: #fff;
        margin-right: 20px;
        font-weight: 500;
    }
    .promotions {
        padding-bottom: 150px;
    }
    .no-promossions{
        margin-bottom: 75px !important;
        margin-top: 50px !important;
    }
    .promotions a:hover{
        text-decoration: none !important;
    }
    .promotions a:hover{
        outline: none !important;
    }
</style>
<script>
    jQuery(function () {
        jQuery('.promotion-list p').matchHeight();
    });
</script>
<script>
    jQuery(function () {
        jQuery('.promotion-list h3').matchHeight();
    });
</script>