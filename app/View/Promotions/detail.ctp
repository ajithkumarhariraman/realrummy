<div class="container">
    <div class="page-inner bx-shadow mb-5 detail_page">
        <div class="page-title pb-5 mb-5 pt-3">
            <h2 class="double-shadow m-0">PROMOTIONS</h2>
        </div>
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <h3 class="text-success-2 mb-5 mt-0"><?php echo!empty($results['Promotion']['title']) ? h($results['Promotion']['title']) : '-'; ?></h3>
                <img class="main-logo" src="<?php echo WEBROOT; ?>img/<?php echo $results['Promotion']['image']; ?>"/>
                <p class="detail_description"><?php echo!empty($results['Promotion']['description']) ? h($results['Promotion']['description']) : '-'; ?></p>
                <?php echo!empty($results['Promotion']['content']) ? ($results['Promotion']['content']) : '-'; ?>
                <?php if (!empty($results['Promotion']['play_now_url'])) { ?>
                    <a class="play_now_btn" target="_blank" href="<?php echo $results['Promotion']['play_now_url']; ?>">Play Now</a>
                <?php } ?>
                <p>* T & C Apply</p>
            </div>
        </div>
    </div>
</div>


<style>
    .main-logo {
        width: 100%;
        height: 250px;
        object-fit: cover;
    }
    .detail_description {
        color: #656565;
        margin-top: 25px;
        margin-bottom: 25px;
        line-height: 25px;
        padding-right: 25px;
    }
    .detail_page h3 {
        margin-bottom: 25px;
    }
    .detail_page thead th {
        padding: 15px 30px !important;
    }
    .detail_page td {
        padding-left: 30px !important;
    }
    .detail_page tfoot {
        background-color: #ededed;
    }
    .detail_page tfoot td:last-child {
        font-size:25px;
    }
    .detail_page tfoot td:last-child {
        font-size: 22px !important;
    }
    .detail_page tfoot td{
        font-size: 15px !important;
        color:#000;
        font-weight:500;
    }
    .detail_page th, .detail_page td{
        font-size: 15px !important;
    }
    .detail_page .terms_conditions p {
        color: #707070;
        font-weight: lighter;
    }
    .detail_page .terms_conditions p strong {
        color:#000;
        margin-right: 5px;
    }
    .detail_page .terms_conditions {
        margin-top: -10px;
    }
    .detail_page .play_now_btn {
        margin-left: auto;
        margin-right: auto;
        display: block;
        width: 120px;
        background-color: #9d0b0e;
        text-align: center;
        color: #fff !important;
        border: 1px solid #9d0b0e;
        padding: 9px 20px;
        margin-top: 30px;
    }
    .detail_page a:hover{
        text-decoration: none !important;
    }
    .detail_page table{
        margin-bottom: 10px;
    }
    .detail_page  tfoot tr td:last-child{
        position: relative;
        left: -40px;  
        display: block;
    }
    @media (max-width:991px){
        .detail_page p, .detail_page h3{
            line-height: 30px;
        }
        .detail_page .play_now_btn {
            margin-bottom: 30px;
        }
    }
</style>
