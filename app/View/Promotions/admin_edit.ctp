<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Promotion</span> - Edit
            </h4>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <fieldset class="content-group">
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Title <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text"  class="form-control validate[required]" name="data[Promotion][title]" value="<?php echo (!empty($this->request->data['Promotion']['title'])) ? $this->request->data['Promotion']['title'] : ""; ?>"/>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Description <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <textarea name="data[Promotion][description]" class="form-control validate[required]"><?php echo (!empty($this->request->data['Promotion']['description'])) ? $this->request->data['Promotion']['description'] : ""; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Image <span class="required">*</span></label>
                            <div class="col-lg-5">
                                <input type="file" class="form-control file-upload validate[optional,custom[image]]" name="data[Promotion][image]"/>                               
                            </div>
                            <div class="col-md-2">
                            <img src="<?php echo BASE_URL; ?>img/<?php echo $this->request->data['Promotion']['image']; ?>" class="img-responsive" style="object-fit: cover; height: 50px; width: 200px;"/>
                        </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Content <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <textarea name="data[Promotion][content]" class="form-control validate[required]" id="promotion"><?php echo (!empty($this->request->data['Promotion']['content'])) ? $this->request->data['Promotion']['content'] : ""; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Play Now url <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text"  class="form-control validate[required]" name="data[Promotion][play_now_url]" value="<?php echo (!empty($this->request->data['Promotion']['play_now_url'])) ? $this->request->data['Promotion']['play_now_url'] : ""; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn bg-teal" type="submit"> Submit </button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .file-upload {
        padding-bottom: 42px;
    }
</style>
<script>
     CKEDITOR.replace('promotion');
                CKEDITOR.instances.editor.on('change', function () {
                    $('#promotion').val(CKEDITOR.instances.promotion.getData())
                });
    </script>