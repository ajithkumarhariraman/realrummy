<div class="single-pro-review-area mt-t-30 mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-xs-12">
                <div class="profile-info-inner">
                    <div class="profile-img">
                        <img src="<?php echo WEBROOT; ?>img/<?php echo!empty($results['Librarybook']['photo']) ? h($results['Librarybook']['photo']) : '-'; ?>" alt="">
                    </div>                     
                </div>
            </div>
            <div class="col-md-9">
                <table class="table-responsive product-status-wrap">
                    <tbody>
                        <tr>
                            <th>Library Book Name</th>                           
                            <th><?php echo!empty($results['Librarybook']['book_name']) ? h($results['Librarybook']['book_name']) : '-'; ?></th>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <?php if($results['Librarybook']['status']=='Active'){?>
                            <td><button class="pd-setting">Active</button></td>
                            <?php } else {?>
                            <td><button class="ds-setting">Inactive</button></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Subject</td>                           
                            <td><?php echo!empty($results['Librarybook']['subject']) ? h($results['Librarybook']['subject']) : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Book Type</td>                           
                            <td><?php echo!empty($results['Librarybook']['type']) ? h($results['Librarybook']['type']) : '-'; ?></td>
                        </tr>
                      
                          </tbody>

                </table>
            </div>

        </div>
    </div>
</div>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
 padding: 15px !important;
  width: 50%;
}

tr:nth-child(even) {
 background-color: #efefef;
}
.footer-copyright-area {
    margin-top: 160px;
}
</style>