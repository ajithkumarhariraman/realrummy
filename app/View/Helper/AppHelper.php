<?php

/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {

    public function read_more($string, $length) {
        $string = strip_tags($string);
        if (strlen($string) > $length) {
            $stringCut = substr($string, 0, $length);
            $string = substr($stringCut, 0, strrpos($stringCut, ' '));
        }
        return $string;
    }

    public function date($date) {
        return date('d-m-Y', strtotime($date));
    }

    function share_link($url = "", $provide = "") {
        switch ($provide) {
            case "fb":
                $return_url = sprintf('https://www.facebook.com/sharer/sharer.php?u=%s', $url);
                break;
            case "tw":
                $return_url = sprintf('https://twitter.com/home?status=%s', $url);
                break;
            case "gp":
                $return_url = sprintf('https://plus.google.com/share?url=%s', $url);
                break;
            case "li":
                $return_url = sprintf('https://www.linkedin.com/shareArticle?mini=true&url=%s&title=&summary=&source=', $url);
                break;
            case "pr":
                $return_url = sprintf('https://pinterest.com/pin/create/button/?url=%s&media=&description=', $url);
                break;
        }
        return $return_url;
    }

}
