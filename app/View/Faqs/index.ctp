<div class="container">
    <div class="page-inner bx-shadow mb-5">
        <div class="page-title pb-5 mb-5 pt-3">
            <h2 class="double-shadow m-0">FREQUENTLY ASKED QUESTIONS</h2>
        </div>
        <div class="row menu-content">
            <div class="col-md-3 match-height">
                <ul class="side-menu list-unstyled">
                    <?php foreach ($categories as $category) { ?>
                        <li class="<?php echo ($category['Faqcategory']['category_id'] == $c_category['Faqcategory']['category_id']) ? "active" : "" ?>"><a href="<?php echo BASE_URL; ?>faqs/index/<?php echo $category['Faqcategory']['category_id'] ?>"><?php echo $category['Faqcategory']['title'] ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-md-9 menu-content-right match-height">
                <h2 class="text-success-2 mb-5 mt-0"><?php echo $c_category['Faqcategory']['title']; ?></h2>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php
                    if (!empty($faqs)) {
                        foreach ($faqs as $faq) {
                            ?>
                            <div class="panel panel-default mb-4">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a class="text-dark" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq['Faq']['faq_id'] ?>" aria-expanded="true" aria-controls="collapse<?php echo $faq['Faq']['faq_id'] ?>">
                                            <?php echo $faq['Faq']['title'] ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse<?php echo $faq['Faq']['faq_id'] ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body article">
                                        <?php echo $faq['Faq']['description'] ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <div class="nodata-found text-center">
                            <img src="<?php echo BASE_URL; ?>img/no-data.png"/>
                            <p class="h5">No Data Found</p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.match-height').matchHeight();
</script>