<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Faq</span>  - Add
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/faqs" class="backcss"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Faq Title <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <input type="text"  class="form-control validate[required]" name="data[Faq][title]" value="<?php echo (!empty($this->request->data['Faq']['title'])) ? $this->request->data['Faq']['title'] : "" ?>" />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Faq Description <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <textarea type="text"  class="form-control validate[required]" name="data[Faq][description]" value="<?php echo (!empty($this->request->data['Faq']['description'])) ? $this->request->data['Faq']['description'] : "" ?>"></textarea>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> Faq Category <span class="required">*</span></label>
                        <div class="col-lg-5">
                            <select class="form-control validate[required]" name="data[Faq][category_id]">
                                <option value="">Select</option>
                                <?php foreach ($categories as $category) { ?>
                                    <option <?php echo (!empty($this->request->data['Faq']['category_id']) && $this->request->data['Faq']['category_id'] == $category['Faqcategory']['category_id']) ? "selected" : "" ?> value="<?php echo $category['Faqcategory']['category_id']; ?>"><?php echo $category['Faqcategory']['title']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-lg-4 control-label"> &nbsp;</label>
                        <div class="col-lg-5">
                            <button class="btn bg-teal" type="submit"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('.ui_datepicker').datepicker({format: 'dd-mm-yyyy'})
</script>