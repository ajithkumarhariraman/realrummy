<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Promocodes</span> - List
            </h4>
            <a href="<?php echo BASE_URL; ?>admin/promocodes/add" class="backcss btn btn-primary"><i class="fa fa-plus"></i> Add</a>
        </div>
    </div>
</div>
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="5%"> # </th>
                        <th>Promo Type</th>
                        <th>Code</th>
                        <th>Description</th>
                        <th>Available From</th>
                        <th>Available To</th>
                        <th>Discount Type</th>
                        <th>Discount Value</th>
                        <th>Discount To</th>
                        <th width="5%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (empty($promocodes)) {
                        ?>
                        <tr><td colspan="10" align="center">No records found</td>
                            <?php
                        } else {
                            $i = $this->Paginator->counter('{:start}');
                            foreach ($promocodes as $promocode) {
                                ?>
                            <tr>
                                <td class="center"><?php echo $i; ?></td>
                                <td><?php echo $promocode['Promocode']['promotype']; ?></td>
                                <td><?php echo $promocode['Promocode']['code']; ?></td>
                                <td><?php echo $promocode['Promocode']['description']; ?></td>
                                <td><?php echo (!empty($promocode['Promocode']['available_from'])) ? date('d-m-Y', strtotime($promocode['Promocode']['available_from'])) : "-"; ?></td>
                                <td><?php echo (!empty($promocode['Promocode']['available_to'])) ? date('d-m-Y', strtotime($promocode['Promocode']['available_to'])) : "-"; ?></td>
                                <td><?php echo $promocode['Promocode']['discount_type']; ?></td>
                                <td><?php echo $promocode['Promocode']['discount_value']; ?></td>
                                <td><?php echo $promocode['Promocode']['discount_to']; ?></td>
                                <td class="actions" style="white-space: nowrap;"> 
                                    <a data-toggle="tooltip" title="Edit" href="<?php echo BASE_URL; ?>admin/promocodes/edit/<?php echo $promocode['Promocode']['promocode_id']; ?>" class="btn btn-secondary"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a data-toggle="tooltip" title="Delete" href="<?php echo BASE_URL; ?>admin/promocodes/delete/<?php echo $promocode['Promocode']['promocode_id']; ?>" class="btn btn-secondary delconfirm"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">
                    <div class="dataTables_info" id="sample-table-2_info" role="status" aria-live="polite">
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Page') . ' {:page} ' . __('of') . ' {:pages}, ' . __('showing') . ' {:current} ' . __('records out of') . ' {:count} ' . __('entries')
                        ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_paginate paging_simple_numbers pull-right" id="sample-table-2_paginate">
                        <ul class="pagination">
                            <?php
                            echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-left"></i></a>', array('class' => 'prev disabled page-link', 'tag' => 'li', 'escape' => false));
                            $numbers = $this->Paginator->numbers();
                            if (empty($numbers)) {
                                echo '<li class="active page-link"><a>1</a></li>';
                            } else {
                                echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'class' => 'page-link', 'first' => 'First page', 'currentClass' => 'active', 'currentTag' => 'a'));
                            }
                            echo $this->Paginator->next('<i class="fa fa-angle-right"></i>', array('tag' => 'li', 'class' => 'page-link', 'escape' => false), '<a><i class="fa fa-angle-right"></i></a>', array('class' => 'next disabled page-link', 'tag' => 'li', 'escape' => false));
                            ?>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .main-logo {
        width: 200px;
        height: 100px;
        object-fit: cover;
    }
</style>