<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-arrow-left52 position-left"></i>
                <span class="text-semibold">Promotion</span> - Add
            </h4>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="" class="form-horizontal validation_form" method="post" enctype="multipart/form-data">
                    <fieldset class="content-group">
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Promotype <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <select class="form-control validate[required]" name="data[Promocode][promotype]">
                                    <option value="">[--Select Promotype--</option>
                                    <option value="Weekly Bonus" <?php echo (!empty($this->request->data['Promocode']['promotype']) && $this->request->data['Promocode']['promotype'] == 'Weekly Bonus') ? 'selected' : '' ?>>Weekly Bonus</option>
                                    <option value="Festival Bonus" <?php echo (!empty($this->request->data['Promocode']['promotype']) && $this->request->data['Promocode']['promotype'] == 'Festival Bonus') ? 'selected' : '' ?>>Festival Bonus</option>
                                    <option value="First Payment Bonus" <?php echo (!empty($this->request->data['Promocode']['promotype']) && $this->request->data['Promocode']['promotype'] == 'First Payment Bonus') ? 'selected' : '' ?>>First Payment Bonus</option>
                                    <option value="Special  Bonus" <?php echo (!empty($this->request->data['Promocode']['promotype']) && $this->request->data['Promocode']['promotype'] == 'Special Bonus') ? 'selected' : '' ?>>Special Bonus</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Promo Code <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control validate[required]" name="data[Promocode][code]" value="<?php echo (!empty($this->request->data['Promocode']['code'])) ? $this->request->data['Promocode']['code'] : "" ?>"/>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Description <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <textarea class="form-control validate[required]" name="data[Promocode][description]"><?php echo (!empty($this->request->data['Promocode']['description'])) ? $this->request->data['Promocode']['description'] : "" ?></textarea>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Available From <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control datepicker" name="data[Promocode][available_from]" value="<?php echo (!empty($this->request->data['Promocode']['available_from'])) ? $this->request->data['Promocode']['available_from'] : "" ?>"/>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Available To <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control datepicker" name="data[Promocode][available_to]" value="<?php echo (!empty($this->request->data['Promocode']['available_to'])) ? $this->request->data['Promocode']['available_to'] : "" ?>"/>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Discount Type <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <label><input type="radio"  class="validate[required]" name="data[Promocode][discount_type]" <?php echo (!empty($this->request->data['Promocode']['discount_type']) && $this->request->data['Promocode']['discount_type'] == 'Cash') ? "checked" : "" ?>  value="Cash" /> Cash</label>
                                <label><input type="radio" class="validate[required]" name="data[Promocode][discount_type]" <?php echo (!empty($this->request->data['Promocode']['discount_type']) && $this->request->data['Promocode']['discount_type'] == 'Percentage') ? "checked" : "" ?>  value="Percentage"/> Percentage</label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Discount Value <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control validate[required,custom[number]]" name="data[Promocode][discount_value]" value="<?php echo (!empty($this->request->data['Promocode']['discount_value'])) ? $this->request->data['Promocode']['discount_value'] : "" ?>"/>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-lg-3 control-label"> Discount To <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <select class="form-control validate[required] discount_to" name="data[Promocode][discount_to]">
                                    <option value="All Users" <?php echo (!empty($this->request->data['Promocode']['discount_to']) && $this->request->data['Promocode']['discount_to'] == 'All Users') ? 'selected' : '' ?>>All Users</option>
                                    <option value="Selected Users" <?php echo (!empty($this->request->data['Promocode']['discount_to']) && $this->request->data['Promocode']['discount_to'] == 'Selected Users') ? 'selected' : '' ?>>Selected Users</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group clearfix selected-users" style="display:none">
                            <label class="col-lg-3 control-label"> Select Users <span class="required">*</span></label>
                            <div class="col-lg-9">
                                <select class="form-contol selected_users validate[required]">
                                </select>
                                <div class="selected_userss">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn bg-teal" type="submit"> Submit </button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .file-upload {
        padding-bottom: 42px;
    }
</style>
<script>
    jQuery(document).ready(function () {
        jQuery(".selected_users").select2({
            multiple: true,
            ajax: {
                url: "<?php echo BASE_URL; ?>admin/promocodes/users",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection

        });

        function formatRepo(repo) {
            if (repo.loading)
                return repo.text;
            var markup = '<span>' + repo.name + '</span>';
            return markup;
        }

        function formatRepoSelection(repo) {
            return repo.name;
        }
    });

    jQuery(".selected_users").on("select2:select", function (e) {
        var items = jQuery(this).val();
        var lastSelectedItem = e.params.data.id;
        jQuery('.selected_userss').append('<input type="hidden" id="user_' + lastSelectedItem + '" name="data[Promocode][user][]" value="' + lastSelectedItem + '"/>');
    });
    jQuery(".selected_users").on("select2:unselect", function (e) {
        var items = jQuery(this).val();
        var lastSelectedItem = e.params.data.id;
        jQuery('#user_' + lastSelectedItem).remove();
    });
    $(document).on('change', '.discount_to', function () {
        if ($(this).val() == 'All Users') {
            $('.selected-users').hide();
        } else {
            $('.selected-users').show();
        }
    });
</script>