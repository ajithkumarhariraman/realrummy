<?php
App::uses('AppController', 'Controller');
/**
 * Adminusers Controller
 *
 * @property Adminuser $Adminuser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class DashboardController extends AppController {
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser');
    public $layout = 'front';
    /**
     * Admin_Index
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $this->layout = 'admin';
    }
}
