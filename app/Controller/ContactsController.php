<?php

App::uses('AppController', 'Controller');

/**
 * Contects Controller
 *
 * @property Contect $Contect
 * @property PaginatorComponent $Paginator
 */
class ContactsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Contact', 'Emailcontent', 'Sitesetting');
    public $layout = 'admin';

    public function index() {
        $this->layout = 'front';
        if ($this->request->is(array('post', 'put'))) {
            $recaptcha = $this->request->data['g-recaptcha-response'];
            $ip = $_SERVER['REMOTE_ADDR'];
            $url = "https://www.google.com/recaptcha/api/siteverify?secret=6LctgqoUAAAAAMDhDwsebFM2-SzNI_vQvYojaPSK&response=" . $recaptcha . "&remoteip=" . $ip;
            $validate = json_decode(file_get_contents($url), true);
            if (isset($validate['success']) && $validate['success'] == true) {
                $this->request->data['Contact']['created_date'] = date('Y-m-d H:i:s');
                $this->Contact->save($this->request->data['Contact']);
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '1')));
                $message = str_replace(array('{name}'), array($this->request->data['Contact']['name']), $emailcontent['Emailcontent']['emailcontent']);
                //$this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $this->request->data['Contact']['email'], $emailcontent['Emailcontent']['subject'], $message);
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '2')));
                $message = str_replace(array('{name}', '{email}', '{phone}', '{subject}', '{message}'), array($this->request->data['Contact']['name'], $this->request->data['Contact']['email'], $this->request->data['Contact']['phone'], $this->request->data['Contact']['subject'], $this->request->data['Contact']['message']), $emailcontent['Emailcontent']['emailcontent']);
                // $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $emailcontent['Emailcontent']['tomail'], $emailcontent['Emailcontent']['subject'], $message);
                $this->Session->setFlash('Successfully sent! Will get back to you soon!', '', array(''), 'front_success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Invalid Captcha!', '', array(''), 'front_danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $this->Contact->recursive = 0;
        $conditions = array();
        if (!empty($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('email LIKE' => "%$s%", 'name LIKE' => "%$s%", 'phone LIKE' => "%$s%", 'subject LIKE' => "%$s%");
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'contact_id DESC', 'limit' => '10');
        $this->set('results', $this->Paginator->paginate('Contact'));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->checkadmin();
        if (!$this->Contact->exists($id)) {
            throw new NotFoundException(__('Contact Not Found'));
        }
        $result = $this->Contact->find('first', array('conditions' => array('contact_id' => $id)));
        $this->set('result', $result);
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Contact->exists($id)) {
            throw new NotFoundException(__('Contact Not Found'));
        }
        $this->request->data['Contact']['contact_id'] = $id;
        if ($this->Contact->delete($id)) {
            $this->Session->setFlash('Contact deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Contact could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

}
