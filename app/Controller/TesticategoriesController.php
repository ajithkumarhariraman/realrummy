<?php

App::uses('AppController', 'Controller');

/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class TesticategoriesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Testicategory');
    public $layout = 'admin';

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $this->Testicategory->recursive = 0;
        $conditions = array('status !=' => 'Trash');
        if (!empty($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['title LIKE'] = "%$s%";
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'category_id DESC', 'limit' => '10');
        $this->set('testicategories', $this->Paginator->paginate('Testicategory'));
    }

    public function admin_add() {
        $this->checkadmin();
        if ($this->request->is('post')) {
            $check = $this->Testicategory->find('first', array('conditions' => array('title' => $this->request->data['Testicategory']['title'], 'status !=' => 'Trash')));
            if (empty($check)) {
                $this->Testicategory->save($this->request->data['Testicategory']);
                $this->Session->setFlash('Testicategory Added', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Testicategory title already exists', '', array(''), 'danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
        $testicategories = $this->Testicategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('testicategories', $testicategories);
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->checkadmin();
        if (!$this->Testicategory->exists($id)) {
            throw new NotFoundException(__('Invalid Testicategory'));
        }
        $category = $this->Testicategory->find('first', array('conditions' => array('category_id' => $id)));
        if ($this->request->is(array('post', 'put'))) {
            $check = $this->Testicategory->find('first', array('conditions' => array('title' => $this->request->data['Testicategory']['title'], 'category_id !=' => $id, 'status !=' => 'Trash')));
            if (empty($check)) {
                $this->request->data['Testicategory']['category_id'] = $id;
                $this->Testicategory->save($this->request->data['Testicategory']);
                $this->Session->setFlash('Testicategory updated ', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Testicategory title already exists', '', array(''), 'danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
        $this->request->data['Testicategory'] = $category['Testicategory'];
        $testicategories = $this->Testicategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('testicategories', $testicategories);
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Testicategory->exists($id)) {
            throw new NotFoundException(__('Testicategory Not Found'));
        }
        $this->request->data['Testicategory']['category_id'] = $id;
        $this->request->data['Testicategory']['status'] = 'Trash';
        if ($this->Testicategory->save($this->request->data['Testicategory'])) {
            $this->Session->setFlash('Testicategory deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Testicategory could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

}
