<?php
App::uses('AppController', 'Controller');
/**
 * Adminusers Controller
 *
 * @property Adminuser $Adminuser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AdminusersController extends AppController {
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser');
    public $layout = 'admin';
    /**
     * Login method
     *
     * @return void
     */
    public function admin_login() {
        $this->layout = 'admin_login';
        $this->admin_logincheck();
if (!empty($this->request->data)) {
            if (!empty($this->request->data['Adminuser']['email'])) {
                $check = $this->Adminuser->find('first', array('conditions' => array('email' => $this->request->data['Adminuser']['email'], 'status' => 'Active')));
                if (!empty($check)) {
                    $pass = $this->str_rand();
                    $password = md5($pass);
                    $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '2')));
                    $link = BASE_URL . "admin/";
                    $name = $check['Adminuser']['adminname'];
                    $username = $check['Adminuser']['username'];
                    $message = str_replace(array('{name}', '{username}', '{password}', '{link}'), array($name, $username, $pass, $link), $emailcontent['Emailcontent']['emailcontent']);
                    $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $check['Adminuser']['email'], $emailcontent['Emailcontent']['subject'], $message);
                    $check['Adminuser']['password'] = $password;
                    $check['Adminuser']['modified_date'] = date('Y-m-d H:i:s');
                    $this->Adminuser->save($check);
                    $this->Session->setFlash('Your password details sent to your email address.', '', array(''), 'admin_success');
                    $this->redirect(array('controller' => 'adminusers', 'action' => 'login'));
                } else
                    $this->Session->setFlash('Invalid email address.', '', array(''), 'admin_danger');
                $this->set('result', 'forgot');
            }
            else {
                $check = $this->Adminuser->find('first', array('conditions' => array('username' => $this->request->data['Adminuser']['username'])));
                if (!empty($check)) {
                    if ($check['Adminuser']['password'] == md5($this->request->data['Adminuser']['password'])) {
                        if ($check['Adminuser']['status'] == 'Active') {
                            $this->Session->write($check);
                            $this->redirect(array('controller' => 'dashboard', 'action' => 'admin_index'));
                        } else
                            $this->Session->setFlash('Username and password mismatch.', '', array(''), 'admin_danger');
                    } else
                        $this->Session->setFlash('Username and password mismatch.', '', array(''), 'admin_danger');
                } else
                    $this->Session->setFlash('Username and password mismatch.', '', array(''), 'admin_danger');
                $this->set('result', '');
            }
        }
    }
    public function admin_forgot() {
        $this->layout = 'admin_login';
        $this->admin_logincheck();
        if (!empty($this->request->data)) {
            $check = $this->Adminuser->find('first', array('conditions' => array('email' => $this->request->data['Adminuser']['email'], 'status' => 'Active')));
            if (!empty($check)) {
                $pass = $this->str_rand();
                $password = md5($pass);
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '1')));
                $link = BASE_URL . "admin/";
                $name = $check['Adminuser']['adminname'];
                $username = $check['Adminuser']['username'];
                $message = str_replace(array('{name}', '{username}', '{password}', '{link}'), array($name, $username, $pass, $link), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $check['Adminuser']['email'], $emailcontent['Emailcontent']['subject'], $message);
                $check['Adminuser']['password'] = $password;
                $check['Adminuser']['modified_date'] = date('Y-m-d H:i:s');
                $this->Adminuser->save($check);
                $this->Session->setFlash('Your password details sent to your email address.', '', array(''), 'admin_success');
                $this->redirect(array('controller' => 'adminusers', 'action' => 'login'));
            } else {
                $this->Session->setFlash('Invalid email address.', '', array(''), 'admin_danger');
            }
        }
        $this->set('result', 'forgot');
    }
    /**
     * change admin user status
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_profile() {
        $this->checkadmin();
        $id = $this->Session->read('Adminuser.admin_id');
        if (!$this->Adminuser->exists($id)) {
            throw new NotFoundException(__('Invalid adminuser'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $checkemail = $this->Adminuser->find('first', array('conditions' => array('email' => $this->request->data['Adminuser']['email'], 'admin_id !=' => $id, 'status !=' => 'Trash')));
            if (empty($checkemail)) {
                $this->request->data['Adminuser']['admin_id'] = $id;
                $this->Adminuser->save($this->request->data);
                $this->Session->setFlash('The admin user detail has been updated successfully.', '', array(''), 'success');
                $this->redirect(array('action' => 'profile'));
            } else {
                $this->Session->setFlash('Email already exists.', '', array(''), 'danger');
            }
        } else {
            $options = array('conditions' => array('Adminuser.' . $this->Adminuser->primaryKey => $id));
            $this->request->data = $this->Adminuser->find('first', $options);
        }
    }
    /**
     * change password
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_changepassword() {
        $this->checkadmin();
        $id = $this->Session->read('Adminuser.admin_id');
        if (!$this->Adminuser->exists($id)) {
            throw new NotFoundException(__('Invalid adminuser'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $checkpass = $this->Adminuser->find('first', array('conditions' => array('password' => md5($this->request->data['Adminuser']['oldpassword']), 'admin_id' => $id)));
            if (!empty($checkpass)) {
                if ($this->request->data['Adminuser']['password'] == $this->request->data['Adminuser']['cpasswords']) {
                    $this->request->data['Adminuser']['password'] = md5($this->request->data['Adminuser']['password']);
                    $this->request->data['Adminuser']['admin_id'] = $id;
                    $this->Adminuser->save($this->request->data);
                    $this->Session->setFlash('The admin password has been updated successfully.', '', array(''), 'success');
                    $this->redirect(array('action' => 'changepassword'));
                } else {
                    $this->Session->setFlash('New password and confirm password did not match.', '', array(''), 'danger');
                }
            } else {
                $this->Session->setFlash('Old Password is incorrect.', '', array(''), 'danger');
            }
        }
    }
}
