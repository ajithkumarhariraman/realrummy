<?php
App::uses('AppController', 'Controller');
/**
 * Adminusers Controller
 *
 * @property Adminuser $Adminuser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SitesettingsController extends AppController {
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Sitesetting');
    public $layout = 'admin';
    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $setting = $this->Sitesetting->find('first');
        if ($this->request->is('post')) {
            //pr($this->request->data);exit;
            $this->request->data['Sitesetting']['id'] = 1;
            if ($this->request->data['Sitesetting']['logo']['name'] != '') {
                $logo = rand(0, 9999) . $this->request->data['Sitesetting']['logo']['name'];
                move_uploaded_file($this->request->data['Sitesetting']['logo']['tmp_name'], 'img/' . $logo);
            } else {
                $logo = $setting['Sitesetting']['logo'];
            }
            if ($this->request->data['Sitesetting']['fav_icon']['name'] != '') {
                $fav_icon = rand(0, 9999) . $this->request->data['Sitesetting']['fav_icon']['name'];
                move_uploaded_file($this->request->data['Sitesetting']['fav_icon']['tmp_name'], 'img/' . $fav_icon);
            } else {
                $fav_icon = $setting['Sitesetting']['fav_icon'];
            }
            if ($this->request->data['Sitesetting']['invite_banner']['name'] != '') {
                $invite_banner = rand(0, 9999) . $this->request->data['Sitesetting']['invite_banner']['name'];
                move_uploaded_file($this->request->data['Sitesetting']['invite_banner']['tmp_name'], 'img/' . $invite_banner);
            } else {
                $invite_banner = $setting['Sitesetting']['invite_banner'];
            }
            if ($this->request->data['Sitesetting']['three_sec_img_1']['name'] != '') {
                $three_sec_img_1 = rand(0, 9999) . $this->request->data['Sitesetting']['three_sec_img_1']['name'];
                move_uploaded_file($this->request->data['Sitesetting']['three_sec_img_1']['tmp_name'], 'img/' . $three_sec_img_1);
            } else {
                $three_sec_img_1 = $setting['Sitesetting']['three_sec_img_1'];
            }
            if ($this->request->data['Sitesetting']['three_sec_img_2']['name'] != '') {
                $three_sec_img_2 = rand(0, 9999) . $this->request->data['Sitesetting']['three_sec_img_2']['name'];
                move_uploaded_file($this->request->data['Sitesetting']['three_sec_img_2']['tmp_name'], 'img/' . $three_sec_img_2);
            } else {
                $three_sec_img_2 = $setting['Sitesetting']['three_sec_img_2'];
            }
            if ($this->request->data['Sitesetting']['three_sec_img_3']['name'] != '') {
                $three_sec_img_3 = rand(0, 9999) . $this->request->data['Sitesetting']['three_sec_img_3']['name'];
                move_uploaded_file($this->request->data['Sitesetting']['three_sec_img_3']['tmp_name'], 'img/' . $three_sec_img_3);
            } else {
                $three_sec_img_3 = $setting['Sitesetting']['three_sec_img_3'];
            }
            $this->request->data['Sitesetting']['three_sec_img_3'] = $three_sec_img_3;
            $this->request->data['Sitesetting']['three_sec_img_2'] = $three_sec_img_2;
            $this->request->data['Sitesetting']['three_sec_img_1'] = $three_sec_img_1;
            $this->request->data['Sitesetting']['logo'] = $logo;
            $this->request->data['Sitesetting']['fav_icon'] = $fav_icon;
            $this->request->data['Sitesetting']['invite_banner'] = $invite_banner;
            $this->Sitesetting->save($this->request->data);
            $this->Session->setFlash('Sitesettings Saved successfully!', '', array(''), 'success');
            $this->redirect(array("controller" => "Sitesettings", "action" => "index"));
        }
        $result = $this->Sitesetting->find('first', array('conditions' => array('id' => '1')));
        $this->set('result', $result);
    }
}
