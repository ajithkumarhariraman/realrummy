<?php

App::uses('AppController', 'Controller');

/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class TestimonialsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Testimonial', 'Testicategory');
    public $layout = 'admin';

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $this->Testimonial->recursive = 0;
        $conditions = array();
        $this->paginate = array('conditions' => $conditions, 'order' => 'id DESC', 'limit' => '10');
        $this->set('testimonials', $this->Paginator->paginate('Testimonial'));
    }

    public function admin_add() {
        $this->checkadmin();
        if ($this->request->is('post')) {
            if (!empty($this->request->data['Testimonial']['profile']['name'])) {
                $imagename = $this->web_to_server($this->request->data['Testimonial']['profile'], 'files/testimonials/');
                $this->request->data['Testimonial']['profile'] = $imagename;
            }
            $this->request->data['Testimonial']['category_id'] = (!empty($this->request->data['Testimonial']['category_id'])) ? implode(',', $this->request->data['Testimonial']['category_id']) : "";
            $this->Testimonial->save($this->request->data['Testimonial']);
            $this->Session->setFlash('Testimonial Added', '', array(''), 'success');
            return $this->redirect(array('action' => 'index'));
        }
        $categories = $this->Testicategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('categories', $categories);
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->checkadmin();
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid Testimonial'));
        }
        $testimonial = $this->Testimonial->find('first', array('conditions' => array('id' => $id)));
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Testimonial']['id'] = $id;
            if (!empty($this->request->data['Testimonial']['profile']['name'])) {
                $imagename = $this->web_to_server($this->request->data['Testimonial']['profile'], 'files/testimonials/');
                $this->request->data['Testimonial']['profile'] = $imagename;
            } else {
                $this->request->data['Testimonial']['profile'] = $testimonial['Testimonial']['profile'];
            }
            $this->request->data['Testimonial']['category_id'] = (!empty($this->request->data['Testimonial']['category_id'])) ? implode(',', $this->request->data['Testimonial']['category_id']) : "";
            $this->Testimonial->save($this->request->data['Testimonial']);
            $this->Session->setFlash('Testimonial updated ', '', array(''), 'success');
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->data['Testimonial'] = $testimonial['Testimonial'];
        $categories = $this->Testicategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('categories', $categories);
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Testimonial Not Found'));
        }
        if ($this->Testimonial->delete($id)) {
            $this->Session->setFlash('Testimonial deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Testimonial could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

    public function index($id = NULL) {
        $this->layout = 'front';
        $categories = $this->Testicategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('categories', $categories);

        if ($id != NULL) {
            $conditions = array('FIND_IN_SET(\'' . $id . '\',category_id)');
        } else {
            $conditions = array('FIND_IN_SET(\'' . $categories[0]['Testicategory']['category_id'] . '\',category_id)');
        }
        $testimonials = $this->Testimonial->find('all', array('conditions' => $conditions));
        $this->set('testimonials', $testimonials);

        $category = $this->Testicategory->find('first', array('conditions' => $conditions));
        $this->set('c_category', $category);
    }

}
