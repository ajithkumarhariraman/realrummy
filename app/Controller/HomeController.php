<?php

App::uses('AppController', 'Controller');

/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class HomeController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Subscriber', 'Staticpage', 'Slider', 'Testimonial', 'Smscontent');
    public $layout = 'front';

    /**
     * admin_index method
     *
     * @return void
     */
    public function index() {
        $testimonials = $this->Testimonial->find('all', array('order' => 'id DESC', 'limit' => '9'));
        $this->set('testimonials', $testimonials);
        $sliders = $this->Slider->find('all', array('order' => 'slider_id DESC', 'limit' => '3'));
        $this->set('sliders', $sliders);
    }

    public function about() {
        $page = $this->Staticpage->find('first', array('conditions' => array('slug' => 'about')));
        $this->set('page', $page);
    }

    public function cms($slug = NULL) {
        $page = $this->Staticpage->find('first', array('conditions' => array('slug' => $slug)));
        $this->set('page', $page);
    }

    public function app_cms($slug = NULL) {
        $this->layout = 'app';
        $page = $this->Staticpage->find('first', array('conditions' => array('slug' => $slug)));
        $this->set('page', $page);
    }

    public function subscribe() {
        if ($this->request->is(array('post', 'put'))) {
            $check = $this->Subscriber->find('first', array('conditions' => array('email' => $this->request->data['Subscriber']['email'])));
            if (empty($check)) {
                $this->request->data['Subscriber']['datetime'] = date('Y-m-d H:i:s');
                $this->Subscriber->save($this->request->data['Subscriber']);
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '1')));
                $message = $emailcontent['Emailcontent']['emailcontent'];
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $this->request->data['Subscriber']['email'], $emailcontent['Emailcontent']['subject'], $message);
                $this->Session->setFlash('Subscribed Succussfully', '', array(''), 'front_success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Already Subscribed', '', array(''), 'front_danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
    }

    public function sendappsms() {
        $this->autoRender = false;
        if (!empty($this->request->data['mobile'])) {
            $sms = $this->Smscontent->find('first', array('conditions' => array('id' => '1')));
            $this->sendSMS($this->request->data['mobile'], $sms['Smscontent']['content']);
            $this->Session->setFlash('The SMS has been sent for an URL to download Rummy app on your mobile', '', array(''), 'front_success');
            return $this->redirect(array('action' => 'index'));
        }
    }

    public function mobile() {
        $this->layout = 'front';
        $conditions = array('status !=' => 'Trash');
    }

}
