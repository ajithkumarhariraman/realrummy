<?php

App::uses('AppController', 'Controller');

/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class TablesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Table', 'Tablecategory');
    public $layout = 'admin';

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $this->Table->recursive = 0;
        $conditions = array();
        if (!empty($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('name LIKE' => "%$s%");
        }
        if (!empty($_REQUEST['type'])) {
            $conditions['type'] = array('type' => $_REQUEST['type']);
        }
        if (!empty($_REQUEST['category'])) {
            $conditions['category'] = array('type' => $_REQUEST['category']);
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'table_id DESC', 'limit' => '10');
        $this->set('tables', $this->Paginator->paginate('Table'));
    }

    public function admin_add() {
        $this->checkadmin();
        if ($this->request->is('post')) {

            $this->Table->save($this->request->data['Table']);
            $this->Session->setFlash('Table Added', '', array(''), 'success');
            return $this->redirect(array('action' => 'index'));
        }
        $tables = $this->Table->find('all', array('conditions' => array()));
        $this->set('tables', $tables);
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->checkadmin();
        if (!$this->Table->exists($id)) {
            throw new NotFoundException(__('Invalid Table'));
        }
        $category = $this->Table->find('first', array('conditions' => array('table_id' => $id)));
        if ($this->request->is(array('post', 'put'))) {

            $this->request->data['Table']['table_id'] = $id;
            $this->Table->save($this->request->data['Table']);
            $this->Session->setFlash('Table updated ', '', array(''), 'success');
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->data['Table'] = $category['Table'];
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Table->exists($id)) {
            throw new NotFoundException(__('Table Not Found'));
        }
        if ($this->Table->delete($id)) {
            $this->Session->setFlash('Table deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Table could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

    public function index($id = NULL) {
        $user = $this->checkuser();
        $this->layout = 'front';
        $conditions = array('status' => 'Active');
        $tables = $this->Table->find('all', array('conditions' => $conditions));
        $this->set('tables', $tables);
    }

    public function updatePlayers($id = NULL) {
        $this->autoRender = false;
        $user = $this->checkuser();
        $tables = $this->Table->find('all', array('conditions' => array('status' => 'Active')));
        $data = array();
        $total = 0;
        foreach ($tables as $table) {
            $playtable = $this->Table->query('SELECT SUM(totalplayers) as totalplayers FROM tabledetail WHERE `table_id`=' . $table['Table']['table_id']);
            $lasT_playtable = $this->Table->query('SELECT totalplayers FROM tabledetail WHERE `table_id`=' . $table['Table']['table_id'] . ' ORDER BY `tableid` DESC');
            if (!empty($lasT_playtable[0])) {
                $data['active'][$table['Table']['table_id']] = ($table['Table']['maxplayer'] - $lasT_playtable[0]['tabledetail']['totalplayers']) . '/' . $table['Table']['maxplayer'];
                $data['registering'][$table['Table']['table_id']] = $playtable[0][0]['totalplayers'];
                $total += $playtable[0][0]['totalplayers'];
            }
        }
        $data['total_active_players'] = $total;
        echo json_encode($data);
        exit;
    }

    public function getMyGames() {
        $this->layout = '';
        $user = $this->checkuser();
        $tables = $this->Table->query('SELECT * FROM tabledetail WHERE `player1id`="' . $user['User']['user_id'] . '" OR `player2id`="' . $user['User']['user_id'] . '" OR `player3id`="' . $user['User']['user_id'] . '" OR `player4id`="' . $user['User']['user_id'] . '" OR `player5id`="' . $user['User']['user_id'] . '" OR `player6id`="' . $user['User']['user_id'] . '"');
        $this->set('tables', $tables);
    }

}
