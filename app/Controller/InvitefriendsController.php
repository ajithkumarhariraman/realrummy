<?php

App::uses('AppController', 'Controller');

/**
 * Invitefriends Controller
 *
 * @property Invitefriend $Invitefriend
 * @property PaginatorComponent $Paginator
 */
class InvitefriendsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $uses = array();
    public $layout = 'home';

    public function invitefriend() {
        $user = $this->checkuser();
        
    }

    public function invitemanually() {
        $this->checkmember();
        $this->layout = '';
        $this->render(false);
        $member_id = $this->Session->read('TblPlayer.id');

        if (!$this->TblPlayer->exists($member_id)) {
            throw new NotFoundException('Invalid Member');
        }
        $member = $this->TblPlayer->find('first', array('conditions' => array('id' => $member_id)));
        $this->request->data['Invitefriend']['member_id'] = $member_id;
        $this->request->data['Invitefriend']['from'] = $member['TblPlayer']['email'];
        $this->request->data['Invitefriend']['to1'] = $this->request->data['Invitefriend']['to_1'];
        $this->request->data['Invitefriend']['to2'] = $this->request->data['Invitefriend']['to_2'];
        $this->request->data['Invitefriend']['to3'] = $this->request->data['Invitefriend']['to_3'];
        $this->request->data['Invitefriend']['message'] = $this->request->data['Invitefriend']['messages'];
        $this->request->data['Invitefriend']['type'] = 'Manual';
        $this->request->data['Invitefriend']['created_date'] = date('Y-m-d H:i:s');
        $this->request->data['Invitefriend']['status'] = 'Active';
        $this->Invitefriend->save($this->request->data);

        $emailcontent = $this->Emailcontents->find('first', array('conditions' => array('eid' => '9')));
        $fname = $member['TblPlayer']['firstname'];
        $lname = $member['TblPlayer']['lastname'];
        $name = $fname . ' ' . $lname;
        $content = $this->request->data['Invitefriend']['message'];
        $frommemberemail = $member['TblPlayer']['email'];
        $to1 = $this->request->data['Invitefriend']['to1'];

        $to2 = $this->request->data['Invitefriend']['to2'];
        $to3 = $this->request->data['Invitefriend']['to3'];
        $subject = $emailcontent['Emailcontents']['subject'];
        $message = str_replace(array('{fromname}', '{message}'), array($name, $content), $emailcontent['Emailcontents']['content']);
        $this->mailsend($name, $frommemberemail, $to1, $subject, $message);
        if (!empty($to2)) {
            $this->mailsend($name, $frommemberemail, $to2, $subject, $message);
        }
        if (!empty($to3)) {
            $this->mailsend($name, $frommemberemail, $to3, $subject, $message);
        }
        $this->Session->setFlash("<div class='success msg'>" . __('Invites Successfully') . "</div>", '');
        $this->redirect(array('action' => 'invitefriend'));
    }

    public function hotmail() {
        $params = ["client_id" => "Hotmail_clientid", "scope" => "wl.basic wl.contacts_phone_numbers", "response_type" => "code", "redirect_uri" => $redirect_uri];
        //header("Location: https://login.live.com/oauth20_authorize.srf?".http_build_query($params));
        //$redirect_uri = BASE_URL.'invitefriends/hotmailcontacts';
        $urls_ = "https://login.live.com/oauth20_authorize.srf?" . http_build_query($params);
        $this->redirect($urls_);
    }

    public function hotmailcontacts() {
        $this->checkmember();

        if (isset($_GET['code'])) {
            $auth_code = $_GET["code"];
            $redirect_uri = BASE_URL . 'invitefriends/hotmailcontacts';
            $fields = array(
                'code' => urlencode($auth_code),
                'client_id' => urlencode(Hotmail_clientid),
                'client_secret' => urlencode(Hotmail_clientsecret),
                'redirect_uri' => urlencode($redirect_uri),
                'grant_type' => urlencode('authorization_code')
            );
            $post = '';
            foreach ($fields as $key => $value) {
                $post .= $key . '=' . $value . '&';
            }
            $post = rtrim($post, '&');
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://login.live.com/oauth20_token.srf');
            curl_setopt($curl, CURLOPT_POST, 5);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            $result = curl_exec($curl);
            curl_close($curl);

            $response = json_decode($result);

            if ($response->access_token != '') {
                $accesstoken = $response->access_token;
                $url = 'https://apis.live.net/v5.0/me/contacts?access_token=' . $accesstoken . '&limit=1000';
                $xmlresponse = $this->curl_file_get_contents($url);
                $xml = json_decode($xmlresponse, true);
                $msn_email = array();
                foreach ($xml['data'] as $emails) {
                    $email_ids = implode(",", array_filter(array_unique($emails['emails'])));
                    $msn_email[] = array('email' => rtrim($email_ids), 'name' => $emails['name']);
                }
                $this->set('emails', $msn_email);
            } else {
                $this->Session->setFlash("<div class='error msg'>" . __('Getting problem to import contacts. Please try once again') . "</div>", '');
                $this->redirect(array('action' => 'invitefriend'));
            }
        }
    }

    public function curl_file_get_contents($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function yahoo() {
        $this->checkmember();
        $callback = BASE_URL . "invitefriends/yahoo_callback";
        App::import('Vendor', 'yahoo/yahoo');
        $yahoo = new Yahoo();
        // Get the request token using HTTP GET and HMAC-SHA1 signature 
        $retarr = $yahoo->get_request_token(Yahoo_key, Yahoo_secret, $callback, false, true, true);
        if (!empty($retarr)) {
            list($info, $headers, $body, $body_parsed) = $retarr;
            if ($info['http_code'] == 200 && !empty($body)) {
                $_SESSION['request_token'] = $body_parsed['oauth_token'];
                $_SESSION['request_token_secret'] = $body_parsed['oauth_token_secret'];
                $_SESSION['oauth_verifier'] = $body_parsed['oauth_token'];
                $this->redirect(urldecode($body_parsed['xoauth_request_auth_url']));
            }
        }
    }

    public function yahoo_callback() {
        $this->checkmember();
        App::import('Vendor', 'yahoo/yahoo');
        $request_token = $_SESSION['request_token'];
        $request_token_secret = $_SESSION['request_token_secret'];
        $oauth_verifier = $_GET['oauth_verifier'];
        $yahoo = new Yahoo();
        $retarr = $yahoo->get_access_token_yahoo(Yahoo_key, Yahoo_secret, $request_token, $request_token_secret, $oauth_verifier, false, true, true);
        if (!empty($retarr)) {
            list($info, $headers, $body, $body_parsed) = $retarr;
            if ($info['http_code'] == 200 && !empty($body)) {
                $guid = $body_parsed['xoauth_yahoo_guid'];
                $access_token = $yahoo->rfc3986_decode($body_parsed['oauth_token']);
                $access_token_secret = $body_parsed['oauth_token_secret'];
                $retarrs = $yahoo->callcontact_yahoo(Yahoo_key, Yahoo_secret, $guid, $access_token, $access_token_secret, false, true);
                if (!empty($retarrs)) {
                    $this->set('emails', $retarrs);
                    $this->render('hotmailcontacts');
                } else {
                    $this->Session->setFlash("<div class='error msg'>" . __('Getting problem to import contacts. Please try once again') . "</div>", '');
                    $this->redirect(array('action' => 'invitefriend'));
                }
            }
        }
    }

    public function google() {
        $this->checkmember();
        App::import('Vendor', 'google/Google_Client');
        App::import('Vendor', 'google/contrib/Google_Oauth2Service.php');
        $redirect_url = BASE_URL . 'invitefriends/googlecallback';
        $gClient = new Google_Client();
        $gClient->setApplicationName(Google_product);
        $gClient->setClientId(Google_appkey);
        $gClient->setClientSecret(Google_appsecretkey);
        $gClient->setRedirectUri($redirect_url);
        $gClient->setScopes('https://www.google.com/m8/feeds');
        $gClient->setDeveloperKey(Google_developerkey);
        $authUrl = $gClient->createAuthUrl();
        $this->redirect($authUrl);
    }

    public function googlecallback() {

        $this->checkmember();

        App::import('Vendor', 'google/Google_Client');
        App::import('Vendor', 'google/contrib/Google_Oauth2Service.php');
        $redirect_url = BASE_URL . 'invitefriends/googlecallback';
        $gClient = new Google_Client();
        $gClient->setApplicationName(Google_product);
        $gClient->setClientId(Google_appkey);
        $gClient->setClientSecret(Google_appsecretkey);
        $gClient->setRedirectUri($redirect_url);
        $gClient->setScopes('https://www.google.com/m8/feeds');
        $gClient->setDeveloperKey(Google_developerkey);

        if (isset($_REQUEST['reset'])) {
            unset($_SESSION['token']);
            $gClient->revokeToken();
            $this->redirect(filter_var($redirect_url, FILTER_SANITIZE_URL)); //redirect user back to page
        }
        if (isset($_GET['code'])) {
            $gClient->authenticate($_GET['code']);
            $_SESSION['token'] = $gClient->getAccessToken();
            $this->redirect(filter_var($redirect_url, FILTER_SANITIZE_URL));
        }

        if ($_SESSION['token']) {
            $gClient->setAccessToken($_SESSION['token']);
        }


        if ($gClient->getAccessToken()) {

            $token = json_decode($_SESSION['token']);
            $token->access_token;
            $curl = curl_init("https://www.google.com/m8/feeds/contacts/default/full?alt=json&max-results=50&access_token=" . $token->access_token);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
            $contacts_json = curl_exec($curl);
            curl_close($curl);
            $contacts = json_decode($contacts_json, true);

            $return = array();

            foreach ($contacts['feed']['entry'] as $contact) {
                $return[] = array('name' => $contact['title']['$t'],
                    'email' => isset($contact['gd$email'][0]['address']) ? $contact['gd$email'][0]['address'] : false,
                    'phone' => isset($contact['gd$phoneNumber'][0]['address']) ? $contact['gd$phoneNumber'][0]['address'] : false);
            }
            $this->set('emails', $return);

            $this->render('hotmailcontacts');
        }
    }

    public function invitefriendscontacts() {
        $this->checkmember();
        $member_id = $this->Session->read('TblPlayer.id');
        //print_r($member_id);exit;
        if (!$this->TblPlayer->exists($member_id)) {
            throw new NotFoundException('Invalid Member');
        }

        $member = $this->TblPlayer->find('first', array('conditions' => array('id' => $member_id)));
        $this->set('member', $member);
        if ($this->request->is('post')) {
            //print_r($this->request->data['action']);exit;
            $emailcontent = $this->Emailcontents->find('first', array('conditions' => array('eid' => '9')));
            $content = $this->request->data['message'];
            $frommemberemail = $this->request->data['email'];
            $name = $this->request->data['name'];
            $tomemberemail[] = implode(",", $this->request->data['action']);

            $tomemberemail = $this->request->data['action'];
            //print_r($tomemberemail);exit;
            $subject = $emailcontent['Emailcontents']['subject'];
            $message = str_replace(array('{fromname}', '{message}'), array($name, $content), $emailcontent['Emailcontents']['content']);
            $this->mailsend($name, $frommemberemail, $tomemberemail, $subject, $message);
            $this->Session->setFlash("<div class='success msg'>" . __('Invites Successfully') . "</div>", '');
            $this->redirect(array('action' => 'invitefriend'));
        }
    }

}
