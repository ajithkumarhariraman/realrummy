<?php

App::uses('AppController', 'Controller');

/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class StaticpagesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Staticpage');
    public $layout = 'admin';

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $this->Staticpage->recursive = 0;
        $conditions = array();
        $this->paginate = array('conditions' => $conditions, 'order' => 'page_id DESC', 'limit' => '10');
        $this->set('staticpages', $this->Paginator->paginate('Staticpage'));
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->checkadmin();
        if (!$this->Staticpage->exists($id)) {
            throw new NotFoundException(__('Invalid staticpage'));
        }
        $options = array('conditions' => array('Staticpage.' . $this->Staticpage->primaryKey => $id));
        $this->set('staticpage', $this->Staticpage->find('first', $options));
    }

    public function admin_add() {
        $this->checkadmin();
        if ($this->request->is('post')) {
            if ($this->Staticpage->save($this->request->data)) {
                $this->Session->setFlash('The staticpage has been saved!', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('The staticpage could not be saved. Please, try again.!', '', array(''), 'danger');
                return $this->redirect($this->referer());
            }
        }
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->checkadmin();
        if (!$this->Staticpage->exists($id)) {
            throw new NotFoundException(__('Invalid staticpage'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $page = $this->Staticpage->find('first', array('conditions' => array('page_id' => $id)));
            $this->request->data['Staticpage']['page_id'] = $id;
            if (!empty($this->request->data['Staticpage']['image']['name'])) {
                $imagename = $this->web_to_server($this->request->data['Staticpage']['image'], 'files/pages/');
                $this->request->data['Staticpage']['image'] = $imagename;
            } else {
                $this->request->data['Staticpage']['image'] = $page['Staticpage']['image'];
            }
            if ($this->Staticpage->save($this->request->data)) {
                $this->Session->setFlash('The staticpage has been saved!', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('The staticpage could not be saved. Please, try again.!', '', array(''), 'danger');
                return $this->redirect($this->referer());
            }
        }
        $options = array('conditions' => array('Staticpage.' . $this->Staticpage->primaryKey => $id));
        $this->request->data = $this->Staticpage->find('first', $options);
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Staticpage->exists($id)) {
            throw new NotFoundException(__('News Not Found'));
        }
        $this->request->data['Staticpage']['page_id'] = $id;
        if ($this->Staticpage->delete($id)) {
            $this->Session->setFlash('News deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('News could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

    public function admin_changestatus($id = null, $status = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Staticpage->exists($id)) {
            throw new NotFoundException(__('Staticpage Not Found'));
        }
        $this->request->data['Staticpage']['page_id'] = $id;
        $this->request->data['Staticpage']['status'] = $status;
        $this->request->data['Staticpage']['modified_date'] = date('Y-m-d H:i:s');
        if ($this->Staticpage->save($this->request->data['Staticpage'])) {
            $this->Session->setFlash('Staticpage status changed successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Staticpage status could not be changed! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

    public function index($slug = null) {
        $this->layout = 'home';
        if ($slug) {
            $result = $this->Staticpage->findBySlug($slug);
            if (!empty($result)) {
                $this->set('result', $result);
            } else {
                throw new NotFoundException(__('Page Not Found'));
            }
        } else {
            throw new NotFoundException(__('Page Not Found'));
        }
    }

}
