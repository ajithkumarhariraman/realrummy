<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::import('Controller', 'Yahoo'); // mention at top
App::uses('Adaptive', 'Lib');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array('Cookie', 'Session');
    public $helpers = array('Session');

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Adminuser', 'Emailcontent', 'State', 'City', 'Country', 'User', 'Tournament','Tournamentplayer');
    private $format;

    protected function str_rand($length = 8, $output = 'alphanum') {
// Possible seeds
        $outputs['alpha'] = 'abcdefghijklmnopqrstuvwqyz';
        $outputs['numeric'] = '0123456789';
        $outputs['alphanum'] = 'abcdefghijklmnopqrstuvwqyz0123456789';
        $outputs['hexadec'] = '0123456789abcdef';
// Choose seed
        if (isset($outputs[$output])) {
            $output = $outputs[$output];
        }
// Seed generator
        list($usec, $sec) = explode(' ', microtime());
        $seed = (float) $sec + ((float) $usec * 100000);
        mt_srand($seed);
// Generate
        $str = '';
        $output_count = strlen($output);
        for ($i = 0; $length > $i; $i++) {
            $str .= $output{mt_rand(0, $output_count - 1)};
        }
        return $str;
    }

    public function admin_logincheck() {
        if ($this->Session->read('Adminuser'))
            $this->redirect(array('controller' => 'adminusers', 'action' => 'profile'));
    }

    public function checkuserlogin() {
        if ($this->Session->read('User'))
            $this->redirect(array('controller' => 'users', 'action' => 'myaccount'));
    }

    public function checkadmin() {
        if (!$this->Session->read('Adminuser')) {
            $this->redirect(array('controller' => 'adminusers', 'action' => 'login'));
        } else {
            $admindetails = $this->Adminuser->find('first', array('conditions' => array('admin_id' => $this->Session->read('Adminuser.admin_id'), 'status' => 'Active')));
            if (!empty($admindetails)) {
                $this->set('sessionadmin', $admindetails);
            } else {
                $this->redirect(array('action' => 'logout'));
            }
        }
    }

    public function checkuser() {
        if (!$this->Session->read('User')) {
            $this->redirect(array('controller' => 'home', 'action' => 'index'));
        } else {
            $userdetails = $this->User->find('first', array('conditions' => array('user_id' => $this->Session->read('User.user_id'), 'status' => 'Active')));
            if (!empty($userdetails)) {
                $this->set('sessionuser', $userdetails);
                return $userdetails;
            } else {
                $this->redirect(array('action' => 'logout'));
            }
        }
    }

    public function checkappuser($user_id) {
        if (empty($user_id)) {
            $this->redirect(array('controller' => 'users', 'action' => 'app_usererror'));
        } else {
            $userdetails = $this->User->find('first', array('conditions' => array('user_id' => $user_id, 'status' => 'Active')));
            if (!empty($userdetails)) {
                $this->set('sessionuser', $userdetails);
                return $userdetails;
            } else {
                $this->redirect(array('controller' => 'users', 'action' => 'app_usererror'));
            }
        }
    }

    public function admin_logout() {
        $this->Session->delete('Adminuser');
        $this->redirect(array('controller' => 'adminusers', 'action' => 'login'));
    }

    public function logout() {
        $this->Session->delete('Sitelogin');
        $this->Session->delete('User');
        $this->Session->delete('Userlogin');
        $this->Session->delete('Contractor');
        $this->Session->delete('Contractorlogin');
        $this->Session->destroy();
        $this->redirect(array('controller' => 'home', 'action' => 'index'));
    }

    protected function mailsend($fromname, $from, $to, $subject, $message, $cc = NULL, $attachment = 0, $attachmentsfiles = '', $template = 'default', $layout = 'default') {
        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail();
        if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '192.168.1.3') {
            $email->config('smtp');
        }
        $email->emailFormat('html');
        $email->from(array(trim($from) => $fromname));
        if ($attachment == 1) {
            $attach = explode(",", $attachmentsfiles);
            $attachments = array();
            foreach ($attach as $attach) {
                $attachments[] = $attach;
            }
            $email->attachments($attachments);
        }
        $email->template($template, $layout);
        $email->to(trim($to));
        if ($cc != '' || $cc != NULL) {
            $email->cc($cc);
        }
        $email->subject($subject);
        $email->send($message);
        $email->reset();
    }

    public function beforeFilter() {
        
    }

    public function slugify($string) {
        $string = preg_replace("`\[.*\]`U", "", $string);
        $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i', '-', $string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace("`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i", "\\1", $string);
        $string = preg_replace(array("`[^a-z0-9]`i", "`[-]+`"), "-", $string);
        return strtolower(trim($string, '-'));
    }

//    public function initialize() {
//        parent::initialize();
//        $this->loadComponent('Flash');
//        /* Authentication */
//        $this->loadComponent('Auth', [
//            'authenticate' => [
//                'Form' => [
//                    'fields' => [
//                        'username' => 'email',
//                        'password' => 'password'
//                    ]
//                ]
//            ],
//            'loginRedirect' => [
//                'controller' => 'users',
//                'action' => 'profile'
//            ],
//            'logoutRedirect' => [
//                'controller' => 'Users',
//                'action' => 'login1'
//            ],
//            'loginAction' => [
//                'controller' => 'Users',
//                'action' => 'login2'
//            ]
//        ]);
//    }
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email']
                ],
                'ADmad/HybridAuth.HybridAuth' => [
                    // All keys shown below are defaults
                    'fields' => [
                        'provider' => 'provider',
                        'openid_identifier' => 'openid_identifier',
                        'email' => 'email'
                    ],
                    'profileModel' => 'ADmad/HybridAuth.SocialProfiles',
                    'profileModelFkField' => 'user_id',
                    'userModel' => 'Users',
                    'hauth_return_to' => [
                        'controller' => 'Users',
                        'action' => 'socail_login',
                        'prefix' => false,
                        'plugin' => false
                    ]
                ]
            ]
        ]);
    }

    function base64_toimage($data, $path) {
        if ($data[0] == '/') {
            $type = "jpeg";
        } else if ($data[0] == 'R') {
            $type = "gif";
        } else if ($data[0] == 'i') {
            $type = "png";
        } else {
            $type = "jpeg";
        }
        $image = base64_decode($data);
        $imgFile = rand(0, 10000) . time() . "." . $type;
        $ifp = fopen($path . $imgFile, "wb");
        fwrite($ifp, base64_decode($data));
        fclose($ifp);
        return $imgFile;
    }

    function web_to_server($data, $path) {
        $extension = $this->getFileExtension($data['name']);
        $extension = strtolower($extension);
        $m = explode(".", $data['name']);
        $imagename = time() . $m[0] . "." . $extension;
        $destination = $path . $imagename;
        move_uploaded_file($data['tmp_name'], $destination);
        return $imagename;
    }

    function sendSMS($number, $message) {
        $url = 'http://www.onlinebulksmslogin.com/spanelv2/api.php';
        $username = 'IDLATRANS';
        $password = 'sidhant@789';
        $request = $url . '?username=' . $username . '&password=' . $password . '&to=' . urlencode($number) . '&message=' . urlencode($message) . '&from=IDLAFV';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function getFileExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    function random_username($string) {
        $name = explode("[ .-]", $string);
        $firstname = $name[0];
        $lastname = (!empty($name[1])) ? $name[1] : "";
        $firstname = strtolower($firstname);
        $lastname = strtolower(substr($lastname, 0, 2));
        $nrRand = rand(0, 100);
        return $firstname . $lastname . $nrRand;
    }

    public function getCities() {
        $this->autoRender = FALSE;
        $state = $this->State->find('first', array('conditions' => array('name' => $_REQUEST['state'])));
        $cities = $this->City->find('all', array('conditions' => array('state_id' => $state['State']['id'])));
        $str = '';
        foreach ($cities as $city) {
            $str .= '<option>' . $city['City']['name'] . '</option>';
        }
        return $str;
    }

    public function getStates() {
        $this->autoRender = FALSE;
        $country = $this->Country->find('first', array('conditions' => array('name' => $_REQUEST['country'])));
        $states = $this->State->find('all', array('conditions' => array('country_id' => $country['Country']['id'])));
        $str = '';
        foreach ($states as $state) {
            $str .= '<option>' . $state['State']['name'] . '</option>';
        }
        return $str;
    }

    public function tournamentStructure($id = NULL) {
        $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id)));
        $i = $tournament['Tournament']['maxplayers'];
        $rounds = array();
        $j = 0;
        while ($i > $tournament['Tournament']['maxplayers_table']) {
            $players = $i;
            $i = ceil($i / $tournament['Tournament']['maxplayers_table']);
            $table = $i;
            $qualifierfrom_table = ($table > 4 || $table == 1 || $j == 0) ? "1" : "2";
            $i = $i * $qualifierfrom_table;
            $rounds[] = array('players' => $players, 'table' => ceil($table), 'qualifier' => $qualifierfrom_table);
            $j++;
        }
        $rounds[] = array('players' => ceil($table) * $qualifierfrom_table, 'table' => 1, 'qualifier' => 1);
        return $rounds;
    }

    public function priceCalculation($id = NULL) {
        $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id)));
        $totalprize_amount = ($tournament['Tournament']['entryfee'] * $tournament['Tournament']['maxplayers']) * 0.85;
        $prize_count = $tournament['Tournament']['totalprize_count'];
        $rounds = $this->tournamentStructure($id);
        $j = 0;
        $rounds = array_reverse($rounds);
        $presentagecal = floor($totalprize_amount / (count($rounds)));
        foreach ($rounds as $key => $round) {
            if ($key == 0) {
                for ($i = 0; $i <= $round['players']; $i++) {
                    $data[] = array(
                        'amount' => floor((($presentagecal / 100) * 10) / ($round['players'] / 6)),
                        'count' => ceil($round['players'] / 6)
                    );
                }
            } else {
                for ($i = 0; $i <= $round['players']; $i++) {
                    $data[] = array(
                        'amount' => floor((($presentagecal / 100) * 10) / ($round['players'] / 6)),
                        'count' => ceil($round['players'] / 6)
                    );
                }
            }
        }
//        for ($i = 1; $i <= $prize_count; $i++) {
//            $prize = ($totalprize_amount / ($prize_count));
//            if (!isset($data[$i])) {
//                $data[$i] = 0;
//            }
//            $data[$i] = $prize;
//            $totalprize_amount = $totalprize_amount - $prize;
//            $j += $prize;
//        }
        return $data;
    }

    public function priceCalculationtest($id = NULL) {
        $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id)));
        $totalprize_amount = ($tournament['Tournament']['entryfee'] * $tournament['Tournament']['maxplayers']) * 0.85;
        $prize_count = $tournament['Tournament']['totalprize_count'];
        $rounds = $this->tournamentStructure($id);
        $j = 0;
        $rounds = array_reverse($rounds);
        $presentagecal = floor($totalprize_amount / (count($rounds)));
//        foreach ($rounds as $key => $round) {
//            if ($key == 0) {
//                for ($i = 0; $i <= $round['players']; $i++) {
//                    $data[] = array(
//                        'amount' => floor((($presentagecal / 100) * 10) / ($round['players'] / 6)),
//                        'count' => ceil($round['players'] / 6)
//                    );
//                }
//            } else {
//                for ($i = 0; $i <= $round['players']; $i++) {
//                    $data[] = array(
//                        'amount' => floor((($presentagecal / 100) * 10) / ($round['players'] / 6)),
//                        'count' => ceil($round['players'] / 6)
//                    );
//                }
//            }
//        }
        for ($i = 1; $i <= $prize_count; $i++) {
            $prize = ($totalprize_amount / ($prize_count));
            if (!isset($data[$i])) {
                $data[$i] = 0;
            }
            $data[$i] = $prize;
            $totalprize_amount = $totalprize_amount - $prize;
            $j += $prize;
        }
        print_r($data);
        exit;
        return $data;
    }

    public function tournamentStructure_json($id = NULL) {
        $this->autoRender = false;
        $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id)));
        $joined_players = $this->Tournamentplayer->find('all', array('conditions' => array('tournament_id' => $id)));
      
	  date_default_timezone_set("Asia/Kolkata");
            $timenow = date('n/j/Y g:i:s A');

            $regclose = date('n/j/Y g:i:s A', strtotime($tournament['Tournament']['reg_ends']));
            if ($regclose > $timenow) {
			        $i = $tournament['Tournament']['maxplayers'];
			}
			else{
       	        $i = count($joined_players);
	}

        $rounds = array();
        $j = 0;
        while ($i > $tournament['Tournament']['maxplayers_table']) {
            $players = $i;
            $i = ceil($i / $tournament['Tournament']['maxplayers_table']);
            $table = $i;
            $qualifierfrom_table = ($table > 4 || $table == 1 || $j == 0) ? "1" : "2";
            $i = $i * $qualifierfrom_table;
            $rounds[] = array('players' => $players, 'table' => ceil($table), 'qualifier' => $qualifierfrom_table);
            $j++;
        }
        $rounds[] = array('players' => ceil($table) * $qualifierfrom_table, 'table' => 1, 'qualifier' => 1);
        echo json_encode($rounds);
        exit;
    }

}
