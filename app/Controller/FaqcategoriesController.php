<?php

App::uses('AppController', 'Controller');

/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class FaqcategoriesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Faqcategory');
    public $layout = 'admin';

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $this->Faqcategory->recursive = 0;
        $conditions = array('status !=' => 'Trash');
        if (!empty($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['title LIKE'] = "%$s%";
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'category_id DESC', 'limit' => '10');
        $this->set('faqcategories', $this->Paginator->paginate('Faqcategory'));
    }

    public function admin_add() {
        $this->checkadmin();
        if ($this->request->is('post')) {
            $check = $this->Faqcategory->find('first', array('conditions' => array('title' => $this->request->data['Faqcategory']['title'], 'status !=' => 'Trash')));
            if (empty($check)) {
                $this->Faqcategory->save($this->request->data['Faqcategory']);
                $this->Session->setFlash('Faqcategory Added', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Faqcategory title already exists', '', array(''), 'danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
        $faqcategories = $this->Faqcategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('faqcategories', $faqcategories);
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->checkadmin();
        if (!$this->Faqcategory->exists($id)) {
            throw new NotFoundException(__('Invalid Faqcategory'));
        }
        $category = $this->Faqcategory->find('first', array('conditions' => array('category_id' => $id)));
        if ($this->request->is(array('post', 'put'))) {
            $check = $this->Faqcategory->find('first', array('conditions' => array('title' => $this->request->data['Faqcategory']['title'], 'category_id !=' => $id, 'status !=' => 'Trash')));
            if (empty($check)) {
                $this->request->data['Faqcategory']['category_id'] = $id;
                $this->Faqcategory->save($this->request->data['Faqcategory']);
                $this->Session->setFlash('Faqcategory updated ', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Faqcategory title already exists', '', array(''), 'danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
        $this->request->data['Faqcategory'] = $category['Faqcategory'];
        $faqcategories = $this->Faqcategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('faqcategories', $faqcategories);
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Faqcategory->exists($id)) {
            throw new NotFoundException(__('Faqcategory Not Found'));
        }
        $this->request->data['Faqcategory']['category_id'] = $id;
        $this->request->data['Faqcategory']['status'] = 'Trash';
        if ($this->Faqcategory->save($this->request->data['Faqcategory'])) {
            $this->Session->setFlash('Faqcategory deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Faqcategory could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

}
