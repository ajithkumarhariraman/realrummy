<?php

App::uses('AppController', 'Controller');
App::import('Vendor', 'Instamojo', array('file' => 'instamojo.php'));

/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Cookie');
    public $uses = array('Adminuser', 'Testimonial', 'Testicategory', 'User', 'Withdrawal', 'Transaction', 'Letter', 'Kycdocument', 'Addcash', 'Pendingbonus', 'Invitation', 'Promocode', 'Userpromocode');
    public $layout = 'front';

    public function register() {
        $this->layout = 'front';
        if ($this->request->is(array('post', 'put'))) {
            $checkemail = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['email'], 'status !=' => 'Trash')));
            $checkphone = $this->User->find('first', array('conditions' => array('phone_number' => $this->request->data['User']['phone_number'], 'status !=' => 'Trash')));
            if (empty($checkemail) && empty($checkphone)) {
                $this->request->data['User']['user_name'] = $this->random_username($this->request->data['User']['name']);
                $this->request->data['User']['emailencode'] = $hash = md5(rand(0, 1000));
                $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
                $this->request->data['User']['created_date'] = date('Y-m-d H:i:s');
                if (!empty($_GET['emailencode'])) {
                    $ref = $this->User->find('first', array('conditions' => array('emailencode' => $_GET['emailencode'])));
                    $this->request->data['User']['refid'] = (!empty($ref)) ? $ref['User']['user_id'] : "";
                }
                $this->User->save($this->request->data['User']);
                $this->Session->write('user_name', $this->request->data['User']['user_name']);
                $activation_link = BASE_URL . 'users/activate/' . $hash;
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '3')));
                $message = str_replace(array('{name}', '{email}', '{phone}', '{gender}', '{state}'), array($this->request->data['User']['name'], $this->request->data['User']['email'], $this->request->data['User']['phone_number'], $this->request->data['User']['gender'], $this->request->data['User']['state']), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $emailcontent['Emailcontent']['tomail'], $emailcontent['Emailcontent']['subject'], $message);
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '4')));
                $message = str_replace(array('{name}', '{activation_link}'), array($this->request->data['User']['name'], $activation_link), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $this->request->data['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
                $this->Session->setFlash('Successfully registered! Please login to continue!', '', array(''), 'front_success');
                return $this->redirect(array('action' => 'registersuccess', 'controller' => 'users'));
            } else {
                $this->Session->setFlash('Email / Phone already exists!', '', array(''), 'front_danger');
                return $this->redirect(array('action' => 'index', 'controller' => 'home'));
            }
        }
    }

    public function registersuccess() {
        
    }

    public function login() {
        $this->autoRender = false;
        if ($this->request->is(array('post', 'put'))) {
            $checkemail = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['email'], 'status !=' => 'Trash')));
            if (!empty($checkemail)) {

                if ($checkemail['User']['password'] == md5($this->request->data['User']['password'])) {
                    $this->Session->write('User', $checkemail['User']);
                    $this->Session->write('Userlogin', TRUE);

                    $this->Cookie->write('ryz_rclb_userid', $checkemail['User']['user_id']);
                    $this->Cookie->write('ryz_rclb_username', $checkemail['User']['user_name']);
                    $this->Cookie->write('ryz_rclb_palyername', $checkemail['User']['name']);
                    $this->Cookie->write('ryz_rclb_playerphoto', 'http://localhost/rum/img/profile.png');


                    $this->Session->setFlash('LoggedIn!', '', array(''), 'front_success');
                    return $this->redirect(array('action' => 'myaccount', 'controller' => 'users'));
                } else {
                    $this->Session->setFlash('Email / Password Mismatch!', '', array(''), 'front_danger');
                    return $this->redirect(array('action' => 'index', 'controller' => 'home'));
                }
            } else {
                $this->Session->setFlash('Email not exists!', '', array(''), 'front_danger');
                return $this->redirect(array('action' => 'index', 'controller' => 'home'));
            }
        }
    }

    public function forgotpassword() {
        $this->autoRender = false;
        if ($this->request->is(array('post', 'put'))) {
            $checkemail = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['email'], 'status !=' => 'Trash')));
            if (!empty($checkemail)) {
                $password = $this->str_rand(6);
                $checkemail['User']['password'] = md5($password);
                $this->User->save($checkemail['User']);
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '5')));
                $message = str_replace(array('{name}', '{temp_password}'), array($checkemail['User']['name'], $password), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $checkemail['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
                $this->Session->setFlash('Email with temp password has been sent to your email Address!', '', array(''), 'front_success');
                return $this->redirect(array('action' => 'index', 'controller' => 'home'));
            } else {
                $this->Session->setFlash('Email not exists!', '', array(''), 'front_danger');
                return $this->redirect(array('action' => 'index', 'controller' => 'home'));
            }
        }
    }

    public function myaccount() {
        $this->layout = 'front';
        $user = $this->checkuser();
    }

    public function app_myaccount() {
        $this->layout = 'app';
        extract($_REQUEST);
        $user = $this->checkappuser($user_id);
    }

    public function app_gamehistory() {
        $this->layout = 'app';
        extract($_REQUEST);
        $user = $this->checkappuser($user_id);
    }

    public function gamehistory() {
        $this->layout = 'front';
        $user = $this->checkuser();
    }

    public function profile() {
        $this->layout = 'front';
        $user = $this->checkuser();
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['User']['user_id'] = $user['User']['user_id'];
            $this->User->save($this->request->data['User']);
            $this->Session->setFlash('Updated!', '', array(''), 'front_success');
            return $this->redirect($this->referer());
        }
        $this->set('user', $user);
    }

    public function app_profile() {
        $this->layout = 'app';
        extract($_REQUEST);
        $user = $this->checkappuser($user_id);
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['User']['user_id'] = $user['User']['user_id'];

            if (!empty($this->request->data['User']['profile']['name'])) {
                $ext = $this->getFileExtension($this->request->data['User']['profile']['name']);
                $filename = uniqid() . '.' . $ext;
                move_uploaded_file($this->request->data['User']['profile']['tmp_name'], 'files/users/' . $filename);
                $this->request->data['User']['profile'] = $filename;
            } else {
                $this->request->data['User']['profile'] = $user['User']['profile'];
            }

            $this->User->save($this->request->data['User']);
            $this->Session->setFlash('Updated!', '', array(''), 'front_success');
            return $this->redirect($this->referer());
        }
        $this->set('user', $user);
    }

    public function kyc() {
        $this->layout = 'front';
        $user = $this->checkuser();
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['User']['user_id'] = $user['User']['user_id'];
            $this->User->save($this->request->data['User']);
            $this->Session->setFlash('Updated!', '', array(''), 'front_success');
            return $this->redirect($this->referer());
        }
        $this->set('user', $user);
        $pan = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'PAN Card')));
        $front = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'Address Proof Front')));
        $back = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'Address Proof Back')));
        $this->set('pan', $pan);
        $this->set('front', $front);
        $this->set('back', $back);
    }

    public function app_kyc() {
        $this->layout = 'app';
        extract($_REQUEST);
        $user = $this->checkappuser($user_id);
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['User']['user_id'] = $user['User']['user_id'];
            $this->User->save($this->request->data['User']);
            $this->Session->setFlash('Updated!', '', array(''), 'front_success');
            return $this->redirect($this->referer());
        }
        $this->set('user', $user);
        $pan = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'PAN Card')));
        $front = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'Address Proof Front')));
        $back = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'Address Proof Back')));
        $this->set('pan', $pan);
        $this->set('front', $front);
        $this->set('back', $back);
    }

    public function kycUpload($type = NULL) {
        $this->autoRender = false;
        $user = $this->checkuser();
        $types = array('image/png', 'image/jpg', 'image/jpeg');
        if ($type == 'pan_card' && !empty($this->request->data['User']['pancard']['name'])) {
            if (in_array($this->request->data['User']['pancard']['type'], $types)) {
                $check = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'PAN Card')));
                $pan = rand(0, 9999) . $this->request->data['User']['pancard']['name'];
                move_uploaded_file($this->request->data['User']['pancard']['tmp_name'], 'files/users/' . $pan);
                $this->request->data['Kycdocument']['user_id'] = $user['User']['user_id'];
                $this->request->data['Kycdocument']['document'] = 'PAN Card';
                $this->request->data['Kycdocument']['document_path'] = $pan;
                $this->request->data['Kycdocument']['document_id'] = (!empty($check)) ? $check['Kycdocument']['document_id'] : NULL;
                $this->Kycdocument->save($this->request->data['Kycdocument']);
                echo 'success';
            } else {
                echo 'Could not be Uploaded. Only files with the following extensions are allowed: png jpg jpeg.Error in importing file. Please try again.';
            }
        } else if ($type == 'addressproof_front' && !empty($this->request->data['User']['addressproof_front']['name'])) {
            if (in_array($this->request->data['User']['addressproof_front']['type'], $types)) {
                $check = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'Address Proof Front')));
                $pan = rand(0, 9999) . $this->request->data['User']['addressproof_front']['name'];
                move_uploaded_file($this->request->data['User']['addressproof_front']['tmp_name'], 'files/users/' . $pan);
                $this->request->data['Kycdocument']['user_id'] = $user['User']['user_id'];
                $this->request->data['Kycdocument']['document'] = 'Address Proof Front';
                $this->request->data['Kycdocument']['document_path'] = $pan;
                $this->request->data['Kycdocument']['document_id'] = (!empty($check)) ? $check['Kycdocument']['document_id'] : NULL;
                $this->Kycdocument->save($this->request->data['Kycdocument']);
                echo 'success';
            } else {
                echo 'Could not be Uploaded. Only files with the following extensions are allowed: png jpg jpeg.Error in importing file. Please try again.';
            }
        } else if ($type == 'addressproof_back' && !empty($this->request->data['User']['addressproof_back']['name'])) {
            if (in_array($this->request->data['User']['addressproof_back']['type'], $types)) {
                $check = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'Address Proof Front')));
                $pan = rand(0, 9999) . $this->request->data['User']['addressproof_back']['name'];
                move_uploaded_file($this->request->data['User']['addressproof_back']['tmp_name'], 'files/users/' . $pan);
                $this->request->data['Kycdocument']['user_id'] = $user['User']['user_id'];
                $this->request->data['Kycdocument']['document'] = 'Address Proof Back';
                $this->request->data['Kycdocument']['document_path'] = $pan;
                $this->request->data['Kycdocument']['document_id'] = (!empty($check)) ? $check['Kycdocument']['document_id'] : NULL;
                $this->Kycdocument->save($this->request->data['Kycdocument']);
                echo 'success';
            } else {
                echo 'Could not be Uploaded. Only files with the following extensions are allowed: png jpg jpeg.Error in importing file. Please try again.';
            }
        }
    }

    public function app_kycUpload($type = NULL) {
        $this->autoRender = false;
        $user = $this->checkappuser($_REQUEST['user_id']);
        $types = array('image/png', 'image/jpg', 'image/jpeg');
        if ($type == 'pan_card' && !empty($this->request->data['User']['pancard']['name'])) {
            if (in_array($this->request->data['User']['pancard']['type'], $types)) {
                $check = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'PAN Card')));
                $pan = rand(0, 9999) . $this->request->data['User']['pancard']['name'];
                move_uploaded_file($this->request->data['User']['pancard']['tmp_name'], 'files/users/' . $pan);
                $this->request->data['Kycdocument']['user_id'] = $user['User']['user_id'];
                $this->request->data['Kycdocument']['document'] = 'PAN Card';
                $this->request->data['Kycdocument']['document_path'] = $pan;
                $this->request->data['Kycdocument']['document_id'] = (!empty($check)) ? $check['Kycdocument']['document_id'] : NULL;
                $this->Kycdocument->save($this->request->data['Kycdocument']);
                echo 'success';
            } else {
                echo 'Could not be Uploaded. Only files with the following extensions are allowed: png jpg jpeg.Error in importing file. Please try again.';
            }
        } else if ($type == 'addressproof_front' && !empty($this->request->data['User']['addressproof_front']['name'])) {
            if (in_array($this->request->data['User']['addressproof_front']['type'], $types)) {
                $check = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'Address Proof Front')));
                $pan = rand(0, 9999) . $this->request->data['User']['addressproof_front']['name'];
                move_uploaded_file($this->request->data['User']['addressproof_front']['tmp_name'], 'files/users/' . $pan);
                $this->request->data['Kycdocument']['user_id'] = $user['User']['user_id'];
                $this->request->data['Kycdocument']['document'] = 'Address Proof Front';
                $this->request->data['Kycdocument']['document_path'] = $pan;
                $this->request->data['Kycdocument']['document_id'] = (!empty($check)) ? $check['Kycdocument']['document_id'] : NULL;
                $this->Kycdocument->save($this->request->data['Kycdocument']);
                echo 'success';
            } else {
                echo 'Could not be Uploaded. Only files with the following extensions are allowed: png jpg jpeg.Error in importing file. Please try again.';
            }
        } else if ($type == 'addressproof_back' && !empty($this->request->data['User']['addressproof_back']['name'])) {
            if (in_array($this->request->data['User']['addressproof_back']['type'], $types)) {
                $check = $this->Kycdocument->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'document' => 'Address Proof Front')));
                $pan = rand(0, 9999) . $this->request->data['User']['addressproof_back']['name'];
                move_uploaded_file($this->request->data['User']['addressproof_back']['tmp_name'], 'files/users/' . $pan);
                $this->request->data['Kycdocument']['user_id'] = $user['User']['user_id'];
                $this->request->data['Kycdocument']['document'] = 'Address Proof Back';
                $this->request->data['Kycdocument']['document_path'] = $pan;
                $this->request->data['Kycdocument']['document_id'] = (!empty($check)) ? $check['Kycdocument']['document_id'] : NULL;
                $this->Kycdocument->save($this->request->data['Kycdocument']);
                echo 'success';
            } else {
                echo 'Could not be Uploaded. Only files with the following extensions are allowed: png jpg jpeg.Error in importing file. Please try again.';
            }
        }
    }

    public function changepassword() {
        $this->layout = 'front';
        $user = $this->checkuser();
        if ($this->request->is(array('post', 'put'))) {
            if ($user['User']['password'] == md5($this->request->data['User']['current_password'])) {
                $user['User']['password'] = md5($this->request->data['User']['confirm_password']);
                $this->User->save($user['User']);
                $this->Session->setFlash('Updated!', '', array(''), 'front_success');
                return $this->redirect($this->referer());
            } else {
                $this->Session->setFlash('Old Password New Password Mismatch!', '', array(''), 'front_danger');
                return $this->redirect($this->referer());
            }
        }
    }

    public function app_changepassword() {
        $this->layout = 'app';
        extract($_REQUEST);
        $user = $this->checkappuser($user_id);
        if ($this->request->is(array('post', 'put'))) {
            if ($user['User']['password'] == md5($this->request->data['User']['current_password'])) {
                $user['User']['password'] = md5($this->request->data['User']['confirm_password']);
                $this->User->save($user['User']);
                $this->Session->setFlash('Updated!', '', array(''), 'front_success');
                return $this->redirect($this->referer());
            } else {
                $this->Session->setFlash('Old Password New Password Mismatch!', '', array(''), 'front_danger');
                return $this->redirect($this->referer());
            }
        }
    }

    public function changeemail() {
        $this->layout = 'front';
        $user = $this->checkuser();
        if ($this->request->is(array('post', 'put'))) {
            if ($this->request->data['User']['email'] != $user['User']['email']) {
                $check = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['email'], 'status !=' => 'Trash', 'user_id !=' => $user['User']['user_id'])));
                if (empty($check)) {
                    $user['User']['emailencode'] = $hash = md5(rand(0, 1000));
                    $user['User']['email'] = $this->request->data['User']['email'];
                    $user['User']['emailverified'] = 1;
                    $this->User->save($user['User']);
                    $activation_link = BASE_URL . 'users/activate/' . $hash;
                    $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '4')));
                    $message = str_replace(array('{name}', '{activation_link}'), array($user['User']['name'], $activation_link), $emailcontent['Emailcontent']['emailcontent']);
                    $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $user['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
                    $this->Session->setFlash('Welcome Message with instruction sent to your email ID!', '', array(''), 'front_success');
                    return $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash('Email id already exists!', '', array(''), 'front_danger');
                    return $this->redirect($this->referer());
                }
            } else {
                $this->Session->setFlash('Current email ID is same!', '', array(''), 'front_danger');
                return $this->redirect($this->referer());
            }
        }
    }

    public function app_changeemail() {
        $this->layout = 'app';
        extract($_REQUEST);
        $user = $this->checkappuser($user_id);
        if ($this->request->is(array('post', 'put'))) {
            if ($this->request->data['User']['email'] != $user['User']['email']) {
                $check = $this->User->find('first', array('conditions' => array('email' => $this->request->data['User']['email'], 'status !=' => 'Trash', 'user_id !=' => $user['User']['user_id'])));
                if (empty($check)) {
                    $user['User']['emailencode'] = $hash = md5(rand(0, 1000));
                    $user['User']['email'] = $this->request->data['User']['email'];
                    $user['User']['emailverified'] = 1;
                    $this->User->save($user['User']);
                    $activation_link = BASE_URL . 'users/activate/' . $hash;
                    $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '4')));
                    $message = str_replace(array('{name}', '{activation_link}'), array($user['User']['name'], $activation_link), $emailcontent['Emailcontent']['emailcontent']);
                    $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $user['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
                    $this->Session->setFlash('Welcome Message with instruction sent to your email ID!', '', array(''), 'front_success');
                    return $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash('Email id already exists!', '', array(''), 'front_danger');
                    return $this->redirect($this->referer());
                }
            } else {
                $this->Session->setFlash('Current email ID is same!', '', array(''), 'front_danger');
                return $this->redirect($this->referer());
            }
        }
    }

    public function changemobile() {
        $this->layout = 'front';
        $user = $this->checkuser();
        if ($this->request->is(array('post', 'put'))) {
            if ($this->request->data['User']['phone_number'] != $user['User']['phone_number']) {
                $check = $this->User->find('first', array('conditions' => array('phone_number' => $this->request->data['User']['phone_number'], 'status !=' => 'Trash', 'user_id !=' => $user['User']['user_id'])));
                if (!empty($check)) {
                    $user['User']['phone_number'] = $this->request->data['User']['phone_number'];
                    $this->User->save($user['User']);
                    return $this->redirect(array('action' => 'verifymobile'));
                }
            } else {
                $this->Session->setFlash('Current email ID is same!', '', array(''), 'front_danger');
                return $this->redirect($this->referer());
            }
        }
    }

    public function app_changemobile() {
        $this->layout = 'app';
        extract($_REQUEST);
        $user = $this->checkappuser($user_id);
        if ($this->request->is(array('post', 'put'))) {
            if ($this->request->data['User']['phone_number'] != $user['User']['phone_number']) {
                $check = $this->User->find('first', array('conditions' => array('phone_number' => $this->request->data['User']['phone_number'], 'status !=' => 'Trash', 'user_id !=' => $user['User']['user_id'])));
                if (!empty($check)) {
                    $user['User']['phone_number'] = $this->request->data['User']['phone_number'];
                    $this->User->save($user['User']);
                    return $this->redirect(array('action' => 'verifymobile'));
                }
            } else {
                $this->Session->setFlash('Current email ID is same!', '', array(''), 'front_danger');
                return $this->redirect($this->referer());
            }
        }
    }

    public function verifymobile() {
        $this->layout = 'front';
        $user = $this->checkuser();
        if ($this->request->is(array('post', 'put'))) {
            $check = $this->User->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'pin' => $user['User']['pin'])));
            if (!empty($check)) {
                $user['User']['phoneverified'] = '1';
                $user['User']['pin'] = '';
                $this->User->save($user['User']);
                $this->Session->setFlash('Mobile Number Verified', '', array(''), 'front_success');
                return $this->redirect(array('controller' => 'users', 'action' => 'profile'));
            } else {
                $this->Session->setFlash('Invalid PIN!', '', array(''), 'front_danger');
                return $this->redirect($this->referer());
            }
        }
    }

    public function resendpin() {
        $this->autoRender = false;
        $user = $this->checkuser();
        $user['User']['pin'] = $this->str_rand('5', 'numeric');
        $this->User->save($user['User']);
        $this->Session->setFlash('Pin Sent!', '', array(''), 'front_success');
        return $this->redirect(array('controller' => 'users', 'action' => 'verifymobile'));
    }

    public function verifyemail() {
        $this->layout = 'front';
        $user = $this->checkuser();
        if ($this->request->is(array('post', 'put'))) {
            $user['User']['emailencode'] = $hash = md5(rand(0, 1000));
            $this->User->save($user['User']);
            $activation_link = BASE_URL . 'users/activate/' . $hash;
            $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '4')));
            $message = str_replace(array('{name}', '{activation_link}'), array($user['User']['name'], $activation_link), $emailcontent['Emailcontent']['emailcontent']);
            $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $user['User']['email'], $emailcontent['Emailcontent']['subject'], $message);
            $this->Session->setFlash('Welcome Message with instruction sent to your email ID!', '', array(''), 'front_success');
            return $this->redirect(array('controller' => 'users', 'action' => 'profile'));
        }
    }

    public function activate($hash = NULL) {
        $this->autoRender = false;
        $check = $this->User->find('first', array('conditions' => array('emailencode' => $hash)));
        if (!empty($check)) {
            $check['User']['emailverified'] = '1';
            $check['User']['hash'] = '';
            $this->User->save($check['User']);
            $this->Session->setFlash('Email verified!', '', array(''), 'front_success');
            return $this->redirect(array('controller' => 'home', 'action' => 'index'));
        } else {
            $this->Session->setFlash('Activation Link Expired!', '', array(''), 'front_danger');
            return $this->redirect(array('controller' => 'home', 'action' => 'index'));
        }
    }

    public function withdrawals() {
        $this->layout = 'front';
        $user = $this->checkuser();
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Withdrawal']['user_id'] = $user['User']['user_id'];
            $this->request->data['Withdrawal']['date'] = date('Y-m-d H:i:s');
            $this->Withdrawal->save($this->request->data['Withdrawal']);
            $id = $this->Withdrawal->getLastInsertID();
            $withdrawal_id = 'WID' . ($id + 10000);
            $this->Withdrawal->updateAll(
                    array('Withdrawal.withdrawal_id' => "'" . $withdrawal_id . "'"), array('Withdrawal.id' => $id)
            );
            $amount = $user['User']['account_balance'] - $this->request->data['Withdrawal']['amount'];
            $this->User->updateAll(
                    array('User.account_balance' => "'" . $amount . "'"), array('User.user_id' => $user['User']['user_id'])
            );
            $this->Session->setFlash('Request Sent!', '', array(''), 'front_success');
            return $this->redirect(array('controller' => 'users', 'action' => 'withdrawalstatus'));
        }
    }

    public function app_withdrawals() {
        $this->layout = 'app';
        $user = $this->checkappuser($_REQUEST['user_id']);
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Withdrawal']['user_id'] = $user['User']['user_id'];
            $this->request->data['Withdrawal']['date'] = date('Y-m-d H:i:s');
            $this->Withdrawal->save($this->request->data['Withdrawal']);
            $id = $this->Withdrawal->getLastInsertID();
            $withdrawal_id = 'WID' . ($id + 10000);
            $this->Withdrawal->updateAll(
                    array('Withdrawal.withdrawal_id' => "'" . $withdrawal_id . "'"), array('Withdrawal.id' => $id)
            );
            $amount = $user['User']['withdrawable_balance'] - $this->request->data['Withdrawal']['amount'];
            $this->User->updateAll(
                    array('User.withdrawable_balance' => "'" . $amount . "'"), array('User.user_id' => $user['User']['user_id'])
            );
            $this->Session->setFlash('Request Sent!', '', array(''), 'front_success');
            return $this->redirect(array('controller' => 'users', 'action' => 'app_withdrawalstatus', '?' => array('user_id' => $_REQUEST['user_id'])));
        }
    }

    public function withdrawalstatus() {
        $this->layout = 'front';
        $user = $this->checkuser();
        $withdrawals = $this->Withdrawal->find('all', array('conditions' => array('user_id' => $user['User']['user_id'])));
        $this->set('withdrawals', $withdrawals);
    }

    public function app_withdrawalstatus() {
        $this->layout = 'app';
        $user = $this->checkappuser($_REQUEST['user_id']);
        $withdrawals = $this->Withdrawal->find('all', array('conditions' => array('user_id' => $user['User']['user_id'])));
        $this->set('withdrawals', $withdrawals);
    }

    public function transactions() {
        $this->layout = 'front';
        $user = $this->checkuser();
        $transactions = $this->Transaction->find('all', array('conditions' => array('user_id' => $user['User']['user_id'])));
        $this->set('transactions', $transactions);
    }

    public function inbox() {
        $this->layout = 'front';
        $user = $this->checkuser();
        $letters = $this->Letter->find('all', array('conditions' => array('NOT FIND_IN_SET(\'' . $user['User']['user_id'] . '\',Letter.delete)', 'user_id' => $user['User']['user_id'], 'type' => 'User')));
        $letters1 = $this->Letter->find('all', array('conditions' => array('NOT FIND_IN_SET(\'' . $user['User']['user_id'] . '\',Letter.delete)', 'type' => 'Common')));
        $this->set('letters', array_merge($letters, $letters1));
        if ($this->request->is(array('post', 'put'))) {
            $user_id = $user['User']['user_id'];
            $this->Letter->updateAll(
                    array('Letter.delete' => "concat(ifnull(Letter.delete,'0'), ',$user_id')"), array('Letter.letter_id' => $this->request->data['Letter']['letter_id'])
            );
            $this->Session->setFlash('Deleted!', '', array(''), 'front_success');
            return $this->redirect(array('controller' => 'users', 'action' => 'inbox'));
        }
    }

    public function addcash() {
        $this->layout = 'front';
        $user = $this->checkuser();
        if ($this->request->is(array('post', 'put'))) {
            $amount = $this->request->data['Addcash']['amount'];

            if (!empty($this->request->data['Addcash']['promocode'])) {
                $promo = $this->Promocode->find('first', array('conditions' => array('code' => $this->request->data['Addcash']['promocode'])));
                if (!empty($promo)) {
                    if ($promo['Promocode']['discount_to'] == 'All Users') {
                        $availability = true;
                    } elseif ($promo['Promocode']['discount_to'] == 'Selected Users' && in_array($user['User']['user_id'], explode(',', $promo['Promocode']['user_id']))) {
                        $availability = true;
                    } else {
                        $availability = false;
                    }
                    if ($availability) {
                        if (!empty($promo['Promocode']['available_from']) && ($promo['Promocode']['available_from'] > date('Y-m-d H:i:s'))) {
                            $expired = true;
                        } elseif (!empty($promo['Promocode']['available_to']) && ($promo['Promocode']['available_to'] < date('Y-m-d H:i:s'))) {
                            $expired = true;
                        } else {
                            $expired = false;
                        }
                        if ($expired == false) {
                            $check = $this->Userpromocode->find('first', array('conditions' => array('code' => $this->request->data['Addcash']['promocode'], 'user_id' => $user['User']['user_id'])));
                            if (empty($check)) {
                                if ($promo['Promocode']['discount_type'] == "Cash") {
                                    $discountamount = $promo['Promocode']['discount_value'];
                                } else {
                                    $discountamount = ($promo['Promocode']['discount_value'] / 100) * $amount;
                                }
                                $this->Session->write('discount', $discountamount);
                            }
                        }
                    }
                }
            }



            $name = $user['User']['name'];
            $number = $user['User']['phone_number'];
            $email = $user['User']['email'];
            $api_key = '993d68c59e86aa3913d4e6ac9ae8db2b';
            $api_secret = '86a174c9fa071c5b4d8cc68bb79969f6';
            $api_salt = 'f6abe546d4554711bd884d12cd6e5fda';
            $webhook_url = BASE_URL . 'users/webhook.php';
            $redirect_url = BASE_URL . 'users/cashreturn.php';
            $mode = "test"; //You can change it to live by jest replacing it by 'live'
            if ($mode == 'live') {
                $mode = 'www';
            } else {
                $mode = 'test';
            }
            $api = new Instamojo\Instamojo($api_key, $api_secret, 'https://test.instamojo.com/api/1.1/');
            try {
                $response = $api->paymentRequestCreate(array(
                    "purpose" => "Rummy Club - Add Cash",
                    "amount" => $amount,
                    "send_email" => true,
                    "email" => $user['User']['email'],
                    "redirect_url" => BASE_URL . "users/cash_return"
                ));
                $this->Session->write('request_id', $response['id']);
                $this->Session->write('Addcash', $this->request->data['Addcash']);
                $this->redirect($response['longurl']);
            } catch (Exception $e) {
                print('Error: ' . $e->getMessage());
            }
            exit;
        }
    }

    public function app_addcash() {
        $this->layout = 'app';
        $user = $this->checkappuser($_REQUEST['user_id']);
        if ($this->request->is(array('post', 'put'))) {
            $amount = $this->request->data['Addcash']['amount'];
            if (!empty($this->request->data['Addcash']['promocode'])) {
                $promo = $this->Promocode->find('first', array('conditions' => array('code' => $this->request->data['Addcash']['promocode'])));
                if (!empty($promo)) {
                    if ($promo['Promocode']['discount_to'] == 'All Users') {
                        $availability = true;
                    } elseif ($promo['Promocode']['discount_to'] == 'Selected Users' && in_array($user['User']['user_id'], explode(',', $promo['Promocode']['user_id']))) {
                        $availability = true;
                    } else {
                        $availability = false;
                    }
                    if ($availability) {
                        if (!empty($promo['Promocode']['available_from']) && ($promo['Promocode']['available_from'] > date('Y-m-d H:i:s'))) {
                            $expired = true;
                        } elseif (!empty($promo['Promocode']['available_to']) && ($promo['Promocode']['available_to'] < date('Y-m-d H:i:s'))) {
                            $expired = true;
                        } else {
                            $expired = false;
                        }
                        if ($expired == false) {
                            $check = $this->Userpromocode->find('first', array('conditions' => array('code' => $this->request->data['Addcash']['promocode'], 'user_id' => $user['User']['user_id'])));
                            if (empty($check)) {
                                if ($promo['Promocode']['discount_type'] == "Cash") {
                                    $discountamount = $promo['Promocode']['discount_value'];
                                } else {
                                    $discountamount = ($promo['Promocode']['discount_value'] / 100) * $amount;
                                }
                                $this->Session->write('discount', $discountamount);
                            }
                        }
                    }
                }
            }
            $name = $user['User']['name'];
            $number = $user['User']['phone_number'];
            $email = $user['User']['email'];
            $api_key = '993d68c59e86aa3913d4e6ac9ae8db2b';
            $api_secret = '86a174c9fa071c5b4d8cc68bb79969f6';
            $api_salt = 'f6abe546d4554711bd884d12cd6e5fda';
            $webhook_url = BASE_URL . 'users/webhook.php';
            $redirect_url = BASE_URL . 'users/cashreturn.php';
            $mode = "test"; //You can change it to live by jest replacing it by 'live'
            if ($mode == 'live') {
                $mode = 'www';
            } else {
                $mode = 'test';
            }
            $api = new Instamojo\Instamojo($api_key, $api_secret, 'https://test.instamojo.com/api/1.1/');
            try {
                $response = $api->paymentRequestCreate(array(
                    "purpose" => "Rummy Club - Add Cash",
                    "amount" => $amount,
                    "send_email" => true,
                    "email" => $user['User']['email'],
                    "redirect_url" => BASE_URL . "users/app_cash_return?user_id=" . $_REQUEST['user_id']
                ));
                $this->Session->write('request_id', $response['id']);
                $this->Session->write('Addcash', $this->request->data['Addcash']);
                $this->redirect($response['longurl']);
            } catch (Exception $e) {
                print('Error: ' . $e->getMessage());
            }
            exit;
        }
    }

    public function cash_return() {
        $this->layout = '';
        $this->autoRender = false;
        $user = $this->checkuser();
        $api_key = '993d68c59e86aa3913d4e6ac9ae8db2b';
        $api_secret = '86a174c9fa071c5b4d8cc68bb79969f6';
        $api_salt = 'f6abe546d4554711bd884d12cd6e5fda';
        $webhook_url = BASE_URL . 'users/webhook.php';
        $redirect_url = BASE_URL . 'users/cashreturn.php';
        $mode = "test"; //You can change it to live by jest replacing it by 'live'
        if ($mode == 'live') {
            $mode = 'www';
        } else {
            $mode = 'test';
        }
        $api = new Instamojo\Instamojo($api_key, $api_secret, 'https://test.instamojo.com/api/1.1/');
        try {
            $response = $api->paymentRequestStatus($this->Session->read('request_id'));
            if ($response['status'] == 'Completed') {
                $this->request->data['Addcash']['user_id'] = $user['User']['user_id'];
                $this->request->data['Addcash']['amount'] = $response['amount'];
                $this->request->data['Addcash']['discount'] = $this->Session->read('discount');
                $this->request->data['Addcash']['request_id'] = $response['id'];
                $this->request->data['Addcash']['payment_id'] = $response['payments'][0]['payment_id'];
                $this->request->data['Addcash']['datetime'] = date('Y-m-d H:i:s');
                $this->Addcash->save($this->request->data['Addcash']);
                $user['User']['user_id'] = $user['User']['user_id'];
                $user['User']['deposit_balance'] = $user['User']['deposit_balance'] + $this->Session->read('Addcash.deposit');
                $user['User']['total_cash_balance'] = $user['User']['withdrawable_balance'] + $user['User']['deposit_balance'];
                $user['User']['pending_bonus'] = $user['User']['pending_bonus'] + $this->Session->read('Addcash.pending_bonus');
                $this->User->save($user['User']);

                if (!empty($this->Session->read('Addcash.pending_bonus')) && $this->Session->read('Addcash.pending_bonus') > 0) {
                    $this->request->data['Pendingbonus']['user_id'] = $user['User']['user_id'];
                    $this->request->data['Pendingbonus']['title'] = 'On Add Cash';
                    $this->request->data['Pendingbonus']['bonus'] = $this->Session->read('Addcash.pending_bonus');
                    $this->request->data['Pendingbonus']['datetime'] = date('Y-m-d H:i:s');
                    $this->Pendingbonus->save($this->request->data['Pendingbonus']);
                }

                $this->Session->setFlash('Cash Added To Your Account!', '', array(''), 'front_success');
                return $this->redirect(array('controller' => 'users', 'action' => 'myaccount'));
            }
        } catch (Exception $e) {
            print('Error: ' . $e->getMessage());
        }
    }

    public function app_cash_return() {
        $this->layout = '';
        $this->autoRender = false;
        $user = $this->checkappuser($_REQUEST['user_id']);
        $api_key = '993d68c59e86aa3913d4e6ac9ae8db2b';
        $api_secret = '86a174c9fa071c5b4d8cc68bb79969f6';
        $api_salt = 'f6abe546d4554711bd884d12cd6e5fda';
        $webhook_url = BASE_URL . 'users/webhook.php';
        $redirect_url = BASE_URL . 'users/cashreturn.php';
        $mode = "test"; //You can change it to live by jest replacing it by 'live'
        if ($mode == 'live') {
            $mode = 'www';
        } else {
            $mode = 'test';
        }
        $api = new Instamojo\Instamojo($api_key, $api_secret, 'https://test.instamojo.com/api/1.1/');
        try {
            $response = $api->paymentRequestStatus($this->Session->read('request_id'));
            if ($response['status'] == 'Completed') {
                $this->request->data['Addcash']['user_id'] = $user['User']['user_id'];
                $this->request->data['Addcash']['amount'] = $response['amount'];
                $this->request->data['Addcash']['discount'] = $this->Session->read('discount');
                $this->request->data['Addcash']['request_id'] = $response['id'];
                $this->request->data['Addcash']['payment_id'] = $response['payments'][0]['payment_id'];
                $this->request->data['Addcash']['datetime'] = date('Y-m-d H:i:s');
                $this->Addcash->save($this->request->data['Addcash']);
                $user['User']['user_id'] = $user['User']['user_id'];
                $user['User']['deposit_balance'] = $user['User']['deposit_balance'] + $this->Session->read('Addcash.deposit');
                $user['User']['total_cash_balance'] = $user['User']['withdrawable_balance'] + $user['User']['deposit_balance'];
                $user['User']['pending_bonus'] = $user['User']['pending_bonus'] + $this->Session->read('Addcash.pending_bonus');
                $this->User->save($user['User']);

                if (!empty($this->Session->read('Addcash.pending_bonus')) && $this->Session->read('Addcash.pending_bonus') > 0) {
                    $this->request->data['Pendingbonus']['user_id'] = $user['User']['user_id'];
                    $this->request->data['Pendingbonus']['title'] = 'On Add Cash';
                    $this->request->data['Pendingbonus']['bonus'] = $this->Session->read('Addcash.pending_bonus');
                    $this->request->data['Pendingbonus']['datetime'] = date('Y-m-d H:i:s');
                    $startTime = date("Y-m-d H:i:s");
                    $add_date2 = date('Y-m-d H:i:s', strtotime('+2 days', strtotime($startTime)));
                    $timevariance = '';
                    $this->request->data['Pendingbonus']['enddatetime'] = $add_date2;
                    $this->Pendingbonus->save($this->request->data['Pendingbonus']);
                }
                $this->Session->setFlash('Cash Added To Your Account!' . date('Y-m-d H:i:s') . '--' . $timevariance . '--' . $add_date2, '', array(''), 'front_success');
                return $this->redirect(array('controller' => 'users', 'action' => 'app_myaccount', '?' => array('user_id' => $_REQUEST['user_id'])));
            }
        } catch (Exception $e) {
            print('Error: ' . $e->getMessage());
        }
    }

    public function refresh_practice() {
        $this->autoRender = false;
        $user = $this->checkuser();
        $this->User->updateAll(
                array('User.practice_cash' => "'5000'"), array('User.user_id' => $user['User']['user_id'])
        );
        $this->Session->setFlash('Practice Cash Updated!', '', array(''), 'front_success');
        return $this->redirect(array('controller' => 'users', 'action' => 'myaccount'));
    }

    public function app_refresh_practice() {
        $this->autoRender = false;
        $user = $this->checkappuser($_REQUEST['user_id']);
        $this->User->updateAll(
                array('User.practice_cash' => "'5000'"), array('User.user_id' => $user['User']['user_id'])
        );
        $this->Session->setFlash('Practice Cash Updated!', '', array(''), 'front_success');
        return $this->redirect(array('controller' => 'users', 'action' => 'app_myaccount', '?' => array('user_id' => $_REQUEST['user_id'])));
    }

    public function bonus() {
        $this->layout = 'front';
        $user = $this->checkuser();
    }

    public function app_bonus() {
        $this->layout = 'app';
        $user = $this->checkappuser($_REQUEST['user_id']);
    }

    public function admin_sendmail() {
        $this->layout = 'admin';
        $this->checkadmin();
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Letter']['datetime'] = date('Y-m-d H:i:s');
            $this->Letter->save($this->request->data['Letter']);
            $this->Session->setFlash('Sent!', '', array(''), 'success');
            return $this->redirect(array('controller' => 'users', 'action' => 'mail'));
        }
    }

    public function admin_mail() {
        $this->layout = 'admin';
        $this->checkadmin();
        $this->Letter->recursive = 0;
        $conditions = array();
        $this->paginate = array('conditions' => $conditions, 'order' => 'letter_id DESC', 'limit' => '10');
        $this->set('letters', $this->Paginator->paginate('Letter'));
    }

    public function admin_deletemail($id = NULL) {
        $this->layout = 'admin';
        $this->checkadmin();
        $this->Letter->delete($id);
        $this->Session->setFlash('Deleted!', '', array(''), 'success');
        return $this->redirect(array('controller' => 'users', 'action' => 'mail'));
    }

    public function admin_index() {
        $this->layout = 'admin';
        $this->checkadmin();
        $this->User->recursive = 0;
        $conditions = array('status !=' => 'Trash');
        if (!empty($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('user_name LIKE' => "%$s%", 'name LIKE' => "%$s%", 'email LIKE' => "%$s%", 'phone_number LIKE' => "%$s%", 'city LIKE' => "%$s%", 'state LIKE' => "%$s%", 'pincode LIKE' => "%$s%");
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'user_id DESC', 'limit' => '10');
        $this->set('users', $this->Paginator->paginate('User'));
    }

    public function admin_updatestatus($user_id = NULL, $status = NULL) {
        $this->layout = 'admin';
        $this->checkadmin();
        if (!$this->User->exists(['user_id' => $user_id])) {
            throw new NotFoundException(__("Invalid User "));
        }
        $this->User->updateAll(
                array('User.status' => "'$status'"), array('User.user_id' => $user_id)
        );
        $this->Session->setFlash('Updated!', '', array(''), 'success');
        return $this->redirect(array('controller' => 'users', 'action' => 'index'));
    }

    public function admin_view($user_id = NULL) {
        $this->layout = 'admin';
        $this->checkadmin();
        if (!$this->User->exists(['user_id' => $user_id])) {
            throw new NotFoundException(__("Invalid User "));
        }
        $user = $this->User->find('first', array('conditions' => array('user_id' => $user_id)));
        $this->set('user', $user);
    }

    public function admin_withdrawals() {
        $this->layout = 'admin';
        $this->checkadmin();
        $this->Withdrawal->recursive = 0;
        $conditions = array('status !=' => 'Trash');
        if (!empty($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('withdrawal_id' => "$s");
        }
        if (!empty($_REQUEST['status'])) {
            $conditions['status'] = $_REQUEST['status'];
        }
        if (!empty($_REQUEST['date'])) {
            $conditions['DATE(date)'] = date('Y-m-d', strtotime($_REQUEST['date']));
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'id DESC', 'limit' => '10');
        $this->set('withdrawals', $this->Paginator->paginate('Withdrawal'));
    }

    public function admin_updatewithdraw($id = NULL) {
        $this->layout = 'admin';
        $this->checkadmin();
        $withdraw = $this->Withdrawal->find('first', array('conditions' => array('id' => $id)));
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Withdrawal']['id'] = $id;
            $this->request->data['Withdrawal']['status'] = 'Transfered';
            $this->Withdrawal->save($this->request->data['Withdrawal']);
            $this->Session->setFlash('Updated!', '', array(''), 'success');
            return $this->redirect(array('controller' => 'users', 'action' => 'withdrawals'));
        }
        $this->set('withdrawal', $withdraw);
        $user = $this->User->find('first', array('conditions' => array('user_id' => $withdraw['Withdrawal']['user_id'])));
        $this->set('user', $user);
    }

    public function admin_viewwithdrawrequest($id = NULL) {
        $this->layout = 'admin';
        $this->checkadmin();
        $withdraw = $this->Withdrawal->find('first', array('conditions' => array('id' => $id)));
        $this->set('withdrawal', $withdraw);
        $user = $this->User->find('first', array('conditions' => array('user_id' => $withdraw['Withdrawal']['user_id'])));
        $this->set('user', $user);
    }

    public function admin_transactions() {
        $this->layout = 'admin';
        $this->checkadmin();
        $this->Transaction->recursive = 0;
        $conditions = array();
        if (!empty($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('transaction_id' => "$s");
        }
        if (!empty($_REQUEST['date'])) {
            $conditions['DATE(date)'] = date('Y-m-d', strtotime($_REQUEST['date']));
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'id DESC', 'limit' => '10');
        $this->set('transactions', $this->Paginator->paginate('Transaction'));
    }

    public function invitefriend() {
        $user = $this->checkuser();
    }

    public function app_invitefriend() {
        $this->layout = 'app';
        $user = $this->checkappuser($_REQUEST['user_id']);
    }

    public function google() {
        $user = $this->checkuser();
        App::import('Vendor', 'google/Google_Client');
        App::import('Vendor', 'google/contrib/Google_Oauth2Service.php');
        $redirect_url = BASE_URL . 'users/googlecallback';
        $gClient = new Google_Client();
        $gClient->setAccessType('offline');
        $gClient->setApplicationName(Google_product);
        $gClient->setClientId(Google_appkey);
        $gClient->setClientSecret(Google_appsecretkey);
        $gClient->setRedirectUri($redirect_url);
        $gClient->setScopes('https://www.google.com/m8/feeds');
        $gClient->setDeveloperKey(Google_developerkey);
        $authUrl = $gClient->createAuthUrl();
        $this->redirect($authUrl);
    }

    public function app_google() {
        $user = $this->checkappuser($_REQUEST['user_id']);
        $this->Session->write('app_user_id', $_REQUEST['user_id']);
        App::import('Vendor', 'google/Google_Client');
        App::import('Vendor', 'google/contrib/Google_Oauth2Service.php');
        $redirect_url = BASE_URL . 'users/app_googlecallback';
        $gClient = new Google_Client();
        $gClient->setAccessType('offline');
        $gClient->setApplicationName(Google_product);
        $gClient->setClientId(Google_appkey);
        $gClient->setClientSecret(Google_appsecretkey);
        $gClient->setRedirectUri($redirect_url);
        $gClient->setScopes('https://www.google.com/m8/feeds');
        $gClient->setDeveloperKey(Google_developerkey);
        $authUrl = $gClient->createAuthUrl();
        $this->redirect($authUrl);
    }

//    public function googlecallback() {
//
//        $user = $this->checkuser();
//
//        App::import('Vendor', 'google/Google_Client');
//        App::import('Vendor', 'google/contrib/Google_Oauth2Service.php');
//        $redirect_url = BASE_URL . 'users/googlecallback';
//        $gClient = new Google_Client();
//        $gClient->setApplicationName(Google_product);
//        $gClient->setClientId(Google_appkey);
//        $gClient->setClientSecret(Google_appsecretkey);
//        $gClient->setRedirectUri($redirect_url);
//        $gClient->setAccessType('offline');
//        $gClient->setScopes('https://www.google.com/m8/feeds');
//        $gClient->setDeveloperKey(Google_developerkey);
//
//        if (isset($_REQUEST['reset'])) {
//            unset($_SESSION['token']);
//            $gClient->revokeToken();
//            $this->redirect(filter_var($redirect_url, FILTER_SANITIZE_URL)); //redirect user back to page
//        }
//        if (isset($_GET['code'])) {
//            $gClient->authenticate($_GET['code']);
//            $_SESSION['token'] = $gClient->getAccessToken();
//            $this->redirect(filter_var($redirect_url, FILTER_SANITIZE_URL));
//        }
//
//        if ($_SESSION['token']) {
//            $gClient->setAccessToken($_SESSION['token']);
//        }
//
//
//
//        if ($gClient->getAccessToken()) {
//
//            $token = json_decode($_SESSION['token']);
//            $token->access_token;
//            $this->Session->write('access_token', $token->access_token);
//            $curl = curl_init("https://www.google.com/m8/feeds/contacts/default/full?alt=json&max-results=50&access_token=" . $token->access_token);
//            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
//            $contacts_json = curl_exec($curl);
//            curl_close($curl);
//            $contacts = json_decode($contacts_json, true);
//            $return = array();
//            foreach ($contacts['feed']['entry'] as $contact) {
//                if (!empty($contact['gd$email'][0]['address'])) {
//                    $check = $this->Invitation->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'email' => $contact['gd$email'][0]['address'])));
//                    if (empty($check)) {
//                        $return[] = array('name' => $contact['title']['$t'],
//                            'email' => isset($contact['gd$email'][0]['address']) ? $contact['gd$email'][0]['address'] : false,
//                            'phone' => isset($contact['gd$phoneNumber'][0]['address']) ? $contact['gd$phoneNumber'][0]['address'] : false);
//                    }
//                }
//            }
//            $this->set('emails', $return);
//            $this->render('hotmailcontacts');
//        }
//    }

    public function app_googlecallback() {
        $this->layout = 'app';
        $user = $this->checkappuser($this->Session->read('app_user_id'));

        App::import('Vendor', 'google/Google_Client');
        App::import('Vendor', 'google/contrib/Google_Oauth2Service.php');
        $redirect_url = BASE_URL . 'users/app_googlecallback';
        $gClient = new Google_Client();
        $gClient->setApplicationName(Google_product);
        $gClient->setClientId(Google_appkey);
        $gClient->setClientSecret(Google_appsecretkey);
        $gClient->setRedirectUri($redirect_url);
        $gClient->setAccessType('offline');
        $gClient->setScopes('https://www.google.com/m8/feeds');
        $gClient->setDeveloperKey(Google_developerkey);

        if (isset($_REQUEST['reset'])) {
            unset($_SESSION['token']);
            $gClient->revokeToken();
            $this->redirect(filter_var($redirect_url, FILTER_SANITIZE_URL)); //redirect user back to page
        }
        if (isset($_GET['code'])) {
            $gClient->authenticate($_GET['code']);
            $_SESSION['token'] = $gClient->getAccessToken();
            $this->redirect(filter_var($redirect_url, FILTER_SANITIZE_URL));
        }

        if ($_SESSION['token']) {
            $gClient->setAccessToken($_SESSION['token']);
        }


        if ($gClient->getAccessToken()) {

            $token = json_decode($_SESSION['token']);
            $token->access_token;
            $curl = curl_init("https://www.google.com/m8/feeds/contacts/default/full?alt=json&max-results=50&access_token=" . $token->access_token);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
            $contacts_json = curl_exec($curl);
            curl_close($curl);
            $contacts = json_decode($contacts_json, true);
            $return = array();
            foreach ($contacts['feed']['entry'] as $contact) {
                if (!empty($contact['gd$email'][0]['address'])) {
                    $check = $this->Invitation->find('first', array('conditions' => array('user_id' => $user['User']['user_id'], 'email' => $contact['gd$email'][0]['address'])));
                    if (empty($check)) {
                        $return[] = array('name' => $contact['title']['$t'],
                            'email' => isset($contact['gd$email'][0]['address']) ? $contact['gd$email'][0]['address'] : false,
                            'phone' => isset($contact['gd$phoneNumber'][0]['address']) ? $contact['gd$phoneNumber'][0]['address'] : false);
                    }
                }
            }
            $this->set('emails', $return);
            $this->render('app_hotmailcontacts');
        }
    }

    public function sendinvitation() {
        $user = $this->checkuser();
        if (isset($_POST['email'])) {
            foreach ($_POST['email'] as $key => $email) {
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '6')));
                $message = str_replace(array('{name}', '{base_url}', '{link}'), array($_POST['name'][$key], BASE_URL, BASE_URL . '?refid=' . $user['User']['emailencode']), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $email, $emailcontent['Emailcontent']['subject'], $message);

                $this->request->data['Invitation']['user_id'] = $user['User']['user_id'];
                $this->request->data['Invitation']['name'] = $_POST['name'][$key];
                $this->request->data['Invitation']['email'] = $email;
                $this->Invitation->saveAll($this->request->data['Invitation']);
            }
        }
        if (isset($_REQUEST['ajax'])) {
            echo json_encode(array('code' => '200', 'message' => 'Successly Invited'));
        }
        $this->Session->setFlash('Invitation Sent!', '', array(''), 'success');
        return $this->redirect(array('controller' => 'users', 'action' => 'googlecallback'));
    }

    public function app_sendinvitation() {
        $user = $this->checkappuser($_REQUEST['user_id']);
        if (isset($_POST['email'])) {
            foreach ($_POST['email'] as $key => $email) {
                $emailcontent = $this->Emailcontent->find('first', array('conditions' => array('emailcontent_id' => '6')));
                $message = str_replace(array('{name}', '{base_url}', '{link}'), array($_POST['name'][$key], BASE_URL, BASE_URL . '?refid=' . $user['User']['emailencode']), $emailcontent['Emailcontent']['emailcontent']);
                $this->mailsend($emailcontent['Emailcontent']['fromname'], $emailcontent['Emailcontent']['fromemail'], $email, $emailcontent['Emailcontent']['subject'], $message);

                $this->request->data['Invitation']['user_id'] = $user['User']['user_id'];
                $this->request->data['Invitation']['name'] = $_POST['name'][$key];
                $this->request->data['Invitation']['email'] = $email;
                $this->Invitation->saveAll($this->request->data['Invitation']);
            }
        }
        if (isset($_REQUEST['ajax'])) {
            echo json_encode(array('code' => '200', 'message' => 'Successly Invited'));
        }
        $this->Session->setFlash('Invitation Sent!', '', array(''), 'success');
        return $this->redirect(array('controller' => 'users', 'action' => 'app_googlecallback'));
    }

    public function invitationstatus() {
        $user = $this->checkuser();
        $invitations = $this->Invitation->find('all', array('conditions' => array('user_id' => $user['User']['user_id'])));
        $this->set('invitations', $invitations);
    }

    public function app_invitationstatus() {
        $this->layout = 'app';
        $user = $this->checkappuser($_REQUEST['user_id']);
        $invitations = $this->Invitation->find('all', array('conditions' => array('user_id' => $user['User']['user_id'])));
        $this->set('invitations', $invitations);
    }

    public function applypromocode() {
        $this->autoRender = false;
        $user = $this->checkuser();
        $promo = $this->Promocode->find('first', array('conditions' => array('code' => $_REQUEST['promocode'])));
        if (!empty($promo)) {
            if ($promo['Promocode']['discount_to'] == 'All Users') {
                $availability = true;
            } elseif ($promo['Promocode']['discount_to'] == 'Selected Users' && in_array($user['User']['user_id'], explode(',', $promo['Promocode']['user_id']))) {
                $availability = true;
            } else {
                $availability = false;
            }
            if ($availability) {
                if (!empty($promo['Promocode']['available_from']) && ($promo['Promocode']['available_from'] > date('Y-m-d H:i:s'))) {
                    $expired = true;
                } elseif (!empty($promo['Promocode']['available_to']) && ($promo['Promocode']['available_to'] < date('Y-m-d H:i:s'))) {
                    $expired = true;
                } else {
                    $expired = false;
                }
                if ($expired == false) {
                    $check = $this->Userpromocode->find('first', array('conditions' => array('code' => $_REQUEST['promocode'], 'user_id' => $user['User']['user_id'])));
                    if (empty($check)) {
                        if ($promo['Promocode']['discount_type'] == "Cash") {
                            $amount = $promo['Promocode']['discount_value'];
                        } else {
                            $amount = ($promo['Promocode']['discount_value'] / 100) * $_REQUEST['amount'];
                        }
                        $result = array('code' => '0', 'message' => 'Promocode successfully applied!', 'discount_type' => $promo['Promocode']['discount_type'], 'discount_value' => $promo['Promocode']['discount_value'], 'amount' => $amount);
                    } else {
                        $result = array('code' => '200', 'message' => 'Promocode already applied!');
                    }
                } else {
                    $result = array('code' => '200', 'message' => 'Promocode Expired!');
                }
            } else {
                $result = array('code' => '200', 'message' => 'Promocode not available for you!');
            }
        } else {
            $result = array('code' => '200', 'message' => 'Invalid Promocode');
        }
        echo json_encode($result);
        exit;
    }

    public function app_applypromocode() {
        $this->autoRender = false;
        $user = $this->checkappuser($_REQUEST['user_id']);
        $promo = $this->Promocode->find('first', array('conditions' => array('code' => $_REQUEST['promocode'])));
        if (!empty($promo)) {
            if ($promo['Promocode']['discount_to'] == 'All Users') {
                $availability = true;
            } elseif ($promo['Promocode']['discount_to'] == 'Selected Users' && in_array($user['User']['user_id'], explode(',', $promo['Promocode']['user_id']))) {
                $availability = true;
            } else {
                $availability = false;
            }
            if ($availability) {
                if (!empty($promo['Promocode']['available_from']) && ($promo['Promocode']['available_from'] > date('Y-m-d H:i:s'))) {
                    $expired = true;
                } elseif (!empty($promo['Promocode']['available_to']) && ($promo['Promocode']['available_to'] < date('Y-m-d H:i:s'))) {
                    $expired = true;
                } else {
                    $expired = false;
                }
                if ($expired == false) {
                    $check = $this->Userpromocode->find('first', array('conditions' => array('code' => $_REQUEST['promocode'], 'user_id' => $user['User']['user_id'])));
                    if (empty($check)) {
                        if ($promo['Promocode']['discount_type'] == "Cash") {
                            $amount = $promo['Promocode']['discount_value'];
                        } else {
                            $amount = ($promo['Promocode']['discount_value'] / 100) * $_REQUEST['amount'];
                        }
                        $result = array('code' => '0', 'message' => 'Promocode successfully applied!', 'discount_type' => $promo['Promocode']['discount_type'], 'discount_value' => $promo['Promocode']['discount_value'], 'amount' => $amount);
                    } else {
                        $result = array('code' => '200', 'message' => 'Promocode already applied!');
                    }
                } else {
                    $result = array('code' => '200', 'message' => 'Promocode Expired!');
                }
            } else {
                $result = array('code' => '200', 'message' => 'Promocode not available for you!');
            }
        } else {
            $result = array('code' => '200', 'message' => 'Invalid Promocode');
        }
        echo json_encode($result);
        exit;
    }

    public function viewmail() {
        $this->autoRender = false;
        $letter = $this->Letter->find('first', array('conditions' => array('letter_id' => $_REQUEST['id'])));
        if (!empty($letter)) {
            $result = array(
                'code' => '0',
                'subject' => $letter['Letter']['subject'],
                'message' => $letter['Letter']['detail'],
            );
        } else {
            $result = array(
                'code' => '200',
                'message' => 'Mail not found',
            );
        }
        echo json_encode($result);
        exit;
    }

    public function googlemybusiness() {
        $user = $this->checkuser();
        App::import('Vendor', 'google/Google_Client');
        App::import('Vendor', 'google/contrib/Google_Oauth2Service.php');
        $redirect_url = BASE_URL . 'users/googlecallback';
        $gClient = new Google_Client();
        $gClient->setAccessType('offline');
        $gClient->setApplicationName(Google_product);
        $gClient->setClientId(Google_appkey);
        $gClient->setClientSecret(Google_appsecretkey);
        $gClient->setRedirectUri($redirect_url);
        $gClient->setScopes('https://www.googleapis.com/auth/business.manage');
        $gClient->setDeveloperKey(Google_developerkey);
        $authUrl = $gClient->createAuthUrl();
        $this->redirect($authUrl);
    }

    public function googlecallback() {

        $user = $this->checkuser();

        App::import('Vendor', 'google/Google_Client');
        App::import('Vendor', 'google/contrib/Google_Oauth2Service.php');
        $redirect_url = BASE_URL . 'users/googlecallback';
        $gClient = new Google_Client();
        $gClient->setApplicationName(Google_product);
        $gClient->setClientId(Google_appkey);
        $gClient->setClientSecret(Google_appsecretkey);
        $gClient->setRedirectUri($redirect_url);
        $gClient->setAccessType('offline');
        $gClient->setScopes('https://www.google.com/m8/feeds');
        $gClient->setDeveloperKey(Google_developerkey);

        if (isset($_REQUEST['reset'])) {
            unset($_SESSION['token']);
            $gClient->revokeToken();
            $this->redirect(filter_var($redirect_url, FILTER_SANITIZE_URL)); //redirect user back to page
        }
        if (isset($_GET['code'])) {
            $gClient->authenticate($_GET['code']);
            $_SESSION['token'] = $gClient->getAccessToken();
            $this->redirect(filter_var($redirect_url, FILTER_SANITIZE_URL));
        }
        if ($_SESSION['token']) {
            $gClient->setAccessToken($_SESSION['token']);
        }
        if ($gClient->getAccessToken()) {
            $token = json_decode($_SESSION['token']);
            $token->access_token;
            $this->Session->write('access_token', $token->access_token);
            $curl = curl_init("https://mybusiness.googleapis.com/v4/accounts/?access_token=" . $this->Session->read('access_token'));
            $headers = [
                'Authorization: Bearer ' . $this->Session->read('access_token'),
            ];

            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
            $contacts_json = curl_exec($curl);
            curl_close($curl);
            pr($contacts_json);
            echo $this->Session->read('access_token');
            exit;
        }
    }

}
