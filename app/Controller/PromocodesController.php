<?php

App::uses('AppController', 'Controller');

class PromocodesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Promocode');
    public $layout = 'home';

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->layout = 'admin';
        $this->Promocode->recursive = 0;
        $this->checkadmin();
        $conditions = array('status !=' => 'Trash');
        if (isset($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('promotype LIKE' => '%' . $s . '%', 'code LIKE' => $s);
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'promocode_id DESC', 'limit' => '50');
        $this->set('promocodes', $this->Paginator->paginate('Promocode'));
    }

    public function admin_add() {
        $this->checkadmin();
        $this->layout = 'admin';
        if ($this->request->is('post')) {
            $check = $this->Promocode->find('first', array('conditions' => array('code' => $this->request->data['Promocode']['code'], 'status !=' => 'Trash')));
            if (!empty(check)) {
                if (!empty($this->request->data['Promocode']['available_from'])) {
                    $this->request->data['Promocode']['available_from'] = date('Y-m-d h:i:s', strtotime($this->request->data['Promocode']['available_from']));
                }
                if (!empty($this->request->data['Promocode']['available_to'])) {
                    $this->request->data['Promocode']['available_to'] = date('Y-m-d h:i:s', strtotime($this->request->data['Promocode']['available_to']));
                }
                if ($this->request->data['Promocode']['discount_to'] == 'Selected Users') {
                    $this->request->data['Promocode']['user_id'] = implode(',', $this->request->data['Promocode']['user']);
                } else {
                    $this->request->data['Promocode']['user_id'] = '';
                }
                $this->request->data['Promocode']['created_date'] = date('Y-m-d h:i:s');
                $this->Promocode->save($this->request->data);
                $this->Session->setFlash('Data Saved successfully!', '', array(''), 'success');
                $this->redirect(array("controller" => "promocodes", "action" => "index"));
            } else {
                $this->Session->setFlash('Code already exists!', '', array(''), 'success');
                $this->redirect(array("controller" => "promocodes", "action" => "index"));
            }
        }
    }

    public function admin_edit($id = null) {
        $this->layout = 'admin';
        $this->checkadmin();
        if (!$this->Promocode->exists($id)) {
            throw new NotFoundException(__('Invalid Promocode'));
        }
        $promocode = $this->Promocode->find('first', array('conditions' => array('promocode_id' => $id)));
        if ($this->request->is(array('post', 'put'))) {
            $check = $this->Promocode->find('first', array('conditions' => array('code' => $this->request->data['Promocode']['code'], 'promocode_id !=' => $id, 'status !=' => 'Trash')));
            if (empty($check)) {
                $this->request->data['Promocode']['promocode_id'] = $id;
                if (!empty($this->request->data['Promocode']['available_from'])) {
                    $this->request->data['Promocode']['available_from'] = date('Y-m-d h:i:s', strtotime($this->request->data['Promocode']['available_from']));
                }
                if (!empty($this->request->data['Promocode']['available_to'])) {
                    $this->request->data['Promocode']['available_to'] = date('Y-m-d h:i:s', strtotime($this->request->data['Promocode']['available_to']));
                }
                if ($this->request->data['Promocode']['discount_to'] == 'Selected Users') {
                    $this->request->data['Promocode']['user_id'] = implode(',', $this->request->data['Promocode']['user_id']);
                } else {
                    $this->request->data['Promocode']['user_id'] = '';
                }
                $this->Promocode->save($this->request->data['Promocode']);
                $this->Session->setFlash('Promocode details updated ', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Promocode name already exists', '', array(''), 'danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
        $this->request->data['Promocode'] = $promocode['Promocode'];
    }

    public function admin_delete($id = NULL) {
        $this->autoRender = false;
        $this->checkadmin();
        $this->Promocode->updateAll(
                array('Promocode.status' => "'Trash'"), array('Promocode.promocode_id' => $id)
        );
        $this->Session->setFlash('Promocode deleted successfully!', '', array(''), 'success');
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_users() {
        $this->autoRender = false;
        $this->checkadmin();
        extract($_REQUEST);
        $users = $this->User->find('all', array('conditions' => array('user_name LIKE' => "%$q%", 'status' => 'Active')));
        foreach ($users as $user) {
            $items[] = array(
                'name' => $user['User']['user_name'],
                'id' => $user['User']['user_id']
            );
        }
        return json_encode(array('total_count' => count($users), 'incomplete_results' => false, 'items' => $items));
    }

}
