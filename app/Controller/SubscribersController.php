<?php

App::uses('AppController', 'Controller');

/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class SubscribersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Subscriber');
    public $layout = 'admin';

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $this->Subscriber->recursive = 0;
        $conditions = array();
        if (!empty($_REQUEST['s'])) {
            $conditions['email'] = $_REQUEST['s'];
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'subscriber_id DESC', 'limit' => '10');
        $this->set('subscribers', $this->Paginator->paginate('Subscriber'));
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Subscriber->exists($id)) {
            throw new NotFoundException(__('Subscriber Not Found'));
        }
        if ($this->Subscriber->delete($id)) {
            $this->Session->setFlash('Subscriber deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Subscriber could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

}
