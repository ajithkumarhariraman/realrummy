<?php

App::uses('AppController', 'Controller');

class PromotionsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Promotion');
    public $layout = 'home';

    /**
     * AdminIndex
     *
     * @return void
     */
    public function admin_index() {
        $this->layout = 'admin';
        $this->Promotion->recursive = 0;
        $this->checkadmin();
        $conditions = array('status !=' => 'Trash');
        if (isset($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('name LIKE' => '%' . $s . '%', 'email LIKE' => '%' . $s . '%', 'mobile LIKE' => '%' . $s . '%');
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'promotion_id DESC', 'limit' => '50');
        $this->set('promotions', $this->Paginator->paginate('Promotion'));
    }

    public function index() {
        $this->layout = 'front';
        $this->Promotion->recursive = 0;
        $conditions = array('status !=' => 'Trash');
        if (isset($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('name LIKE' => '%' . $s . '%', 'email LIKE' => '%' . $s . '%', 'mobile LIKE' => '%' . $s . '%');
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'promotion_id DESC', 'limit' => '50');
        $this->set('promotions', $this->Paginator->paginate('Promotion'));
    }

    public function app_index() {
        $this->layout = 'app';
        $this->Promotion->recursive = 0;
        $conditions = array('status !=' => 'Trash');
        if (isset($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('name LIKE' => '%' . $s . '%', 'email LIKE' => '%' . $s . '%', 'mobile LIKE' => '%' . $s . '%');
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'promotion_id DESC', 'limit' => '50');
        $this->set('promotions', $this->Paginator->paginate('Promotion'));
    }

    public function admin_add() {
        $this->checkadmin();
        $this->layout = 'admin';
        if ($this->request->is('post')) {
            if ($this->request->data['Promotion']['image']['name'] != '') {
                $logo = rand(0, 9999) . $this->request->data['Promotion']['image']['name'];
                move_uploaded_file($this->request->data['Promotion']['image']['tmp_name'], 'img/' . $logo);
            } else {
                $logo = $setting['Promotion']['image'];
            }
            $this->request->data['Promotion']['image'] = $logo;
            $this->request->data['Promotion']['created_date'] = date('Y-m-d h:i:s');
            $this->request->data['Promotion']['modified_date'] = date('Y-m-d h:i:s');
            $this->Promotion->save($this->request->data);
            $this->Session->setFlash('Data Saved successfully!', '', array(''), 'success');
            $this->redirect(array("controller" => "promotions", "action" => "index"));
        }
    }

    public function admin_edit($id = null) {
        $this->layout = 'admin';
        $this->checkadmin();
        if (!$this->Promotion->exists($id)) {
            throw new NotFoundException(__('Invalid Promotion'));
        }
        $promotion = $this->Promotion->find('first', array('conditions' => array('promotion_id' => $id)));
        if ($this->request->is(array('post', 'put'))) {
            $check = $this->Promotion->find('first', array('conditions' => array('title' => $this->request->data['Promotion']['title'], 'promotion_id !=' => $id)));

            if (empty($check)) {
                if (!empty($this->request->data['Promotion']['image']['name'])) {
                    $imagename = $this->web_to_server($this->request->data['Promotion']['image'], 'img/');
                    $this->request->data['Promotion']['image'] = $imagename;
                } else {
                    $this->request->data['Promotion']['image'] = $promotion['Promotion']['image'];
                }
                $this->request->data['Promotion']['promotion_id'] = $id;
                $this->Promotion->save($this->request->data['Promotion']);
                $this->Session->setFlash('Promotion details updated ', '', array(''), 'success');
                $this->request->data['Promotion']['modified_date'] = date('Y-m-d h:i:s');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Promotion name already exists', '', array(''), 'danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
        $this->request->data['Promotion'] = $promotion['Promotion'];
        $promotion = $this->Promotion->find('all');
        $this->set('Promotions', $promotion);
    }

    public function detail($id = NULL) {
        $this->layout = 'front';
        $promotion = ClassRegistry::init('Promotion')->find('first', array('conditions' => array('promotion_id' => $id)));
        $this->set('results', $promotion);
        $conditions = array('promotion_id' => $promotion['Promotion']['promotion_id']);
        $this->paginate = array('conditions' => $conditions, 'promotion' => 'promotion_id DESC', 'limit' => '50');
        $this->set('Promotions', $this->Paginator->paginate('Promotion'));
    }

    public function app_detail($id = NULL) {
        $this->layout = 'app';
        $promotion = ClassRegistry::init('Promotion')->find('first', array('conditions' => array('promotion_id' => $id)));
        $this->set('results', $promotion);
        $conditions = array('promotion_id' => $promotion['Promotion']['promotion_id']);
        $this->paginate = array('conditions' => $conditions, 'promotion' => 'promotion_id DESC', 'limit' => '50');
        $this->set('Promotions', $this->Paginator->paginate('Promotion'));
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Promotion->exists($id)) {
            throw new NotFoundException(__('Promotion Not Found'));
        }
        $this->request->data['Promotion']['promotion_id'] = $id;
        $this->request->data['Promotion']['status'] = 'Trash';
        if ($this->Promotion->save($this->request->data['Promotion'])) {
            $this->Session->setFlash('Promotion detail deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Promotion detail could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

}
