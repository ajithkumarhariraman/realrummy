<?php

App::uses('AppController', 'Controller');

/**
 * Sliders Controller
 *
 * @property Slider $Slider
 * @property PaginatorComponent $Paginator
 */
class SlidersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Slider', 'User', 'Adminuser', 'Category', 'Size', 'Color', 'Product', 'Attr', 'Productimage', 'Subcategory');
    public $layout = 'admin';

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $this->Slider->recursive = 0;
        $conditions = array();
        $this->paginate = array('conditions' => $conditions, 'order' => 'slider_id DESC', 'limit' => '10');
        $this->set('sliders', $this->Paginator->paginate('Slider'));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        $this->checkadmin();
        if ($this->request->is('post')) {
            $this->Slider->create();
            if (!empty($this->request->data['Slider']['image']['name'])) {
                extract($this->request->data['Slider']['image']);
                if ($size && !$error) {
                    $extension = $this->getFileExtension($name);
                    $extension = strtolower($extension);
                    $m = explode(".", $name);
                    $imagename = time() . $m[0] . "." . $extension;
                    $destination = 'files/sliders/' . $imagename;
                    move_uploaded_file($tmp_name, $destination);
                }
                $this->request->data['Slider']['image'] = $imagename;
            }
            $this->Slider->save($this->request->data);
            $this->Session->setFlash('Slider added successfully.', '', array(''), 'success');
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {

        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Slider->exists($id)) {
            throw new NotFoundException(__('Slider Not Found'));
        }
        if ($this->Slider->delete($id)) {
            $this->Session->setFlash('Slider deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Slider could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

}
