<?php

App::uses('AppController', 'Controller');

/**
 * Staticpages Controller
 *
 * @property Staticpage $Staticpage
 * @property PaginatorComponent $Paginator
 */
class FaqsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Faq', 'Faqcategory');
    public $layout = 'admin';

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->checkadmin();
        $this->Faq->recursive = 0;
        $conditions = array();
        if (!empty($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('title LIKE' => "%$s%");
        }
        if (!empty($_REQUEST['category_id'])) {
            $conditions['category_id'] = $_REQUEST['category_id'];
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'faq_id DESC', 'limit' => '10');
        $this->set('faqs', $this->Paginator->paginate('Faq'));

        $categories = $this->Faqcategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('categories', $categories);
    }

    public function admin_add() {
        $this->checkadmin();
        if ($this->request->is('post')) {
            $check = $this->Faq->find('first', array('conditions' => array('title' => $this->request->data['Faq']['title'])));
            if (empty($check)) {
                $this->Faq->save($this->request->data['Faq']);
                $this->Session->setFlash('Faq Added', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Faq title already exists', '', array(''), 'danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
        $faqs = $this->Faq->find('all', array('conditions' => array()));
        $this->set('faqs', $faqs);

        $categories = $this->Faqcategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('categories', $categories);
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->checkadmin();
        if (!$this->Faq->exists($id)) {
            throw new NotFoundException(__('Invalid Faq'));
        }
        $category = $this->Faq->find('first', array('conditions' => array('faq_id' => $id)));
        if ($this->request->is(array('post', 'put'))) {
            $check = $this->Faq->find('first', array('conditions' => array('title' => $this->request->data['Faq']['title'], 'faq_id !=' => $id)));
            if (empty($check)) {
                $this->Faq->save($this->request->data['Faq']);
                $this->Session->setFlash('Faq updated ', '', array(''), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Faq title already exists', '', array(''), 'danger');
                return $this->redirect(array('action' => 'index'));
            }
        }
        $this->request->data['Faq'] = $category['Faq'];

        $categories = $this->Faqcategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('categories', $categories);
    }

    public function admin_delete($id = null) {
        $this->autorender = false;
        $this->checkadmin();
        if (!$this->Faq->exists($id)) {
            throw new NotFoundException(__('Faq Not Found'));
        }
        if ($this->Faq->delete($id)) {
            $this->Session->setFlash('Faq deleted successfully!', '', array(''), 'success');
        } else {
            $this->Session->setFlash('Faq could not be deleted! Please try again later!', '', array(''), 'danger');
        }
        $this->redirect(array('action' => 'index'));
    }

    public function index($id = NULL) {
        $this->layout = 'front';

        $categories = $this->Faqcategory->find('all', array('conditions' => array('status' => 'Active')));
        $this->set('categories', $categories);
        
        if ($id != NULL) {
            $conditions = array('category_id' => $id);
        } else {
            $conditions = array('category_id' => $categories[0]['Faqcategory']['category_id']);
        }
        $faqs = $this->Faq->find('all', array('conditions' => $conditions));
        $this->set('faqs', $faqs);
        
        $category = $this->Faqcategory->find('first', array('conditions' => $conditions));
        $this->set('c_category', $category);
    }

}
