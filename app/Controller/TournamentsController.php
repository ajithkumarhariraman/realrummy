<?php
include 'path.php';
App::uses('AppController', 'Controller');

/**
 * Contects Controller
 *
 * @property Contect $Contect
 * @property PaginatorComponent $Paginator
 */
class TournamentsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Adminuser', 'Tournament', 'Emailcontent', 'Sitesetting', 'Tournamentplayer');
    public $layout = 'admin';

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->request->data['Tournament']['starts'] = date('Y-m-d H:i:s', strtotime($this->request->data['Tournament']['starts_date'] . ' ' . $this->request->data['Tournament']['starts_time']));
            $this->request->data['Tournament']['reg_starts'] = date('Y-m-d H:i:s', strtotime($this->request->data['Tournament']['reg_starts_date'] . ' ' . $this->request->data['Tournament']['reg_starts_time']));
            $this->request->data['Tournament']['reg_ends_date'] = date('Y-m-d H:i:s', strtotime($this->request->data['Tournament']['reg_ends_date'] . ' ' . $this->request->data['Tournament']['reg_ends_time']));
            $this->request->data['Tournament']['created_date'] = date('Y-m-d H:i:s');
            $this->Tournament->save($this->request->data['Tournament']);
            $id = $this->Tournament->getLastInsertID();
            $this->Tournament->updateAll(
                    array('Tournament.tournamentid' => "'" . (1000 + $id) . "'"), array('Tournament.tournament_id' => $id)
            );
            $this->Session->setFlash('Tournament Created', '', array(''), 'success');
            return $this->redirect(array('action' => 'index'));
        }
    }

    public function admin_edit($id = NULL) {
        $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id)));
        if ($this->request->is('post')) {
            $this->request->data['Tournament']['tournament_id'] = $id;
            $this->request->data['Tournament']['starts'] = date('Y-m-d H:i:s', strtotime($this->request->data['Tournament']['starts_date'] . ' ' . $this->request->data['Tournament']['starts_time']));
            $this->request->data['Tournament']['reg_starts'] = date('Y-m-d H:i:s', strtotime($this->request->data['Tournament']['reg_starts_date'] . ' ' . $this->request->data['Tournament']['reg_starts_time']));
            $this->request->data['Tournament']['reg_ends'] = date('Y-m-d H:i:s', strtotime($this->request->data['Tournament']['reg_ends_date'] . ' ' . $this->request->data['Tournament']['reg_ends_time']));
            $this->Tournament->save($this->request->data['Tournament']);
            $this->Session->setFlash('Tournament Updated', '', array(''), 'success');
            return $this->redirect(array('action' => 'index'));
        }
        $this->request->data['Tournament'] = $tournament['Tournament'];
        $this->request->data['Tournament']['reg_starts_date'] = date('d-m-Y', strtotime($tournament['Tournament']['reg_starts']));
        $this->request->data['Tournament']['reg_ends_date'] = date('d-m-Y', strtotime($tournament['Tournament']['reg_ends']));
        $this->request->data['Tournament']['starts_date'] = date('d-m-Y', strtotime($tournament['Tournament']['starts']));

        $this->request->data['Tournament']['reg_starts_time'] = date('h:i A', strtotime($tournament['Tournament']['reg_starts']));
        $this->request->data['Tournament']['reg_ends_time'] = date('h:i A', strtotime($tournament['Tournament']['reg_ends']));
        $this->request->data['Tournament']['starts_time'] = date('h:i A', strtotime($tournament['Tournament']['starts']));
    }

    public function admin_index() {
        $this->checkadmin();
        $this->Tournament->recursive = 0;
        $conditions = array('status' => 'Active');
        if (!empty($_REQUEST['s'])) {
            $s = $_REQUEST['s'];
            $conditions['OR'] = array('name LIKE' => "%$s%");
        }
        if (!empty($_REQUEST['type'])){
            $conditions['type'] = array('type' => $_REQUEST['type']);
        }
        $this->paginate = array('conditions' => $conditions, 'order' => 'tournament_id DESC', 'limit' => '10');
        $this->set('tournaments', $this->Paginator->paginate('Tournament'));
    }

    public function admin_delete($id = NULL) {
        $this->autoRender = false;
        $this->checkadmin();
        $this->Tournament->updateAll(
                array('Tournament.status' => "'Trash'"), array('Tournament.tournament_id' => $id)
        );
        $this->Session->setFlash('Tournament Deleted', '', array(''), 'success');
        return $this->redirect(array('action' => 'index'));
    }

    public function tournamentDetails($id = NULL) {
        $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id)));
        $joined_players = $this->Tournamentplayer->find('count', array('conditions' => array('tournament_id' => $tournament['Tournament']['tournament_id'])));
       
            date_default_timezone_set("Asia/Kolkata");
            $timenow = date('n/j/Y g:i:s A');

            $regclose = date('n/j/Y g:i:s A', strtotime($tournament['Tournament']['reg_ends']));
            if ($regclose < $timenow) {
            $i = $tournament['Tournament']['total_members'];
        }else{
            $i = count($joined_players);
        }
		echo $i;
        $rounds = array();
        $j = 0;
        while ($i > $tournament['Tournament']['maxplayers']) {
            $players = $i;
            $i = ceil($i / $tournament['Tournament']['maxplayers']);
            $table = $i;
            $qualifierfrom_table = ($table > 4 || $table == 1 || $j == 0) ? "1" : "2";
            $i = $i * $qualifierfrom_table;
            $rounds[] = array('players' => $players, 'table' => ceil($table), 'qualifier' => $qualifierfrom_table);
            $j++;
        }
        $rounds[] = array('players' => ceil($table) * $qualifierfrom_table, 'table' => 1, 'qualifier' => 1);
        $rounds = array_reverse($rounds);
        $prize = $total_prize = 15000;
        $totalprize = '10';
        $presentagecal = floor($prize / $totalprize);
//        if ($totalprize == '3') {
//            $data[] = floor((($presentagecal / 100) * 20));
//            $data[] = floor((($presentagecal / 100) * 30));
//            $data[] = floor((($presentagecal / 100) * 50));
//        } else {
//            $total_price = 0;
//            while ($total_price <= $total_prize) {
//                for ($i = 1; $i <= $totalprize; $i++) {
//                    $presentagecal = floor($prize / ($totalprize));
//                    if (!isset($data[$i])) {
//                        $data[$i] = 0;
//                    }
//                    $data[$i] += $presentagecal;
//                    $prize = $prize - $presentagecal;
//                    $total_price += $presentagecal;
//                }
//            }
//        }

//        $totalramount = array();
//        $presentagecal = floor($prize / (count($rounds)));
//        for ($i = 0; $i < count($rounds); $i++) {
//            if ($i <> count($rounds)) {
//                $totalprice = $totalprice + floor((($presentagecal / 100) * 10) / ($rounds[$i]['table'] / 6)) * ceil($rounds[$i]['table'] / 6);
//                $totalramount[$i][] = $totalramounts = floor((($presentagecal / 100) * 10) / ($rounds[$i]['table'] / 6)) * ceil($rounds[$i]['table'] / 6);
//                $totalprice = $totalprice + floor((($presentagecal / 100) * 15) / ($rounds[$i]['table'] / 6)) * ceil($rounds[$i]['table'] / 6) + $totalramounts;
//                $totalramount[$i][] = $totalramounts = floor((($presentagecal / 100) * 15) / ($rounds[$i]['table'] / 6)) * ceil($rounds[$i]['table'] / 6);
//                $totalprice = $totalprice + floor((($presentagecal / 100) * 20) / ($rounds[$i]['table'] / 6)) * ceil($rounds[$i]['table'] / 6) + $totalramounts;
//                $totalramount[$i][] = $totalramounts = floor((($presentagecal / 100) * 20) / ($rounds[$i]['table'] / 6)) * ceil($rounds[$i]['table'] / 6);
//                $totalprice = $totalprice + floor((($presentagecal / 100) * 25) / ($rounds[$i]['table'] / 6)) * ceil($rounds[$i]['table'] / 6) + $totalramounts;
//                $totalramount[$i][] = $totalramounts = floor((($presentagecal / 100) * 25) / ($rounds[$i]['table'] / 6)) * ceil($rounds[$i]['table'] / 6);
//                $totalprice = $totalprice + floor((($presentagecal / 100) * 30) / ($rounds[$i]['table'] / 6)) * ceil($rounds[$i]['table'] / 6) + $totalramounts;
//                $totalramount[$i][] = $totalramounts = floor((($presentagecal / 100) * 30) / ($rounds[$i]['table'] / 6)) * ceil($rounds[$i]['table'] / 6);
//            } else {
//                if ($rounds[$i]['table'] == 6) {
//                    $totalprice = $totalprice + floor((($presentagecal / 100) * 6) + floor((($presentagecal / 100) * 11)) + floor((($presentagecal / 100) * 16)) + floor((($presentagecal / 100) * 19)) + floor((($presentagecal / 100) * 23)) + floor((($presentagecal / 100) * 25)));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 6));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 11));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 16));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 19));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 23));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 25));
//                }
//                if ($rounds[$i]['table'] == 5) {
//                    $totalprice = $totalprice + floor((($presentagecal / 100) * 10) + floor((($presentagecal / 100) * 14)) + floor((($presentagecal / 100) * 19)) + floor((($presentagecal / 100) * 25)) + floor((($presentagecal / 100) * 30)));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 10));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 14));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 19));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 25));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 30));
//                }
//                if ($rounds[$i]['table'] == 4) {
//                    $totalprice = $totalprice + floor((($presentagecal / 100) * 16) + floor((($presentagecal / 100) * 22)) + floor((($presentagecal / 100) * 27)) + floor((($presentagecal / 100) * 33)));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 16));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 22));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 27));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 33));
//                }
//                if ($rounds[$i]['table'] == 3) {
//                    $totalprice = $totalprice + floor((($presentagecal / 100) * 23) + floor((($presentagecal / 100) * 35)) + floor((($presentagecal / 100) * 40)));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 23));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 35));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 40));
//                }
//                if ($rounds[$i]['table'] == 2) {
//                    $totalprice = $totalprice + floor((($presentagecal / 100) * 39) + floor((($presentagecal / 100) * 59)));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 59));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 39));
//                }
//                if ($rounds[$i]['table'] == 1) {
//                    $totalprice = $totalprice + floor((($presentagecal / 100) * 98));
//                    $totalramount[$i][] = floor((($presentagecal / 100) * 98));
//                }
//            }
//        }
//        print_r($rounds);
//        exit;
        return $rounds;
    }

    public function popupDetail($id = NULL) {
        $this->layout = '';
        $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id)));
        $joined_players = $this->Tournamentplayer->find('all', array('conditions' => array('tournament_id' => $id)));
        $rounds = $this->tournamentStructure($tournament['Tournament']['tournament_id']);
        $prize_structure = $this->priceCalculation($tournament['Tournament']['tournament_id']);
        $totalprize_amount = ($tournament['Tournament']['entryfee'] * $tournament['Tournament']['maxplayers']) * 0.85;
        $data = array(
            'tournament_id' => $tournament['Tournament']['tournament_id'],
            'tournamentid' => $tournament['Tournament']['tournamentid'],
            'name' => $tournament['Tournament']['name'],
            'starts' => date('d-m-Y h:i A', strtotime($tournament['Tournament']['starts'])),
            'type' => $tournament['Tournament']['type'],
            'format' => 'Deals Rummy',
            'entryfee' => $tournament['Tournament']['entryfee'],
            'joined' => count($joined_players),
            'maxplayers' => $tournament['Tournament']['maxplayers'],
            'maxplayers_table' => $tournament['Tournament']['maxplayers_table'],
            'totalprize_amount' => $totalprize_amount,
            'totalprize_count' => $tournament['Tournament']['totalprize_count'],
            'totalround' => count($rounds),
            'reg_starts' => date('d-m-Y h:i A', strtotime($tournament['Tournament']['reg_starts'])),
            'reg_ends' => date('d-m-Y h:i A', strtotime($tournament['Tournament']['reg_ends'])),
            'tournament_status' => $tournament['Tournament']['tournament_status'],
        );
        foreach ($rounds as $key => $round) {
            $structure[] = array(
                'players' => $round['players'],
                'table' => $round['table'],
                'qualifier' => $round['qualifier'],
                'round' => $key + 1
            );
        }
        $data['tournament_structure'] = $structure;
        $players = array();
        foreach ($joined_players as $joined_player) {
            $user = $this->User->find('first', array('conditions' => array('user_id' => $joined_player['Tournamentplayer']['user_id'])));
            $players[] = array(
                'id' => $joined_player['Tournamentplayer']['tp_id'],
                'player_name' => $user['User']['user_name'],
                'status' => $joined_player['Tournamentplayer']['status'],
            );
        }
        $data['players'] = $players;
        //     $data['prize_structure'] = file_get_contents("http://localhost/rum/app/webroot/pricecal/price.php?fee_id=".$tournament['Tournament']['entryfee']."&maxper_id=".$tournament['Tournament']['maxplayers_table']."&joinplayer_id=".$tournament['Tournament']['maxplayers']);
        //     $prize_structure = $this->priceCalculation($tournament['Tournament']['tournament_id']);
//        $data['prize_structure'] = $prize_structure;
        $this->set('data', $data);
    }

    public function join($id = NULL) {
        $this->autoRender = false;
        $user = $this->checkuser();
        $check = $this->Tournamentplayer->find('first', array('conditions' => array('tournament_id' => $id, 'user_id' => $user['User']['user_id'])));
        if (empty($check)) {
            $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id, 'status' => 'Active')));
            if (!empty($tournament)) {
                if (!empty($tournament['Tournament']['entryfee']) && $tournament['Tournament']['entryfee'] > 0) {
                    $entryfee_check = false;
                    if ($user['User']['deposit_balance'] >= $tournament['Tournament']['entryfee']) {
                        $entryfee_check = true;
                        $this->User->updateAll(
                                array('User.deposit_balance' => "'User.deposit_balance-" . $tournament['Tournament']['entryfee'] . "'"), array('User.user_id' => $user['User']['user_id'])
                        );
                    }
                } else {
                    $entryfee_check = true;
                }
                if ($entryfee_check) {
                    $totaljoined = $this->Tournamentplayer->find('count', array('conditions' => array('tournament_id' => $id)));
                    $this->request->data['Tournamentplayer']['user_id'] = $user['User']['user_id'];
                    $this->request->data['Tournamentplayer']['tournament_id'] = $id;
                    $this->request->data['Tournamentplayer']['status'] = ($totaljoined < $tournament['Tournament']['maxplayers']) ? 'Joined' : "Waitlist";
                    $this->request->data['Tournamentplayer']['datetime'] = date('Y-m-d H:i:s');
                    $this->Tournamentplayer->save($this->request->data['Tournamentplayer']);
                    $result = array('code' => '0', 'message' => 'Joined successfully!');
                } else {
                    $result = array('code' => '200', 'message' => 'Insufficiant balance');
                }
            } else {
                $result = array('code' => '200', 'message' => 'Tournament not exists!');
            }
        } else {
            $result = array('code' => '200', 'message' => 'Already joined');
        }
        echo json_encode($result);
        exit;
    }

    public function withdraw($id = NULL) {
        $this->autoRender = false;
        $user = $this->checkuser();
        $check = $this->Tournamentplayer->find('first', array('conditions' => array('tournament_id' => $id, 'user_id' => $user['User']['user_id'])));
        if (!empty($user)) {
            $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id, 'status' => 'Active')));
            if (!empty($tournament['Tournament']['entryfee']) && $tournament['Tournament']['entryfee'] > 0) {
                $this->User->updateAll(
                        array('User.deposit_balance' => "'User.deposit_balance+" . $tournament['Tournament']['entryfee'] . "'"), array('User.user_id' => $user['User']['user_id'])
                );
            }
            $this->Tournamentplayer->deleteAll(array('Tournamentplayer.tournament_id' => $id, 'Tournamentplayer.user_id' => $user['User']['user_id']), false);
            $result = array('code' => '0', 'message' => 'Withdrawn successfully!');
        } else {
            $result = array('code' => '200', 'message' => 'Not joined');
        }
        echo json_encode($result);
        exit;
    }

    public function tourAjax($id = NULL) {
        $this->layout = '';
        $user = $this->checkuser();
    }

    public function updatePopup() {
        $this->layout = '';
        $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id)));
        $joined_players = $this->Tournamentplayer->find('all', array('conditions' => array('tournament_id' => $id)));
        $rounds = $this->tournamentStructure($tournament['Tournament']['tournament_id']);
        $prize_structure = $this->priceCalculation($tournament['Tournament']['tournament_id']);
        $totalprize_amount = ($tournament['Tournament']['entryfee'] * $tournament['Tournament']['maxplayers']) * 0.85;
        $data = array(
            'tournament_id' => $tournament['Tournament']['tournament_id'],
            'tournamentid' => $tournament['Tournament']['tournamentid'],
            'name' => $tournament['Tournament']['name'],
            'starts' => date('d-m-Y h:i A', strtotime($tournament['Tournament']['starts'])),
            'type' => $tournament['Tournament']['type'],
            'format' => 'Deals Rummy',
            'entryfee' => $tournament['Tournament']['entryfee'],
            'joined' => count($joined_players),
            'maxplayers' => $tournament['Tournament']['maxplayers'],
            'maxplayers_table' => $tournament['Tournament']['maxplayers_table'],
            'totalprize_amount' => $totalprize_amount,
            'totalprize_count' => $tournament['Tournament']['totalprize_count'],
            'totalround' => count($rounds),
            'reg_starts' => date('d-m-Y h:i A', strtotime($tournament['Tournament']['reg_starts'])),
            'reg_ends' => date('d-m-Y h:i A', strtotime($tournament['Tournament']['reg_ends'])),
            'tournament_status' => $tournament['Tournament']['tournament_status'],
        );
        foreach ($rounds as $key => $round) {
            $structure[] = array(
                'players' => $round['players'],
                'table' => $round['table'],
                'qualifier' => $round['qualifier'],
                'round' => $key + 1
            );
        }
        $data['tournament_structure'] = $structure;
        $players = array();
        foreach ($joined_players as $joined_player) {
            $user = $this->User->find('first', array('conditions' => array('user_id' => $joined_player['Tournamentplayer']['user_id'])));
            $players[] = array(
                'id' => $joined_player['Tournamentplayer']['tp_id'],
                'player_name' => $user['User']['user_name'],
                'status' => $joined_player['Tournamentplayer']['status'],
            );
        }
        $data['players'] = $players;
        $prize_structure = $this->priceCalculation($tournament['Tournament']['tournament_id']);
//        $data['prize_structure'] = $prize_structure;
        $this->set('data', $data);
    }

}
