<?php
include 'path.php';

App::uses('AppController', 'Controller');

/**
 * Sociallinks Controller
 *
 * @property Sociallink $Sociallink
 * @property PaginatorComponent $Paginator
 */
class WebservicesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Image');
    public $uses = array('User', 'Transaction', 'Tournament', 'Tournamentplayer', 'pendingbonuses', 'loyalty');

    /* ----------------------------------------------------------------NEW WEBSERVICE--------------------------------------------------------------------------------------- */

    public function login() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);

        $checkemail = $this->User->find('first', array('conditions' => array('email' => $_REQUEST['email'], 'status !=' => 'Trash')));
        if (!empty($checkemail)) {
            if ($checkemail['User']['password'] == md5($_REQUEST['password'])) {
                $result = array('code' => '200', 'user_id' => $checkemail['User']['user_id'], 'access_token' => $checkemail['User']['access_token']);
            } else {
                $result = array('code' => '0', 'message' => 'Email / Password Mismatch!');
            }
        } else {
            $result = array('code' => '0', 'message' => 'Email not exists!');
        }

        return json_encode($result);
        exit;
    }

    public function register() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
		

        $checkemail = $this->User->find('first', array('conditions' => array('email' => $_REQUEST['email'], 'status !=' => 'Trash')));
        $checkphone = $this->User->find('first', array('conditions' => array('phone_number' => $_REQUEST['mobile'], 'status !=' => 'Trash')));
        if (empty($checkemail) && empty($checkphone)) {
            $this->request->data['User']['name'] = $_REQUEST['name'];
            $this->request->data['User']['email'] = $_REQUEST['email'];
            $this->request->data['User']['phone_number'] = $_REQUEST['mobile'];
            $this->request->data['User']['user_name'] = $this->random_username($_REQUEST['name']);
            $this->request->data['User']['emailencode'] = $hash = md5(rand(0, 1000));
            $this->request->data['User']['access_token'] = $this->str_rand(10);
            $this->request->data['User']['password'] = md5($_REQUEST['password']);
            $this->request->data['User']['created_date'] = date('Y-m-d H:i:s');
            $this->User->save($this->request->data['User']);
            $user_id = $this->User->getLastInsertID();

            $result = array('code' => '200', 'user_id' => $user_id, 'access_token' => $this->request->data['User']['access_token']);
//            $result = '200|' . $user_id;
        } else {
            $result = array('code' => '0', 'message' => 'Email / Phone already exists!');
//            $result = '0|Email / Phone already exists!';
        }

        //return json_encode($result);
        return json_encode($result);
        exit;
    }

    public function sociallogin() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        $checkemail = $this->User->find('first', array('conditions' => array('fbid' => $_REQUEST['fbid'], 'status !=' => 'Trash')));
        if (!empty($checkemail)) {
            if ($checkemail['User']['status'] == 'Active') {
                $result = array('code' => '200', 'user_id' => $checkemail['User']['user_id'], 'access_token' => $checkemail['User']['access_token']);
            } else {
                $result = array('code' => '0', 'message' => 'Account Deactivated By Admin!');
            }
        } else {
            $this->request->data['User']['user_name'] = $_REQUEST['user_name'];
            $this->request->data['User']['fbid'] = $_REQUEST['fbid'];
            $this->request->data['User']['access_token'] = $this->str_rand(10);
            $this->request->data['User']['created_date'] = date('Y-m-d H:i:s');
            $this->User->save($this->request->data['User']);
            $user_id = $this->User->getLastInsertID();
            $result = array('code' => '200', 'user_id' => $user_id, 'access_token' => $this->request->data['User']['access_token']);
        }
        return json_encode($result);
        exit;
    }

    public function update_userpoint() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        try {
            $user = $this->User->find('first', array('conditions' => array('user_id' => $_REQUEST['user_id'])));
            if (!empty($user)) {
                $user['User']['points'] = $user['User']['points'] + $_REQUEST['points'];
                $this->User->save($user['User']);
                $result = '200|Email / Invalid User!';
                $this->User->updateAll(
                        array('User.lastactive' => "'" . date('Y-m-d H:i:s') . "'"), array('User.user_id' => $user['User']['user_id'])
                );
            } else {
                $result = '0|Email / Invalid User!';
            }
            return json_encode($result);
            exit;
        } catch (Exception $e) {
            return json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function get_userpoint() {
        date_default_timezone_set("Asia/Kolkata");
        $timenow = date('j-m-Y G:i:s');
		$this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        try {
            $user = $this->User->find('first', array('conditions' => array('user_id' => $_REQUEST['user_id'])));
            $bonus = $this->pendingbonuses->find('first', array('conditions' => array('user_id' => $_REQUEST['user_id']),
                'order' => 'enddatetime DESC'));
            if (!empty($user)) {
                if (!empty($bonus)) {
                    date_default_timezone_set("Asia/Kolkata");
                    $btime = date('Y-m-d G:i:s', strtotime($bonus['pendingbonuses']['enddatetime']));
                    $timenow1 = date('Y-m-d G:i:s');
                    if ($btime > $timenow1 and $user['User']['pending_bonus'] <> '0') {
                        $date2 = strtotime($btime);
                        $date1 = strtotime("now");
                        $diff = abs($date2 - $date1);

// To get the year divide the resultant date into 
// total seconds in a year (365*60*60*24) 
                        $years = floor($diff / (365 * 60 * 60 * 24));


// To get the month, subtract it with years and 
// divide the resultant date into 
// total seconds in a month (30*60*60*24) 
                        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));


// To get the day, subtract it with years and  
// months and divide the resultant date into 
// total seconds in a days (60*60*24) 
                        $days = floor(($diff - $years * 365 * 60 * 60 * 24 -
                                $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));


// To get the hour, subtract it with years,  
// months & seconds and divide the resultant 
// date into total seconds in a hours (60*60) 
                        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));


// To get the minutes, subtract it with years, 
// months, seconds and hours and divide the  
// resultant date into total seconds i.e. 60 
                        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);


// To get the minutes, subtract it with years, 
// months, seconds, hours and minutes  
                        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));
                        // Print the result 
                        $bmessage = "Avaliable for " . $days . "days, " . $minutes . "minutes, " . $seconds . "seconds";
                        $pendbonus = $user['User']['pending_bonus'];
                    } else {
                        $bmessage = 'Not Avaliable';

                        $pendbonus = '0';
                    }
                } else {
                    $bmessage = 'Not Avaliable';
                    $pendbonus = '0';
                }
                $data = file_get_contents('http://localhost/rum/web5/web5/web2/getactiveplayer.php?uid=' . $_REQUEST['user_id']);
                $result = array('code' => '200', 'ppoint' => $user['User']['practice_cash'], 'bonavaliable' => $bmessage, 'point' => $user['User']['deposit_balance'], 'abonous' => $user['User']['avaliable_bonus'], 'rbonous' => $user['User']['released_bonus'], 'pbonous' => $pendbonus, 'ebonous' => $user['User']['earn_bonus'], 'timenow' => $timenow);
                $this->User->updateAll(
                        array('User.lastactive' => "'" . date('Y-m-d H:i:s') . "'"), array('User.user_id' => $user['User']['user_id'])
                );
            } else {
                $result = array('code' => '0', 'message' => 'Invalid User', 'timenow' => $timenow);
            }
            echo json_encode($result) . "^" . $data;
            exit;
        } catch (Exception $e) {
            echo json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage(), 'timenow' => $timenow));
            exit;
        }
    }

    public function top_players() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        try {
            $users = $this->Transaction->find('all', array('fields' => array('user_id', 'SUM(amount) as Win'), 'conditions' => array('transaction_type' => 'Winning'), 'group' => array('user_id'), 'order' => array('Win DESC'), 'limit' => '10'));

            $data = array();
            foreach ($users as $user) {
                $user = $this->User->find('first', array('conditions' => array('user_id' => $user['Transaction']['user_id'])));
                $data[] = array(
                    'user_name' => $user['User']['user_name'], 'type' => '', 'fbid' => '',
                    'profile' => ($user['User']['profile'] != '') ? BASE_URL . 'files/users/' . $user['User']['profile'] : ""
                );
            }
            echo json_encode($data);
            exit;
        } catch (Exception $e) {
            echo json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function tournaments() {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        try {
            $tournaments = $this->Tournament->find('all', array('conditions' => array('status' => 'Active')));

            foreach ($tournaments as $tournament) {
                $regend = date('n/j/Y g:i:s A', strtotime($tournament['Tournament']['starts']));
                date_default_timezone_set("Asia/Kolkata");
                $timenow = date('n/j/Y g:i:s A', time() - 86400);
//			echo $tournament['Tournament']['tournament_id']."---".strtotime($regend)."---".strtotime($timenow)."---".$regend."---".$timenow."<br>";
                if (strtotime($regend) < strtotime($timenow)) {
                    //	file_get_contents('http://localhost/rum/admin/tournaments/Delete/'.$tournament['Tournament']['tournament_id']);	
                } else {
                    $rounds = $this->tournamentStructure($tournament['Tournament']['tournament_id']);
                    $joined_players = $this->Tournamentplayer->find('count', array('conditions' => array('tournament_id' => $tournament['Tournament']['tournament_id'])));
                    if (!empty($_REQUEST['user_id'])) {
                        $user = $this->User->find('first', array('conditions' => array('user_id' => $_REQUEST['user_id'])));
                        $joinplayer = $this->Tournamentplayer->find('first', array('conditions' => array('user_id' => $_REQUEST['user_id'], 'tournament_id' => $tournament['Tournament']['tournament_id'])));
                        $this->User->updateAll(
                                array('User.lastactive' => "'" . date('Y-m-d H:i:s') . "'"), array('User.user_id' => $user['User']['user_id'])
                        );
                    }

                    $totalprize_amount = ($tournament['Tournament']['entryfee'] * $tournament['Tournament']['maxplayers']) * 0.85;
                    $data[] = array(
                        'tournament_id' => $tournament['Tournament']['tournament_id'],
                        'tournamentid' => $tournament['Tournament']['tournamentid'],
                        'starts' => date('n/j/Y g:i:s A', strtotime($tournament['Tournament']['starts'])),
                        'type' => $tournament['Tournament']['type'],
                        'format' => 'Deals Rummy',
                        'entryfee' => $tournament['Tournament']['entryfee'],
                        'joined' => $joined_players,
                        'maxplayers' => $tournament['Tournament']['maxplayers'],
                        'maxplayers_table' => $tournament['Tournament']['maxplayers_table'],
                        'minplayers_game' => $tournament['Tournament']['minplayers_game'],
                        'totalprize_amount' => $totalprize_amount,
                        'totalprize_count' => $tournament['Tournament']['totalprize_count'],
                        'totalround' => count($rounds),
                        'reg_starts' => date('n/j/Y g:i:s A', strtotime($tournament['Tournament']['reg_starts'])),
                        'reg_ends' => date('n/j/Y g:i:s A', strtotime($tournament['Tournament']['reg_ends'])),
                        'player_status' => (!empty($joinplayer)) ? $joinplayer['Tournamentplayer']['status'] : "",
                        'joined_time' => (!empty($joinplayer)) ? $joinplayer['Tournamentplayer']['datetime'] : ""
                    );
                }
            }

            function date_compare($element1, $element2) {
                $datetime1 = strtotime($element1['joined_time']);
                $datetime2 = strtotime($element2['joined_time']);
                return $datetime2 - $datetime1;
            }

// Sort the array  
            usort($data, 'date_compare');
            return json_encode($data);
            exit;
        } catch (Exception $e) {
            echo json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function tournamentDetail($id = NULL) {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        try {
            $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id)));
            $joined_players = $this->Tournamentplayer->find('all', array('conditions' => array('tournament_id' => $id)));
            $jplayer = count($joined_players);
            $regend = date('n/j/Y g:i:s A', strtotime($tournament['Tournament']['reg_ends']));
            date_default_timezone_set("Asia/Kolkata");
            $timenow = date('n/j/Y g:i:s A', time());
            if ($regend > $timenow) {
                $data2 = json_decode(file_get_contents('http://localhost/rum/pricecal/price.php?maxper_id=6&joinplayer_id=' . $jplayer . '&minper_id=' . $tournament['Tournament']['minplayers_game'] . '&fee_id=' . $tournament['Tournament']['entryfee'] . '&amount_id=' . $tournament['Tournament']['prizeamount'] . '&matchtype_id=' . $tournament['Tournament']['type'] . '&totalpalyer_id=' . $tournament['Tournament']['maxplayers'] . '&control=1&tournament_id=' . $tournament['Tournament']['tournament_id']), true);
            } else {
                if ($tournament['Tournament']['prizestatus'] == 1) {
                    $data2 = json_decode(file_get_contents('http://localhost/rum/pricecal/price.php?maxper_id=6&joinplayer_id=' . $jplayer . '&minper_id=' . $tournament['Tournament']['minplayers_game'] . '&fee_id=' . $tournament['Tournament']['entryfee'] . '&amount_id=' . $tournament['Tournament']['prizeamount'] . '&matchtype_id=' . $tournament['Tournament']['type'] . '&totalpalyer_id=' . $tournament['Tournament']['maxplayers'] . '&control=3&tournament_id=' . $tournament['Tournament']['tournament_id']), true);
                } else {
                    $data2 = json_decode(file_get_contents('http://localhost/rum/pricecal/price.php?maxper_id=6&joinplayer_id=' . $jplayer . '&minper_id=' . $tournament['Tournament']['minplayers_game'] . '&fee_id=' . $tournament['Tournament']['entryfee'] . '&amount_id=' . $tournament['Tournament']['prizeamount'] . '&matchtype_id=' . $tournament['Tournament']['type'] . '&totalpalyer_id=' . $tournament['Tournament']['maxplayers'] . '&control=2&tournament_id=' . $tournament['Tournament']['tournament_id']), true);
                }
            }
            //		echo 'http://localhost/rum/pricecal/price.php?maxper_id=6&joinplayer_id=' . $jplayer . '&minper_id=' . $tournament['Tournament']['minplayers_game'] . '&fee_id=' . $tournament['Tournament']['entryfee'] . '&amount_id=' . $tournament['Tournament']['prizeamount'] . '&matchtype_id=' . $tournament['Tournament']['type'] . '&totalpalyer_id=' . $tournament['Tournament']['maxplayers'] . '&control=1&tournament_id=' . $tournament['Tournament']['tournament_id'];
            $rounds = $this->tournamentStructure($tournament['Tournament']['tournament_id']);
            $prize_structure = $this->priceCalculation($tournament['Tournament']['tournament_id']);
            $totalprize_amount = ($tournament['Tournament']['entryfee'] * $tournament['Tournament']['maxplayers']) * 0.85;
            if (!empty($_REQUEST['user_id'])) {
                $user = $this->User->find('first', array('conditions' => array('user_id' => $_REQUEST['user_id'])));
                $joinplayer = $this->Tournamentplayer->find('first', array('conditions' => array('user_id' => $_REQUEST['user_id'], 'tournament_id' => $tournament['Tournament']['tournament_id'])));
                $this->User->updateAll(
                        array('User.lastactive' => "'" . date('Y-m-d H:i:s') . "'"), array('User.user_id' => $user['User']['user_id'])
                );
            }
            $data = array(
                'tournament_id' => $tournament['Tournament']['tournament_id'],
                'tournamentid' => $tournament['Tournament']['tournamentid'],
                'name' => $tournament['Tournament']['name'],
                'starts' => date('d-m-Y h:i A', strtotime($tournament['Tournament']['starts'])),
                'type' => $tournament['Tournament']['type'],
                'format' => 'Deals Rummy',
                'entryfee' => $tournament['Tournament']['entryfee'],
                'joined' => count($joined_players),
                'maxplayers' => $tournament['Tournament']['maxplayers'],
                'maxplayers_table' => $tournament['Tournament']['maxplayers_table'],
                'minplayers_game' => $tournament['Tournament']['minplayers_game'],
                'totalprize_amount' => $data2[0]['eprice'],
                'totalgprize_amount' => $data2[0]['gprice'],
                'totalprize_count' => $data2[0]['pricecount'],
                'totalround' => count($rounds),
                'reg_starts' => date('d-m-Y h:i A', strtotime($tournament['Tournament']['reg_starts'])),
                'reg_ends' => date('d-m-Y h:i A', strtotime($tournament['Tournament']['reg_ends'])),
                'tournament_status' => $tournament['Tournament']['tournament_status'],
                'player_status' => (!empty($joinplayer)) ? $joinplayer['Tournamentplayer']['status'] : ""
            );
            foreach ($rounds as $key => $round) {
                $structure[] = array(
                    'players' => $round['players'],
                    'table' => $round['table'],
                    'qualifier' => $round['qualifier'],
                    'round' => $key + 1
                );
            }
            $data['tournament_structure'] = $structure;
            $players = array();
            foreach ($joined_players as $joined_player) {
                $user = $this->User->find('first', array('conditions' => array('user_id' => $joined_player['Tournamentplayer']['user_id'])));
                $players[] = array(
                    'id' => $joined_player['Tournamentplayer']['tp_id'],
                    'player_name' => $user['User']['user_name'],
                    'status' => $joined_player['Tournamentplayer']['status'],
                );
            }
            $data['players'] = $players;
//            $prize_structure = $this->priceCalculation($tournament['Tournament']['tournament_id']);
            $prize_structure = $data2[0]['pricestruct'];
            $data['prize_structure'] = $prize_structure;
            $data['prize_list'] = $data2[0]['pricestructlow'];
            return json_encode($data);
            exit;
        } catch (Exception $e) {
            echo json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
            exit;
        }
    }

    public function joinTournament($id = NULL, $user_id = NULL) {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        try {
            $user = $this->User->find('first', array('conditions' => array('user_id' => $user_id)));
            $this->User->updateAll(
                    array('User.lastactive' => "'" . date('Y-m-d H:i:s') . "'"), array('User.user_id' => $user['User']['user_id'])
            );
            $check = $this->Tournamentplayer->find('first', array('conditions' => array('tournament_id' => $id, 'user_id' => $user['User']['user_id'])));
            if (empty($check)) {
                $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id, 'status' => 'Active')));
                if (!empty($tournament)) {
                    if (!empty($tournament['Tournament']['entryfee']) && $tournament['Tournament']['entryfee'] > 0) {
                        $entryfee_check = false;

                        if ($user['User']['deposit_balance'] >= $tournament['Tournament']['entryfee']) {
                            $entryfee_check = true;
                            $entryfeee = $tournament['Tournament']['entryfee'];
                            $this->User->updateAll(
                                    array('User.deposit_balance' => "User.deposit_balance - $entryfeee"), array('User.user_id' => $user['User']['user_id'])
                            );
                        }
                    } else {
                        $entryfee_check = true;
                    }
                    if ($entryfee_check) {
                        $totaljoined = $this->Tournamentplayer->find('count', array('conditions' => array('tournament_id' => $id)));
                        $this->request->data['Tournamentplayer']['user_id'] = $user['User']['user_id'];
                        $this->request->data['Tournamentplayer']['tournament_id'] = $id;
                        $this->request->data['Tournamentplayer']['status'] = ($totaljoined < $tournament['Tournament']['maxplayers']) ? 'Joined' : "Waitlist";
                        $this->request->data['Tournamentplayer']['datetime'] = date('Y-m-d H:i:s');
                        $this->Tournamentplayer->save($this->request->data['Tournamentplayer']);
                        $result = array('code' => '200', 'message' => 'Joined successfully!');
                    } else {
                        $result = array('code' => '0', 'message' => 'Insufficiant balance');
                    }
                } else {
                    $result = array('code' => '0', 'message' => 'Tournament not exists!');
                }
            } else {
                $result = array('code' => '0', 'message' => 'Already joined');
            }
        } catch (Exception $e) {
            $result = json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
        }
        echo json_encode($result);
        exit;
    }

    public function withdraw($id = NULL, $user_id = NULL) {
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $params = json_decode($this->request->input(), true);
        extract($_REQUEST);
        try {
            $user = $this->User->find('first', array('conditions' => array('user_id' => $user_id)));
            $this->User->updateAll(
                    array('User.lastactive' => "'" . date('Y-m-d H:i:s') . "'"), array('User.user_id' => $user['User']['user_id'])
            );
            $check = $this->Tournamentplayer->find('first', array('conditions' => array('tournament_id' => $id, 'user_id' => $user['User']['user_id'])));
            if (!empty($user)) {
                $tournament = $this->Tournament->find('first', array('conditions' => array('tournament_id' => $id, 'status' => 'Active')));
                if (!empty($tournament['Tournament']['entryfee']) && $tournament['Tournament']['entryfee'] > 0) {
                    $entryfeee = $tournament['Tournament']['entryfee'];
                    $this->User->updateAll(
                            array('User.deposit_balance' => "User.deposit_balance + $entryfeee"), array('User.user_id' => $user['User']['user_id'])
                    );
                }
                $this->Tournamentplayer->deleteAll(array('Tournamentplayer.tournament_id' => $id, 'Tournamentplayer.user_id' => $user['User']['user_id']), false);
                $result = array('code' => '0', 'message' => 'Withdrawn successfully!');
            } else {
                $result = array('code' => '200', 'message' => 'Not joined');
            }
        } catch (Exception $e) {
            $result = json_encode(array("code" => 0, "message" => 'Error:' . $e->getMessage()));
        }
        echo json_encode($result);
        exit;
    }

    public function prize() {
        $last_prize = 10;
        $total_prizes = 20;
        $prizes[$total_prizes + 1];
        $money = 1000;
        $s = 0;
        $first_prize = 2 * $money / $total_prizes + $last_prize; //last member of the progresion
        $ratio = ($first_prize - $last_prize) / ($total_prizes - 1);
        $prizes[$total_prizes] = $last_prize;
        for ($i = $total_prizes - 1; $i >= 1; $i--)
            $prizes[i] = $prizes[$i + 1] + $ratio;
        for ($i = 1; $i <= $total_prizes; $i++) {
            echo $i . '.' . $prizes[$i];
            $s += $prizes[$i];
        }
        echo 'SUM:' . $s;
    }

    public function updateloyalty() {
        $this->autoRender = false;
        $users = $this->User->find('all', array('conditions' => array('status' => 'Active')));
        foreach ($users as $user) {
            if ($user['User']['loyalty_point'] > 99) {
                $lpoint = ($user['User']['loyalty_point']) / 100;
                $user['User']['lp_point'] = $lpoint;
                $user['User']['loyalty_point'] = ($lpoint % 100);
                $this->User->saveAll($user['User']);
            }
        }
        return "334";
    }

    public function closeReg() {
        $this->autoRender = false;
        $closedtournaments = $this->Tournament->find('all', array('conditions' => array('DATE(reg_ends)' => date('Y-m-d'), 'TIME(reg_ends) <=' => date('H:i:s'), 'callcron' => '0')));
        file_get_contents('http://localhost/rum/web5/web5/web2/splitprizeamount.php');
        file_get_contents('http://localhost/rum/web5/web5/web2/bonus_loyaltypoint.php');

        foreach ($closedtournaments as $closedtournament) {
            $joined_players = $this->Tournamentplayer->find('all', array('conditions' => array('tournament_id' => $closedtournament['Tournament']['tournament_id'])));
            $jplayer = count($joined_players);
            if ($closedtournament['Tournament']['minplayers_game'] < $jplayer) {
                file_get_contents('http://localhost/rum/web5/web5/web2/jointournamentplayers.php?tournament_id=' . $closedtournament['Tournament']['tournament_id'] . '&c_round=1');
                $this->Tournament->updateAll(
                        array('Tournament.callcron' => "'1'"), array('Tournament.tournament_id' => $closedtournament['Tournament']['tournament_id'])
                );
            } else {
                foreach ($joined_players as $joined_player) {
                    file_get_contents("http://localhost/rum/webservices/withdraw/" . $joined_player['tournamentplayers']['user_id'] . "/" . $joined_player['tournamentplayers']['user_id']);
                }
                file_get_contents('http://localhost/rum/admin/tournaments/Delete/' . $closedtournament['Tournament']['tournament_id']);
            }
        }
        echo "asdas";
        exit;
    }

}
