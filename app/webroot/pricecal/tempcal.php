<?php 
// PHP implementation of the approach 
echo 7+2*6;
echo "<br>";
// Function that prints 
// the required sequence 
function split($x, $n) 
{ 
	// If we cannot split the 
	// number into exactly 'N' parts 
	if($x < $n) 
		echo (-1); 

	// If x % n == 0 then the minimum 
	// difference is 0 and all 
	// numbers are x / n 
	else if ($x % $n == 0) 
	{ 
		for($i = 0; $i < $n; $i++) 
		{ 
			echo ($x / $n); 
			echo (" "); 
		} 
	} 
	
	else
	{ 
		// upto n-(x % n) the values 
		// will be x / n 
		// after that the values 
		// will be x / n + 1 
		$zp = $n - ($x % $n); 
		$pp = $x / $n; 
		for ($i = 0; $i < $n; $i++) 
		{ 
			if($i >= $zp) 
			{ 
				echo (int)$pp + 1; 
				echo (" "); 
			} 
			else
			{ 
				echo (int)$pp; 
				echo (" "); 
			} 
		} 
	} 
} 

// Driver code	 
$x = 11; 
$n = 2; 
split( $x, $n); 

// This code is contributed 
// by Shivi_Aggarwal 
?> 
