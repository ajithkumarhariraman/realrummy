<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Unity WebGL Player | Rummy</title>
    <link rel="shortcut icon" href="TemplateData/favicon.ico">
    <link rel="stylesheet" href="TemplateData/style.css">
    <script src="TemplateData/UnityProgress.js"></script>
    <script src="Build/UnityLoader.js"></script>
    <script>
      var gameInstance = UnityLoader.instantiate("gameContainer", "Build/web2.json", {onProgress: UnityProgress});
    </script>
	   <script src="jquery.min.js"></script>
        <script>

           

            //cardbutclick start (ButtonClick)

            //getcurrentdetail_url
            function getcurrentdetail_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("ButtonClick", "getcurrentdetail_result", html);

                            }
                        });
                    }
                });

            }
            //getcurrentmatchplayerprofile_url
            function getcurrentmatchplayerprofile_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("ButtonClick", "getcurrentmatchplayerprofile_result", html);

                            }
                        });
                    }
                });

            }

            //shortbyval_url
            function shortbyval_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("ButtonClick", "shortbyval_result", html);

                            }
                        });
                    }
                });

            }

            //groupcard_url
            function groupcard_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("ButtonClick", "groupcard_result", html);

                            }
                        });
                    }
                });

            }


            //shortbysute_url
            function shortbysute_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("ButtonClick", "shortbysute_result", html);

                            }
                        });
                    }
                });

            }
            //cardbutclick end (ButtonClick)

            //Drawing start (Controller)

            //gettossresult_url
            function gettossresult_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "gettossresult_result", html);

                            }
                        });
                    }
                });

            }

            //checkdummystatus_url
            function checkdummystatus_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","checkdummystatus_result",html);

                            }
                        });
                    }
                });

            }


            //checkdummystatusresult_url
            function checkdummystatusresult_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "checkdummystatusresult_result", html);

                            }
                        });
                    }
                });

            }


            //updatecard14val_url
            function updatecard14val_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "updatecard14val_result", html);

                            }
                        });
                    }
                });

            }

            //updatecardswapval_url
            function updatecardswapval_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "updatecardswapval_result", html);

                            }
                        });
                    }
                });

            }

            //updatecardpos_url
            function updatecardpos_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "updatecardpos_result", html);

                            }
                        });
                    }
                });

            }

            //updatecard14val1_url
            function updatecard14val1_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "updatecard14val1_result", html);

                            }
                        });
                    }
                });

            }

            //getcard_url
            function getcard_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "getcard_result", html);

                            }
                        });
                    }
                });

            }
            //Drawing end (Controller)

            //main start (Controller)

            //getinitialdata_url
            function getinitialdata_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "getinitialdata_result", html);

                            }
                        });
                    }
                });

            }

            //getplayermatchstatus_url
            function getplayermatchstatus_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "getplayermatchstatus_result", html);

                            }
                        });
                    }
                });

            }

            //updateplayermatchstatus_url
            function updateplayermatchstatus_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","updateplayermatchstatus_result",html);

                            }
                        });
                    }
                });

            }

            //deckpointfun_url
            function deckpointfun_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","deckpointfun_result",html);

                            }
                        });
                    }
                });

            }

            //updateunactivecount_url
            function updateunactivecount_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "updateunactivecount_result", html);

                            }
                        });
                    }
                });

            }

            //deckpointfun_url
            function deckpointfun_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","deckpointfun_result",html);

                            }
                        });
                    }
                });

            }

            //deckpointfun_url
            function deckpointfun_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","deckpointfun_result",html);

                            }
                        });
                    }
                });

            }

            //deckpointfun_url
            function deckpointfun_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","deckpointfun_result",html);

                            }
                        });
                    }
                });

            }

            //putdummystatus_url
            function putdummystatus_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","putdummystatus_result",html);

                            }
                        });
                    }
                });

            }


            //deckpointfun_url
            function deckpointfun_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","deckpointfun_result",html);

                            }
                        });
                    }
                });

            }

            //updatechat_url
            function updatechat_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","updatechat_result",html);

                            }
                        });
                    }
                });

            }

            //updatecard14val2_url
            function updatecard14val2_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","updatecard14val2_result",html);

                            }
                        });
                    }
                });

            }

            //checkcardstatus_url
            function checkcardstatus_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "checkcardstatus_result", html);

                            }
                        });
                    }
                });

            }

            //getpointdetails_url
            function getpointdetails_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Controller", "getpointdetails_result", html);

                            }
                        });
                    }
                });

            }


            //withdrawableuseramount_url
            function withdrawableuseramount_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","withdrawableuseramount_result",html);

                            }
                        });
                    }
                });

            }


            //deposituseramount_url
            function deposituseramount_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","deposituseramount_result",html);

                            }
                        });
                    }
                });

            }





            //main end (Controller)

            // movement start 

            function deposituseramount_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                //gameInstance.SendMessage("Controller","deposituseramount_result",html);

                            }
                        });
                    }
                });

            }



            //end
            //start
            function updateplayerlocalmove_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("Player0Card0", "updateplayerlocalmove_result", html);

                            }
                        });
                    }
                });

            }

            //end

            //maincontrol start 

            function getCookie(cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            function carddistributer_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("script", "carddistributer_result", html);

                            }
                        });
                    }
                });
            }

            function deposituseramount_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("script", "deposituseramount_result", html);

                            }
                        });
                    }
                });
            }
            function checkmytable_url(url) {
                $(document).ready(function () {
                    {
                        fun_alert(url);
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (html)

                            {
                                fun_alert(html)
                                gameInstance.SendMessage("script", "checkmytable_result", html);

                            }
                        });
                    }
                });
            }

            function get_user_data() {
          	var fid=getCookie("playerRCRayaz");

                var maxplayer = getCookie("maxplayerRCRayaz");
                var playeramountdeal = getCookie("playeramountdealRCRayaz");
                var gamename = getCookie("gamenameRCRayaz");
                var gameinfdealsize = getCookie("gameinfdealsizeRCRayaz");
                var gameinfpoolsize = getCookie("gameinfpoolsizeRCRayaz");
                var mid = getCookie("gameinfmatchidRCRayaz");
                var mpos = getCookie("gameinfmatchposRCRayaz");
                var data = fid + "^" + maxplayer + "^" + playeramountdeal + "^" + gamename + "^" + gameinfdealsize + "^" + gameinfpoolsize + "^" + mid + "^" + mpos;
if (fid != "") {
                    alert("Welcome again  Payer ID :" + fid + "maxplayer : " + maxplayer + "playeramountdeal : " + playeramountdeal + "gamename :" + gamename + "gameinfdealsize :" + gameinfdealsize + "gameinfpoolsize" + gameinfpoolsize);
                    gameInstance.SendMessage("script", "gamedata", data);
                } else {
                    alert("NO data");
                }
            }
            //maincontrol end
            function fun_alert(result)
            {
                alert(result);

            }

            ///

        </script> 
  </head>
  <body>
    <div class="webgl-content">
      <div id="gameContainer" style="width: 960px; height: 600px"></div>
      <div class="footer">
        <div class="webgl-logo"></div>
        <div class="fullscreen" onclick="gameInstance.SetFullscreen(1)"></div>
        <div class="title">Rummy</div>
      </div>
    </div>
  </body>
</html>
