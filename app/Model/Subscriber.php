<?php

App::uses('AppModel', 'Model');

/**
 * Staticpage Model
 *
 */
class Subscriber extends AppModel {
    /**
     * Primary key field
     *
     * @var string
     */
    public $primaryKey = 'subscriber_id';

}
