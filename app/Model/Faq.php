<?php

App::uses('AppModel', 'Model');

/**
 * Staticpage Model
 *
 */
class Faq extends AppModel {

    /**
     * Primary key field
     *
     * @var string
     */
    public $primaryKey = 'faq_id';

}
