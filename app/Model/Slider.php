<?php

App::uses('AppModel', 'Model');

/**
 * Staticpage Model
 *
 */
class Slider extends AppModel {
    /**
     * Primary key field
     *
     * @var string
     */
    public $primaryKey = 'slider_id';

}
