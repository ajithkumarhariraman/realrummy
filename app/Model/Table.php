<?php
App::uses('AppModel', 'Model');
/**
 * Staticpage Model
 *
 */
class Table extends AppModel {
    /**
     * Primary key field
     *
     * @var string
     */
    public $primaryKey = 'table_id';
}
