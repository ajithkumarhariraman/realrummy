<?php
App::uses('AppModel', 'Model');
/**
 * Adminuser Model
 *
 */
class Tournament extends AppModel {
/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'tournament_id';
}
