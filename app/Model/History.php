<?php

App::uses('AppModel', 'Model');

/**
 * Adminuser Model
 *
 */
class History extends AppModel {

    /**
     * Primary key field
     *
     * @var string
     */
    public $useTable = 'history';
    public $primaryKey = 'history_id';

}
