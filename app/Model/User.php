<?php

App::uses('AppModel', 'Model');

/**
 * Staticpage Model
 *
 */
class User extends AppModel {

    /**
     * Primary key field
     *
     * @var string
     */
    public $primaryKey = 'user_id';

}
