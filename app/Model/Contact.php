<?php

App::uses('AppModel', 'Model');

/**
 * Adminuser Model
 *
 */
class Contact extends AppModel {

    /**
     * Primary key field
     *
     * @var string
     */
    public $primaryKey = 'contact_id';

}
