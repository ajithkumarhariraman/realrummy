<?php

App::uses('AppModel', 'Model');

/**
 * Staticpage Model
 *
 */
class Faqcategory extends AppModel {

    /**
     * Primary key field
     *
     * @var string
     */
    public $primaryKey = 'category_id';

}
