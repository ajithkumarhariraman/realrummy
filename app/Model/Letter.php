<?php

App::uses('AppModel', 'Model');

/**
 * Staticpage Model
 *
 */
class Letter extends AppModel {

    /**
     * Primary key field
     *
     * @var string
     */
    public $primaryKey = 'letter_id';

}
