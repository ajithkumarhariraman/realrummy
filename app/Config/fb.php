<?php
define('FACEBOOK_SDK_V4_SRC_DIR', '../Vendor/Facebook');
require_once("../Vendor/Facebook/autoload.php");
require_once("../Vendor/Facebook/FacebookSession.php");
require_once("../Vendor/Facebook/Helpers/FacebookRedirectLoginHelper.php");
require_once("../Vendor/Facebook/FacebookRequest.php");
require_once("../Vendor/Facebook/FacebookResponse.php");
require_once("../Vendor/Facebook/Exceptions/FacebookSDKException.php");
require_once("../Vendor/Facebook/Exceptions/FacebookAuthorizationException.php");
require_once("../Vendor/Facebook/GraphNodes/GraphObject.php");
require_once("../Vendor/Facebook/GraphNodes/GraphUser.php");
require_once("../Vendor/Facebook/GraphNodes/GraphSessionInfo.php");
use Facebook\Facebook;
use Facebook\FacebookSession;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\ExceptionsFacebookRequestException;
use Facebook\Exceptions\FacebookAuthorizationException;
use Facebook\GraphNodes\GraphObject;
use Facebook\GraphNodes\GraphUser;
use Facebook\GraphNodes\GraphSessionInfo;

