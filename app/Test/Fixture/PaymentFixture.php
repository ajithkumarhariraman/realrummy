<?php
/**
 * PaymentFixture
 *
 */
class PaymentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'payment_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'payerdetail_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'book_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'amount' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'transaction_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 250, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'status' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'mihpayid' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'bankcode' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'bankname' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'bank_ref_no' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'name_on_card' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'net_amount_debit' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'total_payment' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'transaction_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'payment_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'payment_id' => 1,
			'payerdetail_id' => 1,
			'book_id' => 1,
			'amount' => 1,
			'transaction_id' => 'Lorem ipsum dolor sit amet',
			'status' => 'Lorem ipsum dolor sit amet',
			'mihpayid' => 'Lorem ipsum dolor sit amet',
			'bankcode' => 'Lorem ipsum dolor sit amet',
			'bankname' => 'Lorem ipsum dolor sit amet',
			'bank_ref_no' => 'Lorem ipsum dolor sit amet',
			'name_on_card' => 'Lorem ipsum dolor sit amet',
			'net_amount_debit' => 1,
			'total_payment' => 1,
			'transaction_date' => '2016-01-11 15:44:57',
			'created_date' => '2016-01-11 15:44:57'
		),
	);

}
