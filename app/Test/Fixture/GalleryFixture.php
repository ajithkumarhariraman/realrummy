<?php
/**
 * GalleryFixture
 *
 */
class GalleryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'gallery_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'page_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'gallery' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'description' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'gallery_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'gallery_id' => 1,
			'page_id' => 1,
			'gallery' => 'Lorem ipsum dolor sit amet',
			'description' => 1,
			'created_date' => '2016-01-07 13:25:58',
			'modified_date' => '2016-01-07 13:25:58'
		),
	);

}
