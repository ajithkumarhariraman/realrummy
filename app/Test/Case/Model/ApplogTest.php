<?php
App::uses('Applog', 'Model');

/**
 * Applog Test Case
 *
 */
class ApplogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.applog'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Applog = ClassRegistry::init('Applog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Applog);

		parent::tearDown();
	}

}
