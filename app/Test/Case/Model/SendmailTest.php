<?php
App::uses('Sendmail', 'Model');

/**
 * Sendmail Test Case
 *
 */
class SendmailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sendmail'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Sendmail = ClassRegistry::init('Sendmail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Sendmail);

		parent::tearDown();
	}

}
