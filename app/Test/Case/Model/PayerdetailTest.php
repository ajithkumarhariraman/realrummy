<?php
App::uses('Payerdetail', 'Model');

/**
 * Payerdetail Test Case
 *
 */
class PayerdetailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.payerdetail'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Payerdetail = ClassRegistry::init('Payerdetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Payerdetail);

		parent::tearDown();
	}

}
